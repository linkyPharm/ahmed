FROM node:10 as BUILD_STAGE

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install -g @angular/cli

RUN ng config -g cli.warnings.versionMismatch false

RUN npm install

COPY . .

#RUN ng build --prod --aot --buildOptimizer
RUN ng build

#############

FROM nginx as RUN_STAGE

WORKDIR /usr/share/nginx/html

COPY --from=BUILD_STAGE /usr/src/app/dist/ .

EXPOSE 80

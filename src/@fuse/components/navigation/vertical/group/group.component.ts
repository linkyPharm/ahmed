import { Component, HostBinding, Input, OnInit } from '@angular/core';

import { FuseNavigationItem } from '@fuse/types';
import { StorageService } from 'app/pages/shared/services/storage.service';
import { RoleService } from 'app/pages/shared/services/role.service';
import {rolesConst} from '../../../../../app/pages/main/manage-role/RolesPriority';
@Component({
    selector   : 'fuse-nav-vertical-group',
    templateUrl: './group.component.html',
    styleUrls  : ['./group.component.scss']
})
export class FuseNavVerticalGroupComponent implements OnInit
{
    role0 = rolesConst[0];
    role1 = rolesConst[1];
    role2 = rolesConst[2];
    role3 = rolesConst[3];
    @HostBinding('class')
    classes = 'nav-group nav-item';

    @Input()
    item: FuseNavigationItem;
    userRolePriority = '';
    /**
     * Constructor
     */
    constructor(
        private storageService: StorageService,
        private roleService: RoleService
    )
    {
    }

    ngOnInit(){
        console.log(this.item);
        let userToken ; 
        setTimeout(()=>{
            userToken = this.storageService.read('user');
            this.userRolePriority = this.roleService.getUserPriorityRole(userToken.role);
            this.roleService.changeAccountActivity(this.userRolePriority);
            if(this.storageService.read('currentRole') == undefined) {
               this.storageService.write('currentRole', this.userRolePriority);
           }
           this.userRolePriority = this.storageService.read('currentRole');
        }, 1000) ;
    }

}

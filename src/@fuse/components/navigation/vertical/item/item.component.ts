import { Component, HostBinding, Input, OnInit } from '@angular/core';

import { FuseNavigationItem } from '@fuse/types';
import { RoleService } from 'app/pages/shared/services/role.service';
import { StorageService } from 'app/pages/shared/services/storage.service';

@Component({
    selector   : 'fuse-nav-vertical-item',
    templateUrl: './item.component.html',
    styleUrls  : ['./item.component.scss']
})
export class FuseNavVerticalItemComponent implements OnInit
{
    @HostBinding('class')
    classes = 'nav-item';

    @Input()
    item: FuseNavigationItem;
    userRolePriority: string = '';

    /**
     * Constructor
     */
    constructor(private roleService: RoleService
        , private storageService: StorageService
        )
    {
    }
    ngOnInit(){
        let userToken;
        setTimeout(()=>{
            userToken = this.storageService.read('user');
            this.userRolePriority = this.roleService.getUserPriorityRole(userToken.role);
            this.roleService.changeAccountActivity(this.userRolePriority);
            if(this.storageService.read('currentRole') == undefined) {
               this.storageService.write('currentRole', this.userRolePriority);
           }
           this.userRolePriority = this.storageService.read('currentRole');
        }, 1000) ;
    }

}

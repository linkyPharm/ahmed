import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
   
   {
        id       : 'applications',
        title    : 'Applications',
        translate: 'NAV.APPLICATIONS',
        type     : 'group',
        children : [
            {
                id       : 'sample',
                title    : 'Sample',
                translate: 'DASHBOARD',
                type     : 'item',
                icon     : 'home',
                url      : 'overview/dashboard',
                
            },
            {
                id       : 'Affiches',
                title    : 'Affiches',
                translate: 'Affiches ',
                type     : 'collapsable',
                icon     : 'picture_as_pdf',
                children:[
                    {
                        id   : 'Générer une affiche',
                        title: 'Générer une affiche',
                        type : 'item',
                        url  : 'add-post'
                    },{
                        id:'Historique des affiches',
                        title:'Historique des affiches',
                        type:'item',
url:'historique'
                    }
                    
                    
        ] //url      : 'generate-flyer',
    },
    {
        id       : 'Leaflet',
        title    : 'Leaflet',
        translate: 'Leaflet',
        type     : 'collapsable',
        icon     : 'import_contacts',
        children:[
            {
                id:'Configurer une leaflet ',
                title:'Configurer une leaflet',
                type:'item',
url:'configurer-flyer'
            },
            {
                id:'Hisotrique des leaflets  ',
                title:'Hisotrique des leaflets',
                type:'item',
url:'historique-flyer' 
            },
            {
                id:'Génerer une leaflet  ',
                title:'Génerer une leaflet  ',
                type:'item',
url:'generer-flyer' 
            }
            
            ]
        },
    
    {
        id       : 'Pharmacies',
                title    : 'Pharmacies',
                translate: 'Pharmacies',
                type     : 'collapsable',
                icon     : 'local_pharmacy',
                children:[/*{
                    id   : 'Ajouter une pharmacie',
                    title: 'Ajouter une pharmacie',
                    type : 'item',
                    url  : 'add-pharmacy'
                },*/
                {
                    id   : 'Mes Pharmacies',
                    title: 'Mes Pharmacies',
                    type : 'item',
                    url  : 'Mespharmacies'   
                },
                /*{
                    id   : 'Liste des Pharmacies',
                    title: 'Liste des Pharmacies',
                    type : 'item',
                    url  : 'listAll-pharmacy'   
                }*/
                {
                    id   : 'Liste des Pharmacies',
                    title: 'Liste des Pharmacies',
                    type : 'item',
                    url  : 'pharmacies'   
                }
            ]
    },
    {
        id       : 'Produits',
                title    : 'Produits',
                translate: 'Produits',
                type     : 'collapsable',
                icon     : 'add_shopping_cart',
            
                children:[/*{
                    id   : 'Ajouter un produit',
                    title: 'Ajouter un produit',
                    type : 'item',
                   
                    url  : 'add-produit'
                },*/
                {
                    id   : 'Liste des produits ',
                    title: 'Liste des produits',
                    type : 'item',
                    url  : 'products'   
                },{
                    id   : 'Valider des produits ',
                    title: 'Valider des produits',
                    type : 'item',
                    url  : 'product/valid'   
                }
            ]
    },{
        id       : 'Services',
                title    : 'Services',
                translate: 'Services',
                type     : 'collapsable',
                icon     : 'room_service',
            
                children:[/*{
                    id   : 'Ajouter un service',
                    title: 'Ajouter un service',
                    type : 'item',
                   
                    url  : 'add-service'
                },*/
                {
                    id   : 'Liste des services ',
                    title: 'Liste des services',
                    type : 'item',
                    url  : 'Services'   
                }
               
            ]
    },{
        id       : 'Conseils',
                title    : 'Conseils',
                translate: 'Conseils',
                type     : 'collapsable',
                icon     : 'info',
            
                children:[/*{
                    id   : 'Ajouter un conseil',
                    title: 'Ajouter un conseil',
                    type : 'item',
                   
                    url  : 'add-conseil'
                },*/
                {
                    id   : 'Liste des conseils ',
                    title: 'Liste des conseils',
                    type : 'item',
                    url  : 'Conseils'   
                }
               
            ]
    }, {
        id       : 'Role',
        title    : 'Gestion Des Rôles',
        translate: 'Gestion Des Rôles',
        type     : 'collapsable',
        icon     : 'info',
        children : [
            {
                id   : 'users',
                title: 'Liste Des Utilisateurs',
                type : 'item',
                url  : 'users'
            }, {
                id   : 'add user ',
                title: 'Ajouter Un Ou Des Utilisateurs',
                type : 'item',
                url  : 'add users'
            }
        ]
    }
   ]}]

    
       
        

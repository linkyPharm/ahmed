import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {AuthGuard} from './pages/shared/guards/auth.guard';
import {AuthModule} from './pages/authentification/auth.module';
import {MainModule} from './pages/main/main.module';
import {ErrorsModule} from './pages/errors/errors.module';
import {ThemeSettingGuard} from "./pages/shared/guards/theme-setting.guard";
import {NoActivatedComponent} from "../app/pages/main/manage-no-activated/no-activated/no-activated.component";

export function loadMainModule() {
    return MainModule;
}

export function loadAuthModule() {
    return AuthModule;
}

export function loadErrorModule() {
    return ErrorsModule;
}

   
const AppRouting: Routes = [
    {path: '', redirectTo: 'app/overview/dashboard', pathMatch: 'full'},
    {
        path: 'auth',
        loadChildren: loadAuthModule
    }, {
        path: 'app',
// canActivate:[AuthGuard],
        loadChildren: loadMainModule
    }, {
        path: 'error',
        loadChildren: loadErrorModule
    },
   /* {
        path        : 'apps',
        loadChildren: './pages/main/manage-post/apps.module#AppsModule'
    },*/
    {path : 'not-activated' , component : NoActivatedComponent},
    {path: '**', redirectTo: 'error/404'},
    
];

@NgModule({
    imports: [
        RouterModule.forRoot(AppRouting)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {

}

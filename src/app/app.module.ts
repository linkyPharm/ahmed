import {NgModule, InjectionToken} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatMomentDateModule} from '@angular/material-moment-adapter';
import {MatButtonModule, MatIconModule, MatCardModule, MatDivider, MatDividerModule} from '@angular/material';
import {TranslateModule} from '@ngx-translate/core';
import 'hammerjs';

import {FuseModule} from '@fuse/fuse.module';
import {FuseSharedModule} from '@fuse/shared.module';
import {FuseProgressBarModule, FuseSidebarModule, FuseThemeOptionsModule} from '@fuse/components';

import {fuseConfig} from 'app/fuse-config';

import {AppComponent} from 'app/app.component';
import {LayoutModule} from 'app/layout/layout.module';
import {CommonModule, HashLocationStrategy, LocationStrategy} from '@angular/common';
import {AppRoutingModule} from './app.routing';
import {AuthModule} from './pages/authentification/auth.module';
import {MainModule} from './pages/main/main.module';
import {FormsModule} from '@angular/forms';
import {UserService} from './pages/shared/services/user.service';
import {StorageService} from './pages/shared/services/storage.service';
import {MenuService} from './pages/shared/services/menu.service';
import {LanguageService} from './pages/shared/services/language.service';
import {SharedService} from './pages/shared/services/shared.service';
import {Select2Module} from 'ng2-select2';
import {TokenInterceptor} from './pages/shared/interceptors/token.interceptor';
import {ErrorsModule} from './pages/errors/errors.module';
import {UploadDialogModule} from "./pages/shared/ui/upload-dialog/upload-dialog.module";
import {NotificationService} from "./pages/shared/services/notification.service";
import {SweetAlert2Module} from '@toverux/ngx-sweetalert2';




@NgModule({
    declarations: [
        AppComponent,
        
     

       
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        FormsModule,
        CommonModule,
        Select2Module,
        AppRoutingModule,

        TranslateModule.forRoot(),

        // Material moment date module
        MatMomentDateModule,
MatCardModule,
MatDividerModule,
        // Material
        MatButtonModule,
        MatIconModule,

        // Fuse modules
        FuseModule.forRoot(fuseConfig),
        FuseProgressBarModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseThemeOptionsModule,

        // App modules
        LayoutModule,
        AuthModule,
        MainModule,
        ErrorsModule,

        UploadDialogModule,


        SweetAlert2Module.forRoot()
    ],
    bootstrap: [
        AppComponent
    ],
    providers: [
     
        {
            provide: HTTP_INTERCEPTORS,
            useClass: TokenInterceptor,
            multi: true
        },
        {
            provide: LocationStrategy,
            useClass: HashLocationStrategy
        },
        UserService,
        StorageService,
        MenuService,
        LanguageService,

        SharedService,
        NotificationService
    ]
})
export class AppModule {
}

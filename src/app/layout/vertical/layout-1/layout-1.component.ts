import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {FuseConfigService} from '@fuse/services/config.service';
import {navigation} from 'app/navigation/navigation';
import {FuseNavigationService} from '../../../../@fuse/components/navigation/navigation.service';
import {TranslateService} from '@ngx-translate/core';
import {StorageService} from '../../../pages/shared/services/storage.service';
import {UserService} from '../../../pages/shared/services/user.service';
import {MenuService} from '../../../pages/shared/services/menu.service';

import {AppConfig} from '../../../pages/shared/app.config';
import jqXHR = JQuery.jqXHR;
import { Credentials } from 'app/pages/shared/credentials';

@Component({
    selector: 'vertical-layout-1',
    templateUrl: './layout-1.component.html',
    styleUrls: ['./layout-1.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class VerticalLayout1Component implements OnInit, OnDestroy {
    fuseConfig: any;
    navigation: any;
credentials:Credentials
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FuseNavigationService} _fuseNavigationService
     * @param {StorageService} storageService
     * @param {TranslateService} _translateService
     * @param {MenuService} menuService
     * @param {UserService} userService
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _fuseNavigationService: FuseNavigationService,
        private storageService: StorageService,
        private _translateService: TranslateService,
        private menuService: MenuService,
        private userService: UserService,
      
        ) {
        // Set the defaults

        this.navigation = navigation;


        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Subscribe to config changes
       this._fuseConfigService.config
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((config) => {
                this.fuseConfig = config;
            });
      //  this.userService.getCurrentUser().subscribe();
     
     // console.log(this.storageService.read('username'))
   
    }


    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }







}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSousGroupementComponent } from './edit-sous-groupement.component';

describe('EditSousGroupementComponent', () => {
  let component: EditSousGroupementComponent;
  let fixture: ComponentFixture<EditSousGroupementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditSousGroupementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSousGroupementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

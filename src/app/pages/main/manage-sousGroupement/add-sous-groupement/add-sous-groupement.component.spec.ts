import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSousGroupementComponent } from './add-sous-groupement.component';

describe('AddSousGroupementComponent', () => {
  let component: AddSousGroupementComponent;
  let fixture: ComponentFixture<AddSousGroupementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddSousGroupementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSousGroupementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

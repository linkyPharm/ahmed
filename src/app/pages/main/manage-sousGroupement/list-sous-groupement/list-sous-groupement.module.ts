import { NgModule } from "@angular/core";
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { MatButtonModule, MatChipsModule, MatFormFieldModule, MatIconModule, MatInputModule, MatPaginatorModule, MatRippleModule, MatSelectModule, MatSortModule, MatSnackBarModule, MatTableModule, MatTabsModule } from "@angular/material";
import { FuseWidgetModule } from "@fuse/components";
import { FuseSharedModule } from "@fuse/shared.module";
//import { EcommerceProductsService } from "app/pages/shared/services/ecommerce.service";
import { Routes, RouterModule } from "@angular/router";
import { MesServices } from "app/pages/shared/services/mesServices.service";
//import { ListGroupementComponent } from "./list-groupement.component";
import { ListSousGroupementComponent } from "./list-sous-groupement.component";



@NgModule({
    declarations: [
        
        ListSousGroupementComponent
    ],
    imports     : [
     
        RouterModule,
        MatButtonModule,
        MatChipsModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatPaginatorModule,
        MatRippleModule,
        MatSelectModule,
        MatSortModule,
        MatSnackBarModule,
        MatTableModule,
        MatTabsModule,

        NgxChartsModule,
      

        FuseSharedModule,
        FuseWidgetModule
    ],
    providers   : [
        MesServices
 
    ]
})

export class SousGroupementModule{}
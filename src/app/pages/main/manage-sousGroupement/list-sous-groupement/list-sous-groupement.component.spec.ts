import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListSousGroupementComponent } from './list-sous-groupement.component';

describe('ListSousGroupementComponent', () => {
  let component: ListSousGroupementComponent;
  let fixture: ComponentFixture<ListSousGroupementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListSousGroupementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListSousGroupementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {RouterModule, Routes} from '@angular/router';
import {NgModule, Component} from '@angular/core';
import {MainComponent} from './main.component';

import {SampleComponent} from './sample/sample.component';

import {ThemeSettingGuard} from "../shared/guards/theme-setting.guard";
import { AddPostComponent } from './manage-post/add-post/add-post.component';
import { ListPostComponent } from './manage-post/list-post/list-post.component';
import { EditPostComponent } from './manage-post/edit-post/edit-post.component';
//import { DeletepostComponent } from './manage-post/deletepost/deletepost.component';
import { AddPharmacyComponent } from './mange-pharmacy/add-pharmacy/add-pharmacy.component';
import { ListPharmacyComponent } from './mange-pharmacy/list-pharmacy/list-pharmacy.component';
import { EditPharmacyComponent } from './mange-pharmacy/edit-pharmacy/edit-pharmacy.component';
import { AjouterProduitComponent } from './manage-produits/ajouter-produit/ajouter-produit.component';
import { ListProduitsComponent } from './manage-produits/list-produits/list-produits.component';
import { EditProduitComponent } from './manage-produits/edit-produit/edit-produit.component';
import { AllPharmacyComponent } from './mange-pharmacy/all-pharmacy/all-pharmacy.component';
import { DetailPharmacyComponent } from './mange-pharmacy/detail-pharmacy/detail-pharmacy.component';
import { PostHistoryComponent } from './manage-post/post-history/post-history.component';
import { VisualiserPostComponent } from './manage-post/visualiser-post/visualiser-post.component';
import { AddServiceComponent } from './manage-services/add-service/add-service.component';
import { ListeServicesComponent } from './manage-services/liste-services/liste-services.component';
import { UpdateServiceComponent } from './manage-services/update-service/update-service.component';
import { AddConseilComponent } from './manage-conseil/add-conseil/add-conseil.component';
import { ListConseilComponent } from './manage-conseil/list-conseil/list-conseil.component';
import { UpdateConseilComponent } from './manage-conseil/update-conseil/update-conseil.component';
import { TestListComponent } from './manage-produits/test-list/test-list.component';
import { EcommerceProductsService } from '../shared/services/ecommerce.service';
import { TestListPharmaComponent } from './mange-pharmacy/test-list-pharma/test-list-pharma.component';
import { PharmaService } from '../shared/services/pharma.service';
import { MesPharmaciesComponent } from './mange-pharmacy/mes-pharmacies/mes-pharmacies.component';
import { MesPharmaService } from '../shared/services/MesPharma.service';
import { HistoriqueAfficheComponent } from './manage-post/historique-affiche/historique-affiche.component';
import { historiqueService } from '../shared/services/historique.service';
import { MesServicesComponent } from './manage-services/mes-services/mes-services.component';
import { MesServices } from '../shared/services/mesServices.service';
import { MaListComponent } from './manage-conseil/ma-list/ma-list.component';
import { MesConseils } from '../shared/services/MesConseils.service';
//import { TestPostComponent } from './manage-post/test-post/test-post.component';
import { GenerateFlyerComponent } from './flyer/generate-flyer/generate-flyer.component';
import { TestFlyerComponent } from './flyer/test-flyer/test-flyer.component';
import { ConfigurerLeafletComponent } from './flyer/configurer-leaflet/configurer-leaflet.component';
import { ConfigLeafletComponent } from './flyer/config-leaflet/config-leaflet.component';
import { configLeafletService } from '../shared/services/config.service';
import { EditLeafletComponent } from './flyer/edit-leaflet/edit-leaflet.component';
//import { DeletePharmacyComponent } from './mange-pharmacy/delete-pharmacy/delete-pharmacy.component';


import {ListOfUsersComponent} from './manage-role/list-of-users/list-of-users.component';
import { HistoriqueLeafletComponent } from './flyer/historique-leaflet/historique-leaflet.component';
import { EditConfigLeafletComponent } from './flyer/edit-config-leaflet/edit-config-leaflet.component';
import {ValidProduitsComponent} from '../main/manage-produits/valid-produits/valid-produits.component';



const AppRouting: Routes = [
    {
        path: '',
        component: MainComponent,
        children: [
            
            {
                path: 'overview/dashboard',
                //canActivate: [ThemeSettingGuard],
                component: SampleComponent,
            },
            {
                path:'add-post',
                component:AddPostComponent
            },
            {
                path:'list-post',
                component:ListPostComponent
            },
            {
                path:'edit-post/:id',
                component:EditPostComponent
            },
            /*{
                path:'delete-post/:id',
                component:DeletepostComponent
            },*/
            {
                path:'add-pharmacy',
                component:AddPharmacyComponent
            },
            {
                path:'list-pharmacy',
                component:ListPharmacyComponent
            },
            {
                path     : 'Mespharmacies',
                component: MesPharmaciesComponent,
                resolve  : {
                    data: MesPharmaService
                }
            },

            {
                path     : 'pharmacies',
                component: TestListPharmaComponent,
                resolve  : {
                    data: PharmaService
                }
            },
            /*{
                path:'listAll-pharmacy',
                component:AllPharmacyComponent
            },*/
            {
                path:'detail-pharmacy/:id',
                component:DetailPharmacyComponent
            },
            {
                path:'edit-pharmacy/:id',
                component:EditPharmacyComponent
            },
            /*{
                path:'delete-pharmacy',
                component:DeletePharmacyComponent
            }*/
            {
                path:'add-produit',
                component:AjouterProduitComponent
            },
          /*  {
                path:'list-produits',
                component:ListProduitsComponent 
            },*/
            {
                path     : 'products',
                component: TestListComponent,
            },{
                path     : 'product/valid',
                component: ValidProduitsComponent
            },
            {
                path:'edit-produit/:id',
                component:EditProduitComponent
            },{
                path:'history-post',
                component:PostHistoryComponent
            },
            {
                path:'historique',
                component:HistoriqueAfficheComponent,
                resolve  : {
                    data: historiqueService
                }
            },
            {
                path:'visualiser-post/:id',
                component:VisualiserPostComponent
            },
            {
                path:'add-service',
                component:AddServiceComponent
            },
            {
            path:'list-services',
            component:ListeServicesComponent
            },
            {
            path:'Services',
            component:MesServicesComponent,
            resolve  : {
                data: MesServices
            }
        },
            {
                path:'update-service/:id',
                component:UpdateServiceComponent
            },
            {
                path:'add-conseil',
                component:AddConseilComponent
            },
            {
                path:'Conseils',
                component:MaListComponent,
                resolve  : {
                    data: MesConseils
                }
            },
            {
                path:'list-conseils',
                component:ListConseilComponent
            },
            {
                path:'update-conseil/:id',
                component:UpdateConseilComponent
            },
            /*{
                path:'Post',
                component:TestPostComponent
            },*/
            {
                path:'configurer-flyer',
                component:ConfigLeafletComponent,
                resolve  : {
                    data: configLeafletService
                }
            },
            
    
{
    path:'create-leaflet',
    component:GenerateFlyerComponent
},
{
    path:'generer-flyer',
    component:TestFlyerComponent
},
{
    path:'generate-leaflet/:id',
    component:ConfigurerLeafletComponent
},
{
    path:'edit-leaflet/:id',
    component:EditLeafletComponent
},
{
    path:'edit-config/:id',
    component:EditConfigLeafletComponent
},
{
    path:'historique-flyer',
    component:HistoriqueLeafletComponent
},

            {
                path:'test-flyer',
                component:TestFlyerComponent
            },{
                path:'generate-flyer',
                component:ConfigurerLeafletComponent
            },
            {
                path: 'users',
                component: ListOfUsersComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(AppRouting)
    ],
    exports: [
        RouterModule
    ]
})
export class MainRoutingModule {

}

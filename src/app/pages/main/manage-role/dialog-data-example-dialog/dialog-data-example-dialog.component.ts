import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { RoleService } from 'app/pages/shared/services/role.service';
import { StorageService } from 'app/pages/shared/services/storage.service';

@Component({
  selector: 'app-dialog-data-example-dialog',
  templateUrl: './dialog-data-example-dialog.component.html',
  styleUrls: ['./dialog-data-example-dialog.component.scss']
})
export class DialogDataExampleDialogComponent implements OnInit {

    allRoles = [ 'pharmacist', 'admin', 'groupement','employee'];
    
    allRolesObject = [];
    roles;
    returnedRoles = [];
    checkedRoles: string;

  constructor(private roleService: RoleService,private storageService: StorageService) { }

  ngOnInit() {
    this.roles = [];
      this.roles = this.roleService.getUserRoles();
      for (let i = 0; i < this.allRoles.length; i++){  
          const checked = this.checkExistance(this.allRoles[i]);
        const myObj = {"role" : this.allRoles[i], "checked" : checked};
        this.allRolesObject.push(myObj);
      }
      
  }


  addRole(role){
    let i = 0;
    while ((i < this.allRolesObject.length - 1) && (this.allRolesObject[i].role !== role)){
        i++;
    }
    this.allRolesObject[i].checked = !(this.allRolesObject[i].checked);
    console.log(this.allRolesObject[i]);
  }


  checkExistance(role){
      if (this.roles.indexOf(role) >= 0){
          return true;
      }else{
          return false;
      }
  }


  returnRoles(){
    this.storageService.remove('currentRole');
    for (let i = 0; i < this.allRolesObject.length; i++){
      if (this.allRolesObject[i].checked === true){
          const myObj = {"label" : this.allRolesObject[i].role , "roleId" : 1};
          console.log(myObj);
          this.returnedRoles.push(myObj);
      }
    }
    if (this.returnedRoles[0] === undefined){
        this.returnedRoles = [{"label" : 'guest' , "roleId" : 1}];
    }
    
    this.roleService.setAllRolesReturned(this.returnedRoles);
    this.ngOnInit();
  }

}

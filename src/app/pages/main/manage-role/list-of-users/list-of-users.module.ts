//import { TestListComponent } from "./test-list.component";
import { NgModule } from "@angular/core";
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { MatButtonModule, MatChipsModule, MatFormFieldModule, MatIconModule, MatInputModule, MatPaginatorModule, MatRippleModule, MatSelectModule, MatSortModule, MatSnackBarModule, MatTableModule, MatTabsModule } from "@angular/material";
import { FuseWidgetModule } from "@fuse/components";
import { FuseSharedModule } from "@fuse/shared.module";
import { EcommerceProductsService } from "app/pages/shared/services/ecommerce.service";
import { Routes, RouterModule } from "@angular/router";
//import { TestListPharmaComponent } from "./test-list-pharma.component";
import { LottieAnimationViewModule } from 'ng-lottie';
import { ListOfUsersComponent } from "./list-of-users.component";
import { MesPharmaService } from "app/pages/shared/services/MesPharma.service";
import { RoleService } from "app/pages/shared/services/role.service";
import { DialogDataExampleDialogComponent } from '../dialog-data-example-dialog/dialog-data-example-dialog.component';
import {ErrorDialogComponent} from '../error-dialog/error-dialog.component';

@NgModule({
    declarations: [
        ListOfUsersComponent,
        
    ],
    imports     : [
        LottieAnimationViewModule.forRoot(),
        RouterModule,
        MatButtonModule,
        MatChipsModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatPaginatorModule,
        MatRippleModule,
        MatSelectModule,
        MatSortModule,
        MatSnackBarModule,
        MatTableModule,
        MatTabsModule,

        NgxChartsModule,
      

        FuseSharedModule,
        FuseWidgetModule,
        
    ],
    providers   : [
        
 RoleService
    ],
    entryComponents: [
        DialogDataExampleDialogComponent,
        ErrorDialogComponent
    ]
})

export class UsersModule{}
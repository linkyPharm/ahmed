import { Component, OnInit,ViewChild} from '@angular/core';
import { RoleService } from 'app/pages/shared/services/role.service';
import {MatPaginator} from '@angular/material/paginator';

import {MatDialog} from '@angular/material/dialog';

import { DialogDataExampleDialogComponent } from '../dialog-data-example-dialog/dialog-data-example-dialog.component';

import {ErrorDialogComponent} from '../error-dialog/error-dialog.component';
import { StorageService } from 'app/pages/shared/services/storage.service';

import {rolesConst} from '../RolesPriority';
  @Component({
    selector: 'app-list-of-users',
    templateUrl: './list-of-users.component.html',
    styleUrls: ['./list-of-users.component.scss']
  })
export class ListOfUsersComponent implements OnInit {

    role0 = rolesConst[0];
    role1 = rolesConst[1];
    role2 = rolesConst[2];
    role3 = rolesConst[3];
    userRolePriority;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    users;
    allRoles = [];
    searchText: any = "";   
    displayedColumns: string[] = ['FirstName', 'LastName', 'Email', 'Roles', 'Add'];
    
  constructor(private roleService: RoleService,
    private storageService:StorageService,
     private dialog: MatDialog
    ) { }

  ngOnInit() {
    let userToken ; 
    userToken = this.storageService.read('user');
     this.userRolePriority = this.roleService.getUserPriorityRole(userToken.role);
     this.roleService.changeAccountActivity(this.userRolePriority);
     if(this.storageService.read('currentRole') == undefined) {
        this.storageService.write('currentRole', this.userRolePriority);
    }
    this.userRolePriority = this.storageService.read('currentRole');
    this.storageService.write('configBool',false)
    this.storageService.write('historiqueBool',false) 
    this.roleService.getUsersList().subscribe({
        next : response => {
            this.users = response;
            console.log(this.users);        
        }
    });
  }

  openDialog(userId , userRoles) {
      this.roleService.roles = [];
    if (userRoles !== null){
        for (let i = 0; i < userRoles.length; i++){
            this.roleService.setUserRoles(userRoles[i].label);
        }
        
    }
    const dialogRef = this.dialog.open(DialogDataExampleDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
        this.allRoles = this.roleService.getAllRolesReturned();
        if (navigator.onLine) {
            if (result !== false){
                this.roleService.putUserRoles(userId , this.allRoles).subscribe({
                    next : () => {
                        this.roleService.editToken(this.allRoles);
                        this.ngOnInit();
                        location.reload();
                    }
                });
            }
        } else {
            const dialogError = this.dialog.open(ErrorDialogComponent);
            
        }
      });

  }




}

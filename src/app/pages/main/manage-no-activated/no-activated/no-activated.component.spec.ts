import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NoActivatedComponent } from './no-activated.component';

describe('NoActivatedComponent', () => {
  let component: NoActivatedComponent;
  let fixture: ComponentFixture<NoActivatedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NoActivatedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NoActivatedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

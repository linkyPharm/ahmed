import { Component, OnInit } from '@angular/core';
import { produit } from 'app/pages/shared/models/Produits';
import { ProduitService } from 'app/pages/shared/services/produits.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NotificationService } from 'app/pages/shared/services/notification.service';
import { StorageService } from 'app/pages/shared/services/storage.service';

@Component({
  selector: 'app-edit-produit',
  templateUrl: './edit-produit.component.html',
  styleUrls: ['./edit-produit.component.scss']
})
export class EditProduitComponent implements OnInit {
produit:produit
form:FormGroup
format:string[]=['l','h']
uniter: string[] = ['Kilogramme', 'Litre', 'mètre'];
  constructor(private produitService:ProduitService, private activatedRoute:ActivatedRoute,private _formBuilder:FormBuilder
             ,private route:Router,private notificationService:NotificationService
             ,private storageService:StorageService) { }

  ngOnInit() {
    this.storageService.write('configBool',false)
    this.storageService.write('historiqueBool',false)
    this.form = this._formBuilder.group({
      nom:['',Validators.required],
      cipaclean:['',Validators.required],
      laboratoire:['',Validators.required],
      gamme:['',Validators.required],
      quantity:['',Validators.required],
      unit:['',Validators.required],
      format:['',Validators.required],
      description:['',Validators.required],
      prix:['']
    })
    this.activatedRoute.params.subscribe(
      (params)=>{
        console.log(params.id)
        this.produitService.getProduitById(params.id).subscribe(
          produit=>{this.produit=produit
          console.log(produit)}
        )
      }
    )

  }
  submit(){
    this.activatedRoute.params.subscribe(
      params=>{
        this.produitService.updtateProduit(params.id,this.form.value).subscribe(
          res=>{
            console.log(res)
          const link=['/app/products']
this.route.navigate(link)
this.notificationService.showToast('Modification du produit avec succés');
          }
        )
      }
    )

  }

}

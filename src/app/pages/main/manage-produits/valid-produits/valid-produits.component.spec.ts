import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValidProduitsComponent } from './valid-produits.component';

describe('ValidProduitsComponent', () => {
  let component: ValidProduitsComponent;
  let fixture: ComponentFixture<ValidProduitsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValidProduitsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValidProduitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

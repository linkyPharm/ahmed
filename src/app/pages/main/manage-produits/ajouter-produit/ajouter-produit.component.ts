import { Component, OnInit, HostListener } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProduitService } from 'app/pages/shared/services/produits.service';
import * as XLSX from 'ts-xlsx';
import { produit } from 'app/pages/shared/models/Produits';
import { Router } from '@angular/router';
import {saveAs} from 'file-saver'
import { HttpClient } from '@angular/common/http';
import { NotificationService } from 'app/pages/shared/services/notification.service';
@Component({
  selector: 'app-ajouter-produit',
  templateUrl: './ajouter-produit.component.html',
  styleUrls: ['./ajouter-produit.component.scss']
})
export class AjouterProduitComponent implements OnInit {
form:FormGroup
arrayBuffer:any
base64textString:string=""
plusieursProduits=false
PropreProduit=false
json=[{
  nom:'',
  cipaclean:'',
  laboratoire:'',
  gamme:'',
  quantity:'',
  unit:'',
  format:'',
  description:'',
  img:'',
  prix:''
}]
produit:produit[]=[]
  constructor(private _formBuilder:FormBuilder,private produitService:ProduitService,private route:Router,private http:HttpClient,
              private notificationService:NotificationService) { }
  uniter: string[] = ['Kilogramme', 'Litre', 'mètre'];
  public file: File | null = null;
  format:string[]=['l','h']
  ngOnInit() {
    this.form = this._formBuilder.group({
      nom:[''],
      cipaclean:[''],
      laboratoire:[''],
      gamme:[''],
      quantity:[''],
      unit:[''],
      format:[''],
      description:[''],
      img:[''],
      prix:['']
    })
  }
  @HostListener('change', ['$event.target.files']) emitFiles( event: FileList) {
    console.log('heyy')
    
    const file = event && event.item(0);
    this.file = file;
  }
  handleFileSelect(evt){
    var files = evt.target.files;
    var file = files[0];
  
  if (files && file) {
      var reader = new FileReader();

      reader.onload =this._handleReaderLoaded.bind(this);

      reader.readAsBinaryString(file);
  }
}

_handleReaderLoaded(readerEvt) {
   var binaryString = readerEvt.target.result;
          this.base64textString= btoa(binaryString);
          this.form.value.img=btoa(binaryString)
          //console.log(btoa(binaryString));
          console.log(this.base64textString)
  }
  incomingfile(event) 
  {
  this.file= event.target.files[0]; 
  }
  download(){
    this.http.get('http://51.77.158.247:3000/excel/produits.xlsx', {responseType:'blob'} )
 .subscribe((res) => {

console.log(res)


    //this.storageService.write('fakeToken',55)
    const pdfblob=new Blob([res],{type:'application/xlsx'})
        saveAs(pdfblob,`${Date.now()}.xlsx`)
        console.log('working!')

 })
  }
 Upload() {
      let fileReader = new FileReader();
        fileReader.onload = (e) => {
            this.arrayBuffer = fileReader.result;
            var data = new Uint8Array(this.arrayBuffer);
            var arr = new Array();
            for(var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
            var bstr = arr.join("");
            var workbook = XLSX.read(bstr, {type:"binary"});
            var first_sheet_name = workbook.SheetNames[0];
            var worksheet = workbook.Sheets[first_sheet_name];
            console.log(XLSX.utils.sheet_to_json(worksheet,{raw:true}));
            this.json=XLSX.utils.sheet_to_json(worksheet,{raw:true})
      
            for (let i=0;i<this.json.length;i++){
              let product=new produit('','','','','','','','','','','',0)
              
              product.nom=this.json[i].nom
              product.cipaclean=parseInt(this.json[i].cipaclean) 
              product.gamme=this.json[i].gamme
              product.format=this.json[i].format
              product.laboratoire=this.json[i].laboratoire
              product.description=this.json[i].description
              product.quantity=parseInt(this.json[i].quantity)
              product.description=this.json[i].description
              product.prix=parseInt(this.json[i].prix)
              product.unit=this.json[i].unit

              
              
              
              this.produit[i]=product
              
              
                        }
                        console.log(this.produit)
              
                          this.produitService.addMultipleProduits(this.produit).subscribe(
                            res=>{
                              console.log('workin!')
                              console.log(res)
                              alert('Ajout des pharmacies avec succès')
                         //  location.reload()
                            },
                            err=>{
                              console.log(err)
                            }
                          )
                         
                                  }
                      fileReader.readAsArrayBuffer(this.file);
                                }
  disablePropreProduit(){
this.PropreProduit=false
  }
  disableplusieursProduits(){
this.plusieursProduits=false
  }
  submit(){
    console.log(this.form.value)
   // this.form.value.format='h'
   
   // this.form.value.unit='Kilogramme'
this.produitService.submitProduit(this.form.value).subscribe(
  (res)=>{
    console.log(res)
    console.log("working!")
   
    const link=['/app/product/valid']
    this.route.navigate(link)
    this.notificationService.showToast('Ajout du produit avec succés');
  },(err)=>{
    console.log(err)
  }
)
  }

}

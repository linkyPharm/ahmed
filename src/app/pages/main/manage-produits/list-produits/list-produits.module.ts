
import {NgModule} from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
    MatButtonModule, MatDatepickerModule, MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule, MatSlideToggleModule, MatToolbarModule, MatTooltipModule, MatCardModule, MatDividerModule, MatTableModule, MatPaginatorModule
} from '@angular/material';
import { ColorPickerModule } from 'ngx-color-picker';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule } from '@fuse/components';
import { ListProduitsComponent } from './list-produits.component';
import { HttpClientModule } from '@angular/common/http';
import { ProduitService } from 'app/pages/shared/services/produits.service';



const routes: Routes = [
    {
        path     : '**',
        component: ListProduitsComponent,
        children : [
        ]
       
    }
];

@NgModule({
    declarations   : [
ListProduitsComponent
        
       
    ],
    imports        : [
        RouterModule.forChild(routes),
HttpClientModule,
        MatButtonModule,
        MatDatepickerModule,
        MatDialogModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatSlideToggleModule,
        MatToolbarModule,
        MatTooltipModule,
        MatPaginatorModule,
MatCardModule,
MatDividerModule,
MatTableModule,
        
        ColorPickerModule,

        FuseSharedModule,
        FuseConfirmDialogModule
    ],
    providers      : [
        ProduitService,
    ],
    entryComponents: [
     
    ]
})
export class ListProduitsModule
{
}

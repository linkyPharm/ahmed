import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { Pharmacy } from 'app/pages/shared/models/Pharmacy';
import { Router } from '@angular/router';
import { UserService } from 'app/pages/shared/services/user.service';
import { Personne } from '../../manage-post/model/personne';
import { produit } from 'app/pages/shared/models/Produits';
import { ProduitService } from 'app/pages/shared/services/produits.service';

@Component({
  selector: 'app-list-produits',
  templateUrl: './list-produits.component.html',
  styleUrls: ['./list-produits.component.scss']
})
export class ListProduitsComponent implements OnInit {
  displayedColumns: string[] = [
    'img',  'nom', 'laboratoire', 'description',
   'cipaclean', 'gamme' , 'quantity', 'unit', 'prix' , 'format',  'action'];

  dataSource: MatTableDataSource<produit>;
personnes:Personne[]
produits:produit[]
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private router:Router,private userService:UserService,
    private produitService:ProduitService
    
   
    ) {
    // Create 100 users
    //const users = Array.from({length: 100}, (_, k) => createNewUser(k + 1));
    

    // Assign the data to the data source for the table to render

  }

  ngOnInit() {
  
      this.fetchProduits()
this.personnes= this.userService.getFakePersonne()


   /* this.dataSource = new MatTableDataSource(this.produits);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;*/
  }
fetchProduits(){
  this.produitService.getProduits().subscribe(
    (produits)=>{
      //console.log(produits)
      this.produits=produits
      console.log(this.produits)
      this.dataSource = new MatTableDataSource(this.produits);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    }
  )
  console.log(this.produits)
}
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  /*getoption(){
    console.log("hey")
    this.option=this.userService.returnOption()
  }*/
  editproduit(id){


    const link=[`/app/edit-produit/${id}`]
    this.router.navigate(link)
    console.log(id)
    /*this.userService.updatePharmacy(this.pharmacy).subscribe(
      response=>{
        const link=[`/app/edit-pharmacy/${id}`]
    this.router.navigate(link)
      }
    )*/

  }
  deleteproduit(id){
/*this.userService.deletePharmacy(id)
  console.log("in")
  const link=['/app/edit-pharmacy']
this.router.navigate(link)*/
this.produitService.deleteproduit(id).subscribe(
  res=>{console.log(res)
  this.ngOnInit()
  })

  }

}

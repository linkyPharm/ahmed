import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ServicePharma } from 'app/pages/shared/services/Services.service';
import { NotificationService } from 'app/pages/shared/services/notification.service';
import { StorageService } from 'app/pages/shared/services/storage.service';

@Component({
  selector: 'app-add-groupement',
  templateUrl: './add-groupement.component.html',
  styleUrls: ['./add-groupement.component.scss']
})
export class AddGroupementComponent implements OnInit {

  form:FormGroup
  base64textString: string;
  constructor(private _formBuilder:FormBuilder,
              private servicePharma:ServicePharma,
              private notificationService:NotificationService,
              private storageService:StorageService ) { }

  ngOnInit() {
    this.storageService.write('configBool',false)
    this.storageService.write('historiqueBool',false)
    this.form = this._formBuilder.group({
        
      description : [''],
      icon:[''],
      title  : [''],
      subtitle : ['']
    })
  }
  submit(){
this.servicePharma.ajouterService(this.form.value).subscribe(
  res=>{
    console.log(res)
    this.notificationService.showToast('Ajout du service avec succés');
  }
)
  }

  handleFileSelect(evt){
    var files = evt.target.files;
    var file = files[0];
  
  if (files && file) {
      var reader = new FileReader();

      reader.onload =this._handleReaderLoaded.bind(this);

      reader.readAsBinaryString(file);
  }
}

_handleReaderLoaded(readerEvt) {
   var binaryString = readerEvt.target.result;
          this.base64textString= btoa(binaryString);
          this.form.value.img=btoa(binaryString)
          //console.log(btoa(binaryString));
          console.log(this.base64textString)
  }


}

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ServicePharma } from 'app/pages/shared/services/Services.service';
import { NotificationService } from 'app/pages/shared/services/notification.service';
import { StorageService } from 'app/pages/shared/services/storage.service';

@Component({
  selector: 'app-edit-groupement',
  templateUrl: './edit-groupement.component.html',
  styleUrls: ['./edit-groupement.component.scss']
})
export class EditGroupementComponent implements OnInit {

  form:FormGroup
  constructor(private _formBuilder:FormBuilder,
              private servicePharma:ServicePharma,
              private notificationService:NotificationService,
              private storageService:StorageService ) { }

  ngOnInit() {
    this.storageService.write('configBool',false)
    this.storageService.write('historiqueBool',false)
    this.form = this._formBuilder.group({
        
      description : [''],
      icon:[''],
      title  : [''],
      subtitle : ['']
    })
  }
  submit(){
this.servicePharma.ajouterService(this.form.value).subscribe(
  res=>{
    console.log(res)
    this.notificationService.showToast('Ajout du service avec succés');
  }
)
  }


}

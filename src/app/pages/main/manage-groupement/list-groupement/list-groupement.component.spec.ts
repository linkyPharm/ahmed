import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListGroupementComponent } from './list-groupement.component';

describe('ListGroupementComponent', () => {
  let component: ListGroupementComponent;
  let fixture: ComponentFixture<ListGroupementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListGroupementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListGroupementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

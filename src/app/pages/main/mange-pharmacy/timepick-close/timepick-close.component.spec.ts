import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimepickCloseComponent } from './timepick-close.component';

describe('TimepickCloseComponent', () => {
  let component: TimepickCloseComponent;
  let fixture: ComponentFixture<TimepickCloseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimepickCloseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimepickCloseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

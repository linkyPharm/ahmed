import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
@Component({
  selector: 'app-timepick-close',
  templateUrl: './timepick-close.component.html',
  styleUrls: ['./timepick-close.component.scss']
})
export class TimepickCloseComponent implements OnInit {
    @Output() emittedTime = new EventEmitter();
    @Input() defaultClosingTime;
  constructor() { }

  ngOnInit() {
  }
  timePicked(time){
    console.log(time);

    this.emittedTime.emit(
        time
    );
}

}

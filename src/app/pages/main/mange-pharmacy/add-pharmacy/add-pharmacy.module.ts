
import {NgModule} from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
    MatButtonModule, MatDatepickerModule, MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule, MatSlideToggleModule, MatToolbarModule, MatTooltipModule, MatCardModule, MatDividerModule, MatTableModule, MatPaginatorModule, MatAutocompleteModule, MatSelectModule, MatRadioModule, MatCheckboxModule
} from '@angular/material';
import { ColorPickerModule } from 'ngx-color-picker';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule } from '@fuse/components';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import { AddPharmacyComponent } from './add-pharmacy.component';
import { ReactiveFormsModule } from '@angular/forms';
const routes: Routes = [
    {
        path     : '**',
        component: AddPharmacyComponent,
        children : [
        ]
       
    }
];

@NgModule({
    declarations   : [
  AddPharmacyComponent
        
       
    ],
    imports        : [
        RouterModule.forChild(routes),
        NgxMaterialTimepickerModule,
        MatButtonModule,
        MatDatepickerModule,
        MatDialogModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatSlideToggleModule,
        MatToolbarModule,
        MatTooltipModule,
        MatAutocompleteModule,
        MatPaginatorModule,
        ReactiveFormsModule,
MatRadioModule,
MatCardModule,
MatDividerModule,
MatTableModule,
MatSelectModule,
MatCheckboxModule,
        
        ColorPickerModule,

        FuseSharedModule,
        FuseConfirmDialogModule
    ],
    providers      : [
        
    ],
    entryComponents: [
     
    ]
})
export class AddPharmacyModule
{
}

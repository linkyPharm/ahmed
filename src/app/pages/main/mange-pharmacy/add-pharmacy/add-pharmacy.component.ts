import {Component, OnInit, HostListener} from '@angular/core';
import {FormControl,FormGroup,FormBuilder, Validators, NgForm, FormArray} from '@angular/forms';
import {Observable, Subject} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { UserService } from 'app/pages/shared/services/user.service';
import { Address } from 'app/pages/shared/models/Address';
import { openingDetails } from 'app/pages/shared/models/openingdetail';
import { additionalInfo } from 'app/pages/shared/models/additionalinfo';
import { HttpClient } from '@angular/common/http';
import { List } from 'lodash';
import { PharmacieService } from 'app/pages/shared/services/pharmacie.service';
import { Organization } from 'app/pages/shared/models/Organization';
import * as XLSX from 'ts-xlsx';
import { Pharmacy } from 'app/pages/shared/models/Pharmacy';
import { pharmaExc } from 'app/pages/shared/models/pharmaExc';
import { StorageService } from 'app/pages/shared/services/storage.service';
import { Router } from '@angular/router';
import {saveAs} from 'file-saver';
import { toFormData } from '../../manage-post/add-post/add-post.component';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import { NotificationService } from 'app/pages/shared/services/notification.service';
export interface horaire{
    value:any
}
export class link{
  lien:string
  id:any
}



@Component({
  selector: 'app-add-pharmacy',
  templateUrl: './add-pharmacy.component.html',
  styleUrls: ['./add-pharmacy.component.scss']
})
export class AddPharmacyComponent implements OnInit {
link=new link()
clicked=false
json=[{
  PHARMACIE:'',
  CIP:'',
  GROUPEMENT:'',	
  PHARMACIENAME:'',	
  TITULAIRE:'',	
  Adresse:'',
  	CP:'',
  VILLE:'',
  	Téléphone:''

}]
arrayBuffer:any
dataarray=[]
x=0
showlable=[false,false,false,false]
pharmaExcel:pharmaExc[]=[]
//pharma:pharmaExc
pharmacy:Pharmacy[]=[]
show:boolean=false
address:Address=new Address()
//openingDetails:List<openingDetails>=[{id:0,openingTime:'',closingTime:'',data:'',day:''}]
//additionalInfo:List<additionalInfo>=[{label:'',value:''},{label:'',value:''},{label:'',value:''},{label:'',value:''}]
  selectedFile:File
  form: FormGroup;
  demoForm: FormGroup;
  orderForm: FormGroup;
  plusieursPharma=false
  ProprePharma=false
items: FormArray;
public file: File | null = null;

  myControl = new FormControl();
  options: string[] = ['Monday', 'Tuesday', 'Wednesday','Thursday','Friday','Saturday','Sunday'];
horaires:horaire[]=[
  {  value:`lundi-->vendredi: 8h-17h`},
  {value:`samed:8h->13h`}

]
organs:Organization[]
liens:FormGroup

userform:FormGroup
  filteredOptions: Observable<string[]>;
  filtered:Observable<any[]>
  // Horizontal Stepper
  horizontalStepperStep1: FormGroup;
  horizontalStepperStep2: FormGroup;
  horizontalStepperStep3: FormGroup;

  // Vertical Stepper
  verticalStepperStep1: FormGroup;
  verticalStepperStep2: FormGroup;
  verticalStepperStep3: FormGroup;

  // Private
  private _unsubscribeAll: Subject<any>;


  /**
   * Constructor
   *
   * @param {FormBuilder} _formBuilder
   */
  constructor(
      private _formBuilder: FormBuilder,
      private userService:UserService,
      private formBuilder: FormBuilder,
      private user:UserService,
      private http:HttpClient,
      private pharamacyService:PharmacieService,
      private storageService:StorageService,
      private Route:Router,
      private fb:FormBuilder,
      private notificationService:NotificationService
  )
  {
      // Set the private defaults
      this._unsubscribeAll = new Subject();
      this.demoForm = this._formBuilder.group({
        demoArray: this._formBuilder.array([])
     });
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void
  {
    this.dataarray.push(this.link)
   this.liens=this._formBuilder.group({
     numberofLinks:['',Validators.required],
     liens:new FormArray([])
   })
   this.orderForm = this.formBuilder.group({
    customerName: ['', Validators.required],
    email: ['', Validators.required],
    items: this.formBuilder.array([ this.createItem() ])
  }); 
    this.filteredOptions = this.myControl.valueChanges
    .pipe(
      startWith(''),
      map(value => this._filter(value))
    );
 
      // Reactive Form
     this.form = this._formBuilder.group({
        
          email : [''],
         organs:[''],
          fax  : [''],
          name : [''],
          phone : [''],
        website : [''],
          organizationId : [''],
          ownerId : [''],          
          Relationship : [''],
          address : [''],
          city:[''],
          zip:[''],
          street:[''],
          country:[''],
          //openingDetails: [[]],
         // additionalInfo:  [[]],
          //closingTime : [''],
          //data : [''],
          //openingTime : [''],
          //Day : [''],
          label : [''],
          value : [''],
          openingDetails: this.fb.array([this.fb.group({openingTime:'',closingTime:'',data:'',day:''})]),
          additionalInfo:this.fb.array([this.fb.group({label:'',value:''})])
          
        });
        this.pharamacyService.getorgans().subscribe(
          res=>{
            console.log(res)
            this.organs=res
            console.log(this.organs)
          }
        )

      // Horizontal Stepper form steps
     /* this.horizontalStepperStep1 = this._formBuilder.group({
          firstName: ['', Validators.required],
          lastName : ['', Validators.required]
      });

      this.horizontalStepperStep2 = this._formBuilder.group({
          cin: ['', Validators.required]
      });

      this.horizontalStepperStep3 = this._formBuilder.group({
          job      : ['', Validators.required],
        
      });

      // Vertical Stepper form stepper
      this.verticalStepperStep1 = this._formBuilder.group({
          firstName: ['', Validators.required],
          lastName : ['', Validators.required]
      });

      this.verticalStepperStep2 = this._formBuilder.group({
          address: ['', Validators.required]
      });

      this.verticalStepperStep3 = this._formBuilder.group({
          city      : ['', Validators.required],
          state     : ['', Validators.required],
          postalCode: ['', [Validators.required, Validators.maxLength(5)]]
      });*/
  }
  @HostListener('change', ['$event.target.files']) emitFiles( event: FileList) {
    console.log('heyy')
    
    const file = event && event.item(0);

   
    this.file = file;
  }
  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }
  incomingfile(event) 
  {
  this.file= event.target.files[0]; 
  }
  get sellingPoints() {
    return this.form.get('openingDetails') as FormArray;
  }
  addSellingPoint() {
    this.sellingPoints.push(this.fb.group({openingTime:'',closingTime:'',data:'',day:''}));
  }
  deleteSellingPoint(index) {
    this.sellingPoints.removeAt(index);
  }

  get Info() {
    return this.form.get('additionalInfo') as FormArray;
  }
  addInfo() {
    this.Info.push(this.fb.group({label:'',value:''}));
  }
  deleteInfo(index) {
    this.Info.removeAt(index);
  }
 /* workDay(day){
    console.log(day)
    this.openingDetails[0].day=day
  }*/
  /*afficheLabel(){

    this.x++
    console.log(this.x)
    if (this.x==1){
      this.showlable[0]=true
    }
    else if (this.x==2){
      this.showlable[1]=true
    }
    else if (this.x==3){
      this.showlable[2]=true
    }
    else {
      this.showlable[3]=true
    }
  }*/
 /* disableLabel(){
    if (this.x==1){
      this.showlable[0]=false
    }
    else if (this.x==2){
      this.showlable[1]=false
    }
    else if (this.x==3){
      this.showlable[2]=false
    }
    else {
      this.showlable[3]=false
    }
  }*/
  download(){
    this.http.get('http://51.77.158.247:3000/excel/pharmacie.xlsx', {responseType:'blob'} )
 .subscribe((res) => {

console.log(res)


    //this.storageService.write('fakeToken',55)
    const pdfblob=new Blob([res],{type:'application/xlsx'})
        saveAs(pdfblob,`${Date.now()}.xlsx`)
        console.log('working!')
this.clicked=true
 })
  }
 Upload() {
      let fileReader = new FileReader();
      let pharmacie:Pharmacy
        fileReader.onload = (e) => {
            this.arrayBuffer = fileReader.result;
            var data = new Uint8Array(this.arrayBuffer);
            var arr = new Array();
            for(var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
            var bstr = arr.join("");
            var workbook = XLSX.read(bstr, {type:"binary"});
            var first_sheet_name = workbook.SheetNames[0];
            var worksheet = workbook.Sheets[first_sheet_name];
            //console.log(XLSX.utils.sheet_to_json(worksheet,{raw:true}));
          this.json=XLSX.utils.sheet_to_json(worksheet,{raw:true})
          console.log(this.json);
       for (let i=0;i<this.json.length;i++){
let pharma=new pharmaExc('','','','','',0,{city:'',street:'',contry:'',zip:''},{},null,null,null)

//pharma.email=this.json[i].Email
pharma.name=this.json[i].PHARMACIENAME  
pharma.organizationId=0
//pharma.address.street=this.json[i].RUE
pharma.address.city=this.json[i].VILLE
pharma.phone=this.json[i].Téléphone
pharma.owner.firstName=this.json[i].TITULAIRE
pharma.additionalInfo=null
pharma.openingDetails=null
pharma.relationship=null


this.pharmaExcel[i]=pharma


          }
          console.log(this.pharmaExcel)

            this.pharamacyService.addMultiplePharmacy(this.pharmaExcel).subscribe(
              res=>{
                console.log('workin!')
                console.log(res)
                this.notificationService.showToast('Ajout des pharmacies avec succés');
           //  location.reload()
              },
              err=>{
                console.log(err)
              }
            )
           
                    }
        fileReader.readAsArrayBuffer(this.file);
                  }
  transmitOption(option){
    console.log(option)
this.userService.getoption(option)
  }
  /**
   * On destroy
   */
  ngOnDestroy(): void
  {
      // Unsubscribe from all subscriptions
      this._unsubscribeAll.next();
      this._unsubscribeAll.complete();
  }
  disableProprePharma(){
    this.ProprePharma=false
  }
  disableplusieursPharma(){
    this.plusieursPharma=false
  }
  submit(){
   // this.openingDetails[0].day=this.form.value.Day
    this.form.value.address=this.address
   // this.form.value.openingDetails=this.openingDetails
    //this.form.value.additionalInfo=this.additionalInfo
    this.form.value.Relationship=''
    console.log(this.storageService.read('id'))
    this.form.value.ownerId=this.storageService.read('id') //c bn 
console.log(this.form.invalid);

this.pharamacyService.submitPharmacy(this.form.value).subscribe(
  res=>{
    console.log('working!')
    console.log(res)
    this.notificationService.showToast('Ajout de la pharmacie avec succés');
   // location.reload()
  /* const link=['/app/list-pharmacy']
   this.Route.navigate(link)*/
  }
)
  }
  addForm(){
    this.show=true
this.link=new link()
this.dataarray.push(this.link)
}
  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------
  createItem(): FormGroup {
    return this.formBuilder.group({
      name: '',
      description: '',
      price: ''
    });
  }
  addItem(): void {
    this.items = this.orderForm.get('items') as FormArray;
    this.items.push(this.createItem());
  }

  /**
   * Finish the horizontal stepper
   */
  finishHorizontalStepper(): void
  {
      alert('You have finished the horizontal stepper!');
  }

  /**
   * Finish the vertical stepper
   */
  finishVerticalStepper(): void
  {
      alert('You have finished the vertical stepper!');
  }

  onFileSelected(event){
    //console.log(event)
    this.selectedFile=event.target.files[0]
    console.log(this.selectedFile)

  }
  submitPharmacy(formulaire:NgForm){
    console.log(formulaire.value)
    //this.userService.addPharmacy(formulaire.value)

  } 
  get f(){
    return this.liens.controls
  }
  get t(){
    return this.f.liens as FormArray
  }
  addLien(e){
    const numberofLinks = e.target.value || 0;
    if (this.t.length < numberofLinks) {
        for (let i = this.t.length; i < numberofLinks; i++) {
            this.t.push(this._formBuilder.group({
              socialmedialink : ['', Validators.required],
                email: ['', [Validators.required, Validators.email]]
            }));
        }
    } else {
        for (let i = this.t.length; i >= numberofLinks; i--) {
            this.t.removeAt(i);
        }
    }
 
  }
}
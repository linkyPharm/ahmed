import { Component, OnInit } from '@angular/core';
import { Pharmacy } from 'app/pages/shared/models/Pharmacy';
import { PharmacieService } from 'app/pages/shared/services/pharmacie.service';
import { HttpClient } from '@angular/common/http';
import { StorageService } from 'app/pages/shared/services/storage.service';
import { RoleService } from 'app/pages/shared/services/role.service';

@Component({
  selector: 'app-all-pharmacy',
  templateUrl: './all-pharmacy.component.html',
  styleUrls: ['./all-pharmacy.component.scss']
})
export class AllPharmacyComponent implements OnInit {
    userRolePriority;
pharmacies:Pharmacy[]
  constructor(
    private storageService:StorageService,
    private roleService:RoleService  ,
    private pharmacyService:PharmacieService,
    private http:HttpClient) { }
    displayedColumns: string[] = [
      'Name', 'owner','email', 'fax',
     'phone', 'website' , 'city', 'country'];
  
  ngOnInit() {
    let userToken ; 
    userToken = this.storageService.read('user');
   this.userRolePriority = this.roleService.getUserPriorityRole(userToken.role);
   console.log("hey");
   console.log(this.userRolePriority);

this.pharmacyService.getAllPharmacies().subscribe(
  res=>{
    console.log(res)
    this.pharmacies=res
  }
)
  }
  applyFilter(e){}

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestListPharmaComponent } from './test-list-pharma.component';

describe('TestListPharmaComponent', () => {
  let component: TestListPharmaComponent;
  let fixture: ComponentFixture<TestListPharmaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestListPharmaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestListPharmaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

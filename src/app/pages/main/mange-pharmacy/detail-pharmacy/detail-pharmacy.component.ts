import { Component, OnInit } from '@angular/core';
import { PharmacieService } from 'app/pages/shared/services/pharmacie.service';
import { Pharmacy } from 'app/pages/shared/models/Pharmacy';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-detail-pharmacy',
  templateUrl: './detail-pharmacy.component.html',
  styleUrls: ['./detail-pharmacy.component.scss']
})
export class DetailPharmacyComponent implements OnInit {
pharmacy:Pharmacy
bankCtrl=new FormControl()
bankFilterCtrl=new FormControl
displayedColumns: string[] = ['street','zip','Item'];
test=['1','2','3']
public variables = ['One','Two','County', 'Three', 'Zebra', 'XiOn'];
public filteredList1 = this.variables.slice();
  constructor(private pharmacyService:PharmacieService) { }

  ngOnInit() {
this.pharmacyService.getPharmacyById(0).subscribe(
  pharmacy=>{this.pharmacy=pharmacy
    console.log(pharmacy)}
)

  }

}

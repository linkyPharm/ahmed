import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailPharmacyComponent } from './detail-pharmacy.component';

describe('DetailPharmacyComponent', () => {
  let component: DetailPharmacyComponent;
  let fixture: ComponentFixture<DetailPharmacyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailPharmacyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailPharmacyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});


import {NgModule} from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
    MatButtonModule, MatDatepickerModule, MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule, MatSlideToggleModule, MatToolbarModule, MatTooltipModule, MatCardModule, MatDividerModule, MatTableModule, MatPaginatorModule, MatListModule
} from '@angular/material';
import { ColorPickerModule } from 'ngx-color-picker';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule } from '@fuse/components';


import { ListPharmacyComponent } from './list-pharmacy.component';
import { DetailPharmacyComponent } from '../detail-pharmacy/detail-pharmacy.component';
const routes: Routes = [
    {
        path     : '**',
        component: ListPharmacyComponent,
        children : [
        ]
       
    }
];

@NgModule({
    declarations   : [
ListPharmacyComponent,
//DetailPharmacyComponent
        
       
    ],
    imports        : [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatDatepickerModule,
        MatDialogModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatSlideToggleModule,
        MatToolbarModule,
        MatTooltipModule,
        MatPaginatorModule,
MatCardModule,
MatDividerModule,
MatTableModule,
MatListModule,
        
        ColorPickerModule,

        FuseSharedModule,
        FuseConfirmDialogModule
    ],
    providers      : [
        
    ],
    entryComponents: [
     
    ]
})
export class ListPharmacyModule
{
}

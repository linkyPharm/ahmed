import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { DataSource } from '@angular/cdk/table';
import { UserService } from 'app/pages/shared/services/user.service';

import { Personne } from '../../manage-post/model/personne';
import { Pharmacy } from 'app/pages/shared/models/Pharmacy';
import { StorageService } from 'app/pages/shared/services/storage.service';
import { PharmacieService } from 'app/pages/shared/services/pharmacie.service';
import {MatDialog} from '@angular/material'
//import { EditPharmacyComponent } from '../edit-pharmacy/edit-pharmacy.component';
import { DetailPharmacyComponent } from '../detail-pharmacy/detail-pharmacy.component';
import { RoleService } from 'app/pages/shared/services/role.service';
export interface UserData {
  id: string;
  name: string;
  progress: string;
  color: string;
}
export interface option{
  option:any
}
@Component({
  selector: 'app-list-pharmacy',
  templateUrl: './list-pharmacy.component.html',
  styleUrls: ['./list-pharmacy.component.css']
})
export class ListPharmacyComponent implements OnInit {
userRolePriority;
personnes:Personne[]
show:boolean=false
showpharma=false
pharmacies:Pharmacy[]
option:option
  displayedColumns: string[] = [
    'organizationName', 'owner','email', 'fax',
   'phone', 'website' , 'city',  'country', 'action'];

  dataSource: MatTableDataSource<Personne>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
      private roleService:RoleService,
    private router:Router,private userService:UserService,
    private ActivatedRoute:ActivatedRoute,
    private storageService:StorageService,
    private pharmacyService:PharmacieService,
    private dialog:MatDialog
   
    ) {
    // Create 100 users
    //const users = Array.from({length: 100}, (_, k) => createNewUser(k + 1));
    

    // Assign the data to the data source for the table to render

  }

  ngOnInit() {

      this.pharmacyService.getPharmaciesByOwnerId(this.storageService.read('id')).subscribe(
        (pharmacy)=>{
          this.pharmacies=pharmacy
          console.log(pharmacy)
          console.log(this.pharmacies)
          if (this.pharmacies==[]){
            console.log(true)
            this.showpharma=false
          }
          else{
            this.showpharma=true
          }
        }
      )
        
      
this.personnes= this.userService.getFakePersonne()

    this.dataSource = new MatTableDataSource(this.personnes);
    console.log(this.dataSource)
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  pharmacyDetails(id){
    const dialogRef = this.dialog.open(DetailPharmacyComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
   /* const link=[`/app/detail-pharmacy/${id}`]
    this.router.navigate(link)*/
  
  getoption(){
    console.log("hey")
    this.option=this.userService.returnOption()
  }
  editpharmacy(id,name){


    const link=[`/app/edit-pharmacy/${id}`]
    this.router.navigate(link)
    console.log(id)
    /*this.userService.updatePharmacy(this.pharmacy).subscribe(
      response=>{
        const link=[`/app/edit-pharmacy/${id}`]
    this.router.navigate(link)
      }
    )*/

  }
  deletepharmacy(id){
this.pharmacyService.deletePharmacy(id).subscribe(
  res=>{
    console.log(res) 
    console.log("in")
this.ngOnInit()
  }
)

  }
  

 
}


  
    




  


/** Builds and returns a new User. */
/*function createNewUser(id: number): UserData {
  const name =
      NAMES[Math.round(Math.random() * (NAMES.length - 1))] + ' ' +
      NAMES[Math.round(Math.random() * (NAMES.length - 1))].charAt(0) + '.';

  return {
    id: id.toString(),
    name: name,
    progress: Math.round(Math.random() * 100).toString(),
    color: COLORS[Math.round(Math.random() * (COLORS.length - 1))]
  };
}*/
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListPharmacyComponent } from './list-pharmacy.component';

describe('ListPharmacyComponent', () => {
  let component: ListPharmacyComponent;
  let fixture: ComponentFixture<ListPharmacyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListPharmacyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListPharmacyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

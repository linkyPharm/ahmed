import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Timepick2Component } from './timepick2.component';

describe('Timepick2Component', () => {
  let component: Timepick2Component;
  let fixture: ComponentFixture<Timepick2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Timepick2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Timepick2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';

@Component({
  selector: 'app-timepick',
  templateUrl: './timepick.component.html',
  styleUrls: ['./timepick.component.scss']
})
export class TimepickComponent implements OnInit {

    @Input() defaultOpeningTime;
   @Output() emittedTime = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }
  timePicked(time){
      console.log(time);

      this.emittedTime.emit(
          time
      );
  }

}

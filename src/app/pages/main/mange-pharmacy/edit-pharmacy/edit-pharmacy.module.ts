
import {NgModule, CUSTOM_ELEMENTS_SCHEMA, SchemaMetadata} from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
    MatButtonModule, MatDatepickerModule, MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule, MatSlideToggleModule, MatToolbarModule, MatTooltipModule, MatCardModule, MatDividerModule, MatTableModule, MatPaginatorModule, MatAutocompleteModule, MatSelectModule
} from '@angular/material';
import { ColorPickerModule } from 'ngx-color-picker';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule } from '@fuse/components';
import {TimepickComponent} from '../timepick/timepick.component';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import {TimepickCloseComponent} from '../timepick-close/timepick-close.component';
import { EditPharmacyComponent } from './edit-pharmacy.component';
const routes: Routes = [
    {
        path     : '**',
        component: EditPharmacyComponent,
        children : [
            
        ]
       
    }
];


@NgModule({
    declarations   : [
EditPharmacyComponent,
TimepickComponent,
TimepickCloseComponent
       
    ],
    imports        : [
        
        RouterModule.forChild(routes),
        NgxMaterialTimepickerModule,
MatAutocompleteModule,
MatSelectModule,
        MatButtonModule,
        MatDatepickerModule,
        MatDialogModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatSlideToggleModule,
        MatToolbarModule,
        MatTooltipModule,
        MatPaginatorModule,
MatCardModule,
MatDividerModule,
MatTableModule,
        
        ColorPickerModule,

        FuseSharedModule,
        FuseConfirmDialogModule
    ],
    providers      : [
        
    ],
    entryComponents: [
     
    ],

}

)
export class EditPharmacyModule
{
}

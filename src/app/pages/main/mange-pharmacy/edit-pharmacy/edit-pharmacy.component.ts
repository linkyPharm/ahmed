import {Component, OnInit, enableProdMode} from '@angular/core';
import {FormControl,FormGroup,FormBuilder, Validators, NgForm} from '@angular/forms';
import {Observable, Subject} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { Pharmacy } from 'app/pages/shared/models/Pharmacy';
import { UserService } from 'app/pages/shared/services/user.service';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { PharmacieService } from 'app/pages/shared/services/pharmacie.service';
import { openingDetails } from 'app/pages/shared/models/openingdetail';
import { additionalInfo } from 'app/pages/shared/models/additionalinfo';
import { List } from 'lodash';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import { StorageService } from 'app/pages/shared/services/storage.service';
import { NotificationService } from 'app/pages/shared/services/notification.service';
@Component({
  selector: 'app-edit-pharmacy',
  templateUrl: './edit-pharmacy.component.html',
  styleUrls: ['./edit-pharmacy.component.scss']
})
export class EditPharmacyComponent implements OnInit {
pharmacy:Pharmacy
  selectedFile:File
  form: FormGroup;
  id:number
  ownerId: number;
  myControl = new FormControl();
  options: string[] = ['One', 'Two', 'Three'];
show=[false,false]
openingEmittedTime;

 // openingDetails:List<openingDetails>=[{id:0,openingTime:'',closingTime:'',data:'',day:''}]
  //additionalInfo:List<additionalInfo>=[{label:'',value:''},{label:'',value:''},{label:'',value:''},{label:'',value:''}]
  openingDetails:ArrayLike<openingDetails>=[]
  additionalInfo:ArrayLike<additionalInfo>=[]
openingTime=[]
  filteredOptions: Observable<string[]>;
  filtered:Observable<any[]>
  // Horizontal Stepper
  horizontalStepperStep1: FormGroup;
  horizontalStepperStep2: FormGroup;
  horizontalStepperStep3: FormGroup;

  // Vertical Stepper
  verticalStepperStep1: FormGroup;
  verticalStepperStep2: FormGroup;
  verticalStepperStep3: FormGroup;

  // Private
  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   *
   * @param {FormBuilder} _formBuilder
   */
  constructor(
      private _formBuilder: FormBuilder,
      private userService:UserService,
      private ActivatedRoute:ActivatedRoute,
      private Route:Router,
      private pharacyService:PharmacieService,
      private localStorageService: StorageService,
      private notificationService:NotificationService
  )
  {
      // Set the private defaults
      this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void
  {

    this.ActivatedRoute.params.subscribe(params=>{
      this.pharacyService.getPharmacyById(params.id).subscribe(
        (pharmacy)=>{
      
          this.pharmacy=pharmacy
          console.log(this.pharmacy)
          /*if(this.pharmacy.additionalInfo[1].label!=""){
            this.show[0]=true
          }
          if(this.pharmacy.additionalInfo[2].label!=""){
            this.show[1]=true
          }*/
          this.openingDetails=pharmacy.openingDetails
          this.additionalInfo=pharmacy.additionalInfo
          console.log('additionalInof:',this.additionalInfo)
          console.log('openingDetails:',this.openingDetails)
          this.id=pharmacy.id;
          this.ownerId = this.localStorageService.read('id');
        }
      )
        })
    this.filteredOptions = this.myControl.valueChanges
    .pipe(
      startWith(''),
      map(value => this._filter(value))
    );
 
      // Reactive Form
     this.form = this._formBuilder.group({
         
      email : [''],
      timePickOpening:[''],
      fax  : [''],
      name : [''],
      phone : [''],
    website : [''],
      organizationId : [''],
      ownerId : [''],          
      relationship : [''],
      address : [{
        city:'',
        country:'',
        street:'',
        zip:''
      }
      
      ],
      openingDetails : [{
          openingTime:''
      }],
      additionalInfo:  [[]],
      city : [''],
      country : [''],
      street : [''],
      zip : [''],
      
      closingTime : [''],
      data : [[]],
      
      Day : [[]],
      label : [[]],
      value : [[]],
        });

      // Horizontal Stepper form steps
      this.horizontalStepperStep1 = this._formBuilder.group({
          firstName: ['', Validators.required],
          lastName : ['', Validators.required]
      });

      this.horizontalStepperStep2 = this._formBuilder.group({
          cin: ['', Validators.required]
      });

      this.horizontalStepperStep3 = this._formBuilder.group({
          job      : ['', Validators.required],
        
      });

      // Vertical Stepper form stepper
      this.verticalStepperStep1 = this._formBuilder.group({
          firstName: ['', Validators.required],
          lastName : ['', Validators.required]
      });

      this.verticalStepperStep2 = this._formBuilder.group({
          address: ['', Validators.required]
      });

      this.verticalStepperStep3 = this._formBuilder.group({
          city      : ['', Validators.required],
          state     : ['', Validators.required],
          postalCode: ['', [Validators.required, Validators.maxLength(5)]]
      });
  }
  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }


  /**
   * On destroy
   */
  ngOnDestroy(): void
  {
      // Unsubscribe from all subscriptions
      this._unsubscribeAll.next();
      this._unsubscribeAll.complete();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Finish the horizontal stepper
   */
  finishHorizontalStepper(): void
  {
      alert('You have finished the horizontal stepper!');
  }

  /**
   * Finish the vertical stepper
   */
  finishVerticalStepper(): void
  {
      alert('You have finished the vertical stepper!');
  }

  onFileSelected(event){
    //console.log(event)
    this.selectedFile=event.target.files[0]
    console.log(this.selectedFile)

  }

  receiveOpeningTime(event, detail){
    detail.openingTime = event;
}
receiveClosingTime(event, detail){
    detail.closingTime = event ;
}
  Submit(){
      console.log(this.form.invalid);
    //console.log(this.form.value)
   // this.additionalInfo[0].label=this.form.value.label
    //this.additionalInfo[0].value= this.form.value.value
    //this.form.value.additionalInfo[0]=this.additionalInfo[0]
    this.pharmacy.ownerId=this.id
    this.form.value.ownerId=this.id
    this.form.value.address.city=this.pharmacy.address.city
    this.form.value.address.street=this.pharmacy.address.street
    this.form.value.address.country=this.pharmacy.address.country
    this.form.value.address.zip=this.pharmacy.address.zip
    this.form.value.openingDetails=this.pharmacy.openingDetails
    console.log('pharmacie:', this.pharmacy)
    //console.log(this.form.value)

  //  this.openingDetails[0].openingTime=this.form.value.openingTime
    //this.openingDetails[0].closingTime= this.form.value.closingTime
    //this.openingDetails[0].day= this.form.value.Day
    //this.openingDetails[0].data= this.form.value.data
    //this.form.value.openingDetails[0]=this.openingDetails[0]
    /*this.userService.updatePharmacy(this.pharmacy).subscribe(
      reponse=>{
        const link=['/app/list-pharmacy']
        this.Route.navigate(link)
      }
    )*/
    this.ActivatedRoute.params.subscribe(params=>{
 
      this.pharacyService.updatePharmacy(params.id,this.pharmacy).subscribe(
        res=>{
          //console.log(this.pharmacy)
          console.log(res)
          //alert('modification avec succès')
          const link1=['/app/Mespharmacies']
          this.Route.navigate(link1)
          this.notificationService.showToast('Modification de la  pharmacie avec succés');
        }
        
      )
        })
  }


}

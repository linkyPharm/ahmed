//import { TestListComponent } from "./test-list.component";
import { NgModule } from "@angular/core";
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { MatButtonModule, MatChipsModule, MatFormFieldModule, MatIconModule, MatInputModule, MatPaginatorModule, MatRippleModule, MatSelectModule, MatSortModule, MatSnackBarModule, MatTableModule, MatTabsModule } from "@angular/material";
import { FuseWidgetModule } from "@fuse/components";
import { FuseSharedModule } from "@fuse/shared.module";
import { EcommerceProductsService } from "app/pages/shared/services/ecommerce.service";
import { Routes, RouterModule } from "@angular/router";
//import { TestListPharmaComponent } from "./test-list-pharma.component";
import { PharmaService } from "app/pages/shared/services/pharma.service";
import { MesPharmaciesComponent } from "./mes-pharmacies.component";
import { MesPharmaService } from "app/pages/shared/services/MesPharma.service";



@NgModule({
    declarations: [
        MesPharmaciesComponent,
    
    ],
    imports     : [
     
        RouterModule,
        MatButtonModule,
        MatChipsModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatPaginatorModule,
        MatRippleModule,
        MatSelectModule,
        MatSortModule,
        MatSnackBarModule,
        MatTableModule,
        MatTabsModule,

        NgxChartsModule,
      

        FuseSharedModule,
        FuseWidgetModule
    ],
    providers   : [
        
 MesPharmaService
    ]
})

export class MesPharmaModule{}
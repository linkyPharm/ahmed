import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MesPharmaciesComponent } from './mes-pharmacies.component';

describe('MesPharmaciesComponent', () => {
  let component: MesPharmaciesComponent;
  let fixture: ComponentFixture<MesPharmaciesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MesPharmaciesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MesPharmaciesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

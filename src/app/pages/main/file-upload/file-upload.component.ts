import { Component, OnInit, ElementRef, Input, HostListener, ViewChild } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor, FormGroup, FormBuilder, FormControl } from '@angular/forms';
//import { requiredFileType } from '../add-post/add-post.component';


@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: FileUploadComponent,
      multi: true
    }
  ]
})
export class FileUploadComponent implements ControlValueAccessor  {
  
  @Input() progress;
  onChange: Function;
  public file: File | null = null;
  form: FormGroup= this._formBuilder.group({
    myFile: new FormControl(null)
  })
  @ViewChild('coverFilesInput') imgType:ElementRef;
  size: any;
  height: number;
  width: number;
  
  @HostListener('change', ['$event.target.files']) emitFiles( event: FileList) {
    console.log('yoo')
    
    const file = event && event.item(0);

    this.onChange(file);
    this.file = file;
    let image:any = event.item(0);
    this.size = image.size;
    let fr = new FileReader();
    fr.onload = () => { // when file has loaded
     var img = new Image();
     img.onload = () => {
         this.width = img.width;
         this.height = img.height;
         console.log("width = "+this.width)
     };
   //  img.src = fr.result
  }

  

   
}

  constructor( private host: ElementRef<HTMLInputElement>, private _formBuilder:FormBuilder) {
  }

  writeValue( value: null ) {
    // clear file input
    this.host.nativeElement.value = '';
    this.file = null;
  }

  registerOnChange( fn: Function ) {
    this.onChange = fn;
  }

  registerOnTouched( fn: Function ) {
  }
  
  change(event){
   
    let image:any = event.target.files[0];
    this.size = image.size;
    let fr = new FileReader();
    fr.onload = () => { // when file has loaded
     var img = new Image();
     img.onload = () => {
         this.width = img.width;
         this.height = img.height;
         console.log("width = "+this.width)
     };
  }

  }
}

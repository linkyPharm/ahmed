/*//import { AddPostComponent } from "./add-post.component";
import {NgModule} from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
    MatButtonModule,MatCardModule, MatDatepickerModule, MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule, MatSlideToggleModule, MatToolbarModule, MatTooltipModule, MatSelectModule, MatStepperModule, MatDividerModule, MatCheckboxModule, MatAutocompleteModule
} from '@angular/material';
import { ColorPickerModule } from 'ngx-color-picker';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule } from '@fuse/components';
//import { FileUploadComponent } from "../file-upload/file-upload.component";
import {NgxSpinnerModule} from "ngx-spinner"
import { VisualiserPostComponent } from './visualiser-post.component';
const routes: Routes = [
    {
        path     : '**',
        component: VisualiserPostComponent,
        children : []
       
    }
];

@NgModule({
    declarations   : [
        VisualiserPostComponent,
       // FileUploadComponent
       
    ],
    imports        : [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatDatepickerModule,
        NgxSpinnerModule,
        MatDialogModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatSlideToggleModule,
        MatToolbarModule,
        MatTooltipModule,
        MatDividerModule,
        MatCardModule,
        MatInputModule,
        MatCheckboxModule,
        MatAutocompleteModule,

  
    
 
   
        MatSelectModule,
        MatStepperModule,

        ColorPickerModule,

        FuseSharedModule,
        FuseConfirmDialogModule
    ],
    providers      : [
        
    ],
    entryComponents: [
     
    ]
})
export class VisualiserModule
{
}*/
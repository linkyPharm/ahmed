import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualiserPostComponent } from './visualiser-post.component';

describe('VisualiserPostComponent', () => {
  let component: VisualiserPostComponent;
  let fixture: ComponentFixture<VisualiserPostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisualiserPostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualiserPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

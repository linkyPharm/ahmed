import { Component, OnInit } from '@angular/core';
import { saveAs } from 'file-saver'
import { ActivatedRoute } from '@angular/router';
import { historique } from 'app/pages/shared/models/historique';
import { ProduitService } from 'app/pages/shared/services/produits.service';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { HttpClient } from '@angular/common/http';
import { toFormData } from '../add-post/add-post.component';
import { StorageService } from 'app/pages/shared/services/storage.service';

@Component({
  selector: 'app-visualiser-post',
  templateUrl: './visualiser-post.component.html',
  styleUrls: ['./visualiser-post.component.scss']
})
export class VisualiserPostComponent implements OnInit {
  historique: historique = new historique(0, 0, [])
  form: FormGroup;
  arr=[{key:'',value:''},{key:'',value:''},{key:'',value:''},{key:'',value:''},{key:'',value:''},{key:'',value:''},{key:'',value:''},{key:'',value:''},{key:'',value:''},{key:'',value:''},{key:'',value:''},{key:'',value:''}]
  formHistory:historique=new historique(0,0,this.arr)
  constructor(private activatedRoute: ActivatedRoute, private produitService: ProduitService,
    private _formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private http:HttpClient,
    private storageService:StorageService
  ) { }
  templateResultatImage = [false, false, false, false, false]
  templateResultatSansImage = [false, false, false, false, false]
  ngOnInit() {

    this.form = this._formBuilder.group({
      /* company   : [
           {
               value   : 'Google',
               disabled: true
           }, Validators.required
       ],*/
      templateID: [''],
      month: [''],

      name: [''],
      type: [''],
      produit: [''],
      discount: [''],
      oldprice: [''],
      newprice: [''],
      prixlitre: [''],
      unitPrice: [''],
      doublePrice: [''],
      imgUrl: [''],
      myFile: new FormControl(null)
    })




    this.activatedRoute.params.subscribe(
      params => {
        this.produitService.getSingleHistorique(params.id).subscribe(
          (res) => {
            console.log(res)
            this.historique = res
            console.log(this.historique)
            if (this.historique.templateId == 11) {
              this.templateResultatImage[0] = true
              console.log("true in")
            }
            if (this.historique.templateId == 12) {
              this.templateResultatImage[1] = true
            }

            if (this.historique.templateId == 13) {
              this.templateResultatImage[2] = true
            }
            if (this.historique.templateId == 14) {
              this.templateResultatImage[3] = true
            }
            if (this.historique.templateId == 15) {
              this.templateResultatImage[4] = true
            }

            if (this.historique.templateId == 21) {
              this.templateResultatSansImage[0] = true
            }
            if (this.historique.templateId == 22) {
              this.templateResultatSansImage[1] = true
            }
            if (this.historique.templateId == 23) {
              this.templateResultatSansImage[2] = true
            }
            if (this.historique.templateId == 24) {
              this.templateResultatSansImage[3] = true
            }
            if (this.historique.templateId == 25) {
              this.templateResultatSansImage[4] = true
            }
          }
        )
      }
    )


  }

  submit() {
    this.storageService.write('fakeToken',12)
this.form.value.templateID=this.historique.templateId
this.form.value.imgUrl=this.historique.value[11].value 
    this.spinner.show();
    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hide();
    }, 5000);

    this.formHistory.userId=this.storageService.read('id')
  this.formHistory.templateId=this.form.value.templateID

for(var i=0;i<12;i++){
this.formHistory.value[i].key=Object.keys(this.form.value)[i]
this.formHistory.value[i].value=Object.values(this.form.value)[i]

}
console.log(this.formHistory)


 /* this.http.post('http://51.77.158.247:3000/pdf/convert', toFormData(this.form.value), {
    responseType:'blob'
 } )*/
 this.http.post('http://51.77.158.247:3000/pdf/convert', toFormData(this.form.value), {
    responseType:'blob'
 } ).subscribe((res) => {
  console.log(this.form.value)



    //this.storageService.write('fakeToken',55)
    const pdfblob=new Blob([res],{type:'application/pdf'})
        saveAs(pdfblob,`${Date.now()}.pdf`)
        console.log('working!')

   
this.produitService.addHistoriqueProduit(this.formHistory).subscribe(
  res=>{console.log(res)}
)
this.storageService.write('fakeToken',55)
  },(error)=>{
    console.log(error)
  })

  }


}


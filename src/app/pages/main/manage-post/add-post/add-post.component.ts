
import { Component, OnDestroy, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, NgForm, FormControl } from '@angular/forms';
import { Subject, Observable } from 'rxjs';
import {saveAs} from 'file-saver'
import { HttpClient, HttpParams, HttpRequest, HttpEvent, HttpEventType } from '@angular/common/http';
import * as FileSaver from "file-saver";
import { NgxSpinnerService } from 'ngx-spinner';
import { ProduitService } from 'app/pages/shared/services/produits.service';
import { produit } from 'app/pages/shared/models/Produits';
import { StorageService } from 'app/pages/shared/services/storage.service';
import { historique } from 'app/pages/shared/models/historique';
import { startWith, map } from 'rxjs/operators';
import {MatSelectFilterModule} from 'mat-select-filter'
import { RoleService } from 'app/pages/shared/services/role.service';
import {rolesConst} from '../../manage-role/RolesPriority';
export interface template {
    value: string;
    viewValue: string;
  }
  export interface User {
    name: string;
  }
  export function toFormData<T>( formValue: T ) {
    const formData = new FormData();
  
    for ( const key of Object.keys(formValue) ) {
      const value = formValue[key];
      formData.append(key, value);
    }
  
    return formData;
  }
  export function toJson<T>( formValue: T ) {
    const formData = new FormData();
  
    for ( const key of Object.keys(formValue) ) {
      const value = formValue[key];
      formData.append(key, value);
    }
   
  
    return formData;
  }
  
  export function requiredFileType( type: string ) {
    return function (control: FormControl) {
      const file = control.value;
      if ( file ) {
        const extension = file.name.split('.')[1].toLowerCase();
        if ( type.toLowerCase() !== extension.toLowerCase() ) {
          return {
            requiredFileType: true
          };
        }
        
        return null;
      }
  
      return null;
    };
    
  }
  
@Component({
  selector: 'app-add-post',
  templateUrl: './add-post.component.html',
  styleUrls: ['./add-post.component.scss']
})


export class AddPostComponent implements OnInit, OnDestroy

{
    role0 = rolesConst[0];
    role1 = rolesConst[1];
    role2 = rolesConst[2];
    role3 = rolesConst[3];
  unitaire=false 
  nonUnitaire=false
  firstFormGroup:FormGroup
  progress=0
    templateResultatImage = [false, false, false,false,false]
  templateResultatSansImage=[false, false, false,false,false,false,false,false,false,false,false,false,false]
  tempBoolPortraitSansImage=[false, false, false,false,false,false, false, false]
  tempBoolPortraitSansImageSnsPrixUnitaire=[false, false, false,false,false,false, false, false]
  tempBoolPaysageSansImage=[false, false, false,false,false,false, false, false]
  tempBoolPaysageSansImageSnsPrixUnitaire=[false, false, false,false,false,false, false, false]
  tempBoolPortraitAvecImageV=[false, false, false,false,false,false, false, false]
  tempBoolPortraitAvecImageVSnsPrixUnitaire=[false, false, false,false,false,false, false, false]
  tempBoolPortraitAvecImageH=[false, false, false,false,false,false, false, false]
  tempBoolPortraitAvecImageHSnsPrixUnitaire=[false, false, false,false,false,false, false, false]
  tempBoolPaysageAvecImageV=[false, false, false,false,false,false, false, false]
  tempBoolPaysageAvecImageVSnsPrixUnitaire=[false, false, false,false,false,false, false, false]
  tempBoolPaysageavecImageH=[false, false, false,false,false,false, false, false]
  tempBoolPaysageavecImageHSnsPrixUnitaire=[false, false, false,false,false,false, false, false]

  myControl = new FormControl();
  selections: string[] = new Array(1000)
  filteredOptions: Observable<produit[]>;
  check=true
  portrait=false 
  paysage=false 
  vertical=false
  horizental=false
  orientation
  fakeToken:any
  arr=[{key:'',value:''},{key:'',value:''},{key:'',value:''},{key:'',value:''},{key:'',value:''},{key:'',value:''},{key:'',value:''},{key:'',value:''},{key:'',value:''},{key:'',value:''},{key:'',value:''},{key:'',value:''}]
  formHistory:historique=new historique(0,0,this.arr)
Image=false
SansImage=false
validate=false
produit:produit=new produit('','','','','','','','','','',0,1)
valeur:any
produitForm=false
show=true
imglink=null
selectedFile:File
product:produit[]=[]
testValue:Array<{key:string,value:string}>=[]
Value:{key:string,value:any}={key:'',value:''}
public filteredList5
options: User[] = [
  {name: 'Mary'},
  {name: 'Shelley'},
  {name: 'Igor'}
];
validanot=false
@ViewChild('fileInput') fileInput: ElementRef;
@ViewChild('myImage') myImage: ElementRef;
@ViewChild('AccUserImg') AccUserImage;
AccUserImageFile:File
userRolePriority;
  form: FormGroup;

  // Horizontal Stepper
  horizontalStepperStep1: FormGroup;
  horizontalStepperStep2: FormGroup;
  horizontalStepperStep3: FormGroup;

  // Vertical Stepper
  verticalStepperStep1: FormGroup;
  verticalStepperStep2: FormGroup;
  verticalStepperStep3: FormGroup;
  

  // Private
  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   *
   * @param {FormBuilder} _formBuilder
   */
  constructor(
      private roleService:RoleService,
      private _formBuilder: FormBuilder,
      private fb:FormBuilder,
      private http:HttpClient,
     private spinner:NgxSpinnerService,
     private produitService:ProduitService,
     private storageService:StorageService
  )
  {

      // Set the private defaults
      this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void
  {
    let userToken ; 
    userToken = this.storageService.read('user');
     this.userRolePriority = this.roleService.getUserPriorityRole(userToken.role);
     this.roleService.changeAccountActivity(this.userRolePriority);
     if(this.storageService.read('currentRole') == undefined) {
        this.storageService.write('currentRole', this.userRolePriority);
    }
    this.userRolePriority = this.storageService.read('currentRole');
     this.storageService.write('configBool',false)
  this.storageService.write('historiqueBool',false)
 
      // Reactive Form
      this.form = this.createForm() /*this._formBuilder.group({
          company   : [
              {
                  value   : 'Google',
                  disabled: true
              }, Validators.required
          ],
          templateID:[''],
          month : [''],
         
          name  : [''],
          type  : [''],
         produit:[''],
         discount  : [''],
         oldprice: [''],
         newprice: [''],
         prixlitre: [''],
         unitPrice: [''],
         doublePrice:[''],
         imgUrl:[''],
         selling_points: this.fb.array([this.fb.group({point:''})]),
        myFile: new FormControl(null)



          
      });*/

      // Horizontal Stepper form steps
     /* this.horizontalStepperStep1 = this._formBuilder.group({
          firstName: ['', Validators.required],
          lastName : ['', Validators.required]
      });

      this.horizontalStepperStep2 = this._formBuilder.group({
          cin: ['', Validators.required]
      });

      this.horizontalStepperStep3 = this._formBuilder.group({
          job      : ['', Validators.required],
        
      });

      // Vertical Stepper form stepper
      this.verticalStepperStep1 = this._formBuilder.group({
          firstName: ['', Validators.required],
          lastName : ['', Validators.required]
      });

      this.verticalStepperStep2 = this._formBuilder.group({
          address: ['', Validators.required]
      });

      this.verticalStepperStep3 = this._formBuilder.group({
          city      : ['', Validators.required],
          state     : ['', Validators.required],
          postalCode: ['', [Validators.required, Validators.maxLength(5)]]
      });*/
      this.produitService.getProduits().subscribe(
        produits=>{
  
          this.product=produits
          console.log(produits)
          this.filteredList5=this.product.slice()
          for (var i=0;i<this.product.length;i++){
          this.selections[i]=this.product[i].nom
          }
         }
      )
      this.filteredOptions = this.myControl.valueChanges
      .pipe<produit[]>(
        startWith<string | User>(''),
        map(value => typeof value === 'string' ? value : value.nom),
        map(nom => nom ? this._filter(nom) : this.product.slice())
      );
  }
  
    private _filter(value: string): produit[] {
      const filterValue = value.toLowerCase();
  
      return this.product.filter(option => option.nom.toLowerCase().indexOf(filterValue) === 0);
      
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void
  {
      // Unsubscribe from all subscriptions
      this._unsubscribeAll.next();
      this._unsubscribeAll.complete();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Finish the horizontal stepper
   */
  finishHorizontalStepper(): void
  {
      alert('You have finished the horizontal stepper!');
  }

  /**
   * Finish the vertical stepper
   */
  finishVerticalStepper(): void
  {
      alert('You have finished the vertical stepper!');
  }
  templatesImage: template[] = [
    { value: '11', viewValue: 'VIT V PORTRAIT REDUCTION %' },
    { value: '12', viewValue: 'LOT_1' },
    { value: '13', viewValue: 'N+1' },
    { value: '14', viewValue: 'Nieme à 1euro' },
    { value: '15', viewValue: 'REDUC %' }
  ];
  templateSansImage:template[]=[
    { value: '21', viewValue: 'LOT_1' },
    { value: '22', viewValue: 'LOT_2' },
    { value: '23', viewValue: 'N+1' },
    { value: '24', viewValue: 'NEW PRICE' },
    { value: '25', viewValue: 'REDUC %'},
    { value: '26', viewValue: 'edv portrait unite'},
    { value: '27', viewValue: 'edv portrait unite barre'},
    { value: '28', viewValue: 'edv portrait lot'},
    { value: '210', viewValue:'edv paysage lot barre'},
    { value: '220', viewValue: 'edv paysage + sieurs pdts'},
    { value: '260', viewValue: 'edv paysage unite'},
    { value: '270', viewValue: 'edv paysage unite barre'},
    { value: '280', viewValue: 'edv paysage lot'},
  
  ]
  TemplatePaysageImageH:template[]=[
{value: 'paysage/avcimg/h/1', viewValue: 'PRIX A LUNITE' },
{value: 'paysage/avcimg/h/3', viewValue: 'PRIX DU LOT' },
{value: 'paysage/avcimg/h/5', viewValue: 'PLUSIEURS PRODUITS IDENTIQUES' },
{value: 'paysage/avcimg/h/6', viewValue: 'BON DE REDUCTION IMMEDIAT (BRI)' },
{value: 'paysage/avcimg/h/8', viewValue: 'N PRODUITS ACHETES + 1 GRATUIT' },
{value: 'paysage/avcimg/h/10', viewValue: 'REDUCTION EN %' }, //paysage == landscape
{value: 'paysage/avcimg/h/12', viewValue: 'REDUCTION EN €' },
{value: 'paysage/avcimg/h/14', viewValue: '1€ LE ENIEME PRODUIT ACHETE' }


  ]
  TemplatePaysageImageHSnsPrixUnitaire:template[]=[
    {value: 'paysage/avcimg/h/2', viewValue: 'PRIX A LUNITE' },
    {value: 'paysage/avcimg/h/4', viewValue: 'PRIX DU LOT' },

    {value: 'paysage/avcimg/h/7', viewValue: 'BON DE REDUCTION IMMEDIAT (BRI)' },
    {value: 'paysage/avcimg/h/9', viewValue: 'N PRODUITS ACHETES + 1 GRATUIT' },
    {value: 'paysage/avcimg/h/11', viewValue: 'REDUCTION EN %' }, //paysage == landscape
    {value: 'paysage/avcimg/h/13', viewValue: 'REDUCTION EN €' },
    {value: 'paysage/avcimg/h/15', viewValue: '1€ LE ENIEME PRODUIT ACHETE' }
    
    
      ]

  TemplatePaysageImageV:template[]=[
    {value: 'paysage/avcimg/v/1', viewValue: 'PRIX A LUNITE' },
    {value: 'paysage/avcimg/v/3', viewValue: 'PRIX DU LOT' },
    {value: 'paysage/avcimg/v/5', viewValue: 'PLUSIEURS PRODUITS IDENTIQUES' },
    {value: 'paysage/avcimg/v/6', viewValue: 'BON DE REDUCTION IMMEDIAT (BRI)' },
    {value: 'paysage/avcimg/v/8', viewValue: 'N PRODUITS ACHETES + 1 GRATUIT' },
    {value: 'paysage/avcimg/v/10', viewValue: 'REDUCTION EN %' },
    {value: 'paysage/avcimg/v/12', viewValue: 'REDUCTION EN €' },
    {value: 'paysage/avcimg/v/14', viewValue: '1€ LE ENIEME PRODUIT ACHETE' }
    
    
      ]

      TemplatePaysageImageVSnsPrixUnitaire:template[]=[
        {value: 'paysage/avcimg/v/2', viewValue: 'PRIX A LUNITE' },
        {value: 'paysage/avcimg/v/4', viewValue: 'PRIX DU LOT' },
       
        {value: 'paysage/avcimg/v/7', viewValue: 'BON DE REDUCTION IMMEDIAT (BRI)' },
        {value: 'paysage/avcimg/v/9', viewValue: 'N PRODUITS ACHETES + 1 GRATUIT' },
        {value: 'paysage/avcimg/v/11', viewValue: 'REDUCTION EN %' },
        {value: 'paysage/avcimg/v/13', viewValue: 'REDUCTION EN €' },
        {value: 'paysage/avcimg/v/15', viewValue: '1€ LE ENIEME PRODUIT ACHETE' }
        
        
          ]
      TemplatePaysageSansImage:template[]=[
        {value: 'paysage/snsimg/1', viewValue: 'PRIX A LUNITE' },
        {value: 'paysage/snsimg/3', viewValue: 'PRIX DU LOT' },
        {value: 'paysage/snsimg/5', viewValue: 'PLUSIEURS PRODUITS IDENTIQUES' },
        {value: 'paysage/snsimg/6', viewValue: 'BON DE REDUCTION IMMEDIAT (BRI)' },
        {value: 'paysage/snsimg/8', viewValue: 'N PRODUITS ACHETES + 1 GRATUIT' },
        {value: 'paysage/snsimg/10', viewValue: 'REDUCTION EN %' },
        {value: 'paysage/snsimg/12', viewValue: 'REDUCTION EN €' },
        {value: 'paysage/snsimg/14', viewValue: '1€ LE ENIEME PRODUIT ACHETE' }
        
        
          ]

          TemplatePaysageSansImageSnsPrixUnitair:template[]=[
            {value: 'paysage/snsimg/2', viewValue: 'PRIX A LUNITE' },
            {value: 'paysage/snsimg/4', viewValue: 'PRIX DU LOT' },
           
            {value: 'paysage/snsimg/7', viewValue: 'BON DE REDUCTION IMMEDIAT (BRI)' },
            {value: 'paysage/snsimg/9', viewValue: 'N PRODUITS ACHETES + 1 GRATUIT' },
            {value: 'paysage/snsimg/11', viewValue: 'REDUCTION EN %' },
            {value: 'paysage/snsimg/13', viewValue: 'REDUCTION EN €' },
            {value: 'paysage/snsimg/15', viewValue: '1€ LE ENIEME PRODUIT ACHETE' }
            
            
              ]
          TemplatePortraitSansImage:template[]=[
            {value: 'portrait/snsimg/1', viewValue: 'PRIX A LUNITE' },
            {value: 'portrait/snsimg/3', viewValue: 'PRIX DU LOT' },
            {value: 'portrait/snsimg/5', viewValue: 'PLUSIEURS PRODUITS IDENTIQUES' },
            {value: 'portrait/snsimg/6', viewValue: 'BON DE REDUCTION IMMEDIAT (BRI)' },
            {value: 'portrait/snsimg/8', viewValue: 'N PRODUITS ACHETES + 1 GRATUIT' },
            {value: 'portrait/snsimg/10', viewValue: 'REDUCTION EN %' },
            {value: 'portrait/snsimg/12', viewValue: 'REDUCTION EN €' },
            {value: 'portrait/snsimg/14', viewValue: '1€ LE ENIEME PRODUIT ACHETE' } ]


            TemplatePortraitSansImageSnsPrixUnitaire:template[]=[
              {value: 'portrait/snsimg/2', viewValue: 'PRIX A LUNITE' },
              {value: 'portrait/snsimg/4', viewValue: 'PRIX DU LOT' },
            
              {value: 'portrait/snsimg/7', viewValue: 'BON DE REDUCTION IMMEDIAT (BRI)' },
              {value: 'portrait/snsimg/9', viewValue: 'N PRODUITS ACHETES + 1 GRATUIT' },
              {value: 'portrait/snsimg/11', viewValue: 'REDUCTION EN %' },
              {value: 'portrait/snsimg/13', viewValue: 'REDUCTION EN €' },
              {value: 'portrait/snsimg/15', viewValue: '1€ LE ENIEME PRODUIT ACHETE' } ]


            TemplatePortraitAvecImageH:template[]=[
              {value: 'portrait/avcimg/h/2', viewValue: 'PRIX A LUNITE' },
              {value: 'portrait/avcimg/h/3', viewValue: 'PRIX DU LOT' },
              {value: 'portrait/avcimg/h/5', viewValue: 'PLUSIEURS PRODUITS IDENTIQUES' },
              {value: 'portrait/avcimg/h/6', viewValue: 'BON DE REDUCTION IMMEDIAT (BRI)' },
              {value: 'portrait/avcimg/h/8', viewValue: 'N PRODUITS ACHETES + 1 GRATUIT' },
              {value: 'portrait/avcimg/h/10', viewValue: 'REDUCTION EN %' },
              {value: 'portrait/avcimg/h/12', viewValue: 'REDUCTION EN €' },
              {value: 'portrait/avcimg/h/14', viewValue: '1€ LE ENIEME PRODUIT ACHETE' }
              
              
                ]

                TemplatePortraitAvecImageHSnsPrixUnitaire:template[]=[
                  {value: 'portrait/avcimg/h/1', viewValue: 'PRIX A LUNITE' },
                  {value: 'portrait/avcimg/h/4', viewValue: 'PRIX DU LOT' },
                 
                  {value: 'portrait/avcimg/h/7', viewValue: 'BON DE REDUCTION IMMEDIAT (BRI)' },
                  {value: 'portrait/avcimg/h/9', viewValue: 'N PRODUITS ACHETES + 1 GRATUIT' },
                  {value: 'portrait/avcimg/h/11', viewValue: 'REDUCTION EN %' },
                  {value: 'portrait/avcimg/h/13', viewValue: 'REDUCTION EN €' },
                  {value: 'portrait/avcimg/h/15', viewValue: '1€ LE ENIEME PRODUIT ACHETE' }
                  
                  
                    ]
                
            TemplatePortraitAvecImageV:template[]=[
              {value: 'portrait/avcimg/v/1', viewValue: 'PRIX A LUNITE' },
              {value: 'portrait/avcimg/v/3', viewValue: 'PRIX DU LOT' },
              {value: 'portrait/avcimg/v/5', viewValue: 'PLUSIEURS PRODUITS IDENTIQUES' },
              {value: 'portrait/avcimg/v/6', viewValue: 'BON DE REDUCTION IMMEDIAT (BRI)' },
              {value: 'portrait/avcimg/v/8', viewValue: 'N PRODUITS ACHETES + 1 GRATUIT' },
              {value: 'portrait/avcimg/v/10', viewValue: 'REDUCTION EN %' },
              {value: 'portrait/avcimg/v/12', viewValue: 'REDUCTION EN €' },
              {value: 'portrait/avcimg/v/14', viewValue: '1€ LE ENIEME PRODUIT ACHETE' }
              
              
                ]
                TemplatePortraitAvecImageVSnsPrixUnitaire:template[]=[
                  {value: 'portrait/avcimg/v/2', viewValue: 'PRIX A LUNITE' },
                  {value: 'portrait/avcimg/v/4', viewValue: 'PRIX DU LOT' },
              
                  {value: 'portrait/avcimg/v/7', viewValue: 'BON DE REDUCTION IMMEDIAT (BRI)' },
                  {value: 'portrait/avcimg/v/9', viewValue: 'N PRODUITS ACHETES + 1 GRATUIT' },
                  {value: 'portrait/avcimg/v/11', viewValue: 'REDUCTION EN %' },
                  {value: 'portrait/avcimg/v/13', viewValue: 'REDUCTION EN €' },
                  {value: 'portrait/avcimg/v/15', viewValue: '1€ LE ENIEME PRODUIT ACHETE' }
                  
                  
                    ]
                disablenonUnitaire(stepper){
                  this.nonUnitaire=false
                  stepper.next()
                }
                disableUnitaire(stepper){
                  this.unitaire=false
                  stepper.next()
                }
                disableVertical(){
                  this.vertical=false
                }
                disableHorizental(){
this.horizental=false
                }
                showTempPaysageAvecImageH(viewValue,value){
                  this.valeur=value
                  for (let i = 0; i < this.TemplatePaysageImageH.length; i++) {
                          
                    if (viewValue == this.TemplatePaysageImageH[i].viewValue) {
                      this.tempBoolPaysageavecImageH[i] = true
               
              
                    }
                    else this.tempBoolPaysageavecImageH[i] = false
                  }
                }
                showTempPaysageAvecImageHSnsPrixUnitaire(viewValue,value){
                  this.valeur=value
                  for (let i = 0; i < this.TemplatePaysageImageHSnsPrixUnitaire.length; i++) {
                          
                    if (viewValue == this.TemplatePaysageImageHSnsPrixUnitaire[i].viewValue) {
                      this.tempBoolPaysageavecImageHSnsPrixUnitaire[i] = true
               
              
                    }
                    else this.tempBoolPaysageavecImageHSnsPrixUnitaire[i] = false
                  }
                }

                //
                showTempPaysageAvecImageV(viewValue,value){
                  this.valeur=value
                  for (let i = 0; i < this.TemplatePaysageImageV.length; i++) {
                          
                    if (viewValue == this.TemplatePaysageImageV[i].viewValue) {
                      this.tempBoolPaysageAvecImageV[i] = true
               
              
                    }
                    else this.tempBoolPaysageAvecImageV[i] = false
                  }
                }
                showTempPaysageAvecImageVSnsPrixUnitaire(viewValue,value){
                  this.valeur=value
                  for (let i = 0; i < this.TemplatePaysageImageVSnsPrixUnitaire.length; i++) {
                          
                    if (viewValue == this.TemplatePaysageImageVSnsPrixUnitaire[i].viewValue) {
                      this.tempBoolPaysageAvecImageVSnsPrixUnitaire[i] = true
               
              
                    }
                    else this.tempBoolPaysageAvecImageVSnsPrixUnitaire[i] = false
                  }
                }
                //

                showTempPaysageSansImage(viewValue,value){
                  this.valeur=value
                  for (let i = 0; i < this.TemplatePaysageSansImage.length; i++) {
                          
                    if (viewValue == this.TemplatePaysageSansImage[i].viewValue) {
                      this.tempBoolPaysageSansImage[i] = true
               
              
                    }
                    else this.tempBoolPaysageSansImage[i] = false
                  }
                }
                showTempPaysageSansImageSnsPrixUnitaire(viewValue,value){
                  this.valeur=value
                  for (let i = 0; i < this.TemplatePaysageSansImageSnsPrixUnitair.length; i++) {
                          
                    if (viewValue == this.TemplatePaysageSansImageSnsPrixUnitair[i].viewValue) {
                      this.tempBoolPaysageSansImageSnsPrixUnitaire[i] = true
               
              
                    }
                    else this.tempBoolPaysageSansImageSnsPrixUnitaire[i] = false
                  }
                }
                //
                showTempPortraitSansImage(viewValue,value){
                  this.valeur=value
                  
                      for (let i = 0; i < this.TemplatePortraitSansImage.length; i++) {
                          
                        if (viewValue == this.TemplatePortraitSansImage[i].viewValue) {
                          this.tempBoolPortraitSansImage[i] = true
                   
                  
                        }
                        else this.tempBoolPortraitSansImage[i] = false
                      }
                }
                showTempPortraitSansImageSnsPrixUnitaire(viewValue,value){
                  this.valeur=value
                  
                      for (let i = 0; i < this.TemplatePortraitSansImageSnsPrixUnitaire.length; i++) {
                          
                        if (viewValue == this.TemplatePortraitSansImageSnsPrixUnitaire[i].viewValue) {
                          this.tempBoolPortraitSansImageSnsPrixUnitaire[i] = true
                   
                  
                        }
                        else this.tempBoolPortraitSansImageSnsPrixUnitaire[i] = false
                      }
                }
                //

                //
                showTempPortraitAvecImageV(viewValue,value){
                  this.valeur=value
                  for (let i = 0; i < this.TemplatePortraitAvecImageV.length; i++) {
                          
                    if (viewValue == this.TemplatePortraitAvecImageV[i].viewValue) {
                      this.tempBoolPortraitAvecImageV[i] = true
               
              
                    }
                    else this.tempBoolPortraitAvecImageV[i] = false
                  }
                }
                showTempPortraitAvecImageVSnsPrixUnitaire(viewValue,value){
                  this.valeur=value
                  for (let i = 0; i < this.TemplatePortraitAvecImageVSnsPrixUnitaire.length; i++) {
                          
                    if (viewValue == this.TemplatePortraitAvecImageVSnsPrixUnitaire[i].viewValue) {
                      this.tempBoolPortraitAvecImageVSnsPrixUnitaire[i] = true
               
              
                    }
                    else this.tempBoolPortraitAvecImageVSnsPrixUnitaire[i] = false
                  }
                }
                //
                
                //
                showTempPortraitAvecImageH(viewValue,value){
                  this.valeur=value
                  for (let i = 0; i < this.TemplatePortraitAvecImageH.length; i++) {
                          
                    if (viewValue == this.TemplatePortraitAvecImageH[i].viewValue) {
                      this.tempBoolPortraitAvecImageH[i] = true
               
              
                    }
                    else this.tempBoolPortraitAvecImageH[i] = false
                  }
                }
                showTempPortraitAvecImageHSnsPrixUnitaire(viewValue,value){
                  this.valeur=value
                  for (let i = 0; i < this.TemplatePortraitAvecImageHSnsPrixUnitaire.length; i++) {
                          
                    if (viewValue == this.TemplatePortraitAvecImageHSnsPrixUnitaire[i].viewValue) {
                      this.tempBoolPortraitAvecImageHSnsPrixUnitaire[i] = true
               
              
                    }
                    else this.tempBoolPortraitAvecImageHSnsPrixUnitaire[i] = false
                  }
                }
                
createForm():FormGroup{
return this._formBuilder.group({
  /* company   : [
       {
           value   : 'Google',
           disabled: true
       }, Validators.required
   ],*/
   templateID:[null],
   month : [null],
  
   name  : [null],
   type  : [null],
  produit:[null],
  discount  : [null],
  oldprice: [null],
  newprice: [null],
  prixlitre: [null],
  unitPrice: [null],
  doublePrice:[null],
  imgUrl:[null],
  orientation:[null],
 myFile: new FormControl(null)



   
});
}
disblePortrait(stepper){
  this.portrait=false
  this.orientation='landscape'
  stepper.next()
}
disablepaysage(stepper){
  this.paysage=false
  this.orientation='portrait'
  stepper.next()
}
  showproduit(nom,link){

    this.produitForm=true
   this.check=false
    this.imglink=link
 this.form.value.produit=true
 console.log(this.form.value.produit)
    console.log(link)
    console.log(nom)

    

let index
for (var i=0;i<this.product.length;i++){
   if (this.product[i].nom==nom){
     index=i
   }

}

console.log(index)

this.produit=this.product[index]
console.log(this.product[index])


  }
  produitnonexistant(){
    this.validate=false
  }
  disableSansImage(stepper){
    stepper.next()
    this.SansImage=false 
 
    for (let i = 0; i < this.templateResultatSansImage.length; i++){
      this.templateResultatSansImage[i]=false;
      
    }
  
  }
  disableImage(){
    this.Image=false
  
    for (let i=0;i<this.templateResultatImage.length;i++){
      this.templateResultatImage[i]=false
    }
  }
  produitexistant(){
this.validanot=false

  }
  showtempImage(temp,value) {
      
 this.valeur=value
console.log(this.valeur)
    for (let i = 0; i < this.templatesImage.length; i++) {
        
      if (temp == this.templatesImage[i].viewValue) {
        this.templateResultatImage[i] = true
 

      }
      else this.templateResultatImage[i] = false
    }
  }
  showtempSansImage(temp,value){
      
    this.valeur=value
    console.log(this.valeur)
    for (let i = 0; i < this.templateSansImage.length; i++) {
      if (temp == this.templateSansImage[i].viewValue) {
        this.templateResultatSansImage[i] = true
       

      }
      else this.templateResultatSansImage[i] = false
    }
  }

  onFileSelected(event) {
    
    let reader = new FileReader();
    const resultat=reader.result as string
    if(event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.form.get('myFile').setValue({
          filename: file.name, 
          filetype: file.type,
         
          value: resultat.split(',')[1]
        })
      };
    }
  }
  submitFile(formulaire:NgForm){
    const formModel=this.form.value
    formulaire.value.templateID=this.valeur
    //if(this.selectedFile){
   // formulaire.value.myFile=this.selectedFile}
   console.log(formModel)
  // console.log(formulaire.value.myFile)
   //const Image = this.AccUserImage.nativeElement;
   //if (Image.files && Image.files[0]) {
   // this.AccUserImageFile = Image.files[0];
  //}
  //const ImageFile: File = this.AccUserImageFile;
    //console.log(ImageFile);
     let headers=new Headers()
    const fd=new FormData()
//fd.append('myFile',this.selectedFile,this.selectedFile.name)
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
  
    headers.append('Origin','http://localhost:4200');
  
    const link='http://localhost:5000/pdf/convert'
   /* this.http.post('http://192.168.1.13:3000/pdf/convert',fd).subscribe(res=>{
      console.log(res)
     
    
      }, error=>console.log(error))*/

      /*this.http.post(link,formulaire.value,{responseType:'blob'}).subscribe((res)=>{
        const pdfblob=new Blob([res],{type:'application/pdf'})
        saveAs(pdfblob,`${Date.now()}.pdf`)
        console.log('working!')
    })*/
        
    /*this.http.post(link,formModel).subscribe(Response=>{


    
      FileSaver.saveAs(Response,`${Date.now()}.pdf`);
      console.log('envoi')
      
      //location.reload()
    },  error=>{
      console.log(error)
    })*/
    

}

submit(){
  this.storageService.write('fakeToken',12)
  this.spinner.show();
  setTimeout(() => {
    /** spinner ends after 5 seconds */
    this.spinner.hide();
  }, 5000);
  this.form.value.templateID=this.valeur
  this.form.value.orientation=this.orientation
  //this.form.value.templateID=
  this.form.value.imgUrl=!this.check ? this.imglink : ''
  console.log(this.form.value)
  this.formHistory.userId=this.storageService.read('id')
  this.formHistory.templateId=this.form.value.templateID
  console.log(this.form);
for(var i=0;i<12;i++){
this.formHistory.value[i].key=Object.keys(this.form.value)[i]
this.formHistory.value[i].value=Object.values(this.form.value)[i]

}
//console.log(this.form.value);
//console.log(this.formHistory.value)


 /* this.http.post('http://51.77.158.247:3000/pdf/convert', toFormData(this.form.value), {
    responseType:'blob'
 } )*/ 
 this.http.post('http://51.77.158.247:3000/pdf/convert', toFormData(this.form.value), {responseType:'blob'} )
 .subscribe((res) => {
  console.log(this.form.value)
console.log(res)


    //this.storageService.write('fakeToken',55)
    const pdfblob=new Blob([res],{type:'application/pdf'})
        saveAs(pdfblob,`${Date.now()}.pdf`)
        console.log('working!')

   
this.produitService.addHistoriqueProduit(this.formHistory).subscribe(
  res=>{console.log(res)}
)
this.storageService.write('fakeToken',55)
/*this.disableImage()
this.disableSansImage()
this.form=this.createForm()
console.log(this.form.value)*/
  },(error)=>{
    console.log(error)
  })

}
showerror(){
  console.log(this.form.invalid)
}

}


//import { TestListComponent } from "./test-list.component";
import { NgModule } from "@angular/core";
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { MatButtonModule, MatChipsModule, MatFormFieldModule, MatIconModule, MatInputModule, MatPaginatorModule, MatRippleModule, MatSelectModule, MatSortModule, MatSnackBarModule, MatTableModule, MatTabsModule } from "@angular/material";
import { FuseWidgetModule } from "@fuse/components";
import { FuseSharedModule } from "@fuse/shared.module";
import { EcommerceProductsService } from "app/pages/shared/services/ecommerce.service";
import { Routes, RouterModule } from "@angular/router";
import { HistoriqueAfficheComponent } from "./historique-affiche.component";
import { historiqueService } from "app/pages/shared/services/historique.service";



@NgModule({
    declarations: [
        HistoriqueAfficheComponent,
    
    ],
    imports     : [
     
        RouterModule,
        MatButtonModule,
        MatChipsModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatPaginatorModule,
        MatRippleModule,
        MatSelectModule,
        MatSortModule,
        MatSnackBarModule,
        MatTableModule,
        MatTabsModule,

        NgxChartsModule,
      

        FuseSharedModule,
        FuseWidgetModule
    ],
    providers   : [
        historiqueService,
 
    ]
})

export class HistoriqueModule{}
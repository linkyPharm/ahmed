import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoriqueAfficheComponent } from './historique-affiche.component';

describe('HistoriqueAfficheComponent', () => {
  let component: HistoriqueAfficheComponent;
  let fixture: ComponentFixture<HistoriqueAfficheComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoriqueAfficheComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoriqueAfficheComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

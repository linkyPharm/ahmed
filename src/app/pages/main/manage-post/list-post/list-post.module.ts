
import {NgModule} from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
    MatButtonModule, MatDatepickerModule, MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule, MatSlideToggleModule, MatToolbarModule, MatTooltipModule, MatCardModule, MatDividerModule, MatTableModule, MatPaginatorModule
} from '@angular/material';
import { ColorPickerModule } from 'ngx-color-picker';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule } from '@fuse/components';
import { ListPostComponent } from "./list-post.component";
import { EditPostComponent } from '../edit-post/edit-post.component';
const routes: Routes = [
    {
        path     : '**',
        component: ListPostComponent,
        children : [
        ]
       
    }
];

@NgModule({
    declarations   : [
  ListPostComponent
        
       
    ],
    imports        : [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatDatepickerModule,
        MatDialogModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatSlideToggleModule,
        MatToolbarModule,
        MatTooltipModule,
        MatPaginatorModule,
MatCardModule,
MatDividerModule,
MatTableModule,
        
        ColorPickerModule,

        FuseSharedModule,
        FuseConfirmDialogModule
    ],
    providers      : [
        
    ],
    entryComponents: [
     
    ]
})
export class ListPostModule
{
}

import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, NgForm } from '@angular/forms';
import { Subject } from 'rxjs';
import { UserData } from '../list-post/list-post.component';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { UserService } from 'app/pages/shared/services/user.service';
import { Personne } from '../model/personne';
import { StorageService } from 'app/pages/shared/services/storage.service';

@Component({
  selector: 'app-edit-post',
  templateUrl: './edit-post.component.html',
  styleUrls: ['./edit-post.component.scss']
})
export class EditPostComponent implements OnInit {
userdata:UserData
personnes:Personne[]
personne:Personne
params:any
  selectedFile:File
  form: FormGroup;

  // Horizontal Stepper
  horizontalStepperStep1: FormGroup;
  horizontalStepperStep2: FormGroup;
  horizontalStepperStep3: FormGroup;

  // Vertical Stepper
  verticalStepperStep1: FormGroup;
  verticalStepperStep2: FormGroup;
  verticalStepperStep3: FormGroup;

  // Private
  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   *
   * @param {FormBuilder} _formBuilder
   */
  constructor(
      private _formBuilder: FormBuilder,
      private ActivatedRoute:ActivatedRoute,
      private userService:UserService,
      private route:Router,private storageService:StorageService
  )
  {
      // Set the private defaults
      this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void
  {
    this.storageService.write('configBool',false)
    this.storageService.write('historiqueBool',false)
    this.ActivatedRoute.params.subscribe(params=>{
     this.params=params.id
      this.personnes=this.userService.getFakePersonneById(params.id)
      this.personne=this.personnes[0]

    })
    console.log(this.personne)
      // Reactive Form
      this.form = this._formBuilder.group({
          company   : [
              {
                  value   : 'Google',
                  disabled: true
              }, Validators.required
          ],
          firstName : ['' ],
          lastName  : [''],
          cin   : [''],
          job  : [''],
          
      });

      // Horizontal Stepper form steps
      this.horizontalStepperStep1 = this._formBuilder.group({
          firstName: ['', Validators.required],
          lastName : ['', Validators.required]
      });

      this.horizontalStepperStep2 = this._formBuilder.group({
          address: ['', Validators.required]
      });

      this.horizontalStepperStep3 = this._formBuilder.group({
          city      : ['', Validators.required],
          state     : ['', Validators.required],
          postalCode: ['', [Validators.required, Validators.maxLength(5)]]
      });

      // Vertical Stepper form stepper
      this.verticalStepperStep1 = this._formBuilder.group({
          firstName: ['', Validators.required],
          lastName : ['', Validators.required]
      });

      this.verticalStepperStep2 = this._formBuilder.group({
          address: ['', Validators.required]
      });

      this.verticalStepperStep3 = this._formBuilder.group({
          city      : ['', Validators.required],
          state     : ['', Validators.required],
          postalCode: ['', [Validators.required, Validators.maxLength(5)]]
      });
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void
  {
      // Unsubscribe from all subscriptions
      this._unsubscribeAll.next();
      this._unsubscribeAll.complete();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Finish the horizontal stepper
   */
  finishHorizontalStepper(): void
  {
      alert('You have finished the horizontal stepper!');
  }

  /**
   * Finish the vertical stepper
   */
  finishVerticalStepper(): void
  {
      alert('You have finished the vertical stepper!');
  }
  
  updateFile(formulaire){
    const link=['/app/list-post']
  this.userService.updateFakePersonne(formulaire.value,this.params)
  console.log(formulaire.value)
  this.route.navigate(link)
  
}

}


import {NgModule} from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
    MatButtonModule,MatCardModule, MatDatepickerModule, MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule, MatSlideToggleModule, MatToolbarModule, MatTooltipModule, MatSelectModule, MatStepperModule, MatDividerModule
} from '@angular/material';
import { ColorPickerModule } from 'ngx-color-picker';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule } from '@fuse/components';
import { EditPostComponent } from "./edit-post.component";

const routes: Routes = [
    {
        path     : '**',
        component: EditPostComponent,
        children : []
       
    }
];

@NgModule({
    declarations   : [
     EditPostComponent
        
       
    ],
    imports        : [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatDatepickerModule,
        MatDialogModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatSlideToggleModule,
        MatToolbarModule,
        MatTooltipModule,
        MatDividerModule,
        MatCardModule,

  
    
 
   
        MatSelectModule,
        MatStepperModule,

        ColorPickerModule,

        FuseSharedModule,
        FuseConfirmDialogModule
    ],
    providers      : [
        
    ],
    entryComponents: [
     
    ]
})
export class EditPostModule
{
}
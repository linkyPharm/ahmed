import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { historique } from 'app/pages/shared/models/historique';
import { StorageService } from 'app/pages/shared/services/storage.service';
import { ProduitService } from 'app/pages/shared/services/produits.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-post-history',
  templateUrl: './post-history.component.html',
  styleUrls: ['./post-history.component.scss']
})
export class PostHistoryComponent implements OnInit {
  displayedColumns: string[] = [
    'Template', 'Type', 'Nom Promotion','Nom', 'Description',
   'Produit', 'Image' , 'action'];
   dataSource: MatTableDataSource<historique>;

   @ViewChild(MatPaginator) paginator: MatPaginator;
   @ViewChild(MatSort) sort: MatSort;
 
  constructor(private storageService:StorageService,private produitService:ProduitService,private router:Router) { }
hisotrique:historique[]
  ngOnInit() {
this.produitService.getHistoriqueProduits(this.storageService.read('id')).subscribe(
  (res)=>{
    console.log('!!')
    console.log(res)
    this.hisotrique=res
  }
)
  }
  applyFilter(e){}
  visualiserAfficher(id){
    const link=[`app/visualiser-post/${id}`]
    this.router.navigate(link)
    
  }
}

import {NgModule, InjectionToken} from '@angular/core';
import {MainRoutingModule} from './main.routing';
import {MainComponent} from './main.component';
import {SharedModule} from '../shared/shared.module';
import {HTTP_INTERCEPTORS,HttpClientModule} from '@angular/common/http';
import {MatMomentDateModule} from '@angular/material-moment-adapter';
import {MatButtonModule, MatIconModule, MatOptionModule, MatSelectModule, MatCheckbox, MatCheckboxModule, MatCard, MatCardModule, MatInputModule, MatAutocompleteModule, MatTableModule, MatPaginator, MatPaginatorModule, MatFormFieldModule, MatListModule, MatDatepickerModule, MatDialogModule, MatSlideToggleModule, MatToolbarModule, MatTooltipModule, MatDividerModule, MatStepperModule, MatSortModule, MatTabsModule} from '@angular/material';
import {FuseProgressBarModule, FuseSidebarModule, FuseThemeOptionsModule, FuseConfirmDialogModule} from '../../../@fuse/components';
import {FuseSharedModule} from '../../../@fuse/shared.module';
import {LayoutModule} from '../../layout/layout.module';
import {SampleModule} from './sample/sample.module';
import {TranslateModule} from '@ngx-translate/core';
//import { AddPostModule } from './manage-post/add-post/add-post.module';
import { ListPostModule } from './manage-post/list-post/list-post.module';
import { EditPostModule } from './manage-post/edit-post/edit-post.module';
//import { DeletepostComponent } from './manage-post/deletepost/deletepost.component';
//import { NgxEditorModule } from 'ngx-editor';
//import { NgxEditorModule } from 'ngx-editor';
import { AddPharmacyComponent } from './mange-pharmacy/add-pharmacy/add-pharmacy.component';
import { ListPharmacyComponent } from './mange-pharmacy/list-pharmacy/list-pharmacy.component';
import { EditPharmacyComponent } from './mange-pharmacy/edit-pharmacy/edit-pharmacy.component';
import { AddPharmacyModule } from './mange-pharmacy/add-pharmacy/add-pharmacy.module';
import { ListPharmacyModule } from './mange-pharmacy/list-pharmacy/list-pharmacy.module';
import { EditPharmacyModule } from './mange-pharmacy/edit-pharmacy/edit-pharmacy.module';
//import { DeletePharmacyComponent } from './mange-pharmacy/delete-pharmacy/delete-pharmacy.component';
import {QuillModule, QuillEditorComponent} from 'ngx-quill'
import { FileSelectDirective } from 'ng2-file-upload';
//import { FileUploadComponent } from './manage-post/file-upload/file-upload.component';
import { TokenInterceptor } from '../shared/interceptors/token.interceptor';
import { AjouterProduitComponent } from './manage-produits/ajouter-produit/ajouter-produit.component';
import { ListProduitsComponent } from './manage-produits/list-produits/list-produits.component';
import { ListProduitsModule } from './manage-produits/list-produits/list-produits.module';
import { EditProduitComponent } from './manage-produits/edit-produit/edit-produit.component';
import { DeleteProduitComponent } from './manage-produits/delete-produit/delete-produit.component';
import { AllPharmacyComponent } from './mange-pharmacy/all-pharmacy/all-pharmacy.component';
import { ListAllPharmacyModule } from './mange-pharmacy/all-pharmacy/all-pharmacy.module';
import { DetailPharmacyComponent } from './mange-pharmacy/detail-pharmacy/detail-pharmacy.component';
import { PostHistoryComponent } from './manage-post/post-history/post-history.component';
import { PostHistoryModule } from './manage-post/post-history/post-history.module';
import { VisualiserPostComponent } from './manage-post/visualiser-post/visualiser-post.component';
//import { VisualiserModule } from './manage-post/visualiser-post/visualiser-post.module';
import { AddServiceComponent } from './manage-services/add-service/add-service.component';
import { ListeServicesComponent } from './manage-services/liste-services/liste-services.component';
import { UpdateServiceComponent } from './manage-services/update-service/update-service.component';
import { AddConseilComponent } from './manage-conseil/add-conseil/add-conseil.component';
import { ListConseilComponent } from './manage-conseil/list-conseil/list-conseil.component';
import { UpdateConseilComponent } from './manage-conseil/update-conseil/update-conseil.component';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { RouterModule } from '@angular/router';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ColorPickerModule } from 'ngx-color-picker';
import { AddPostComponent } from './manage-post/add-post/add-post.component';
import { TestListComponent } from './manage-produits/test-list/test-list.component';
import { EcommerceProductsService } from '../shared/services/ecommerce.service';
import { CommonModule } from '@angular/common';
import { EcommerceModule } from './manage-produits/test-list/test-list.module';
import { TestListPharmaComponent } from './mange-pharmacy/test-list-pharma/test-list-pharma.component';
import { PharmaModule } from './mange-pharmacy/test-list-pharma/test-list-pharma.module';
import { MesPharmaciesComponent } from './mange-pharmacy/mes-pharmacies/mes-pharmacies.component';
import { MesPharmaModule } from './mange-pharmacy/mes-pharmacies/mes-pharmacies.modules';
import { UsersModule} from './manage-role/list-of-users/list-of-users.module';
import { HistoriqueAfficheComponent } from './manage-post/historique-affiche/historique-affiche.component';
import { HistoriqueModule } from './manage-post/historique-affiche/historique-affiche.module';
import { MesServicesComponent } from './manage-services/mes-services/mes-services.component';
import { MesServicesModule } from './manage-services/mes-services/mes-services.module';
import { MaListComponent } from './manage-conseil/ma-list/ma-list.component';
import { MaListModule } from './manage-conseil/ma-list/ma-list.module';
import { FormsModule } from '@angular/forms';
//import { TestPostComponent } from './manage-post/test-post/test-post.component';
import { GenerateFlyerComponent } from './flyer/generate-flyer/generate-flyer.component';
import { GenerateFlyerModule } from './flyer/generate-flyer/generate-flyer.module';
import { DialogContentExampleDialogComponent } from './flyer/dialog-content-example-dialog/dialog-content-example-dialog.component';
//import { ListServicesModule } from './manage-services/liste-services/liste-services.modules';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { MatSelectFilterModule } from 'mat-select-filter';
import { TestFlyerComponent } from './flyer/test-flyer/test-flyer.component';
import { testFlyerModule } from './flyer/test-flyer/test-flyer.module';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { ConfigurerFlyerModule } from './flyer/configurer-leaflet/configurer-flyer.module';
import {ValidateProductsModule} from '../main/manage-produits/valid-produits/valid-produits.module';

import { TimepickComponent } from './mange-pharmacy/timepick/timepick.component';
import { Timepick2Component } from './mange-pharmacy/timepick2/timepick2.component';
import { TimepickCloseComponent } from './mange-pharmacy/timepick-close/timepick-close.component';
import { ListOfUsersComponent } from './manage-role/list-of-users/list-of-users.component';
import { DialogDataExampleDialogComponent } from './manage-role/dialog-data-example-dialog/dialog-data-example-dialog.component';
import { ErrorDialogComponent } from './manage-role/error-dialog/error-dialog.component';


import { DialogModule } from './flyer/dialog-content-example-dialog/dialog.module';
import { ConfigLeafletComponent } from './flyer/config-leaflet/config-leaflet.component';
import { ConfigLeafletModule } from './flyer/config-leaflet/config-leaflet.module';
import { ConfigurerLeafletComponent } from './flyer/configurer-leaflet/configurer-leaflet.component';
import { EditLeafletComponent } from './flyer/edit-leaflet/edit-leaflet.component';
import { EditLeafletModule } from './flyer/edit-leaflet/edit-leaflet.module';
import { HistoriqueLeafletModule } from './flyer/historique-leaflet/historique-leaflet.module';
import { EditConfigLeafletComponent } from './flyer/edit-config-leaflet/edit-config-leaflet.component';
import { AddGroupementComponent } from './manage-groupement/add-groupement/add-groupement.component';
import { EditGroupementComponent } from './manage-groupement/edit-groupement/edit-groupement.component';
import { AddSousGroupementComponent } from './manage-sousGroupement/add-sous-groupement/add-sous-groupement.component';
import { EditSousGroupementComponent } from './manage-sousGroupement/edit-sous-groupement/edit-sous-groupement.component';
import { NoActivatedComponent } from './manage-no-activated/no-activated/no-activated.component';

//import { ConfigurerLeafletComponent } from './generate-flyer/configurer-leaflet/configurer-leaflet.component';
export function getToken(): string {
    return localStorage.getItem('token');
}

@NgModule({
    
    declarations: [
        MainComponent,
FileUploadComponent,

       //DeletepostComponent,
      AjouterProduitComponent,
      EditProduitComponent,
      DeleteProduitComponent,
      AddServiceComponent,
      ListeServicesComponent,
      UpdateServiceComponent,
      AddConseilComponent,
      ListConseilComponent,
      UpdateConseilComponent,
      AddPostComponent,
      VisualiserPostComponent,
      //TestPostComponent,
      DetailPharmacyComponent,
      Timepick2Component,
      
      DialogDataExampleDialogComponent,
      
      ErrorDialogComponent,
      
      AddGroupementComponent,
      
      EditGroupementComponent,
      
      //ListSousGroupementComponent,
      
      AddSousGroupementComponent,
      
      EditSousGroupementComponent,
      
      NoActivatedComponent,
      
     // ManageGroupementComponent,
      
      //ListGroupementComponent,
      
     // EditConfigLeafletComponent,
      

     
     
//ConfigLeafletComponent,
    
      
     //ConfigurerLeafletComponent,
      //TestFlyerComponent,
   //   DialogContentExampleDialogComponent,
      //GenerateFlyerComponent,
     // MaListComponent,
      //MesServicesComponent,
     // HistoriqueAfficheComponent,
     // MesPharmaciesComponent,
      //TestListPharmaComponent

     // PostHistoryComponent,
      //DetailPharmacyComponent,
      //AllPharmacyComponent,
      // ListProduitsComponent,
        //DeletePharmacyComponent,
        //FileSelectDirective
    
        
     
    ],
    imports: [
        HttpClientModule,
       // NgxEditorModule,
//NgxEditorModule,
FormsModule,
MatTabsModule,
ConfigLeafletModule,
testFlyerModule,
ConfigurerFlyerModule,
EditLeafletModule,
        // Material moment date module
        MatMomentDateModule,
    MatOptionModule,
    MatSelectModule,
MatCheckboxModule,
MatCardModule,
MatPaginatorModule,
CommonModule,
MesPharmaModule,
HistoriqueModule,
DialogModule,
MesServicesModule,
MaListModule,
MatSelectFilterModule,
GenerateFlyerModule,
CKEditorModule,
HistoriqueLeafletModule,
//ListServicesModule,
        // Material
        MatButtonModule,
        MatIconModule,
        MatTableModule,
        // Fuse modules
        FuseProgressBarModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseThemeOptionsModule,
        // Routing
        MainRoutingModule,
      MatInputModule,
      EditLeafletModule,
        // App modules
        LayoutModule,
        SampleModule,
       // AddPostModule,
        EditPostModule,
        ListPostModule,
        EcommerceModule,
   AddPharmacyModule,
   ListPharmacyModule,
   EditPharmacyModule,
   ListProduitsModule,
   //ListAllPharmacyModule,
        TranslateModule,
        SharedModule,
        MatAutocompleteModule,
        MatFormFieldModule,
        MatInputModule,
        MatListModule,
        PostHistoryModule,

     //   VisualiserModule,
        //RouterModule.forChild(routes),

        MatButtonModule,
        MatDatepickerModule,
        NgxSpinnerModule,
        MatDialogModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatSlideToggleModule,
        MatToolbarModule,
        MatTooltipModule,
        MatDividerModule,
        MatCardModule,
        MatInputModule,
        MatCheckboxModule,
        MatAutocompleteModule,
PharmaModule,
MatSortModule,
  
NgxMatSelectSearchModule,
 
   
        MatSelectModule,
        MatStepperModule,

        ColorPickerModule,

        FuseSharedModule,
        FuseConfirmDialogModule,
        UsersModule,
        ValidateProductsModule
        
    ],
    
    providers:[
        {
            provide: HTTP_INTERCEPTORS,
            useClass: TokenInterceptor,
            multi: true
        }
       
    ]
})
export class MainModule {
}

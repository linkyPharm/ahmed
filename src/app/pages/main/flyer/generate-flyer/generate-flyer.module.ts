//import {MesServicesComponent} from "./mes-services.component"
import { NgModule } from "@angular/core";
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { MatButtonModule, MatChipsModule, MatFormFieldModule, MatIconModule, MatInputModule, MatPaginatorModule, MatRippleModule, MatSelectModule, MatSortModule, MatSnackBarModule, MatTableModule, MatTabsModule, MatStepperModule, MatDatepickerModule, MatAutocompleteModule, MatRadioModule } from "@angular/material";
import { FuseWidgetModule } from "@fuse/components";
import { FuseSharedModule } from "@fuse/shared.module";
//import { EcommerceProductsService } from "app/pages/shared/services/ecommerce.service";
import { Routes, RouterModule } from "@angular/router";
import { MesServices } from "app/pages/shared/services/mesServices.service";
import { GenerateFlyerComponent } from "./generate-flyer.component";
import { DialogContentExampleDialogComponent } from "../dialog-content-example-dialog/dialog-content-example-dialog.component";
import { DetailPharmacyComponent } from "../../mange-pharmacy/detail-pharmacy/detail-pharmacy.component";
import { MatSelectFilterModule } from "mat-select-filter";
import { FormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { ConfigurerLeafletComponent } from "../configurer-leaflet/configurer-leaflet.component";
import { NgxSpinnerModule } from "ngx-spinner";
import { EditConfigModule } from "../edit-config-leaflet/edit-config-leaflet.module";

@NgModule({
    declarations: [
        GenerateFlyerComponent,
 
        ConfigurerLeafletComponent,
   
    ],
    imports     : [
        NgxSpinnerModule,
        MatSelectFilterModule,
        RouterModule,
        MatButtonModule,
        MatChipsModule,
        MatFormFieldModule,
        EditConfigModule,
        MatIconModule,
        MatInputModule,
        MatPaginatorModule,
        MatRippleModule,
        MatSelectModule,
        MatSortModule,
        MatSnackBarModule,
        MatTableModule,
        MatTabsModule,
        MatAutocompleteModule,
MatStepperModule,
        NgxChartsModule,
        MatDatepickerModule,
      MatRadioModule,  
      

        FuseSharedModule,
        FuseWidgetModule
    ],
    providers   : [

 
    ],
    entryComponents: [DialogContentExampleDialogComponent],
})

export class GenerateFlyerModule{}
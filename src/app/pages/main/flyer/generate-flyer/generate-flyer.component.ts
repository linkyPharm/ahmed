import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray } from '@angular/forms';
import { PharmacieService } from 'app/pages/shared/services/pharmacie.service';
import { Pharmacy } from 'app/pages/shared/models/Pharmacy';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { User } from 'app/pages/shared/models/User';
import { MatTabChangeEvent, MatDialog } from '@angular/material';
import { produit } from 'app/pages/shared/models/Produits';
import { ProduitService } from 'app/pages/shared/services/produits.service';
import { DialogContentExampleDialogComponent } from '../dialog-content-example-dialog/dialog-content-example-dialog.component';
import { DetailPharmacyComponent } from '../../mange-pharmacy/detail-pharmacy/detail-pharmacy.component';
import { HttpClient } from '@angular/common/http';
import {saveAs} from 'file-saver'
import { StorageService } from 'app/pages/shared/services/storage.service';
import { toFormData} from '../../manage-post/add-post/add-post.component';
import { HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { flyerService } from 'app/pages/shared/services/flyer.service';
import { NgxSpinner } from 'ngx-spinner/lib/ngx-spinner.enum';
import { NgxSpinnerService } from 'ngx-spinner';
import { configurationsService } from 'app/pages/shared/services/configService.service';
import { NotificationService } from 'app/pages/shared/services/notification.service';

export interface User {
  name: string;
}
@Component({
  selector: 'app-generate-flyer',
  templateUrl: './generate-flyer.component.html',
  styleUrls: ['./generate-flyer.component.scss']
})


export class GenerateFlyerComponent implements OnInit {
  productForm: FormGroup;
  p1;p2;p3;p4;p5;p6;p7;p8;p9;p10;p11;p12
  nameConfig:string=''
  count:number=0
storageArray=[]
  selectedIndex:number=0
  myControl = new FormControl();
  newControl= new FormControl();
  pharmacies:any=[]
  categories=[2,4,8,12]
  public product =[]
  tab
  tableau=[]
selectedPage=[]
  selectionProduit=[]
  selectedPharmacie=[]
  selectedBeaute=[]
  selectedBebe=[]
  selectedBienEtre=[]
  selectedSante=[]
  
  jsonTest={imgConseil1:'',
    titreConseil1:'',
    soustitreConseil1:'',
    descriptionConseil1:'',}
  

  selectedMois
pagesJson={id:0,name:'',text_color:'',bg_color:'',type:0}
  jsonConfig={name:'',pageNumbers:0,organizationId:1,pages:[]}
mois=['JANVIER 2019','FEVRIER 2019','MARS 2019','AVRIL 2019','MAI 2019','JUIN 2019','JUILLET 2019',
'AOUT 2019','SEPTEMBRE 2019','OCTOBRE 2019','NOVEMBRE 2019','DECEMBRE 2019']
  public variables = ['One','Two','County', 'Three', 'Zebra', 'XiOn'];
  public variables2 = [{ id: 0, name: 'One' }, { id: 1, name: 'Two' }];
  pages=[{name:'SELECTION A LA UNE',id:1},{name:'NOUVEAUTES',id:2} ,{name:'PUBLICITE',id:4},{name:'CONSEIL',id:3}, {name:'SELECTION BEAUTE',id:6}, {name:'SELECTION BEBE',id:7},{name:'SELECTION BIEN ETRE',id:8},{name:'SERVICE',id:5},
{name:'SELECTION SANTE',id:9}

]
  public filteredList5
  public filteredList1 
  choosenCategorie=1
  constructor( private _formBuilder: FormBuilder,private pharmacieService:PharmacieService,private produitService:ProduitService,
    
    public dialog:MatDialog,public http:HttpClient, public fb:FormBuilder, private storageService:StorageService,
    public router:Router,
    private flyerService:flyerService,
    private spinner:NgxSpinnerService,
    private configService:configurationsService,
    private notificationService:NotificationService
    
    ) { }
  filteredOptions:Observable<produit[]>
  filteredOptionss:Observable<produit[]>
  ngOnInit() {
    this.storageService.write('configBool',false)
    this.storageService.write('historiqueBool',false)

this.storageArray=this.storageService.read('configurations')

    this.productForm = this.createProductForm();

this.pharmacieService.getAllPharmacies().subscribe(
  res=>{
    console.log(res)
    this.pharmacies=res
    console.log(this.pharmacies)
    this.filteredList1=this.pharmacies.slice()
  }
)
this.produitService.getProduits().subscribe(
  produits=>{

    this.product=produits
    console.log(produits)
  this.filteredList5= this.product.slice();

   }
)
this.filteredOptions=this.myControl.valueChanges
.pipe<produit[]>(
  startWith<string | User>(''),
  map(value => typeof value === 'string' ? value : value.name),
  map(nom => nom ? this._filter(nom) : this.product.slice())
  
);

this.filteredOptionss=this.newControl.valueChanges
.pipe<produit[]>(
  startWith<string | User>(''),
  map(value => typeof value === 'string' ? value : value.name),
  map(nom => nom ? this._filter(nom) : this.product.slice())
);
  }

  private _filter(value: string): produit[] {
    const filterValue = value.toLowerCase();

    return this.product.filter(option => option.nom.toLowerCase().indexOf(filterValue) === 0);

  }

  private _filter1(value: string): Pharmacy[] {
    const filterValue = value.toLowerCase();

    return this.pharmacies.filter(option => option.name.toLowerCase().indexOf(filterValue) === 0);

  }
 
  createProductForm(): FormGroup
  {
      return this._formBuilder.group({
          
   
       nameConfig:[''],
       pageContent:[''],
       nbrElements1:[0],
       nbrElements2:[0],
       pharmacie:[[]],
       pagesOrdre:[[]],
       nbrPages:[null],
       month:[''],
       p1:[null],
       p2:[null],
       p3:[null],
       p4:[null],
       p5:[null],
       p6:[null],
       p7:[null],
       p8:[null],
       p9:[null],
       p10:[null],
       p11:[null],
       p12:[null],
       mois:[''],
     
       selling_points: this.fb.array([this.fb.group({point:''})]),
        events: this.fb.array([])

      
      });
  }
  tabChanged(tabChangeEvent: MatTabChangeEvent): void {
    this.selectedIndex = tabChangeEvent.index;
    console.log(tabChangeEvent.index)
}

selectPages(page,i,id){
  this.pagesJson={
    id:0,
    name :"",
    type:0,
    text_color:"",
    bg_color:""
  }
if(i==0){
  this.p1=id

  this.pagesJson.name=page
  this.pagesJson.type=id
  this.pagesJson.id=i
  this.pagesJson.text_color='#123456'
  this.pagesJson.bg_color='#lzkjer'
  this.jsonConfig.pages[i]=this.pagesJson
  this.selectedPage[i]=page
}
if(i==1){

  this.pagesJson.name=page
  this.pagesJson.type=id
  this.pagesJson.id=i
  this.pagesJson.text_color='#123'
  this.pagesJson.bg_color='#lzkjer'
  this.jsonConfig.pages[i]=this.pagesJson
  this.selectedPage[i]=page
} 
if(i==2){

  this.pagesJson.name=page
  this.pagesJson.type=id
  this.pagesJson.id=i
  this.pagesJson.text_color='#123456'
  this.pagesJson.bg_color='#lzkjer'
  this.jsonConfig.pages[i]=this.pagesJson
}
if(i==3){

  this.pagesJson.name=page
  this.pagesJson.type=id
  this.pagesJson.id=i
  this.pagesJson.text_color='#123456'
  this.pagesJson.bg_color='#lzkjer'
   this.jsonConfig.pages[i]=this.pagesJson
}
if(i==4){
  this.p5=id

  this.pagesJson.name=page
  this.pagesJson.type=id
  this.pagesJson.id=i
  this.pagesJson.text_color='#123456'
  this.pagesJson.bg_color='#lzkjer'
   this.jsonConfig.pages[i]=this.pagesJson
}
if(i==5){
  this.p6=id
  this.pagesJson.name=page
  this.pagesJson.type=id
  this.pagesJson.id=i
  this.pagesJson.text_color='#123456'
  this.pagesJson.bg_color='#lzkjer'
   this.jsonConfig.pages[i]=this.pagesJson
}
if(i==6){
  this.p7=id
  this.pagesJson.name=page
  this.pagesJson.type=id
  this.pagesJson.id=i
  this.pagesJson.text_color='#123456'
  this.pagesJson.bg_color='#lzkjer'
   this.jsonConfig.pages[i]=this.pagesJson
}
if(i==7){
  this.p8=id
  this.pagesJson.name=page
  this.pagesJson.type=id
  this.pagesJson.id=i
  this.pagesJson.text_color='#123456'
  this.pagesJson.bg_color='#lzkjer'
   this.jsonConfig.pages[i]=this.pagesJson
}
if(i==8){
  this.p9=id
  this.pagesJson.name=page
  this.pagesJson.type=id
  this.pagesJson.id=i
  this.pagesJson.text_color='#123456'
  this.pagesJson.bg_color='#lzkjer'
   this.jsonConfig.pages[i]=this.pagesJson
}
if(i==9){
  this.p10=id
  this.pagesJson.name=page
  this.pagesJson.type=id
  this.pagesJson.id=i
  this.pagesJson.text_color='#123456'
  this.pagesJson.bg_color='#lzkjer'
   this.jsonConfig.pages[i]=this.pagesJson
}
if(i==10){
  this.p11=id
  this.pagesJson.name=page
  this.pagesJson.type=id
  this.pagesJson.id=i
  this.pagesJson.text_color='#123456'
  this.pagesJson.bg_color='#lzkjer'
   this.jsonConfig.pages[i]=this.pagesJson
}
if(i==11){
  this.p12=id
  this.pagesJson.name=page
  this.pagesJson.type=id
  this.pagesJson.id=i
  this.pagesJson.text_color='#123456'
  this.pagesJson.bg_color='#lzkjer'
   this.jsonConfig.pages[i]=this.pagesJson
}
  

/*if (index>=0) {
  this.count++
   console.log(this.count)
  
  //this.SelectedPage= this.SelectedPage.slice(index,index+1)
}
 
else{
 
  this.count--
  console.log(this.count)
}*/


console.log(this.selectedPage)
console.log(this.jsonConfig.pages)


}
configChange(input){
  this.nameConfig=input.value
}
Generate(){

/*this.storageArray.push({'Name':this.nameConfig,'selected_pages':this.selectedPage,'nbrPages':this.choosenCategorie,'date':Date.now(),'selectedPages':[this.p1,this.p2,this.p3,this.p4,this.p5,this.p6,this.p7,this.p8,this.p9,this.p10,this.p11,this.p12],'Pharmacie':this.selectedPharmacie,'Mois':this.selectedMois})
console.log(this.storageArray)
  this.storageService.write('configurations',this.storageArray)
  this.storageService.write('selected_page',this.selectedPage)*/

  this.storageService.write('nbrPages',this.choosenCategorie)
this.jsonConfig.name=this.nameConfig
this.jsonConfig.pageNumbers=this.choosenCategorie
this.configService.addConfiguration(this.jsonConfig).subscribe(
  res=>{
    console.log(this.jsonConfig)
    console.log('working')
  }
)
 const link=['/app/configurer-flyer']
  this.router.navigate(link)
  this.notificationService.showToast('Configuration faite avec succés');
    }
public nextStep() {
  /*if (this.choosenCategorie==2){
   // this.tab=[{name:'SELECTION A LA UNE'},{name:'PUBLICITE'}]
    this.tab=this.productForm.value.pagesOrdre
    console.log( this.tab)
   
  }
  if (this.choosenCategorie==4){
    this.tab=this.productForm.value.pagesOrdre
    //this.tab=[{name:'SELECTION A LA UNE'},{name:'NOUVEAUTES'},{name:'PUBLICITE'},{name:'CONSEIL'}]
  }

  if (this.choosenCategorie==8){
    this.tab=[{name:'SELECTION A LA UNE'},{name:'NOUVEAUTES'},{name:'PUBLICITE'},{name:'SELECTION DU MOIS'},{name:'PUBLICITE'},{name:'SERVICE'},{name:'PUBLICITE'},{name:'CONSEIL'}]
  }
  if (this.choosenCategorie==12){
    this.tab=[{name:'SELECTION A LA UNE'},{name:'SERVICE'},{name:'NOUVEAUTES'},{name:'SELECTION BEAUTE'},{name:'PUBLICITE'},{name:'SELECTION BEBE'},{name:'PUBLICITE'},{name:'SELECTION BIEN ETRE'},{name:'PUBLICITE'},{name:'SELECTION SANTE'},{name:'PUBLICITE'},{name:'CONSEIL'}]
  }*/

  this.tab=this.productForm.value.pagesOrdre
  console.log( this.tab)

    this.selectedIndex += 1;
}
get sellingPoints() {
  return this.productForm.get('selling_points') as FormArray;
}
addSellingPoint() {
  this.sellingPoints.push(this.fb.group({point:''}));
}
deleteSellingPoint(index) {
  this.sellingPoints.removeAt(index);
}

get event() {
  return this.productForm.get('events') as FormArray;
}
addEvent() {
  this.event.push(this.fb.group({event:''}));
  this.count++
  console.log(this.count)
}
deleteEvent(index) {
  this.event.removeAt(index);
}

public previousStep() {
    this.selectedIndex -= 1;
}
openDialog(){
  const dialogRef= this.dialog.open(DialogContentExampleDialogComponent)
}
submit(){
  
  console.log('Submit')
  console.log(this.productForm.value)
  
}
selectPharmacie(name){
  this.selectedPharmacie.push(name)
  console.log( this.selectedPharmacie)
}
chooseCategories(categorie){
this.tableau=[]
this.choosenCategorie=categorie
console.log(this.choosenCategorie)
for (let i=0;i<categorie;i++){
  this.tableau.push(i)
}
console.log(this.tableau)

}

choosenPharmacy(pharmacie){
this.productForm.value.pharmacie=pharmacie
}
selectBebe(name){
this.selectedBebe.push(name)
console.log(this.selectedBebe)
}
selectionAlaUne(name){
  console.log('in')
this.productForm.value.selectionAlaUne=name
console.log(this.productForm.value.selectionAlaUne)
}
selectMois(name){
this.selectedMois=name
console.log(this.selectedMois)
}
selectBienEtre(name){
this.selectedBienEtre.push(name)
console.log(this.selectedBienEtre)
}
selectBeaute(name){
  this.selectedBeaute.push(name)
  console.log( this.selectedBeaute)
}
selectSante(name){
this.selectedSante.push(name)
console.log(this.selectedSante)
}

onSubmit(){
  this.storageService.write('fakeToken',12)
this.productForm.value.selectionAlaUne=this.selectionProduit
this.productForm.value.pharmacie= this.selectedPharmacie
this.productForm.value.Beaute= this.selectedBeaute
this.productForm.value.Bebe=this.selectedBebe
this.productForm.value.BienEtre=this.selectedBienEtre
this.productForm.value.Sante=this.selectedSante
this.productForm.value.Mois=this.selectedMois
this.productForm.value.pagesOrdre=this.selectedPage
console.log('Submit')
console.log(this.productForm.value)
//let header = new HttpHeaders();
//header.set('Access-Control-Allow-Origin', '*')
let header = new HttpHeaders({'Access-Control-Allow-Origin': '*'});
/*this.productForm.value.prixproduit11='4'
this.productForm.value.ancienprixproduit11='9'
this.productForm.value.nomproduit11='panadol'
this.productForm.value.uniteproduit11='30ml'
this.productForm.value.gammeproduit11='haute'
this.productForm.value.imgproduit11='https://cdn.shopify.com/s/files/1/0035/4523/5569/products/Panadol_Extra_Solubles_1000x.jpg?v=1556550456'

this.productForm.value.prixproduit12='4'
this.productForm.value.ancienprixproduit12='9'
this.productForm.value.nomproduit12='panadol'
this.productForm.value.uniteproduit12='50ml'
this.productForm.value.gammeproduit12='haute'
this.productForm.value.imgproduit12='https://cdn.shopify.com/s/files/1/0035/4523/5569/products/Panadol_Extra_Solubles_1000x.jpg?v=1556550456'

this.productForm.value.prixproduit13='5'
this.productForm.value.ancienprixproduit13='10'
this.productForm.value.nomproduit13='panadol'
this.productForm.value.uniteproduit13='30ml'
this.productForm.value.gammeproduit13='haute'
this.productForm.value.imgproduit13='https://cdn.shopify.com/s/files/1/0035/4523/5569/products/Panadol_Extra_Solubles_1000x.jpg?v=1556550456'

this.productForm.value.prixproduit14='3'
this.productForm.value.ancienprixproduit14='8'
this.productForm.value.nomproduit14='panadol'
this.productForm.value.uniteproduit14='30ml'
this.productForm.value.gammeproduit14='haute'
this.productForm.value.imgproduit14='https://cdn.shopify.com/s/files/1/0035/4523/5569/products/Panadol_Extra_Solubles_1000x.jpg?v=1556550456'

this.productForm.value.nbrElements1=28
this.productForm.value.nbrElements2=2
this.productForm.value.titreService11="titre11"
this.productForm.value.soustitreService11="soustitre11"
this.productForm.value.descriptionService11="description11"
this.productForm.value.imgService11="https://cdn.shopify.com/s/files/1/0035/4523/5569/products/Panadol_Extra_Solubles_1000x.jpg?v=1556550456 "

this.productForm.value.titreService12="titre12"
this.productForm.value.soustitreService12="soustitre12"
this.productForm.value.descriptionService12="description12"
this.productForm.value.imgService12="https://cdn.shopify.com/s/files/1/0035/4523/5569/products/Panadol_Extra_Solubles_1000x.jpg?v=1556550456 "

this.productForm.value.titreService13="titre13"
this.productForm.value.soustitreService13="soustitre13"
this.productForm.value.descriptionService13="description13"
this.productForm.value.imgService13="https://cdn.shopify.com/s/files/1/0035/4523/5569/products/Panadol_Extra_Solubles_1000x.jpg?v=1556550456 "
this.productForm.value.imgPub1="https://cdn.shopify.com/s/files/1/0035/4523/5569/products/Panadol_Extra_Solubles_1000x.jpg?v=1556550456"

this.productForm.value.titreNew11='titre1'
this.productForm.value.soustitreNew11='titre1'
this.productForm.value.descriptionNew11='titre1'
this.productForm.value.imgNew11='https://cdn.shopify.com/s/files/1/0035/4523/5569/products/Panadol_Extra_Solubles_1000x.jpg?v=1556550456'

this.productForm.value.titreNew12='titre3'
this.productForm.value.soustitreNew12='titre3'
this.productForm.value.descriptionNew12='titre3'
this.productForm.value.imgNew12='https://cdn.shopify.com/s/files/1/0035/4523/5569/products/Panadol_Extra_Solubles_1000x.jpg?v=1556550456'


this.productForm.value.titreNew13='titre3'
this.productForm.value.soustitreNew13='titre3'
this.productForm.value.descriptionNew13='titre3'
this.productForm.value.imgNew13='https://cdn.shopify.com/s/files/1/0035/4523/5569/products/Panadol_Extra_Solubles_1000x.jpg?v=1556550456'

this.productForm.value.prixSante11='4'
this.productForm.value.ancienPrixSante11='6'
this.productForm.value.nomSante11='Ns'
this.productForm.value.uniteSante11='30ml'
this.productForm.value.gammeSante11='haute'
this.productForm.value.imgSante11='https://cdn.shopify.com/s/files/1/0035/4523/5569/products/Panadol_Extra_Solubles_1000x.jpg?v=1556550456'

this.productForm.value.prixSante12='4'
this.productForm.value.ancienPrixSante12='9'
this.productForm.value.nomSante12='Ns'
this.productForm.value.uniteSante12='30ml'
this.productForm.value.gammeSante12='haute'
this.productForm.value.imgSante12='https://cdn.shopify.com/s/files/1/0035/4523/5569/products/Panadol_Extra_Solubles_1000x.jpg?v=1556550456'

this.productForm.value.prixSante13='4'
this.productForm.value.ancienPrixSante13='6'
this.productForm.value.nomSante13='Nss'
this.productForm.value.uniteSante13='30ml'
this.productForm.value.gammeSante13='haute'
this.productForm.value.imgSante13='https://cdn.shopify.com/s/files/1/0035/4523/5569/products/Panadol_Extra_Solubles_1000x.jpg?v=1556550456'

this.productForm.value.prixSante14='4'
this.productForm.value.ancienPrixSante14='8'
this.productForm.value.nomSante14='Ns'
this.productForm.value.uniteSante14='30ml'
this.productForm.value.gammeSante14='haute'
this.productForm.value.imgSante14='https://cdn.shopify.com/s/files/1/0035/4523/5569/products/Panadol_Extra_Solubles_1000x.jpg?v=1556550456'

this.productForm.value.prixSante15='3'
this.productForm.value.ancienPrixSante15='6'
this.productForm.value.nomSante15='Ns'
this.productForm.value.uniteSante15='40ml'
this.productForm.value.gammeSante15='haute'
this.productForm.value.imgSante15='https://cdn.shopify.com/s/files/1/0035/4523/5569/products/Panadol_Extra_Solubles_1000x.jpg?v=1556550456'

this.productForm.value.prixSante16='4'
this.productForm.value.ancienPrixSante16='7'
this.productForm.value.nomSante16='Ns'
this.productForm.value.uniteSante16='30ml'
this.productForm.value.gammeSante16='haute'
this.productForm.value.imgSante16='https://cdn.shopify.com/s/files/1/0035/4523/5569/products/Panadol_Extra_Solubles_1000x.jpg?v=1556550456'

this.productForm.value.prixSante21='4'
this.productForm.value.ancienPrixSante21='6'
this.productForm.value.nomSante21='Ns'
this.productForm.value.uniteSante21='30ml'
this.productForm.value.gammeSante21='haute'
this.productForm.value.imgSante21='https://cdn.shopify.com/s/files/1/0035/4523/5569/products/Panadol_Extra_Solubles_1000x.jpg?v=1556550456'

this.productForm.value.prixSante22='4'
this.productForm.value.ancienPrixSante22='6'
this.productForm.value.nomSante22='Ns'
this.productForm.value.uniteSante22='30ml'
this.productForm.value.gammeSante22='haute'
this.productForm.value.imgSante22='https://cdn.shopify.com/s/files/1/0035/4523/5569/products/Panadol_Extra_Solubles_1000x.jpg?v=1556550456'

this.productForm.value.prixSante23='4'
this.productForm.value.ancienPrixSante23='6'
this.productForm.value.nomSante23='Ns'
this.productForm.value.uniteSante23='30ml'
this.productForm.value.gammeSante23='haute'
this.productForm.value.imgSante23='https://cdn.shopify.com/s/files/1/0035/4523/5569/products/Panadol_Extra_Solubles_1000x.jpg?v=1556550456'

this.productForm.value.prixSante24='4'
this.productForm.value.ancienPrixSante24='6'
this.productForm.value.nomSante24='Ns'
this.productForm.value.uniteSante24='30ml'
this.productForm.value.gammeSante24='haute'
this.productForm.value.imgSante24='https://cdn.shopify.com/s/files/1/0035/4523/5569/products/Panadol_Extra_Solubles_1000x.jpg?v=1556550456'

this.productForm.value.prixSante25='4'
this.productForm.value.ancienPrixSante25='6'
this.productForm.value.nomSante25='Ns'
this.productForm.value.uniteSante25='30ml'
this.productForm.value.gammeSante25='haute'
this.productForm.value.imgSante25='https://cdn.shopify.com/s/files/1/0035/4523/5569/products/Panadol_Extra_Solubles_1000x.jpg?v=1556550456'

this.productForm.value.prixSante26='4'
this.productForm.value.ancienPrixSante26='6'
this.productForm.value.nomSante26='Ns'
this.productForm.value.uniteSante26='30ml'
this.productForm.value.gammeSante26='haute'
this.productForm.value.imgSante26='https://cdn.shopify.com/s/files/1/0035/4523/5569/products/Panadol_Extra_Solubles_1000x.jpg?v=1556550456'
this.productForm.value.nbrPages=this.choosenCategorie

this.productForm.value.prixBebe11='4'
this.productForm.value.ancienPrixBebe11='7'
this.productForm.value.nomBebe11='Ns'
this.productForm.value.uniteBebe11='30ml'
this.productForm.value.gammeBebe11='haute'
this.productForm.value.imgBebe11='https://cdn.shopify.com/s/files/1/0035/4523/5569/products/Panadol_Extra_Solubles_1000x.jpg?v=1556550456'



this.productForm.value.prixBebe12='9'
this.productForm.value.ancienPrixBebe12='7'
this.productForm.value.nomBebe12='Nbss'
this.productForm.value.uniteBebe12='60ml'
this.productForm.value.gammeBebe12='haute'
this.productForm.value.imgBebe12='https://cdn.shopify.com/s/files/1/0035/4523/5569/products/Panadol_Extra_Solubles_1000x.jpg?v=1556550456'

this.productForm.value.prixBebe13='4'
this.productForm.value.ancienPrixBebe13='7'
this.productForm.value.nomBebe13='Ns'
this.productForm.value.uniteBebe13='30ml'
this.productForm.value.gammeBebe13='haute'
this.productForm.value.imgBebe13='https://cdn.shopify.com/s/files/1/0035/4523/5569/products/Panadol_Extra_Solubles_1000x.jpg?v=1556550456'

this.productForm.value.prixBebe14='2'
this.productForm.value.ancienPrixBebe14='7'
this.productForm.value.nomBebe14='Ns'
this.productForm.value.uniteBebe14='35ml'
this.productForm.value.gammeBebe14='haute'
this.productForm.value.imgBebe14='https://cdn.shopify.com/s/files/1/0035/4523/5569/products/Panadol_Extra_Solubles_1000x.jpg?v=1556550456'

this.productForm.value.prixBebe15='4'
this.productForm.value.ancienPrixBebe15='8'
this.productForm.value.nomBebe15='Nbbb'
this.productForm.value.uniteBebe15='30ml'
this.productForm.value.gammeBebe15='haute'
this.productForm.value.imgBebe15='https://cdn.shopify.com/s/files/1/0035/4523/5569/products/Panadol_Extra_Solubles_1000x.jpg?v=1556550456'

this.productForm.value.prixBebe16='4'
this.productForm.value.ancienPrixBebe16='7'
this.productForm.value.nomBebe16='Nbb'
this.productForm.value.uniteBebe16='30ml'
this.productForm.value.gammeBebe16='haute'
this.productForm.value.imgBebe16='https://cdn.shopify.com/s/files/1/0035/4523/5569/products/Panadol_Extra_Solubles_1000x.jpg?v=1556550456'

this.productForm.value.prixBienetre11='4'
this.productForm.value.ancienPrixBienetre11='7'
this.productForm.value.nomBienetre11='Nbb'
this.productForm.value.uniteBienetre11='30ml'
this.productForm.value.gammeBienetre11='haute'
this.productForm.value.imgBienetre11='https://cdn.shopify.com/s/files/1/0035/4523/5569/products/Panadol_Extra_Solubles_1000x.jpg?v=1556550456'

this.productForm.value.prixBienetre12='4'
this.productForm.value.ancienPrixBienetre12='7'
this.productForm.value.nomBienetre12='Nbb'
this.productForm.value.uniteBienetre12='30ml'
this.productForm.value.gammeBienetre12='haute'
this.productForm.value.imgBienetre12='https://cdn.shopify.com/s/files/1/0035/4523/5569/products/Panadol_Extra_Solubles_1000x.jpg?v=1556550456'

this.productForm.value.prixBienetre13='4'
this.productForm.value.ancienPrixBienetre13='7'
this.productForm.value.nomBienetre13='Nbb'
this.productForm.value.uniteBienetre13='30ml'
this.productForm.value.gammeBienetre13='haute'
this.productForm.value.imgBienetre13='https://cdn.shopify.com/s/files/1/0035/4523/5569/products/Panadol_Extra_Solubles_1000x.jpg?v=1556550456'

this.productForm.value.prixBienetre14='4'
this.productForm.value.ancienPrixBienetre14='7'
this.productForm.value.nomBienetre14='Nbb'
this.productForm.value.uniteBienetre14='30ml'
this.productForm.value.gammeBienetre14='haute'
this.productForm.value.imgBienetre14='https://cdn.shopify.com/s/files/1/0035/4523/5569/products/Panadol_Extra_Solubles_1000x.jpg?v=1556550456'

this.productForm.value.prixBienetre15='4'
this.productForm.value.ancienPrixBienetre15='7'
this.productForm.value.nomBienetre15='Nbb'
this.productForm.value.uniteBienetre15='30ml'
this.productForm.value.gammeBienetre15='haute'
this.productForm.value.imgBienetre15='https://cdn.shopify.com/s/files/1/0035/4523/5569/products/Panadol_Extra_Solubles_1000x.jpg?v=1556550456'

this.productForm.value.prixBienetre16='4'
this.productForm.value.ancienPrixBienetre16='7'
this.productForm.value.nomBienetre16='Nbb'
this.productForm.value.uniteBienetre16='30ml'
this.productForm.value.gammeBienetre16='haute'
this.productForm.value.imgBienetre16='https://cdn.shopify.com/s/files/1/0035/4523/5569/products/Panadol_Extra_Solubles_1000x.jpg?v=1556550456'

this.productForm.value.prixBeaute11='4'
this.productForm.value.ancienPrixBeaute11='7'
this.productForm.value.nomBeaute11='Nbb'
this.productForm.value.uniteBeaute11='30ml'
this.productForm.value.gammeBeaute11='haute'
this.productForm.value.imgBeaute11='https://cdn.shopify.com/s/files/1/0035/4523/5569/products/Panadol_Extra_Solubles_1000x.jpg?v=1556550456'

this.productForm.value.prixBeaute12='4'
this.productForm.value.ancienPrixBeaute12='7'
this.productForm.value.nomBeaute12='Nbb'
this.productForm.value.uniteBeaute12='30ml'
this.productForm.value.gammeBeaute12='haute'
this.productForm.value.imgBeaute12='https://cdn.shopify.com/s/files/1/0035/4523/5569/products/Panadol_Extra_Solubles_1000x.jpg?v=1556550456'

this.productForm.value.prixBeaute13='4'
this.productForm.value.ancienPrixBeaute13='7'
this.productForm.value.nomBeaute13='Nbb'
this.productForm.value.uniteBeaute13='30ml'
this.productForm.value.gammeBeaute13='haute'
this.productForm.value.imgBeaute13='https://cdn.shopify.com/s/files/1/0035/4523/5569/products/Panadol_Extra_Solubles_1000x.jpg?v=1556550456'

this.productForm.value.prixBeaute14='4'
this.productForm.value.ancienPrixBeaute14='7'
this.productForm.value.nomBeaute14='Nbb'
this.productForm.value.uniteBeaute14='30ml'
this.productForm.value.gammeBeaute14='haute'
this.productForm.value.imgBeaute14='https://cdn.shopify.com/s/files/1/0035/4523/5569/products/Panadol_Extra_Solubles_1000x.jpg?v=1556550456'

this.productForm.value.prixBeaute15='4'
this.productForm.value.ancienPrixBeaute15='7'
this.productForm.value.nomBeaute15='Nbb'
this.productForm.value.uniteBeaute15='30ml'
this.productForm.value.gammeBeaute15='haute'
this.productForm.value.imgBeaute15='https://cdn.shopify.com/s/files/1/0035/4523/5569/products/Panadol_Extra_Solubles_1000x.jpg?v=1556550456'

this.productForm.value.prixBeaute16='4'
this.productForm.value.ancienPrixBeaute16='7'
this.productForm.value.nomBeaute16='Nbb'
this.productForm.value.uniteBeaute16='30ml'
this.productForm.value.gammeBeaute16='haute'
this.productForm.value.imgBeaute16='https://cdn.shopify.com/s/files/1/0035/4523/5569/products/Panadol_Extra_Solubles_1000x.jpg?v=1556550456'

this.productForm.value.descriptionConseil1='<p>Test</p>'
this.productForm.value.imgConseil1='https://cdn.shopify.com/s/files/1/0035/4523/5569/products/Panadol_Extra_Solubles_1000x.jpg?v=1556550456'
this.productForm.value.titreConseil1='titre'
this.productForm.value.soustitreConseil1='sous titre'*/


this.productForm.value.nbrPages=this.choosenCategorie
this.productForm.value.p1=5
this.productForm.value.p2=5
this.productForm.value.nbrElements1=3




//const httpHeaders=header.append('form',this.productForm.value)
console.log(this.productForm.value)
this.flyerService.addParams((this.productForm.value)).subscribe(
res=>{

  console.log(res)
  console.log('in')
  const pdfblob=new Blob([res],{type:'application/pdf'}) //,{type:'application/pdf'}
  saveAs(pdfblob,`${Date.now()}.pdf`)
  console.log('working!')
  this.storageService.write('fakeToken',55)
}
)
}
testOption(name){
  console.log(name)
  this.selectionProduit.push(name)
}
}


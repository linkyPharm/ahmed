import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerateFlyerComponent } from './generate-flyer.component';

describe('GenerateFlyerComponent', () => {
  let component: GenerateFlyerComponent;
  let fixture: ComponentFixture<GenerateFlyerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenerateFlyerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerateFlyerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

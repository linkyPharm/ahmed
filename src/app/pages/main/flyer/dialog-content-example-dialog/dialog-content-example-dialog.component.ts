import { Component, OnInit, Input, Inject } from '@angular/core';
import { MatDialogRef,MAT_DIALOG_DATA} from '@angular/material';
import { DialogData } from '../configurer-leaflet/configurer-leaflet.component';


@Component({
  selector: 'app-dialog-content-example-dialog',
  templateUrl: './dialog-content-example-dialog.component.html',
  styleUrls: ['./dialog-content-example-dialog.component.scss']
})
export class DialogContentExampleDialogComponent implements OnInit {
ancienPrix
nouveauPrix
outPut={
  aPrix:'',
  nPrix:''
}
  constructor( 
    public dialogRef: MatDialogRef<DialogContentExampleDialogComponent>,
   @Inject(MAT_DIALOG_DATA) public data: DialogData
    ) { }

  ngOnInit() {
  console.log('data',this.data)
  }

}

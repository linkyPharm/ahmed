import {NgModule} from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
    MatButtonModule, MatDatepickerModule, MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule, MatSlideToggleModule, MatToolbarModule, MatTooltipModule, MatCardModule, MatDividerModule, MatTableModule, MatPaginatorModule, MatChipsModule, MatRippleModule, MatSelectModule, MatSortModule, MatSnackBarModule, MatTabsModule
} from '@angular/material';
import { ColorPickerModule } from 'ngx-color-picker';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule, FuseWidgetModule } from '@fuse/components';

import { HttpClientModule } from '@angular/common/http';
import { ProduitService } from 'app/pages/shared/services/produits.service';
import { ConfigLeafletComponent } from './config-leaflet.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { EcommerceProductsService } from 'app/pages/shared/services/ecommerce.service';
import {  configLeafletService } from 'app/pages/shared/services/config.service';
import { SearchPipe } from '@fuse/pipes/search.pipe';



@NgModule({
    declarations: [
 ConfigLeafletComponent,
    SearchPipe
    ],
    imports     : [
     
        RouterModule,
        MatButtonModule,
        MatChipsModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatPaginatorModule,
        MatRippleModule,
        MatSelectModule,
        MatSortModule,
        MatSnackBarModule,
        MatTableModule,
        MatTabsModule,

        NgxChartsModule,
      

        FuseSharedModule,
        FuseWidgetModule
    ],
    providers   : [
        configLeafletService
 
    ]
})

export class ConfigLeafletModule{}
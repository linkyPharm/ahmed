import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigLeafletComponent } from './config-leaflet.component';

describe('ConfigLeafletComponent', () => {
  let component: ConfigLeafletComponent;
  let fixture: ComponentFixture<ConfigLeafletComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigLeafletComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigLeafletComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { configurationsService } from 'app/pages/shared/services/configService.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService } from 'app/pages/shared/services/notification.service';
import { StorageService } from 'app/pages/shared/services/storage.service';

@Component({
  selector: 'app-edit-config-leaflet',
  templateUrl: './edit-config-leaflet.component.html',
  styleUrls: ['./edit-config-leaflet.component.scss']
})
export class EditConfigLeafletComponent implements OnInit {
productForm:FormGroup
categories=[2,4,8,12]
pagesJson={ id:0,
  name :"",
  type:0,
  text_color:"",
  bg_color:""}
pages=[{name:'SELECTION A LA UNE',id:1},{name:'NOUVEAUTES',id:2} ,{name:'PUBLICITE',id:4},{name:'CONSEIL',id:3}, {name:'SELECTION BEAUTE',id:6}, {name:'SELECTION BEBE',id:7},{name:'SELECTION BIEN ETRE',id:8},{name:'SERVICE',id:5},
{name:'SELECTION SANTE',id:9}

]
tableau=[]
configsEdit:any
  constructor(private _formBuilder: FormBuilder,public fb:FormBuilder,
              private configurationService:configurationsService,
              private ActivatedRouter:ActivatedRoute,
              private Router:Router,
              private notificationService:NotificationService,
              private storageService:StorageService) { }

  ngOnInit() {
    this.storageService.write('configBool',false)
    this.storageService.write('historiqueBool',false)
    this.productForm=this.createProductForm()
    this.ActivatedRouter.params.subscribe(
      (params)=>{
        this.configurationService.getUniqueConfiguration(parseInt(params.id)+1).subscribe(
          (res)=>{
              console.log('inn')
              console.log('unique',res)
              this.configsEdit=res
              
              this.tableau.length=this.configsEdit.pageNumbers
          }
        )
      }
    
    
  )
  }
  createProductForm(): FormGroup
  {
      return this._formBuilder.group({
          
   
       nameConfig:[''],
       pageContent:[''],
       nbrElements1:[0],
       nbrElements2:[0],
       pharmacie:[[]],
       pagesOrdre:[[]],
       nbrPages:[null],
       month:[''],
       p1:[null],
       p2:[null],
       p3:[null],
       p4:[null],
       p5:[null],
       p6:[null],
       p7:[null],
       p8:[null],
       p9:[null],
       p10:[null],
       p11:[null],
       p12:[null],
       mois:[''],
     
      
        events: this.fb.array([])

      
      });
  }
  get event() {
    return this.productForm.get('events') as FormArray;
  }
  addEvent() {
    this.event.push(this.fb.group({event:''}));
  
   
  }
  deleteEvent(index) {
    this.event.removeAt(index);
  }
  selectPages(page,i,id){
    this.pagesJson={
      id:0,
      name :"",
      type:0,
      text_color:"",
      bg_color:""
    }
  if(i==0){

  
    this.pagesJson.name=page
    this.pagesJson.type=id
    this.pagesJson.id=i
    this.pagesJson.text_color='#123456'
    this.pagesJson.bg_color='#lzkjer'
    this.configsEdit.pages[i]=this.pagesJson
  
  }
  if(i==1){
  
    this.pagesJson.name=page
    this.pagesJson.type=id
    this.pagesJson.id=i
    this.pagesJson.text_color='#123'
    this.pagesJson.bg_color='#lzkjer'
    this.configsEdit.pages[i]=this.pagesJson
    
  } 
  if(i==2){
  
    this.pagesJson.name=page
    this.pagesJson.type=id
    this.pagesJson.id=i
    this.pagesJson.text_color='#123456'
    this.pagesJson.bg_color='#lzkjer'
    this.configsEdit.pages[i]=this.pagesJson
  }
  if(i==3){
  
    this.pagesJson.name=page
    this.pagesJson.type=id
    this.pagesJson.id=i
    this.pagesJson.text_color='#123456'
    this.pagesJson.bg_color='#lzkjer'
     this.configsEdit.pages[i]=this.pagesJson
  }
  if(i==4){
    
  
    this.pagesJson.name=page
    this.pagesJson.type=id
    this.pagesJson.id=i
    this.pagesJson.text_color='#123456'
    this.pagesJson.bg_color='#lzkjer'
     this.configsEdit.pages[i]=this.pagesJson
  }
  if(i==5){
   
    this.pagesJson.name=page
    this.pagesJson.type=id
    this.pagesJson.id=i
    this.pagesJson.text_color='#123456'
    this.pagesJson.bg_color='#lzkjer'
     this.configsEdit.pages[i]=this.pagesJson
  }
  if(i==6){
 
    this.pagesJson.name=page
    this.pagesJson.type=id
    this.pagesJson.id=i
    this.pagesJson.text_color='#123456'
    this.pagesJson.bg_color='#lzkjer'
     this.configsEdit.pages[i]=this.pagesJson
  }
  if(i==7){
   
    this.pagesJson.name=page
    this.pagesJson.type=id
    this.pagesJson.id=i
    this.pagesJson.text_color='#123456'
    this.pagesJson.bg_color='#lzkjer'
     this.configsEdit.pages[i]=this.pagesJson
  }
  if(i==8){
  
    this.pagesJson.name=page
    this.pagesJson.type=id
    this.pagesJson.id=i
    this.pagesJson.text_color='#123456'
    this.pagesJson.bg_color='#lzkjer'
     this.configsEdit.pages[i]=this.pagesJson
  }
  if(i==9){
    
    this.pagesJson.name=page
    this.pagesJson.type=id
    this.pagesJson.id=i
    this.pagesJson.text_color='#123456'
    this.pagesJson.bg_color='#lzkjer'
     this.configsEdit.pages[i]=this.pagesJson
  }
  if(i==10){
    
    this.pagesJson.name=page
    this.pagesJson.type=id
    this.pagesJson.id=i
    this.pagesJson.text_color='#123456'
    this.pagesJson.bg_color='#lzkjer'
     this.configsEdit.pages[i]=this.pagesJson
  }
  if(i==11){
    
    this.pagesJson.name=page
    this.pagesJson.type=id
    this.pagesJson.id=i
    this.pagesJson.text_color='#123456'
    this.pagesJson.bg_color='#lzkjer'
     this.configsEdit.pages[i]=this.pagesJson
  }
  }
  chooseCategories(nbr){
this.configsEdit.pageNumbers=nbr
this.tableau.length=nbr
const cont=this.configsEdit.pages.length
this.configsEdit.pages.length=nbr
if(cont<nbr){
for (var i=cont;i<nbr;i++){
  this.configsEdit.pages[i]={
    id:0,
    name :"",
    type:0,
    text_color:"",
    bg_color:""
  }
}
}

console.log(nbr)
  }
  Generate(){
console.log(this.configsEdit)
this.configurationService.editConfiguration(this.configsEdit).subscribe(
  res=>{
    console.log('edit done')
    const link=['app/configurer-flyer']
this.Router.navigate(link)
this.notificationService.showToast('Modification réussi avec succes');
  }
)
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditConfigLeafletComponent } from './edit-config-leaflet.component';

describe('EditConfigLeafletComponent', () => {
  let component: EditConfigLeafletComponent;
  let fixture: ComponentFixture<EditConfigLeafletComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditConfigLeafletComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditConfigLeafletComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

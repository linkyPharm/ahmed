import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoriqueLeafletComponent } from './historique-leaflet.component';

describe('HistoriqueLeafletComponent', () => {
  let component: HistoriqueLeafletComponent;
  let fixture: ComponentFixture<HistoriqueLeafletComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoriqueLeafletComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoriqueLeafletComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

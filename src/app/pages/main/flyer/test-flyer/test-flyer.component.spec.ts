import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestFlyerComponent } from './test-flyer.component';

describe('TestFlyerComponent', () => {
  let component: TestFlyerComponent;
  let fixture: ComponentFixture<TestFlyerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestFlyerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestFlyerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

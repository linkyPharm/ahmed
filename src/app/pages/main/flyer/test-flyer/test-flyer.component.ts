import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { Subject, fromEvent, BehaviorSubject, Observable, merge } from 'rxjs';
import { takeUntil, debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { DataSource } from '@angular/cdk/table';
import { FuseUtils } from '@fuse/utils';
import { EcommerceProductsService } from 'app/pages/shared/services/ecommerce.service';
import { Router } from '@angular/router';
import { ProduitService } from 'app/pages/shared/services/produits.service';
import { configLeafletService } from 'app/pages/shared/services/config.service';
import { exists } from 'fs';
import { StorageService } from 'app/pages/shared/services/storage.service';
import { configurationsService } from 'app/pages/shared/services/configService.service';
import { DataService } from 'app/pages/shared/services/data.service';

@Component({
  selector: 'app-test-flyer',
  templateUrl: './test-flyer.component.html',
  styleUrls: ['./test-flyer.component.scss']
})
export class TestFlyerComponent implements OnInit {
  public searchText:any=''
  configs:any[]=[]
  date
    dataSource: FilesDataSource | null;
    displayedColumns = ['Date', 'Nom','Nombres de Pages','List des Pages', 'active'];
  
    @ViewChild(MatPaginator)
    paginator: MatPaginator;
  
    @ViewChild(MatSort)
    sort: MatSort;
  
    @ViewChild('filter')
    filter: ElementRef;
  
    // Private
    private _unsubscribeAll: Subject<any>;
  
    constructor(
        private _configLeafletService: configLeafletService,
        private router:Router,
        private produitService:ProduitService,
        private storageService:StorageService,
        private configartionService:configurationsService,
        private dataService:DataService
    )
    {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }
  
    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------
  
    /**
     * On init
     */
    ngOnInit(): void
    {
      this.storageService.write('configBool',false)
      this.storageService.write('historiqueBool',false)
      this.date=Date.now()
        this.dataSource = new FilesDataSource(this._configLeafletService, this.paginator, this.sort,this.storageService);
  
        /*fromEvent(this.filter.nativeElement, 'keyup')
            .pipe(
                takeUntil(this._unsubscribeAll),
                debounceTime(150),
                distinctUntilChanged()
            )
            .subscribe(() => {
                if ( !this.dataSource )
                {
                    return;
                }
  
                this.dataSource.filter = this.filter.nativeElement.value;
            });*/
      
        this.configartionService.showAllConfigurations().subscribe(
            (res)=>{
                console.log('working')
                console.log('configs',res)
                this.configs=res
                if(!this.configs){
                    this.configs=[]
                    
                }
            }
        )
        this.configartionService.getUniqueConfiguration(2).subscribe(
            (res)=>{
                console.log('inn')
                console.log('unique',res)
                
            }
        )
         
    }
    refresh():void{
      this.dataSource = new FilesDataSource(this._configLeafletService, this.paginator, this.sort,this.storageService);
  
      fromEvent(this.filter.nativeElement, 'keyup')
          .pipe(
              takeUntil(this._unsubscribeAll),
              debounceTime(150),
              distinctUntilChanged()
          )
          .subscribe(() => {
              if ( !this.dataSource )
              {
                  return;
              }
  
              this.dataSource.filter = this.filter.nativeElement.value;
          });
    }
    editConf(id){
        const link=[`/app/edit-config/${id}`]
    this.router.navigate(link)
    console.log(id)
    }
    deleteConf(id){
      console.log(id)
        this._configLeafletService.deleteconf(id)
        this.configs=this.storageService.read('configurations')
  
    }
    showClick(i){
      console.log('this is click')
      console.log(i)
      this.storageService.write('configBool',true)
      const link=[`/app/generate-leaflet/${i}`]
      this.router.navigate(link)
  
    }
  }
  
  
  export class FilesDataSource extends DataSource<any>
  {
    private _filterChange = new BehaviorSubject('');
    private _filteredDataChange = new BehaviorSubject('');
  configs:any[]=[]
    /**
     * Constructor
     *
     * @param {EcommerceProductsService} _ecommerceProductsService
     * @param {MatPaginator} _matPaginator
     * @param {MatSort} _matSort
     */
    constructor(
        private _configLeafletService: configLeafletService,
        private _matPaginator: MatPaginator,
        private _matSort: MatSort,
        private storageService:StorageService
       
    )
    {
        super();
  //commented to test
      this.filteredData = this._configLeafletService.configs;
     //  this.filterData=this.storageService.read('configurations')
    }
  
    /**
     * Connect function called by the table to retrieve one stream containing the data to render.
     *
     * @returns {Observable<any[]>}
     */
    connect(): Observable<any[]>
    {
        const displayDataChanges = [
            this._configLeafletService.onProductsChanged,
            this._matPaginator.page,
            this._filterChange,
            this._matSort.sortChange
        ];
  
        return merge(...displayDataChanges)
            .pipe(
                map(() => {
                        let data = this._configLeafletService.configs.slice();
  
                        data = this.filterData(data);
  
                        this.filteredData = [...data];
  
                        data = this.sortData(data);
  
                        // Grab the page's slice of data.
                        const startIndex = this._matPaginator.pageIndex * this._matPaginator.pageSize;
                        return data.splice(startIndex, this._matPaginator.pageSize);
                    }
                ));
    }
  
    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------
  
    // Filtered data
    get filteredData(): any
    {
        return this._filteredDataChange.value;
    }
  
    set filteredData(value: any)
    {
        this._filteredDataChange.next(value);
    }
  
    // Filter
    get filter(): string
    {
        return this._filterChange.value;
    }
  
    set filter(filter: string)
    {
        this._filterChange.next(filter);
    }
  
    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
  
    /**
     * Filter data
     *
     * @param data
     * @returns {any}
     */
    filterData(data): any
    {
        if ( !this.filter )
        {
            return data;
        }
        return FuseUtils.filterArrayByString(data, this.filter);
    }
  
    /**
     * Sort data
     *
     * @param data
     * @returns {any[]}
     */
    sortData(data): any[]
    {
        if ( !this._matSort.active || this._matSort.direction === '' )
        {
            return data;
        }
  
        return data.sort((a, b) => {
            let propertyA: number | string = '';
            let propertyB: number | string = '';
  
            switch ( this._matSort.active )
            {
                case 'id':
                    [propertyA, propertyB] = [a.id, b.id];
                    break;
                case 'name':
                    [propertyA, propertyB] = [a.name, b.name];
                    break;
                case 'categories':
                    [propertyA, propertyB] = [a.categories[0], b.categories[0]];
                    break;
                case 'price':
                    [propertyA, propertyB] = [a.priceTaxIncl, b.priceTaxIncl];
                    break;
                case 'quantity':
                    [propertyA, propertyB] = [a.quantity, b.quantity];
                    break;
                case 'active':
                    [propertyA, propertyB] = [a.active, b.active];
                    break;
            }
  
            const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
            const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
  
            return (valueA < valueB ? -1 : 1) * (this._matSort.direction === 'asc' ? 1 : -1);
        });
    }
  
    /**
     * Disconnect
     */
    disconnect(): void
    {
    }
}


import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { StorageService } from 'app/pages/shared/services/storage.service';
import { ActivatedRoute, Router } from '@angular/router';
import { PharmaService } from 'app/pages/shared/services/pharma.service';
import { PharmacieService } from 'app/pages/shared/services/pharmacie.service';
import { configurationsService } from 'app/pages/shared/services/configService.service';

@Component({
  selector: 'app-edit-leaflet',
  templateUrl: './edit-leaflet.component.html',
  styleUrls: ['./edit-leaflet.component.scss']
})
export class EditLeafletComponent implements OnInit {
  configEdit
  mois=['JANVIER 2019','FEVRIER 2019','MARS 2019','AVRIL 2019','MAI 2019','JUIN 2019','JUILLET 2019',
  'AOUT 2019','SEPTEMBRE 2019','OCTOBRE 2019','NOVEMBRE 2019','DECEMBRE 2019']
  pages=[{name:'SELECTION A LA UNE',id:1},{name:'NOUVEAUTES',id:2} ,{name:'PUBLICITE',id:4},{name:'CONSEIL',id:3}, {name:'SELECTION BEAUTE',id:6}, {name:'SELECTION BEBE',id:7},{name:'SELECTION BIEN ETRE',id:8},{name:'SERVICE',id:5},
{name:'SELECTION SANTE',id:9}]
pharmacies=[]
tableau=[]
configs
filteredList1
params
Name:string=''
jsonEdit={
  id:'',name: "",
pageNumbers: "",
  organizationId: 1
}
categories=[2,4,8,12]
  productForm:FormGroup
  constructor(
    public fb:FormBuilder,
    private _formBuilder: FormBuilder,
    private storageService:StorageService,
    private activatedRoute:ActivatedRoute,
    private pharmacieService:PharmacieService,
    private configService:configurationsService,
    private router:Router) { }

  ngOnInit() {
    this.productForm=this.createProductForm()
    this.activatedRoute.params.subscribe(
      (params)=>{
        this.configs=this.storageService.read('configurations')
        this.configEdit=this.configs[params.id]
        this.params=params.id
        this.tableau.length=this.configEdit.nbrPages
        console.log('taille',this.tableau.length)
        console.log(this.configEdit)
      }
    )
    
    this.pharmacieService.getAllPharmacies().subscribe(
      res=>{
        console.log(res)
        this.pharmacies=res
        console.log(this.pharmacies)
        this.filteredList1=this.pharmacies.slice()
      }
    )
  }
  createProductForm(): FormGroup
  {
      return this._formBuilder.group({
          
   
       nameConfig:[''],
       pageContent:[''],
       nbrElements1:[0],
       nbrElements2:[0],
       pharmacie:[[]],
       pagesOrdre:[[]],
       nbrPages:[null],
       month:[''],
       p1:[null],
       p2:[null],
       p3:[null],
       p4:[null],
       p5:[null],
       p6:[null],
       p7:[null],
       p8:[null],
       p9:[null],
       p10:[null],
       p11:[null],
       p12:[null],
       mois:[''],
     
       selling_points: this.fb.array([this.fb.group({point:''})]),
        events: this.fb.array([])

      
      });
  }
  get sellingPoints() {
    return this.productForm.get('selling_points') as FormArray;
  }
  addSellingPoint() {
    this.sellingPoints.push(this.fb.group({point:''}));
  }
  deleteSellingPoint(index) {
    this.sellingPoints.removeAt(index);
  }
  
  get event() {
    return this.productForm.get('events') as FormArray;
  }
  addEvent() {
    this.event.push(this.fb.group({event:''}));
  
  }
  deleteEvent(index) {
    this.event.removeAt(index);
  }
  selectMois(moi){
    this.configEdit.Mois=moi
  }
  selectPharmacie(pharmacie){
    this.configEdit.Pharmacie[0]=pharmacie
  }
  chooseCategories(nbr){
    this.configEdit.nbrPages=nbr
    this.tableau.length=nbr
  }
  selectPages(name,index,id){
this.configEdit.selected_pages[index]=name
this.configEdit.selectedPages[index]=id
  }
  Edit(){
    console.log(this.configEdit)
    console.log('params',this.params)
   this.configs[this.params]=this.configEdit
   this.jsonEdit.name=this.configEdit.Name
   this.jsonEdit.pageNumbers=this.configEdit.nbrPages
  // this.configService.editConfiguration(this.jsonEdit)

   this.storageService.write('configurations',this.configs)
   const link=['/app/configurer-flyer']
   this.router.navigate(link)
  
  }
}

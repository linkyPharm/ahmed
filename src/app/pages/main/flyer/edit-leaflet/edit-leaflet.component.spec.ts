import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditLeafletComponent } from './edit-leaflet.component';

describe('EditLeafletComponent', () => {
  let component: EditLeafletComponent;
  let fixture: ComponentFixture<EditLeafletComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditLeafletComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditLeafletComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

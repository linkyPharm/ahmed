import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigurerLeafletComponent } from './configurer-leaflet.component';

describe('ConfigurerLeafletComponent', () => {
  let component: ConfigurerLeafletComponent;
  let fixture: ComponentFixture<ConfigurerLeafletComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigurerLeafletComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigurerLeafletComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

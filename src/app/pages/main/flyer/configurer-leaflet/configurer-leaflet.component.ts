import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { StorageService } from 'app/pages/shared/services/storage.service';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { ProduitService } from 'app/pages/shared/services/produits.service';
import { MatTabChangeEvent } from '@angular/material';
import {MatDialog,MatDialogRef} from '@angular/material/dialog'
import { DialogContentExampleDialogComponent } from '../dialog-content-example-dialog/dialog-content-example-dialog.component';
//import { ForumsComponent } from '../forums/forums.component';
import { DataService } from 'app/pages/shared/services/data.service';
import {saveAs} from 'file-saver'
import { flyerService } from 'app/pages/shared/services/flyer.service';
import { produit } from 'app/pages/shared/models/Produits';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute } from '@angular/router';
import { configurationsService } from 'app/pages/shared/services/configService.service';
import { PharmacieService } from 'app/pages/shared/services/pharmacie.service';
import { HistoriqueLeafletService } from 'app/pages/shared/services/hitoriqueLeaflet.service';
import { HttpClient } from '@angular/common/http';

export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: 'app-configurer-leaflet',
  templateUrl: './configurer-leaflet.component.html',
  styleUrls: ['./configurer-leaflet.component.css']
})
export class ConfigurerLeafletComponent implements OnInit {
tab=[];
boolConfig=false
boolhistory=false
configs=[]
color=[]
colorbienEtre=[]
colorSante=[]
colorBeaute=[]
colorBebe=[]
historiqueContent:any
pharmacies=[]
filteredList1
selectedIndex:number=0;
    product=[];
    pharmacie
produitChoisi;
selectedMois:any[]=[]
selectedPharma:any=[]
produitAlaune:any[]=[]
produitBebe:any[]=[]
produitSante:any[]=[]
produitBienEtre:any[]=[]
pubs:any[]=[]
Services:[{titre:'',soustitre:'',description:''}]=[{titre:'',soustitre:'',description:''}]
conseils:[{titre:'',soustitre:'',description:''}]=[{titre:'',soustitre:'',description:''}]
Nouvaute:[{titre:'',soustitre:'',description:''}]=[{titre:'',soustitre:'',description:''}]
produitBeaute:any[]=[]
value={key:'',value:''}
pagesContents:[{pageNumber:number,value:[{key:string,value:string}]}]=[{pageNumber:0,value:[{key:'',value:''}]}]
historiqueLeaflet={name:'',configLeaflet:0,userId:0,pagesContent:[
  {pageNumber:0,value:[{key:'',value:''}]},{pageNumber:0,value:[{key:'',value:''}]},{pageNumber:0,value:[{key:'',value:''}]},{pageNumber:0,value:[{key:'',value:''}]},{pageNumber:0,value:[{key:'',value:''}]},{pageNumber:0,value:[{key:'',value:''}]},{pageNumber:0,value:[{key:'',value:''}]},{pageNumber:0,value:[{key:'',value:''}]},{pageNumber:0,value:[{key:'',value:''}]},{pageNumber:0,value:[{key:'',value:''}]},{pageNumber:0,value:[{key:'',value:''}]},{pageNumber:0,value:[{key:'',value:''}]}
,{pageNumber:0,value:[{key:'',value:''}]},{pageNumber:0,value:[{key:'',value:''}]},{pageNumber:0,value:[{key:'',value:''}]},{pageNumber:0,value:[{key:'',value:''}]},{pageNumber:0,value:[{key:'',value:''}]},{pageNumber:0,value:[{key:'',value:''}]},{pageNumber:0,value:[{key:'',value:''}]},{pageNumber:0,value:[{key:'',value:''}]},
{pageNumber:0,value:[{key:'',value:''}]},{pageNumber:0,value:[{key:'',value:''}]},{pageNumber:0,value:[{key:'',value:''}]},]}
pageNumbers=0
mois=['JANVIER 2019','FEVRIER 2019','MARS 2019','AVRIL 2019','MAI 2019','JUIN 2019','JUILLET 2019',
'AOUT 2019','OCTOBRE 2019','NOVEMBRE 2019','DECEMBRE 2019',]
arrayConseil =[
  [this.fb.group({conseil:''})],
  [this.fb.group({conseil:''})],
  [this.fb.group({conseil:''})],
  [this.fb.group({conseil:''})],
[this.fb.group({conseil:''})],
[this.fb.group({conseil:''})],[this.fb.group({conseil:''})],[this.fb.group({conseil:''})]
,[this.fb.group({conseil:''})],[this.fb.group({conseil:''})],
[this.fb.group({conseil:''})],[this.fb.group({conseil:''})]];
arrayBeaute =[[],
[],
[],
[],
[],
[],
[],
[],
[],
[],
[],
[]];
arraybienEtre =[[],
[],
[],
[],
[],
[],
[],
[],
[],
[],[],[]];
arrayNouveaute =[[],
[],
[],
[],
[],
[],
[],
[],[],
[],[],[]];
arraySante =[[],[],
[],[],
[],[],
[],[],
[],[],
[],[]];
arrayBebe =[[],[],
[],[],
[],[],
[],[],
[],[],
[],[]];
arrayUne=[
  [],
  [],
  [],
  [],
[],
[],
[],
[]
,[],[],
[],[]];


ancienPrix
nouveauPrix
hidden=[{count:1,hide:false},{count:1,hide:false},{count:1,hide:false},{count:1,hide:false}
  ,{count:1,hide:false},{count:1,hide:false},{count:1,hide:false}
  ,{count:1,hide:false},{count:1,hide:false},{count:1,hide:false},{count:1,hide:false},{count:1,hide:false},]

  hiddenBeaute=[{count:1,hide:false},{count:1,hide:false},{count:1,hide:false},{count:1,hide:false}
    ,{count:1,hide:false},{count:1,hide:false},{count:1,hide:false}
    ,{count:1,hide:false},{count:1,hide:false},{count:1,hide:false},{count:1,hide:false},{count:1,hide:false},]

    hiddenbienEtre=[{count:1,hide:false},{count:1,hide:false},{count:1,hide:false},{count:1,hide:false}
      ,{count:1,hide:false},{count:1,hide:false},{count:1,hide:false}
      ,{count:1,hide:false},{count:1,hide:false},{count:1,hide:false},{count:1,hide:false},{count:1,hide:false},]

      hiddenSante=[{count:1,hide:false},{count:1,hide:false},{count:1,hide:false},{count:1,hide:false}
        ,{count:1,hide:false},{count:1,hide:false},{count:1,hide:false}
        ,{count:1,hide:false},{count:1,hide:false},{count:1,hide:false},{count:1,hide:false},{count:1,hide:false},]
        hiddenBebe=[{count:1,hide:false},{count:1,hide:false},{count:1,hide:false},{count:1,hide:false}
          ,{count:1,hide:false},{count:1,hide:false},{count:1,hide:false}
          ,{count:1,hide:false},{count:1,hide:false},{count:1,hide:false},{count:1,hide:false},{count:1,hide:false},]
          hiddenNouveau=[{count:1,hide:false},{count:1,hide:false},{count:1,hide:false},{count:1,hide:false}
            ,{count:1,hide:false},{count:1,hide:false},{count:1,hide:false}
            ,{count:1,hide:false},{count:1,hide:false},{count:1,hide:false},{count:1,hide:false},{count:1,hide:false},]
    
        hidenNouveau=false;
formJson={  
nbrService:null,
nbrElements1:1,
nbrElements2:1,
nbrElements3:1,
nbrElements4:1,
nbrElements5:1,
nbrElements6:1,
nbrElements7:1,
nbrElements8:1,
nbrElements9:1,
nbrElements10:1,
nbrElements11:1,
nbrElements12:1,
  //pages
p1:null,
p2:null,
p3:null,
p4:null,
p5:null,
p6:null,
p7:null,
p8:null,
p9:null,
p10:null,
p11:null,
p12:null,
p13:null,
nbrPages:null,
  //pages

  //otherDetails
nomPharma1:null,
adressePharma1:null,
odPharma1:null,
nomPharma:null,
villePharma1:null,
telPharma1:null,
webPharma1:null,
mailPharma1:null,
moisPharma1:null,


nomPharma2:null,
adressePharma2:null,
odPharma2:null,

villePharma2:null,
telPharma2:null,
webPharma2:null,
mailPharma2:null,
moisPharma2:null,


nomPharma3:null,
adressePharma3:null,
odPharma3:null,

villePharma3:null,
telPharma3:null,
webPharma3:null,
mailPharma3:null,
moisPharma3:null,

nomPharma4:null,
adressePharma4:null,
odPharma4:null,

villePharma4:null,
telPharma4:null,
webPharma4:null,
mailPharma4:null,
moisPharma4:null,

nomPharma5:null,
adressePharma5:null,
odPharma5:null,

villePharma5:null,
telPharma5:null,
webPharma5:null,
mailPharma5:null,
moisPharma5:null,

nomPharma6:null,
adressePharma6:null,
odPharma6:null,

villePharma6:null,
telPharma6:null,
webPharma6:null,
mailPharma6:null,
moisPharma6:null,

nomPharma7:null,
adressePharma7:null,
odPharma7:null,

villePharma7:null,
telPharma7:null,
webPharma7:null,
mailPharma7:null,
moisPharma7:null,

nomPharma8:null,
adressePharma8:null,
odPharma8:null,

villePharma8:null,
telPharma8:null,
webPharma8:null,
mailPharma8:null,
moisPharma8:null,

nomPharma9:null,
adressePharma9:null,
odPharma9:null,

villePharma9:null,
telPharma9:null,
webPharma9:null,
mailPharma9:null,
moisPharma9:null,

nomPharma10:null,
adressePharma10:null,
odPharma10:null,

villePharma10:null,
telPharma10:null,
webPharma10:null,
mailPharma10:null,
moisPharma10:null,

nomPharma11:null,
adressePharma11:null,
odPharma11:null,

villePharma11:null,
telPharma11:null,
webPharma11:null,
mailPharma11:null,
moisPharma11:null,

nomPharma12:null,
adressePharma12:null,
odPharma12:null,

villePharma12:null,
telPharma12:null,
webPharma12:null,
mailPharma12:null,
moisPharma12:null,

  //otherDetails
//conseil
imgConseil1:null,
titreConseil1:null,
    soustitreConseil1:null,
    descriptionConseil1:null,

   

    imgConseil2:null,
    titreConseil2:null,
    soustitreConseil2:null,
    descriptionConseil2:null,

    imgConseil3:null,
    titreConseil3:null,
    soustitreConseil3:null,
    descriptionConseil3:null,

    imgConseil4:null,
    titreConseil4:null,
    soustitreConseil4:null,
    descriptionConseil4:null,

    imgConseil5:null,
    titreConseil5:null,
    soustitreConseil5:null,
    descriptionConseil5:null,

    

    imgConseil6:null,
    titreConseil6:null,
    soustitreConseil6:null,
    descriptionConseil6:null,

    

    imgConseil7:null,
    titreConseil7:null,
    soustitreConseil7:null,
    descriptionConseil7:null,
    //publicite
imgPub1:null,
imgPub2:null,
imgPub3:null,
imgPub4:null,
imgPub5:null,
imgPub6:null,
imgPub7:null,
imgPub8:null,
imgPub9:null,
imgPub10:null,   
imgPub11:null,     
imgPub12:null,    
//publicite
    imgConseil8:null,
    titreConseil8:null,
    soustitreConseil8:null,
    descriptionConseil8:null,

    imgConseil9:null,
    titreConseil9:null,
    soustitreConseil9:null,
    descriptionConseil9:null,

    imgConseil10:null,
    titreConseil10:null,
    soustitreConseil10:null,
    descriptionConseil10:null,

    imgConseil11:null,
    titreConseil11:null,
    soustitreConseil11:null,
    descriptionConseil11:null,

    imgConseil12:null,
    titreConseil12:null,
    soustitreConseil12:null,
    descriptionConseil12:null,
//conseil

//service
titreService11:null,
soustitreService11:null,
descriptionService11:null,
imgService11:null,

titreService12:null,
soustitreService12:null,
descriptionService12:null,
imgService12:null,

titreService13:null,
soustitreService13:null,
descriptionService13:null,
imgService13:null,

titreService14:null,
soustitreService14:null,
descriptionService14:null,
imgService14:null,

titreService21:null,
soustitreService21:null,
descriptionService21:null,
imgService21:null,

titreService22:null,
soustitreService22:null,
descriptionService22:null,
imgService22:null,

titreService23:null,
soustitreService23:null,
descriptionService23:null,
imgService23:null,

titreService24:null,
soustitreService24:null,
descriptionService24:null,
imgService24:null,

titreService31:null,
soustitreService31:null,
descriptionService31:null,
imgService31:null,

titreService32:null,
soustitreService32:null,
descriptionService32:null,
imgService32:null,

titreService33:null,
soustitreService33:null,
descriptionService33:null,
imgService33:null,

titreService34:null,
soustitreService34:null,
descriptionService34:null,
imgService34:null,

titreService41:null,
soustitreService41:null,
descriptionService41:null,
imgService41:null,

titreService42:null,
soustitreService42:null,
descriptionService42:null,
imgService42:null,

titreService43:null,
soustitreService43:null,
descriptionService43:null,
imgService43:null,

titreService44:null,
soustitreService44:null,
descriptionService44:null,
imgService44:null,


titreService51:null,
soustitreService51:null,
descriptionService51:null,
imgService51:null,

titreService52:null,
soustitreService52:null,
descriptionService52:null,
imgService52:null,

titreService53:null,
soustitreService53:null,
descriptionService53:null,
imgService53:null,

titreService54:null,
soustitreService54:null,
descriptionService54:null,
imgService54:null,


titreService61:null,
soustitreService61:null,
descriptionService61:null,
imgService61:null,

titreService62:null,
soustitreService62:null,
descriptionService62:null,
imgService62:null,

titreService63:null,
soustitreService63:null,
descriptionService63:null,
imgService63:null,

titreService64:null,
soustitreService64:null,
descriptionService64:null,
imgService64:null,

titreService71:null,
soustitreService71:null,
descriptionService71:null,
imgService71:null,

titreService72:null,
soustitreService72:null,
descriptionService72:null,
imgService72:null,

titreService73:null,
soustitreService73:null,
descriptionService73:null,
imgService73:null,

titreService74:null,
soustitreService74:null,
descriptionService74:null,
imgService74:null,

titreService81:null,
soustitreService81:null,
descriptionService81:null,
imgService81:null,

titreService82:null,
soustitreService82:null,
descriptionService82:null,
imgService82:null,

titreService83:null,
soustitreService83:null,
descriptionService83:null,
imgService83:null,

titreService84:null,
soustitreService84:null,
descriptionService84:null,
imgService84:null,

titreService91:null,
soustitreService91:null,
descriptionService91:null,
imgService91:null,

titreService92:null,
soustitreService92:null,
descriptionService92:null,
imgService92:null,

titreService93:null,
soustitreService93:null,
descriptionService93:null,
imgService93:null,

titreService94:null,
soustitreService94:null,
descriptionService94:null,
imgService94:null,

titreService101:null,
soustitreService101:null,
descriptionService101:null,
imgService101:null,

titreService102:null,
soustitreService102:null,
descriptionService102:null,
imgService102:null,

titreService103:null,
soustitreService103:null,
descriptionService103:null,
imgService103:null,

titreService104:null,
soustitreService104:null,
descriptionService104:null,
imgService104:null,

titreService111:null,
soustitreService111:null,
descriptionService111:null,
imgService111:null,

titreService112:null,
soustitreService112:null,
descriptionService112:null,
imgService112:null,

titreService113:null,
soustitreService113:null,
descriptionService113:null,
imgService113:null,

titreService114:null,
soustitreService114:null,
descriptionService114:null,
imgService114:null,

titreService121:null,
soustitreService121:null,
descriptionService121:null,
imgService121:null,

titreService122:null,
soustitreService122:null,
descriptionService122:null,
imgService122:null,

titreService123:null,
soustitreService123:null,
descriptionService123:null,
imgService123:null,

titreService124:null,
soustitreService124:null,
descriptionService124:null,
imgService124:null,
//service

//nouveautés

titreNew11:null,
soustitreNew11:null,
descriptionNew11:null,
imgNew11:null,

titreNew12:null,
soustitreNew12:null,
descriptionNew12:null,
imgNew12:null,

titreNew13:null,
soustitreNew13:null,
descriptionNew13:null,
imgNew13:null,

titreNew14:null,
soustitreNew14:null,
descriptionNew14:null,
imgNew14:null,

titreNew21:null,
soustitreNew21:null,
descriptionNew21:null,
imgNew21:null,

titreNew22:null,
soustitreNew22:null,
descriptionNew22:null,
imgNew22:null,

titreNew23:null,
soustitreNew23:null,
descriptionNew23:null,
imgNew23:null,

titreNew24:null,
soustitreNew24:null,
descriptionNew24:null,
imgNew24:null,

titreNew31:null,
soustitreNew31:null,
descriptionNew31:null,
imgNew31:null,

titreNew32:null,
soustitreNew32:null,
descriptionNew32:null,
imgNew32:null,

titreNew33:null,
soustitreNew33:null,
descriptionNew33:null,
imgNew33:null,

titreNew34:null,
soustitreNew34:null,
descriptionNew34:null,
imgNew34:null,

titreNew41:null,
soustitreNew41:null,
descriptionNew41:null,
imgNew41:null,

titreNew42:null,
soustitreNew42:null,
descriptionNew42:null,
imgNew42:null,

titreNew43:null,
soustitreNew43:null,
descriptionNew43:null,
imgNew43:null,

titreNew44:null,
soustitreNew44:null,
descriptionNew44:null,
imgNew44:null,

titreNew51:null,
soustitreNew51:null,
descriptionNew51:null,
imgNew51:null,

titreNew52:null,
soustitreNew52:null,
descriptionNew52:null,
imgNew52:null,

titreNew53:null,
soustitreNew53:null,
descriptionNew53:null,
imgNew53:null,

titreNew54:null,
soustitreNew54:null,
descriptionNew54:null,
imgNew54:null,

titreNew61:null,
soustitreNew61:null,
descriptionNew61:null,
imgNew61:null,

titreNew62:null,
soustitreNew62:null,
descriptionNew62:null,
imgNew62:null,

titreNew63:null,
soustitreNew63:null,
descriptionNew63:null,
imgNew63:null,

titreNew64:null,
soustitreNew64:null,
descriptionNew64:null,
imgNew64:null,

titreNew71:null,
soustitreNew71:null,
descriptionNew71:null,
imgNew71:null,

titreNew72:null,
soustitreNew72:null,
descriptionNew72:null,
imgNew72:null,

titreNew73:null,
soustitreNew73:null,
descriptionNew73:null,
imgNew73:null,

titreNew74:null,
soustitreNew74:null,
descriptionNew74:null,
imgNew74:null,


titreNew81:null,
soustitreNew81:null,
descriptionNew81:null,
imgNew81:null,

titreNew82:null,
soustitreNew82:null,
descriptionNew82:null,
imgNew82:null,

titreNew83:null,
soustitreNew83:null,
descriptionNew83:null,
imgNew83:null,

titreNew84:null,
soustitreNew84:null,
descriptionNew84:null,
imgNew84:null,

titreNew91:null,
soustitreNew91:null,
descriptionNew91:null,
imgNew91:null,

titreNew92:null,
soustitreNew92:null,
descriptionNew92:null,
imgNew92:null,

titreNew93:null,
soustitreNew93:null,
descriptionNew93:null,
imgNew93:null,

titreNew94:null,
soustitreNew94:null,
descriptionNew94:null,
imgNew94:null,

titreNew101:null,
soustitreNew101:null,
descriptionNew101:null,
imgNew101:null,

titreNew102:null,
soustitreNew102:null,
descriptionNew102:null,
imgNew102:null,

titreNew103:null,
soustitreNew103:null,
descriptionNew103:null,
imgNew103:null,

titreNew104:null,
soustitreNew104:null,
descriptionNew104:null,
imgNew104:null,

titreNew111:null,
soustitreNew111:null,
descriptionNew111:null,
imgNew111:null,

titreNew112:null,
soustitreNew112:null,
descriptionNew112:null,
imgNew112:null,

titreNew113:null,
soustitreNew113:null,
descriptionNew113:null,
imgNew113:null,

titreNew114:null,
soustitreNew114:null,
descriptionNew114:null,
imgNew114:null,

titreNew121:null,
soustitreNew121:null,
descriptionNew121:null,
imgNew121:null,

titreNew122:null,
soustitreNew122:null,
descriptionNew122:null,
imgNew122:null,

titreNew123:null,
soustitreNew123:null,
descriptionNew123:null,
imgNew123:null,

titreNew124:null,
soustitreNew124:null,
descriptionNew124:null,
imgNew124:null,
//nouveautés


  //Sante
  prixSante11:null,
ancienPrixSante11:null,
nomSante11:null,
uniteSante11:null,
gammeSante11:null,
imgSante11:null,

prixSante12:null,
ancienPrixSante12:null,
nomSante12:null,
uniteSante12:null,
gammeSante12:null,
imgSante12:null,

prixSante13:null,
ancienPrixSante13:null,
nomSante13:null,
uniteSante13:null,
gammeSante13:null,
imgSante13:null,

prixSante14:null,
ancienPrixSante14:null,
nomSante14:null,
uniteSante14:null,
gammeSante14:null,
imgSante14:null,

prixSante15:null,
ancienPrixSante15:null,
nomSante15:null,
uniteSante15:null,
gammeSante15:null,
imgSante15:null,

prixSante16:null,
ancienPrixSante16:null,
nomSante16:null,
uniteSante16:null,
gammeSante16:null,
imgSante16:null,


prixSante21:null,
ancienPrixSante21:null,
nomSante21:null,
uniteSante21:null,
gammeSante21:null,
imgSante21:null,

prixSante22:null,
ancienPrixSante22:null,
nomSante22:null,
uniteSante22:null,
gammeSante22:null,
imgSante22:null,

prixSante23:null,
ancienPrixSante23:null,
nomSante23:null,
uniteSante23:null,
gammeSante23:null,
imgSante23:null,

prixSante24:null,
ancienPrixSante24:null,
nomSante24:null,
uniteSante24:null,
gammeSante24:null,
imgSante24:null,

prixSante25:null,
ancienPrixSante25:null,
nomSante25:null,
uniteSante25:null,
gammeSante25:null,
imgSante25:null,

prixSante26:null,
ancienPrixSante26:null,
nomSante26:null,
uniteSante26:null,
gammeSante26:null,
imgSante26:null,

prixSante31:null,
ancienPrixSante31:null,
nomSante31:null,
uniteSante31:null,
gammeSante31:null,
imgSante31:null,

prixSante32:null,
ancienPrixSante32:null,
nomSante32:null,
uniteSante32:null,
gammeSante32:null,
imgSante32:null,

prixSante33:null,
ancienPrixSante33:null,
nomSante33:null,
uniteSante33:null,
gammeSante33:null,
imgSante33:null,

prixSante34:null,
ancienPrixSante34:null,
nomSante34:null,
uniteSante34:null,
gammeSante34:null,
imgSante34:null,

prixSante35:null,
ancienPrixSante35:null,
nomSante35:null,
uniteSante35:null,
gammeSante35:null,
imgSante35:null,

prixSante36:null,
ancienPrixSante36:null,
nomSante36:null,
uniteSante36:null,
gammeSante36:null,
imgSante36:null,

prixSante41:null,
ancienPrixSante41:null,
nomSante41:null,
uniteSante41:null,
gammeSante41:null,
imgSante41:null,

prixSante42:null,
ancienPrixSante42:null,
nomSante42:null,
uniteSante42:null,
gammeSante42:null,
imgSante42:null,

prixSante43:null,
ancienPrixSante43:null,
nomSante43:null,
uniteSante43:null,
gammeSante43:null,
imgSante43:null,

prixSante44:null,
ancienPrixSante44:null,
nomSante44:null,
uniteSante44:null,
gammeSante44:null,
imgSante44:null,

prixSante45:null,
ancienPrixSante45:null,
nomSante45:null,
uniteSante45:null,
gammeSante45:null,
imgSante45:null,

prixSante46:null,
ancienPrixSante46:null,
nomSante46:null,
uniteSante46:null,
gammeSante46:null,
imgSante46:null,

prixSante51:null,
ancienPrixSante51:null,
nomSante51:null,
uniteSante51:null,
gammeSante51:null,
imgSante51:null,

prixSante52:null,
ancienPrixSante52:null,
nomSante52:null,
uniteSante52:null,
gammeSante52:null,
imgSante52:null,

prixSante53:null,
ancienPrixSante53:null,
nomSante53:null,
uniteSante53:null,
gammeSante53:null,
imgSante53:null,

prixSante54:null,
ancienPrixSante54:null,
nomSante54:null,
uniteSante54:null,
gammeSante54:null,
imgSante54:null,

prixSante55:null,
ancienPrixSante55:null,
nomSante55:null,
uniteSante55:null,
gammeSante55:null,
imgSante55:null,

prixSante56:null,
ancienPrixSante56:null,
nomSante56:null,
uniteSante56:null,
gammeSante56:null,
imgSante56:null,

prixSante61:null,
ancienPrixSante61:null,
nomSante61:null,
uniteSante61:null,
gammeSante61:null,
imgSante61:null,

prixSante62:null,
ancienPrixSante62:null,
nomSante62:null,
uniteSante62:null,
gammeSante62:null,
imgSante62:null,

prixSante63:null,
ancienPrixSante63:null,
nomSante63:null,
uniteSante63:null,
gammeSante63:null,
imgSante63:null,

prixSante64:null,
ancienPrixSante64:null,
nomSante64:null,
uniteSante64:null,
gammeSante64:null,
imgSante64:null,

prixSante65:null,
ancienPrixSante65:null,
nomSante65:null,
uniteSante65:null,
gammeSante65:null,
imgSante65:null,

prixSante66:null,
ancienPrixSante66:null,
nomSante66:null,
uniteSante66:null,
gammeSante66:null,
imgSante66:null,

prixSante71:null,
ancienPrixSante71:null,
nomSante71:null,
uniteSante71:null,
gammeSante71:null,
imgSante71:null,

prixSante72:null,
ancienPrixSante72:null,
nomSante72:null,
uniteSante72:null,
gammeSante72:null,
imgSante72:null,

prixSante73:null,
ancienPrixSante73:null,
nomSante73:null,
uniteSante73:null,
gammeSante73:null,
imgSante73:null,

prixSante74:null,
ancienPrixSante74:null,
nomSante74:null,
uniteSante74:null,
gammeSante74:null,
imgSante74:null,

prixSante75:null,
ancienPrixSante75:null,
nomSante75:null,
uniteSante75:null,
gammeSante75:null,
imgSante75:null,

prixSante76:null,
ancienPrixSante76:null,
nomSante76:null,
uniteSante76:null,
gammeSante76:null,
imgSante76:null,

prixSante81:null,
ancienPrixSante81:null,
nomSante81:null,
uniteSante81:null,
gammeSante81:null,
imgSante81:null,

prixSante82:null,
ancienPrixSante82:null,
nomSante82:null,
uniteSante82:null,
gammeSante82:null,
imgSante82:null,

prixSante83:null,
ancienPrixSante83:null,
nomSante83:null,
uniteSante83:null,
gammeSante83:null,
imgSante83:null,

prixSante84:null,
ancienPrixSante84:null,
nomSante84:null,
uniteSante84:null,
gammeSante84:null,
imgSante84:null,

prixSante85:null,
ancienPrixSante85:null,
nomSante85:null,
uniteSante85:null,
gammeSante85:null,
imgSante85:null,

prixSante86:null,
ancienPrixSante86:null,
nomSante86:null,
uniteSante86:null,
gammeSante86:null,
imgSante86:null,

prixSante91:null,
ancienPrixSante91:null,
nomSante91:null,
uniteSante91:null,
gammeSante91:null,
imgSante91:null,

prixSante92:null,
ancienPrixSante92:null,
nomSante92:null,
uniteSante92:null,
gammeSante92:null,
imgSante92:null,

prixSante93:null,
ancienPrixSante93:null,
nomSante93:null,
uniteSante93:null,
gammeSante93:null,
imgSante93:null,

prixSante94:null,
ancienPrixSante94:null,
nomSante94:null,
uniteSante94:null,
gammeSante94:null,
imgSante94:null,

prixSante95:null,
ancienPrixSante95:null,
nomSante95:null,
uniteSante95:null,
gammeSante95:null,
imgSante95:null,

prixSante96:null,
ancienPrixSante96:null,
nomSante96:null,
uniteSante96:null,
gammeSante96:null,
imgSante96:null,

prixSante101:null,
ancienPrixSante101:null,
nomSante101:null,
uniteSante101:null,
gammeSante101:null,
imgSante101:null,

prixSante102:null,
ancienPrixSante102:null,
nomSante102:null,
uniteSante102:null,
gammeSante102:null,
imgSante102:null,

prixSante103:null,
ancienPrixSante103:null,
nomSante103:null,
uniteSante103:null,
gammeSante103:null,
imgSante103:null,

prixSante104:null,
ancienPrixSante104:null,
nomSante104:null,
uniteSante104:null,
gammeSante104:null,
imgSante104:null,

prixSante105:null,
ancienPrixSante105:null,
nomSante105:null,
uniteSante105:null,
gammeSante105:null,
imgSante105:null,

prixSante106:null,
ancienPrixSante106:null,
nomSante106:null,
uniteSante106:null,
gammeSante106:null,
imgSante106:null,

prixSante111:null,
ancienPrixSante111:null,
nomSante111:null,
uniteSante111:null,
gammeSante111:null,
imgSante111:null,

prixSante112:null,
ancienPrixSante112:null,
nomSante112:null,
uniteSante112:null,
gammeSante112:null,
imgSante112:null,

prixSante113:null,
ancienPrixSante113:null,
nomSante113:null,
uniteSante113:null,
gammeSante113:null,
imgSante113:null,

prixSante114:null,
ancienPrixSante114:null,
nomSante114:null,
uniteSante114:null,
gammeSante114:null,
imgSante114:null,

prixSante115:null,
ancienPrixSante115:null,
nomSante115:null,
uniteSante115:null,
gammeSante115:null,
imgSante115:null,

prixSante116:null,
ancienPrixSante116:null,
nomSante116:null,
uniteSante116:null,
gammeSante116:null,
imgSante116:null,

prixSante121:null,
ancienPrixSante121:null,
nomSante121:null,
uniteSante121:null,
gammeSante121:null,
imgSante121:null,

prixSante122:null,
ancienPrixSante122:null,
nomSante122:null,
uniteSante122:null,
gammeSante122:null,
imgSante122:null,

prixSante123:null,
ancienPrixSante123:null,
nomSante123:null,
uniteSante123:null,
gammeSante123:null,
imgSante123:null,

prixSante124:null,
ancienPrixSante124:null,
nomSante124:null,
uniteSante124:null,
gammeSante124:null,
imgSante124:null,

prixSante125:null,
ancienPrixSante125:null,
nomSante125:null,
uniteSante125:null,
gammeSante125:null,
imgSante125:null,

prixSante126:null,
ancienPrixSante126:null,
nomSante126:null,
uniteSante126:null,
gammeSante126:null,
imgSante126:null,
//Sante

//selectionAlaUne

prixProduit11:null,
ancienPrixProduit11:null,
nomProduit11:null,
uniteProduit11:null,
gammeProduit11:null,
imgProduit11:null,

prixProduit12:null,
ancienPrixProduit12:null,
nomProduit12:null,
uniteProduit12:null,
gammeProduit12:null,
imgProduit12:null,

prixProduit13:null,
ancienPrixProduit13:null,
nomProduit13:null,
uniteProduit13:null,
gammeProduit13:null,
imgProduit13:null,

prixProduit14:null,
ancienPrixProduit14:null,
nomProduit14:null,
uniteProduit14:null,
gammeProduit14:null,
imgProduit14:null,

prixProduit21:null,
ancienPrixProduit21:null,
nomProduit21:null,
uniteProduit21:null,
gammeProduit21:null,
imgProduit21:null,

prixProduit22:null,
ancienPrixProduit22:null,
nomProduit22:null,
uniteProduit22:null,
gammeProduit22:null,
imgProduit22:null,

prixProduit23:null,
ancienPrixProduit23:null,
nomProduit23:null,
uniteProduit23:null,
gammeProduit23:null,
imgProduit23:null,

prixProduit24:null,
ancienPrixProduit24:null,
nomProduit24:null,
uniteProduit24:null,
gammeProduit24:null,
imgProduit24:null,

prixProduit31:null,
ancienPrixProduit31:null,
nomProduit31:null,
uniteProduit31:null,
gammeProduit31:null,
imgProduit31:null,

prixProduit32:null,
ancienPrixProduit32:null,
nomProduit32:null,
uniteProduit32:null,
gammeProduit32:null,
imgProduit32:null,

prixProduit33:null,
ancienPrixProduit33:null,
nomProduit33:null,
uniteProduit33:null,
gammeProduit33:null,
imgProduit33:null,

prixProduit34:null,
ancienPrixProduit34:null,
nomProduit34:null,
uniteProduit34:null,
gammeProduit34:null,
imgProduit34:null,

prixProduit41:null,
ancienPrixProduit41:null,
nomProduit41:null,
uniteProduit41:null,
gammeProduit41:null,
imgProduit41:null,

prixProduit42:null,
ancienPrixProduit42:null,
nomProduit42:null,
uniteProduit42:null,
gammeProduit42:null,
imgProduit42:null,

prixProduit43:null,
ancienPrixProduit43:null,
nomProduit43:null,
uniteProduit43:null,
gammeProduit43:null,
imgProduit43:null,

prixProduit44:null,
ancienPrixProduit44:null,
nomProduit44:null,
uniteProduit44:null,
gammeProduit44:null,
imgProduit44:null,

prixProduit51:null,
ancienPrixProduit51:null,
nomProduit51:null,
uniteProduit51:null,
gammeProduit51:null,
imgProduit51:null,

prixProduit52:null,
ancienPrixProduit52:null,
nomProduit52:null,
uniteProduit52:null,
gammeProduit52:null,
imgProduit52:null,

prixProduit53:null,
ancienPrixProduit53:null,
nomProduit53:null,
uniteProduit53:null,
gammeProduit53:null,
imgProduit53:null,

prixProduit54:null,
ancienPrixProduit54:null,
nomProduit54:null,
uniteProduit54:null,
gammeProduit54:null,
imgProduit54:null,

prixProduit61:null,
ancienPrixProduit61:null,
nomProduit61:null,
uniteProduit61:null,
gammeProduit61:null,
imgProduit61:null,

prixProduit62:null,
ancienPrixProduit62:null,
nomProduit62:null,
uniteProduit62:null,
gammeProduit62:null,
imgProduit62:null,

prixProduit63:null,
ancienPrixProduit63:null,
nomProduit63:null,
uniteProduit63:null,
gammeProduit63:null,
imgProduit63:null,

prixProduit64:null,
ancienPrixProduit64:null,
nomProduit64:null,
uniteProduit64:null,
gammeProduit64:null,
imgProduit64:null,

prixProduit71:null,
ancienPrixProduit71:null,
nomProduit71:null,
uniteProduit71:null,
gammeProduit71:null,
imgProduit71:null,

prixProduit72:null,
ancienPrixProduit72:null,
nomProduit72:null,
uniteProduit72:null,
gammeProduit72:null,
imgProduit72:null,

prixProduit73:null,
ancienPrixProduit73:null,
nomProduit73:null,
uniteProduit73:null,
gammeProduit73:null,
imgProduit73:null,

prixProduit74:null,
ancienPrixProduit74:null,
nomProduit74:null,
uniteProduit74:null,
gammeProduit74:null,
imgProduit74:null,

prixProduit81:null,
ancienPrixProduit81:null,
nomProduit81:null,
uniteProduit81:null,
gammeProduit81:null,
imgProduit81:null,

prixProduit82:null,
ancienPrixProduit82:null,
nomProduit82:null,
uniteProduit82:null,
gammeProduit82:null,
imgProduit82:null,

prixProduit83:null,
ancienPrixProduit83:null,
nomProduit83:null,
uniteProduit83:null,
gammeProduit83:null,
imgProduit83:null,

prixProduit84:null,
ancienPrixProduit84:null,
nomProduit84:null,
uniteProduit84:null,
gammeProduit84:null,
imgProduit84:null,

prixProduit91:null,
ancienPrixProduit91:null,
nomProduit91:null,
uniteProduit91:null,
gammeProduit91:null,
imgProduit91:null,

prixProduit92:null,
ancienPrixProduit92:null,
nomProduit92:null,
uniteProduit92:null,
gammeProduit92:null,
imgProduit92:null,

prixProduit93:null,
ancienPrixProduit93:null,
nomProduit93:null,
uniteProduit93:null,
gammeProduit93:null,
imgProduit93:null,

prixProduit94:null,
ancienPrixProduit94:null,
nomProduit94:null,
uniteProduit94:null,
gammeProduit94:null,
imgProduit94:null,

prixProduit101:null,
ancienPrixProduit101:null,
nomProduit101:null,
uniteProduit101:null,
gammeProduit101:null,
imgProduit101:null,

prixProduit102:null,
ancienPrixProduit102:null,
nomProduit102:null,
uniteProduit102:null,
gammeProduit102:null,
imgProduit102:null,

prixProduit103:null,
ancienPrixProduit103:null,
nomProduit103:null,
uniteProduit103:null,
gammeProduit103:null,
imgProduit103:null,

prixProduit104:null,
ancienPrixProduit104:null,
nomProduit104:null,
uniteProduit104:null,
gammeProduit104:null,
imgProduit104:null,

prixProduit111:null,
ancienPrixProduit111:null,
nomProduit111:null,
uniteProduit111:null,
gammeProduit111:null,
imgProduit111:null,

prixProduit112:null,
ancienPrixProduit112:null,
nomProduit112:null,
uniteProduit112:null,
gammeProduit112:null,
imgProduit112:null,

prixProduit113:null,
ancienPrixProduit113:null,
nomProduit113:null,
uniteProduit113:null,
gammeProduit113:null,
imgProduit113:null,

prixProduit114:null,
ancienPrixProduit114:null,
nomProduit114:null,
uniteProduit114:null,
gammeProduit114:null,
imgProduit114:null,


prixProduit121:null,
ancienPrixProduit121:null,
nomProduit121:null,
uniteProduit121:null,
gammeProduit121:null,
imgProduit121:null,


prixProduit122:null,
ancienPrixProduit122:null,
nomProduit122:null,
uniteProduit122:null,
gammeProduit122:null,
imgProduit122:null,


prixProduit123:null,
ancienPrixProduit123:null,
nomProduit123:null,
uniteProduit123:null,
gammeProduit123:null,
imgProduit123:null,


prixProduit124:null,
ancienPrixProduit124:null,
nomProduit124:null,
uniteProduit124:null,
gammeProduit124:null,
imgProduit124:null,
//selectionAlaUne1

//Beaute
prixBeaute11:null,
ancienPrixBeaute11:null,
nomBeaute11:null,
uniteBeaute11:null,
gammeBeaute11:null,
imgBeaute11:null,

prixBeaute12:null,
ancienPrixBeaute12:null,
nomBeaute12:null,
uniteBeaute12:null,
gammeBeaute12:null,
imgBeaute12:null,

prixBeaute13:null,
ancienPrixBeaute13:null,
nomBeaute13:null,
uniteBeaute13:null,
gammeBeaute13:null,
imgBeaute13:null,

prixBeaute14:null,
ancienPrixBeaute14:null,
nomBeaute14:null,
uniteBeaute14:null,
gammeBeaute14:null,
imgBeaute14:null,

prixBeaute15:null,
ancienPrixBeaute15:null,
nomBeaute15:null,
uniteBeaute15:null,
gammeBeaute15:null,
imgBeaute15:null,

prixBeaute16:null,
ancienPrixBeaute16:null,
nomBeaute16:null,
uniteBeaute16:null,
gammeBeaute16:null,
imgBeaute16:null,

prixBeaute21:null,
ancienPrixBeaute21:null,
nomBeaute21:null,
uniteBeaute21:null,
gammeBeaute21:null,
imgBeaute21:null,

prixBeaute22:null,
ancienPrixBeaute22:null,
nomBeaute22:null,
uniteBeaute22:null,
gammeBeaute22:null,
imgBeaute22:null,

prixBeaute23:null,
ancienPrixBeaute23:null,
nomBeaute23:null,
uniteBeaute23:null,
gammeBeaute23:null,
imgBeaute23:null,

prixBeaute24:null,
ancienPrixBeaute24:null,
nomBeaute24:null,
uniteBeaute24:null,
gammeBeaute24:null,
imgBeaute24:null,

prixBeaute25:null,
ancienPrixBeaute25:null,
nomBeaute25:null,
uniteBeaute25:null,
gammeBeaute25:null,
imgBeaute25:null,

prixBeaute26:null,
ancienPrixBeaute26:null,
nomBeaute26:null,
uniteBeaute26:null,
gammeBeaute26:null,
imgBeaute26:null,

prixBeaute31:null,
ancienPrixBeaute31:null,
nomBeaute31:null,
uniteBeaute31:null,
gammeBeaute31:null,
imgBeaute31:null,

prixBeaute32:null,
ancienPrixBeaute32:null,
nomBeaute32:null,
uniteBeaute32:null,
gammeBeaute32:null,
imgBeaute32:null,

prixBeaute33:null,
ancienPrixBeaute33:null,
nomBeaute33:null,
uniteBeaute33:null,
gammeBeaute33:null,
imgBeaute33:null,

prixBeaute34:null,
ancienPrixBeaute34:null,
nomBeaute34:null,
uniteBeaute34:null,
gammeBeaute34:null,
imgBeaute34:null,

prixBeaute35:null,
ancienPrixBeaute35:null,
nomBeaute35:null,
uniteBeaute35:null,
gammeBeaute35:null,
imgBeaute35:null,

prixBeaute36:null,
ancienPrixBeaute36:null,
nomBeaute36:null,
uniteBeaute36:null,
gammeBeaute36:null,
imgBeaute36:null,

prixBeaute41:null,
ancienPrixBeaute41:null,
nomBeaute41:null,
uniteBeaute41:null,
gammeBeaute41:null,
imgBeaute41:null,

prixBeaute42:null,
ancienPrixBeaute42:null,
nomBeaute42:null,
uniteBeaute42:null,
gammeBeaute42:null,
imgBeaute42:null,

prixBeaute43:null,
ancienPrixBeaute43:null,
nomBeaute43:null,
uniteBeaute43:null,
gammeBeaute43:null,
imgBeaute43:null,

prixBeaute44:null,
ancienPrixBeaute44:null,
nomBeaute44:null,
uniteBeaute44:null,
gammeBeaute44:null,
imgBeaute44:null,

prixBeaute45:null,
ancienPrixBeaute45:null,
nomBeaute45:null,
uniteBeaute45:null,
gammeBeaute45:null,
imgBeaute45:null,

prixBeaute46:null,
ancienPrixBeaute46:null,
nomBeaute46:null,
uniteBeaute46:null,
gammeBeaute46:null,
imgBeaute46:null,

prixBeaute51:null,
ancienPrixBeaute51:null,
nomBeaute51:null,
uniteBeaute51:null,
gammeBeaute51:null,
imgBeaute51:null,

prixBeaute52:null,
ancienPrixBeaute52:null,
nomBeaute52:null,
uniteBeaute52:null,
gammeBeaute52:null,
imgBeaute52:null,

prixBeaute53:null,
ancienPrixBeaute53:null,
nomBeaute53:null,
uniteBeaute53:null,
gammeBeaute53:null,
imgBeaute53:null,

prixBeaute54:null,
ancienPrixBeaute54:null,
nomBeaute54:null,
uniteBeaute54:null,
gammeBeaute54:null,
imgBeaute54:null,

prixBeaute55:null,
ancienPrixBeaute55:null,
nomBeaute55:null,
uniteBeaute55:null,
gammeBeaute55:null,
imgBeaute55:null,

prixBeaute56:null,
ancienPrixBeaute56:null,
nomBeaute56:null,
uniteBeaute56:null,
gammeBeaute56:null,
imgBeaute56:null,

prixBeaute61:null,
ancienPrixBeaute61:null,
nomBeaute61:null,
uniteBeaute61:null,
gammeBeaute61:null,
imgBeaute61:null,

prixBeaute62:null,
ancienPrixBeaute62:null,
nomBeaute62:null,
uniteBeaute62:null,
gammeBeaute62:null,
imgBeaute62:null,

prixBeaute63:null,
ancienPrixBeaute63:null,
nomBeaute63:null,
uniteBeaute63:null,
gammeBeaute63:null,
imgBeaute63:null,

prixBeaute64:null,
ancienPrixBeaute64:null,
nomBeaute64:null,
uniteBeaute64:null,
gammeBeaute64:null,
imgBeaute64:null,


prixBeaute65:null,
ancienPrixBeaute65:null,
nomBeaute65:null,
uniteBeaute65:null,
gammeBeaute65:null,
imgBeaute65:null,

prixBeaute66:null,
ancienPrixBeaute66:null,
nomBeaute66:null,
uniteBeaute66:null,
gammeBeaute66:null,
imgBeaute66:null,

prixBeaute71:null,
ancienPrixBeaute71:null,
nomBeaute71:null,
uniteBeaute71:null,
gammeBeaute71:null,
imgBeaute71:null,

prixBeaute72:null,
ancienPrixBeaute72:null,
nomBeaute72:null,
uniteBeaute72:null,
gammeBeaute72:null,
imgBeaute72:null,

prixBeaute73:null,
ancienPrixBeaute73:null,
nomBeaute73:null,
uniteBeaute73:null,
gammeBeaute73:null,
imgBeaute73:null,

prixBeaute74:null,
ancienPrixBeaute74:null,
nomBeaute74:null,
uniteBeaute74:null,
gammeBeaute74:null,
imgBeaute74:null,

prixBeaute75:null,
ancienPrixBeaute75:null,
nomBeaute75:null,
uniteBeaute75:null,
gammeBeaute75:null,
imgBeaute75:null,

prixBeaute76:null,
ancienPrixBeaute76:null,
nomBeaute76:null,
uniteBeaute76:null,
gammeBeaute76:null,
imgBeaute76:null,

prixBeaute81:null,
ancienPrixBeaute81:null,
nomBeaute81:null,
uniteBeaute81:null,
gammeBeaute81:null,
imgBeaute81:null,

prixBeaute82:null,
ancienPrixBeaute82:null,
nomBeaute82:null,
uniteBeaute82:null,
gammeBeaute82:null,
imgBeaute82:null,

prixBeaute83:null,
ancienPrixBeaute83:null,
nomBeaute83:null,
uniteBeaute83:null,
gammeBeaute83:null,
imgBeaute83:null,

prixBeaute84:null,
ancienPrixBeaute84:null,
nomBeaute84:null,
uniteBeaute84:null,
gammeBeaute84:null,
imgBeaute84:null,

prixBeaute85:null,
ancienPrixBeaute85:null,
nomBeaute85:null,
uniteBeaute85:null,
gammeBeaute85:null,
imgBeaute85:null,

prixBeaute86:null,
ancienPrixBeaute86:null,
nomBeaute86:null,
uniteBeaute86:null,
gammeBeaute86:null,
imgBeaute86:null,

prixBeaute91:null,
ancienPrixBeaute91:null,
nomBeaute91:null,
uniteBeaute91:null,
gammeBeaute91:null,
imgBeaute91:null,

prixBeaute92:null,
ancienPrixBeaute92:null,
nomBeaute92:null,
uniteBeaute92:null,
gammeBeaute92:null,
imgBeaute92:null,

prixBeaute93:null,
ancienPrixBeaute93:null,
nomBeaute93:null,
uniteBeaute93:null,
gammeBeaute93:null,
imgBeaute93:null,

prixBeaute94:null,
ancienPrixBeaute94:null,
nomBeaute94:null,
uniteBeaute94:null,
gammeBeaute94:null,
imgBeaute94:null,

prixBeaute95:null,
ancienPrixBeaute95:null,
nomBeaute95:null,
uniteBeaute95:null,
gammeBeaute95:null,
imgBeaute95:null,

prixBeaute96:null,
ancienPrixBeaute96:null,
nomBeaute96:null,
uniteBeaute96:null,
gammeBeaute96:null,
imgBeaute96:null,

prixBeaute101:null,
ancienPrixBeaute101:null,
nomBeaute101:null,
uniteBeaute101:null,
gammeBeaute101:null,
imgBeaute101:null,

prixBeaute102:null,
ancienPrixBeaute102:null,
nomBeaute102:null,
uniteBeaute102:null,
gammeBeaute102:null,
imgBeaute102:null,

prixBeaute103:null,
ancienPrixBeaute103:null,
nomBeaute103:null,
uniteBeaute103:null,
gammeBeaute103:null,
imgBeaute103:null,

prixBeaute104:null,
ancienPrixBeaute104:null,
nomBeaute104:null,
uniteBeaute104:null,
gammeBeaute104:null,
imgBeaute104:null,

prixBeaute105:null,
ancienPrixBeaute105:null,
nomBeaute105:null,
uniteBeaute105:null,
gammeBeaute105:null,
imgBeaute105:null,


prixBeaute106:null,
ancienPrixBeaute106:null,
nomBeaute106:null,
uniteBeaute106:null,
gammeBeaute106:null,
imgBeaute106:null,

prixBeaute111:null,
ancienPrixBeaute111:null,
nomBeaute111:null,
uniteBeaute111:null,
gammeBeaute111:null,
imgBeaute111:null,


prixBeaute112:null,
ancienPrixBeaute112:null,
nomBeaute112:null,
uniteBeaute112:null,
gammeBeaute112:null,
imgBeaute112:null,


prixBeaute113:null,
ancienPrixBeaute113:null,
nomBeaute113:null,
uniteBeaute113:null,
gammeBeaute113:null,
imgBeaute113:null,


prixBeaute114:null,
ancienPrixBeaute114:null,
nomBeaute114:null,
uniteBeaute114:null,
gammeBeaute114:null,
imgBeaute114:null,

prixBeaute115:null,
ancienPrixBeaute115:null,
nomBeaute115:null,
uniteBeaute115:null,
gammeBeaute115:null,
imgBeaute115:null,


prixBeaute116:null,
ancienPrixBeaute116:null,
nomBeaute116:null,
uniteBeaute116:null,
gammeBeaute116:null,
imgBeaute116:null,

prixBeaute121:null,
ancienPrixBeaute121:null,
nomBeaute121:null,
uniteBeaute121:null,
gammeBeaute121:null,
imgBeaute121:null,

prixBeaute122:null,
ancienPrixBeaute122:null,
nomBeaute122:null,
uniteBeaute122:null,
gammeBeaute122:null,
imgBeaute122:null,

prixBeaute123:null,
ancienPrixBeaute123:null,
nomBeaute123:null,
uniteBeaute123:null,
gammeBeaute123:null,
imgBeaute123:null,

prixBeaute124:null,
ancienPrixBeaute124:null,
nomBeaute124:null,
uniteBeaute124:null,
gammeBeaute124:null,
imgBeaute124:null,

prixBeaute125:null,
ancienPrixBeaute125:null,
nomBeaute125:null,
uniteBeaute125:null,
gammeBeaute125:null,
imgBeaute125:null,

prixBeaute126:null,
ancienPrixBeaute126:null,
nomBeaute126:null,
uniteBeaute126:null,
gammeBeaute126:null,
imgBeaute126:null,




//Beaute

//bebe
prixBebe11:null,
ancienPrixBebe11:null,
nomBebe11:null,
uniteBebe11:null,
gammeBebe11:null,
imgBebe11:null,

prixBebe12:null,
ancienPrixBebe12:null,
nomBebe12:null,
uniteBebe12:null,
gammeBebe12:null,
imgBebe12:null,

prixBebe13:null,
ancienPrixBebe13:null,
nomBebe13:null,
uniteBebe13:null,
gammeBebe13:null,
imgBebe13:null,

prixBebe14:null,
ancienPrixBebe14:null,
nomBebe14:null,
uniteBebe14:null,
gammeBebe14:null,
imgBebe14:null,

prixBebe15:null,
ancienPrixBebe15:null,
nomBebe15:null,
uniteBebe15:null,
gammeBebe15:null,
imgBebe15:null,

prixBebe16:null,
ancienPrixBebe16:null,
nomBebe16:null,
uniteBebe16:null,
gammeBebe16:null,
imgBebe16:null,

prixBebe21:null,
ancienPrixBebe21:null,
nomBebe21:null,
uniteBebe21:null,
gammeBebe21:null,
imgBebe21:null,

prixBebe22:null,
ancienPrixBebe22:null,
nomBebe22:null,
uniteBebe22:null,
gammeBebe22:null,
imgBebe22:null,

prixBebe23:null,
ancienPrixBebe23:null,
nomBebe23:null,
uniteBebe23:null,
gammeBebe23:null,
imgBebe23:null,

prixBebe24:null,
ancienPrixBebe24:null,
nomBebe24:null,
uniteBebe24:null,
gammeBebe24:null,
imgBebe24:null,

prixBebe25:null,
ancienPrixBebe25:null,
nomBebe25:null,
uniteBebe25:null,
gammeBebe25:null,
imgBebe25:null,

prixBebe26:null,
ancienPrixBebe26:null,
nomBebe26:null,
uniteBebe26:null,
gammeBebe26:null,
imgBebe26:null,

prixBebe31:null,
ancienPrixBebe31:null,
nomBebe31:null,
uniteBebe31:null,
gammeBebe31:null,
imgBebe31:null,

prixBebe32:null,
ancienPrixBebe32:null,
nomBebe32:null,
uniteBebe32:null,
gammeBebe32:null,
imgBebe32:null,

prixBebe33:null,
ancienPrixBebe33:null,
nomBebe33:null,
uniteBebe33:null,
gammeBebe33:null,
imgBebe33:null,

prixBebe34:null,
ancienPrixBebe34:null,
nomBebe34:null,
uniteBebe34:null,
gammeBebe34:null,
imgBebe34:null,

prixBebe35:null,
ancienPrixBebe35:null,
nomBebe35:null,
uniteBebe35:null,
gammeBebe35:null,
imgBebe35:null,

prixBebe36:null,
ancienPrixBebe36:null,
nomBebe36:null,
uniteBebe36:null,
gammeBebe36:null,
imgBebe36:null,

prixBebe41:null,
ancienPrixBebe41:null,
nomBebe41:null,
uniteBebe41:null,
gammeBebe41:null,
imgBebe41:null,

prixBebe42:null,
ancienPrixBebe42:null,
nomBebe42:null,
uniteBebe42:null,
gammeBebe42:null,
imgBebe42:null,

prixBebe43:null,
ancienPrixBebe43:null,
nomBebe43:null,
uniteBebe43:null,
gammeBebe43:null,
imgBebe43:null,

prixBebe44:null,
ancienPrixBebe44:null,
nomBebe44:null,
uniteBebe44:null,
gammeBebe44:null,
imgBebe44:null,

prixBebe45:null,
ancienPrixBebe45:null,
nomBebe45:null,
uniteBebe45:null,
gammeBebe45:null,
imgBebe45:null,

prixBebe46:null,
ancienPrixBebe46:null,
nomBebe46:null,
uniteBebe46:null,
gammeBebe46:null,
imgBebe46:null,

prixBebe51:null,
ancienPrixBebe51:null,
nomBebe51:null,
uniteBebe51:null,
gammeBebe51:null,
imgBebe51:null,

prixBebe52:null,
ancienPrixBebe52:null,
nomBebe52:null,
uniteBebe52:null,
gammeBebe52:null,
imgBebe52:null,

prixBebe53:null,
ancienPrixBebe53:null,
nomBebe53:null,
uniteBebe53:null,
gammeBebe53:null,
imgBebe53:null,

prixBebe54:null,
ancienPrixBebe54:null,
nomBebe54:null,
uniteBebe54:null,
gammeBebe54:null,
imgBebe54:null,

prixBebe55:null,
ancienPrixBebe55:null,
nomBebe55:null,
uniteBebe55:null,
gammeBebe55:null,
imgBebe55:null,

prixBebe56:null,
ancienPrixBebe56:null,
nomBebe56:null,
uniteBebe56:null,
gammeBebe56:null,
imgBebe56:null,

prixBebe61:null,
ancienPrixBebe61:null,
nomBebe61:null,
uniteBebe61:null,
gammeBebe61:null,
imgBebe61:null,

prixBebe62:null,
ancienPrixBebe62:null,
nomBebe62:null,
uniteBebe62:null,
gammeBebe62:null,
imgBebe62:null,

prixBebe63:null,
ancienPrixBebe63:null,
nomBebe63:null,
uniteBebe63:null,
gammeBebe63:null,
imgBebe63:null,

prixBebe64:null,
ancienPrixBebe64:null,
nomBebe64:null,
uniteBebe64:null,
gammeBebe64:null,
imgBebe64:null,

prixBebe65:null,
ancienPrixBebe65:null,
nomBebe65:null,
uniteBebe65:null,
gammeBebe65:null,
imgBebe65:null,

prixBebe66:null,
ancienPrixBebe66:null,
nomBebe66:null,
uniteBebe66:null,
gammeBebe66:null,
imgBebe66:null,

prixBebe71:null,
ancienPrixBebe71:null,
nomBebe71:null,
uniteBebe71:null,
gammeBebe71:null,
imgBebe71:null,

prixBebe72:null,
ancienPrixBebe72:null,
nomBebe72:null,
uniteBebe72:null,
gammeBebe72:null,
imgBebe72:null,

prixBebe73:null,
ancienPrixBebe73:null,
nomBebe73:null,
uniteBebe73:null,
gammeBebe73:null,
imgBebe73:null,

prixBebe74:null,
ancienPrixBebe74:null,
nomBebe74:null,
uniteBebe74:null,
gammeBebe74:null,
imgBebe74:null,

prixBebe75:null,
ancienPrixBebe75:null,
nomBebe75:null,
uniteBebe75:null,
gammeBebe75:null,
imgBebe75:null,

prixBebe76:null,
ancienPrixBebe76:null,
nomBebe76:null,
uniteBebe76:null,
gammeBebe76:null,
imgBebe76:null,


prixBebe81:null,
ancienPrixBebe81:null,
nomBebe81:null,
uniteBebe81:null,
gammeBebe81:null,
imgBebe81:null,


prixBebe82:null,
ancienPrixBebe82:null,
nomBebe82:null,
uniteBebe82:null,
gammeBebe82:null,
imgBebe82:null,


prixBebe83:null,
ancienPrixBebe83:null,
nomBebe83:null,
uniteBebe83:null,
gammeBebe83:null,
imgBebe83:null,


prixBebe84:null,
ancienPrixBebe84:null,
nomBebe84:null,
uniteBebe84:null,
gammeBebe84:null,
imgBebe84:null,


prixBebe85:null,
ancienPrixBebe85:null,
nomBebe85:null,
uniteBebe85:null,
gammeBebe85:null,
imgBebe85:null,


prixBebe86:null,
ancienPrixBebe86:null,
nomBebe86:null,
uniteBebe86:null,
gammeBebe86:null,
imgBebe86:null,

prixBebe91:null,
ancienPrixBebe91:null,
nomBebe91:null,
uniteBebe91:null,
gammeBebe91:null,
imgBebe91:null,

prixBebe92:null,
ancienPrixBebe92:null,
nomBebe92:null,
uniteBebe92:null,
gammeBebe92:null,
imgBebe92:null,

prixBebe93:null,
ancienPrixBebe93:null,
nomBebe93:null,
uniteBebe93:null,
gammeBebe93:null,
imgBebe93:null,

prixBebe94:null,
ancienPrixBebe94:null,
nomBebe94:null,
uniteBebe94:null,
gammeBebe94:null,
imgBebe94:null,

prixBebe95:null,
ancienPrixBebe95:null,
nomBebe95:null,
uniteBebe95:null,
gammeBebe95:null,
imgBebe95:null,

prixBebe96:null,
ancienPrixBebe96:null,
nomBebe96:null,
uniteBebe96:null,
gammeBebe96:null,
imgBebe96:null,

prixBebe101:null,
ancienPrixBebe101:null,
nomBebe101:null,
uniteBebe101:null,
gammeBebe101:null,
imgBebe101:null,

prixBebe102:null,
ancienPrixBebe102:null,
nomBebe102:null,
uniteBebe102:null,
gammeBebe102:null,
imgBebe102:null,

prixBebe103:null,
ancienPrixBebe103:null,
nomBebe103:null,
uniteBebe103:null,
gammeBebe103:null,
imgBebe103:null,

prixBebe104:null,
ancienPrixBebe104:null,
nomBebe104:null,
uniteBebe104:null,
gammeBebe104:null,
imgBebe104:null,

prixBebe105:null,
ancienPrixBebe105:null,
nomBebe105:null,
uniteBebe105:null,
gammeBebe105:null,
imgBebe105:null,

prixBebe106:null,
ancienPrixBebe106:null,
nomBebe106:null,
uniteBebe106:null,
gammeBebe106:null,
imgBebe106:null,

prixBebe111:null,
ancienPrixBebe111:null,
nomBebe111:null,
uniteBebe111:null,
gammeBebe111:null,
imgBebe111:null,

prixBebe112:null,
ancienPrixBebe112:null,
nomBebe112:null,
uniteBebe112:null,
gammeBebe112:null,
imgBebe112:null,

prixBebe113:null,
ancienPrixBebe113:null,
nomBebe113:null,
uniteBebe113:null,
gammeBebe113:null,
imgBebe113:null,

prixBebe114:null,
ancienPrixBebe114:null,
nomBebe114:null,
uniteBebe114:null,
gammeBebe114:null,
imgBebe114:null,

prixBebe115:null,
ancienPrixBebe115:null,
nomBebe115:null,
uniteBebe115:null,
gammeBebe115:null,
imgBebe115:null,

prixBebe116:null,
ancienPrixBebe116:null,
nomBebe116:null,
uniteBebe116:null,
gammeBebe116:null,
imgBebe116:null,

prixBebe121:null,
ancienPrixBebe121:null,
nomBebe121:null,
uniteBebe121:null,
gammeBebe121:null,
imgBebe121:null,

prixBebe122:null,
ancienPrixBebe122:null,
nomBebe122:null,
uniteBebe122:null,
gammeBebe122:null,
imgBebe122:null,

prixBebe123:null,
ancienPrixBebe123:null,
nomBebe123:null,
uniteBebe123:null,
gammeBebe123:null,
imgBebe123:null,

prixBebe124:null,
ancienPrixBebe124:null,
nomBebe124:null,
uniteBebe124:null,
gammeBebe124:null,
imgBebe124:null,

prixBebe125:null,
ancienPrixBebe125:null,
nomBebe125:null,
uniteBebe125:null,
gammeBebe125:null,
imgBebe125:null,

prixBebe126:null,
ancienPrixBebe126:null,
nomBebe126:null,
uniteBebe126:null,
gammeBebe126:null,
imgBebe126:null,
//bebe

//bienETRE
prixBienetre11:null,
ancienPrixBienetre11:null,
nomBienetre11:null,
uniteBienetre11:null,
gammeBienetre11:null,
imgBienetre11:null,

prixBienetre12:null,
ancienPrixBienetre12:null,
nomBienetre12:null,
uniteBienetre12:null,
gammeBienetre12:null,
imgBienetre12:null,

prixBienetre13:null,
ancienPrixBienetre13:null,
nomBienetre13:null,
uniteBienetre13:null,
gammeBienetre13:null,
imgBienetre13:null,

prixBienetre14:null,
ancienPrixBienetre14:null,
nomBienetre14:null,
uniteBienetre14:null,
gammeBienetre14:null,
imgBienetre14:null,

prixBienetre15:null,
ancienPrixBienetre15:null,
nomBienetre15:null,
uniteBienetre15:null,
gammeBienetre15:null,
imgBienetre15:null,

prixBienetre16:null,
ancienPrixBienetre16:null,
nomBienetre16:null,
uniteBienetre16:null,
gammeBienetre16:null,
imgBienetre16:null,

prixBienetre21:null,
ancienPrixBienetre21:null,
nomBienetre21:null,
uniteBienetre21:null,
gammeBienetre21:null,
imgBienetre21:null,

prixBienetre22:null,
ancienPrixBienetre22:null,
nomBienetre22:null,
uniteBienetre22:null,
gammeBienetre22:null,
imgBienetre22:null,

prixBienetre23:null,
ancienPrixBienetre23:null,
nomBienetre23:null,
uniteBienetre23:null,
gammeBienetre23:null,
imgBienetre23:null,

prixBienetre24:null,
ancienPrixBienetre24:null,
nomBienetre24:null,
uniteBienetre24:null,
gammeBienetre24:null,
imgBienetre24:null,

prixBienetre25:null,
ancienPrixBienetre25:null,
nomBienetre25:null,
uniteBienetre25:null,
gammeBienetre25:null,
imgBienetre25:null,

prixBienetre26:null,
ancienPrixBienetre26:null,
nomBienetre26:null,
uniteBienetre26:null,
gammeBienetre26:null,
imgBienetre26:null,

prixBienetre31:null,
ancienPrixBienetre31:null,
nomBienetre31:null,
uniteBienetre31:null,
gammeBienetre31:null,
imgBienetre31:null,

prixBienetre32:null,
ancienPrixBienetre32:null,
nomBienetre32:null,
uniteBienetre32:null,
gammeBienetre32:null,
imgBienetre32:null,

prixBienetre33:null,
ancienPrixBienetre33:null,
nomBienetre33:null,
uniteBienetre33:null,
gammeBienetre33:null,
imgBienetre33:null,

prixBienetre34:null,
ancienPrixBienetre34:null,
nomBienetre34:null,
uniteBienetre34:null,
gammeBienetre34:null,
imgBienetre34:null,

prixBienetre35:null,
ancienPrixBienetre35:null,
nomBienetre35:null,
uniteBienetre35:null,
gammeBienetre35:null,
imgBienetre35:null,

prixBienetre36:null,
ancienPrixBienetre36:null,
nomBienetre36:null,
uniteBienetre36:null,
gammeBienetre36:null,
imgBienetre36:null,

prixBienetre41:null,
ancienPrixBienetre41:null,
nomBienetre41:null,
uniteBienetre41:null,
gammeBienetre41:null,
imgBienetre41:null,

prixBienetre42:null,
ancienPrixBienetre42:null,
nomBienetre42:null,
uniteBienetre42:null,
gammeBienetre42:null,
imgBienetre42:null,

prixBienetre43:null,
ancienPrixBienetre43:null,
nomBienetre43:null,
uniteBienetre43:null,
gammeBienetre43:null,
imgBienetre43:null,

prixBienetre44:null,
ancienPrixBienetre44:null,
nomBienetre44:null,
uniteBienetre44:null,
gammeBienetre44:null,
imgBienetre44:null,

prixBienetre45:null,
ancienPrixBienetre45:null,
nomBienetre45:null,
uniteBienetre45:null,
gammeBienetre45:null,
imgBienetre45:null,


prixBienetre46:null,
ancienPrixBienetre46:null,
nomBienetre46:null,
uniteBienetre46:null,
gammeBienetre46:null,
imgBienetre46:null,

prixBienetre51:null,
ancienPrixBienetre51:null,
nomBienetre51:null,
uniteBienetre51:null,
gammeBienetre51:null,
imgBienetre51:null,

prixBienetre52:null,
ancienPrixBienetre52:null,
nomBienetre52:null,
uniteBienetre52:null,
gammeBienetre52:null,
imgBienetre52:null,

prixBienetre53:null,
ancienPrixBienetre53:null,
nomBienetre53:null,
uniteBienetre53:null,
gammeBienetre53:null,
imgBienetre53:null,

prixBienetre54:null,
ancienPrixBienetre54:null,
nomBienetre54:null,
uniteBienetre54:null,
gammeBienetre54:null,
imgBienetre54:null,

prixBienetre55:null,
ancienPrixBienetre55:null,
nomBienetre55:null,
uniteBienetre55:null,
gammeBienetre55:null,
imgBienetre55:null,

prixBienetre56:null,
ancienPrixBienetre56:null,
nomBienetre56:null,
uniteBienetre56:null,
gammeBienetre56:null,
imgBienetre56:null,

prixBienetre61:null,
ancienPrixBienetre61:null,
nomBienetre61:null,
uniteBienetre61:null,
gammeBienetre61:null,
imgBienetre61:null,

prixBienetre62:null,
ancienPrixBienetre62:null,
nomBienetre62:null,
uniteBienetre62:null,
gammeBienetre62:null,
imgBienetre62:null,

prixBienetre63:null,
ancienPrixBienetre63:null,
nomBienetre63:null,
uniteBienetre63:null,
gammeBienetre63:null,
imgBienetre63:null,

prixBienetre64:null,
ancienPrixBienetre64:null,
nomBienetre64:null,
uniteBienetre64:null,
gammeBienetre64:null,
imgBienetre64:null,

prixBienetre65:null,
ancienPrixBienetre65:null,
nomBienetre65:null,
uniteBienetre65:null,
gammeBienetre65:null,
imgBienetre65:null,

prixBienetre66:null,
ancienPrixBienetre66:null,
nomBienetre66:null,
uniteBienetre66:null,
gammeBienetre66:null,
imgBienetre66:null,

prixBienetre71:null,
ancienPrixBienetre71:null,
nomBienetre71:null,
uniteBienetre71:null,
gammeBienetre71:null,
imgBienetre71:null,

prixBienetre72:null,
ancienPrixBienetre72:null,
nomBienetre72:null,
uniteBienetre72:null,
gammeBienetre72:null,
imgBienetre72:null,

prixBienetre73:null,
ancienPrixBienetre73:null,
nomBienetre73:null,
uniteBienetre73:null,
gammeBienetre73:null,
imgBienetre73:null,

prixBienetre74:null,
ancienPrixBienetre74:null,
nomBienetre74:null,
uniteBienetre74:null,
gammeBienetre74:null,
imgBienetre74:null,

prixBienetre75:null,
ancienPrixBienetre75:null,
nomBienetre75:null,
uniteBienetre75:null,
gammeBienetre75:null,
imgBienetre75:null,

prixBienetre76:null,
ancienPrixBienetre76:null,
nomBienetre76:null,
uniteBienetre76:null,
gammeBienetre76:null,
imgBienetre76:null,


prixBienetre81:null,
ancienPrixBienetre81:null,
nomBienetre81:null,
uniteBienetre81:null,
gammeBienetre81:null,
imgBienetre81:null,

prixBienetre82:null,
ancienPrixBienetre82:null,
nomBienetre82:null,
uniteBienetre82:null,
gammeBienetre82:null,
imgBienetre82:null,

prixBienetre83:null,
ancienPrixBienetre83:null,
nomBienetre83:null,
uniteBienetre83:null,
gammeBienetre83:null,
imgBienetre83:null,

prixBienetre84:null,
ancienPrixBienetre84:null,
nomBienetre84:null,
uniteBienetre84:null,
gammeBienetre84:null,
imgBienetre84:null,

prixBienetre85:null,
ancienPrixBienetre85:null,
nomBienetre85:null,
uniteBienetre85:null,
gammeBienetre85:null,
imgBienetre85:null,

prixBienetre86:null,
ancienPrixBienetre86:null,
nomBienetre86:null,
uniteBienetre86:null,
gammeBienetre86:null,
imgBienetre86:null,

prixBienetre91:null,
ancienPrixBienetre91:null,
nomBienetre91:null,
uniteBienetre91:null,
gammeBienetre91:null,
imgBienetre91:null,

prixBienetre92:null,
ancienPrixBienetre92:null,
nomBienetre92:null,
uniteBienetre92:null,
gammeBienetre92:null,
imgBienetre92:null,

prixBienetre93:null,
ancienPrixBienetre93:null,
nomBienetre93:null,
uniteBienetre93:null,
gammeBienetre93:null,
imgBienetre93:null,

prixBienetre94:null,
ancienPrixBienetre94:null,
nomBienetre94:null,
uniteBienetre94:null,
gammeBienetre94:null,
imgBienetre94:null,

prixBienetre95:null,
ancienPrixBienetre95:null,
nomBienetre95:null,
uniteBienetre95:null,
gammeBienetre95:null,
imgBienetre95:null,

prixBienetre96:null,
ancienPrixBienetre96:null,
nomBienetre96:null,
uniteBienetre96:null,
gammeBienetre96:null,
imgBienetre96:null,

prixBienetre101:null,
ancienPrixBienetre101:null,
nomBienetre101:null,
uniteBienetre101:null,
gammeBienetre101:null,
imgBienetre101:null,

prixBienetre102:null,
ancienPrixBienetre102:null,
nomBienetre102:null,
uniteBienetre102:null,
gammeBienetre102:null,
imgBienetre102:null,

prixBienetre103:null,
ancienPrixBienetre103:null,
nomBienetre103:null,
uniteBienetre103:null,
gammeBienetre103:null,
imgBienetre103:null,

prixBienetre104:null,
ancienPrixBienetre104:null,
nomBienetre104:null,
uniteBienetre104:null,
gammeBienetre104:null,
imgBienetre104:null,

prixBienetre105:null,
ancienPrixBienetre105:null,
nomBienetre105:null,
uniteBienetre105:null,
gammeBienetre105:null,
imgBienetre105:null,

prixBienetre106:null,
ancienPrixBienetre106:null,
nomBienetre106:null,
uniteBienetre106:null,
gammeBienetre106:null,
imgBienetre106:null,

prixBienetre111:null,
ancienPrixBienetre111:null,
nomBienetre111:null,
uniteBienetre111:null,
gammeBienetre111:null,
imgBienetre111:null,

prixBienetre112:null,
ancienPrixBienetre112:null,
nomBienetre112:null,
uniteBienetre112:null,
gammeBienetre112:null,
imgBienetre112:null,

prixBienetre113:null,
ancienPrixBienetre113:null,
nomBienetre113:null,
uniteBienetre113:null,
gammeBienetre113:null,
imgBienetre113:null,

prixBienetre114:null,
ancienPrixBienetre114:null,
nomBienetre114:null,
uniteBienetre114:null,
gammeBienetre114:null,
imgBienetre114:null,

prixBienetre115:null,
ancienPrixBienetre115:null,
nomBienetre115:null,
uniteBienetre115:null,
gammeBienetre115:null,
imgBienetre115:null,

prixBienetre116:null,
ancienPrixBienetre116:null,
nomBienetre116:null,
uniteBienetre116:null,
gammeBienetre116:null,
imgBienetre116:null,

prixBienetre121:null,
ancienPrixBienetre121:null,
nomBienetre121:null,
uniteBienetre121:null,
gammeBienetre121:null,
imgBienetre121:null,

prixBienetre122:null,
ancienPrixBienetre122:null,
nomBienetre122:null,
uniteBienetre122:null,
gammeBienetre122:null,
imgBienetre122:null,

prixBienetre123:null,
ancienPrixBienetre123:null,
nomBienetre123:null,
uniteBienetre123:null,
gammeBienetre123:null,
imgBienetre123:null,

prixBienetre124:null,
ancienPrixBienetre124:null,
nomBienetre124:null,
uniteBienetre124:null,
gammeBienetre124:null,
imgBienetre124:null,

prixBienetre125:null,
ancienPrixBienetre125:null,
nomBienetre125:null,
uniteBienetre125:null,
gammeBienetre125:null,
imgBienetre125:null,

prixBienetre126:null,
ancienPrixBienetre126:null,
nomBienetre126:null,
uniteBienetre126:null,
gammeBienetre126:null,
imgBienetre126:null,
//bienEtre


};

public filteredList5;
  productForm:FormGroup;

  constructor(
    private _formBuilder: FormBuilder,
    private storageService:StorageService,
    private fb:FormBuilder,
    private produitService:ProduitService,
    public dialog:MatDialog,
    private data:DataService,
    private flyerService:flyerService,
    private spinner:NgxSpinnerService,
    private activatedRoute:ActivatedRoute,
    private configService:configurationsService,
    private pharmacieService:PharmacieService,
    private historiqueService:HistoriqueLeafletService,
    private  dataService:DataService,
    private http:HttpClient
    ) { }

  ngOnInit() {

   const count=Object.keys(this.formJson).length
   console.log(count)


    this.configs=this.storageService.read('configurations')
    this.historiqueLeaflet.userId=this.storageService.read('id')
    this.activatedRoute.params.subscribe(
      (params)=>{
        if (this.storageService.read('configBool')==true){
        this.configService.getUniqueConfiguration(parseInt(params.id)+1).subscribe(
     
        (res)=>{
          
          this.historiqueLeaflet.pagesContent.length=res.pageNumbers
          for(var i=0;i<res.pageNumbers;i++){
            this.historiqueLeaflet.pagesContent[i].pageNumber=i
          }
          
      
     console.log(res)
      this.tab=res.pages
      
         if (res.pages[0]){
           this.formJson.p1=res.pages[0].type
         }
         if (res.pages[1]){
          this.formJson.p2=res.pages[1].type
        }
        if (res.pages[2]){
          this.formJson.p3=res.pages[2].type
        }
        if (res.pages[3]){
          this.formJson.p4=res.pages[3].type
        }
        if (res.pages[4]){
          this.formJson.p5=res.pages[4].type
        }
        if (res.pages[5]){
          this.formJson.p6=res.pages[5].type
        }
        if (res.pages[6]){
          this.formJson.p7=res.pages[6].type
        }
        if (res.pages[7]){
          this.formJson.p8=res.pages[7].type
        }
        if (res.pages[8]){
          this.formJson.p9=res.pages[8].type
        }
        if (res.pages[9]){
          this.formJson.p10=res.pages[9].type
        }
        if (res.pages[10]){
          this.formJson.p11=res.pages[10].type
        }
        if (res.pages[11]){
          this.formJson.p12=res.pages[11].type
        }
        this.formJson.nbrPages=res.pageNumbers
    
        })
      }
      this.historiqueLeaflet.configLeaflet=parseInt(params.id)+1
      if (this.storageService.read('historiqueBool')==true){
     
        this.historiqueService.getUniqueHistorique(parseInt(params.id)+1).subscribe(
          (res)=>{
            
            console.log('single ', res)
            this.tab=res.configLeaflet.pages
          this.historiqueContent=res
          this.historyContent(res)
          
          }
        )
      }
        }
      
    )
  for(var i=0;i<120;i++){
    this.historiqueLeaflet.pagesContent.length=120;
    this.historiqueLeaflet.pagesContent[i]={pageNumber:0,value:[{key:'',value:''}]}
  }
    this.pharmacieService.getAllPharmacies().subscribe(
      res=>{
        console.log(res)
        this.pharmacies=res
        console.log(this.pharmacies)
        this.filteredList1=this.pharmacies.slice()
      })

    console.log(this.tab);
this.productForm=this.createProductForm();
this.produitService.getProduits().subscribe(
  produits=>{

    this.product=produits;
    console.log(produits);
  this.filteredList5= this.product.slice();

   }
)

  }
  tabChanged(tabChangeEvent: MatTabChangeEvent): void {

    this.selectedIndex = tabChangeEvent.index;}
    
  createProductForm(): FormGroup
  {
      return this._formBuilder.group({
          
          titre             :[null],
          icon              :[null],  
          month:[null],
          title:[null],
          subtitle:[null],
          tilte1:[null],
          subtitle1:[null],
          text:[''],
          date:[''],
        Bebe:[''],
          selectionAlaUne:[[]],
       BienEtre:[[]],
       Sante:[[]],
       pharmacie:[[]],
       Beaute:[[]],
       Mois:[[]],
       pagesOrdre:[[]],

    
       selection_Beaute: this.fb.array([this.fb.group({Beaute:''})]),
       selection_bienEtre:this.fb.array([this.fb.group({bienEtre:''})]),
       selection_Sante:this.fb.array([this.fb.group({Sante:''})]),
       selection_Bebe:this.fb.array([this.fb.group({Bebe:''})]),
       selling_points:this.fb.array([this.fb.group({point:''})]),
        events: this.fb.array([this.fb.group({event:''})]),
        nouveautes:this.fb.array([this.fb.group({nouveaute:''})]),
        conseils:this.fb.array([this.fb.group({})])

      
      });
  }
  historyContent(historique:any){
    for (var i=0;i<historique.pagesContent.length;i++){
      if (i==0){
        for(var j=0;j<historique.pagesContent[i].value.length;j++){
          if(historique.pagesContent[i].value[j].key=="mois"){
this.formJson.moisPharma1=historique.pagesContent[i].value[j].value
this.selectedMois[i]=historique.pagesContent[i].value[j].value
this.selectedMois.length++
          }
        

          
if(historique.pagesContent[i].value[j].key==`produitAlaune 0`){

 this.arrayUne[i].push(this.fb.group({Une:''}))
 console.log('in')
 this.produitAlaune[i]=JSON.parse(historique.pagesContent[i].value[j].value)
 this.produitAlaune.length++

}
  else if(historique.pagesContent[i].value[j].key==`produitAlaune ${j-2}`) {

                        this.arrayUne[i].push(this.fb.group({Une:''}))
                        this.produitAlaune.push(JSON.parse(historique.pagesContent[i].value[j].value))
                      
          }
        

                      if(historique.pagesContent[i].value[j].key==`produitBebe 0`){
                       
             this.arrayBebe[i].push(this.fb.group({Sante:''}))
             this.produitBebe[i]=JSON.parse(historique.pagesContent[i].value[j].value)
             this.produitBebe.length++
             }
            else if(historique.pagesContent[i].value[j].key==`produitBebe ${j}`){
                       
              this.arrayBebe[i].push(this.fb.group({Sante:''}))
              this.produitBebe.push(JSON.parse(historique.pagesContent[i].value[j].value))
              this.produitBebe.length++
             }
                                  if(historique.pagesContent[i].value[j].key==`produitbienEtre 0`){
                       
                                    this.arraybienEtre[i].push(this.fb.group({bienEtre:''}))
                               this.produitBienEtre[i]=JSON.parse(historique.pagesContent[i].value[j].value)
                               this.produitBienEtre.length++     
  }
                                  else if(historique.pagesContent[i].value[j].key==`produitbienEtre ${j}`){
                       
                                    this.arraybienEtre[i].push(this.fb.group({bienEtre:''}))
                               this.produitBienEtre.push(JSON.parse(historique.pagesContent[i].value[j].value))
                               this.produitBienEtre.length++     
  }

  if(historique.pagesContent[i].value[j].key==`produitBeaute 0`){
                       
    this.arrayBeaute[i].push(this.fb.group({Beaute:''}))
    this.produitBeaute[i]=JSON.parse(historique.pagesContent[i].value[j].value)
    this.produitBeaute.length++
} 
else if(historique.pagesContent[i].value[j].key==`produitBeaute ${j}`){
                       
  this.arrayBeaute[i].push(this.fb.group({Beaute:''}))
  this.produitBeaute[i].push(JSON.parse(historique.pagesContent[i].value[j].value))
  this.produitBeaute.length++
}    
                                                                                                              
                          
        if(historique.pagesContent[i].value[j].key==`produitSante 0`){
                       
     this.arraySante[i].push(this.fb.group({Bebe:''}))  
    this.produitSante[i]=JSON.parse(historique.pagesContent[i].value[j].value)
    this.produitSante.length++
    }  
   else if(historique.pagesContent[i].value[j].key==`produitSante ${j}`){
                       
      this.arraySante[i].push(this.fb.group({Bebe:''}))  
     this.produitSante.push(JSON.parse(historique.pagesContent[i].value[j].value))
     this.produitSante.length++
     }  

          if(historique.pagesContent[i].value[j].key=='titreNouveaute 0' ){
            this.formJson.titreNew11=historique.pagesContent[i].value[j].value
            this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
            
            this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
            this.Nouvaute.push({titre:'',soustitre:'',description:''})
          }
          if(historique.pagesContent[i].value[j].key=='titreNouveaute 1'){
            this.formJson.titreNew12=historique.pagesContent[i].value[j].value
            this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
            this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
            this.Nouvaute.push({titre:'',soustitre:'',description:''})
          }
          if(historique.pagesContent[i].value[j].key=='titreNouveaute 2'){
            this.formJson.titreNew13=historique.pagesContent[i].value[j].value
            this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
            this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
            this.Nouvaute.push({titre:'',soustitre:'',description:''})
          }
          if(historique.pagesContent[i].value[j].key=='titreNouveaute 3'){
            this.formJson.titreNew14=historique.pagesContent[i].value[j].value
            this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
            this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
            this.Nouvaute.push({titre:'',soustitre:'',description:''})
          }
      
          if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 0'){
            this.formJson.soustitreNew11=historique.pagesContent[i].value[j].value
            this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
  
          }
          if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 1'){
            this.formJson.soustitreNew12=historique.pagesContent[i].value[j].value
            this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
          
          }
          if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 2'){
            this.formJson.soustitreNew13=historique.pagesContent[i].value[j].value
            this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
          
          }
          if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 3'){
            this.formJson.soustitreNew14=historique.pagesContent[i].value[j].value
            this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
          
          }
          if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 0'){
            this.formJson.descriptionNew11=historique.pagesContent[i].value[j].value
            this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
          }
          if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 1'){
            this.formJson.descriptionNew12=historique.pagesContent[i].value[j].value
            this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
          }
          if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 2'){
            this.formJson.descriptionNew13=historique.pagesContent[i].value[j].value
            this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
          }
          if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 3'){
            this.formJson.descriptionNew14=historique.pagesContent[i].value[j].value
            this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
          }
          
          if(historique.pagesContent[i].value[j].key=='imgPub 0'){
            this.formJson.imgPub1=historique.pagesContent[i].value[j].value
            this.pubs[i]=historique.pagesContent[i].value[j].value
            this.pubs.length++
          }
          if(historique.pagesContent[i].value[j].key=='titreconseil 0'){
            this.formJson.titreConseil1=historique.pagesContent[i].value[j].value
            this.conseils[j].titre=historique.pagesContent[i].value[j].value
            this.conseils.push({titre:'',soustitre:'',description:''})
            
          }
          if(historique.pagesContent[i].value[j].key=='sous-titre Conseil 0'){
            this.formJson.soustitreConseil1=historique.pagesContent[i].value[j].value
            this.conseils[j].soustitre=historique.pagesContent[i].value[j].value
          }
          if(historique.pagesContent[i].value[j].key=='textConseil 0'){
            this.formJson.descriptionConseil1=historique.pagesContent[i].value[j].value
            this.conseils[j].description=historique.pagesContent[i].value[j].value
          }
          if(historique.pagesContent[i].value[j].key=='titreService 0'){
            this.formJson.titreService11=historique.pagesContent[i].value[j].value
            this.arrayConseil[i].push(this.fb.group({conseil:''}))
            this.Services[j].titre=historique.pagesContent[i].value[j].value
            this.Services.push({titre:'',soustitre:'',description:''})
          }
          if(historique.pagesContent[i].value[j].key=='soustitre Service 0'){
            this.formJson.soustitreService11=historique.pagesContent[i].value[j].value
            this.Services[j].soustitre=historique.pagesContent[i].value[j].value
          }
          if(historique.pagesContent[i].value[j].key=='ImageService 0'){
            this.formJson.imgService11=historique.pagesContent[i].value[j].value
            this.Services[j].description=historique.pagesContent[i].value[j].value
          }
    

          if(historique.pagesContent[i].value[j].key=='titreService 1'){
            this.formJson.titreService12=historique.pagesContent[i].value[j].value
            this.arrayConseil[i].push(this.fb.group({conseil:''}))
            this.Services[j].titre=historique.pagesContent[i].value[j].value
            this.Services.push({titre:'',soustitre:'',description:''})
          }
          if(historique.pagesContent[i].value[j].key=='soustitre Service 1'){
            this.formJson.soustitreService12=historique.pagesContent[i].value[j].value
            this.Services[j].soustitre=historique.pagesContent[i].value[j].value
          }
          if(historique.pagesContent[i].value[j].key=='ImageService 1'){
            this.formJson.imgService12=historique.pagesContent[i].value[j].value
            this.Services[j].description=historique.pagesContent[i].value[j].value
          }
          if(historique.pagesContent[i].value[j].key=='titreService 2'){
            this.formJson.titreService13=historique.pagesContent[i].value[j].value
            this.arrayConseil[i].push(this.fb.group({conseil:''}))
            this.Services[j].titre=historique.pagesContent[i].value[j].value
            this.Services.push({titre:'',soustitre:'',description:''})
          }
          if(historique.pagesContent[i].value[j].key=='soustitre Service 2'){
            this.formJson.soustitreService13=historique.pagesContent[i].value[j].value
            this.Services[j].soustitre=historique.pagesContent[i].value[j].value
          }
          if(historique.pagesContent[i].value[j].key=='ImageService 2'){
            this.formJson.imgService13=historique.pagesContent[i].value[j].value
            this.Services[j].description=historique.pagesContent[i].value[j].value
          }
          if(historique.pagesContent[i].value[j].key=='titreService 3'){
            this.formJson.titreService14=historique.pagesContent[i].value[j].value
            this.arrayConseil[i].push(this.fb.group({conseil:''}))
            this.Services[j].titre=historique.pagesContent[i].value[j].value
            this.Services.push({titre:'',soustitre:'',description:''})
          }
          if(historique.pagesContent[i].value[j].key=='soustitre Service 3'){
            this.formJson.soustitreService14=historique.pagesContent[i].value[j].value
            this.Services[j].soustitre=historique.pagesContent[i].value[j].value
          }
          if(historique.pagesContent[i].value[j].key=='ImageService 3'){
            this.formJson.imgService14=historique.pagesContent[i].value[j].value
            this.Services[j].description=historique.pagesContent[i].value[j].value
          }
        }
      
      }
      if (i==1){
        for(var j=0;j<historique.pagesContent[i].value.length;j++){
          if(historique.pagesContent[i].value[j].key=="mois"){
            this.formJson.moisPharma2=historique.pagesContent[i].value[j].value
            this.selectedMois[i]=historique.pagesContent[i].value[j].value
            this.selectedMois.length++
                      }
                    
            
                      
            if(historique.pagesContent[i].value[j].key==`produitAlaUne 0`){
            
             this.arrayUne[i].push(this.fb.group({Une:''}))
             this.produitAlaune[i]=JSON.parse(historique.pagesContent[i].value[j].value)
             
            
            }
              else if(historique.pagesContent[i].value[j].key==`produitAlaUne ${j-2}`) {
            
                                    this.arrayUne[i].push(this.fb.group({Une:''}))
                                    this.produitAlaune.push(JSON.parse(historique.pagesContent[i].value[j].value))
                                    
                      }
                    
            
                                  if(historique.pagesContent[i].value[j].key==`produitBebe 0`){
                                   
                         this.arrayBebe[i].push(this.fb.group({Sante:''}))
                         this.produitBebe[i]=JSON.parse(historique.pagesContent[i].value[j].value)
                         
                         }
                        else if(historique.pagesContent[i].value[j].key==`produitBebe ${j}`){
                                   
                          this.arrayBebe[i].push(this.fb.group({Sante:''}))
                          this.produitBebe.push(JSON.parse(historique.pagesContent[i].value[j].value))
                          
                         }
                                              if(historique.pagesContent[i].value[j].key==`produitbienEtre 0`){
                                   
                                                this.arraybienEtre[i].push(this.fb.group({bienEtre:''}))
                                           this.produitBienEtre[i]=JSON.parse(historique.pagesContent[i].value[j].value)
                                         
              }
                                              else if(historique.pagesContent[i].value[j].key==`produitbienEtre ${j}`){
                                   
                                                this.arraybienEtre[i].push(this.fb.group({bienEtre:''}))
                                           this.produitBienEtre.push(JSON.parse(historique.pagesContent[i].value[j].value))
                                         
              }
            
              if(historique.pagesContent[i].value[j].key==`produitBeaute 0`){
                                   
                this.arrayBeaute[i].push(this.fb.group({Beaute:''}))
                this.produitBeaute[i]=JSON.parse(historique.pagesContent[i].value[j].value)
            
            } 
            else if(historique.pagesContent[i].value[j].key==`produitBeaute ${j}`){
                                   
              this.arrayBeaute[i].push(this.fb.group({Beaute:''}))
              this.produitBeaute[i].push(JSON.parse(historique.pagesContent[i].value[j].value))
          
            }    
                                                                                                                          
                                      
                    if(historique.pagesContent[i].value[j].key==`produitSante 0`){
                                   
                 this.arraySante[i].push(this.fb.group({Bebe:''}))  
                this.produitSante[i]=JSON.parse(historique.pagesContent[i].value[j].value)
            
                }  
               else if(historique.pagesContent[i].value[j].key==`produitSante ${j}`){
                                   
                  this.arraySante[i].push(this.fb.group({Bebe:''}))  
                 this.produitSante.push(JSON.parse(historique.pagesContent[i].value[j].value))
             
                 }  
            
                      if(historique.pagesContent[i].value[j].key=='titreNouveaute 0'){
                        this.formJson.titreNew21=historique.pagesContent[i].value[j].value
                        this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
                        
                        this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
                        this.Nouvaute.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='titreNouveaute 1'){
                        this.formJson.titreNew22=historique.pagesContent[i].value[j].value
                        this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
                        this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
                        this.Nouvaute.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='titreNouveaute 2'){
                        this.formJson.titreNew23=historique.pagesContent[i].value[j].value
                        this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
                        this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
                        this.Nouvaute.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='titreNouveaute 3'){
                        this.formJson.titreNew24=historique.pagesContent[i].value[j].value
                        this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
                        this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
                        this.Nouvaute.push({titre:'',soustitre:'',description:''})
                      }
                  
                      if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 0'){
                        this.formJson.soustitreNew21=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
              
                      }
                      if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 1'){
                        this.formJson.soustitreNew22=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
                      
                      }
                      if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 2'){
                        this.formJson.soustitreNew23=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
                      
                      }
                      if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 3'){
                        this.formJson.soustitreNew24=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
                      
                      }
                      if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 0'){
                        this.formJson.descriptionNew21=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 1'){
                        this.formJson.descriptionNew22=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 2'){
                        this.formJson.descriptionNew23=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 3'){
                        this.formJson.descriptionNew24=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
                      }
                      
                      if(historique.pagesContent[i].value[j].key=='imgPub 1'){
                        this.formJson.imgPub2=historique.pagesContent[i].value[j].value
                        this.pubs[i]=historique.pagesContent[i].value[j].value
                        this.pubs.length++
                      }
                      if(historique.pagesContent[i].value[j].key=='titreconseil 1'){
                        this.formJson.titreConseil2=historique.pagesContent[i].value[j].value
                        this.conseils[j].titre=historique.pagesContent[i].value[j].value
                        this.conseils.push({titre:'',soustitre:'',description:''})
                        
                      }
                      if(historique.pagesContent[i].value[j].key=='sous-titre Conseil 1'){
                        this.formJson.soustitreConseil2=historique.pagesContent[i].value[j].value
                        this.conseils[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='textConseil 1'){
                        this.formJson.descriptionConseil2=historique.pagesContent[i].value[j].value
                        this.conseils[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='titreService 0'){
                        this.formJson.titreService21=historique.pagesContent[i].value[j].value
                        this.arrayConseil[i].push(this.fb.group({conseil:''}))
                        this.Services[j].titre=historique.pagesContent[i].value[j].value
                        this.Services.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='soustitre Service 0'){
                        this.formJson.soustitreService21=historique.pagesContent[i].value[j].value
                        this.Services[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='ImageService 0'){
                        this.formJson.imgService21=historique.pagesContent[i].value[j].value
                        this.Services[j].description=historique.pagesContent[i].value[j].value
                      }
                 
            
                      if(historique.pagesContent[i].value[j].key=='titreService 1'){
                        this.formJson.titreService22=historique.pagesContent[i].value[j].value
                        this.arrayConseil[i].push(this.fb.group({conseil:''}))
                        this.Services[j].titre=historique.pagesContent[i].value[j].value
                        this.Services.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='soustitre Service 1'){
                        this.formJson.soustitreService22=historique.pagesContent[i].value[j].value
                        this.Services[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='ImageService 1'){
                        this.formJson.imgService22=historique.pagesContent[i].value[j].value
                        this.Services[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='titreService 2'){
                        this.formJson.titreService23=historique.pagesContent[i].value[j].value
                        this.arrayConseil[i].push(this.fb.group({conseil:''}))
                        this.Services[j].titre=historique.pagesContent[i].value[j].value
                        this.Services.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='soustitre Service 2'){
                        this.formJson.soustitreService23=historique.pagesContent[i].value[j].value
                        this.Services[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='ImageService 2'){
                        this.formJson.imgService23=historique.pagesContent[i].value[j].value
                        this.Services[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='titreService 3'){
                        this.formJson.titreService24=historique.pagesContent[i].value[j].value
                        this.arrayConseil[i].push(this.fb.group({conseil:''}))
                        this.Services[j].titre=historique.pagesContent[i].value[j].value
                        this.Services.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='soustitre Service 3'){
                        this.formJson.soustitreService24=historique.pagesContent[i].value[j].value
                        this.Services[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='ImageService 3'){
                        this.formJson.imgService24=historique.pagesContent[i].value[j].value
                        this.Services[j].description=historique.pagesContent[i].value[j].value
                      }
        }
      
      }
      if (i==2){
        for(var j=0;j<historique.pagesContent[i].value.length;j++){
          if(historique.pagesContent[i].value[j].key=="mois"){
            this.formJson.moisPharma3=historique.pagesContent[i].value[j].value
            this.selectedMois[i]=historique.pagesContent[i].value[j].value
            this.selectedMois.length++
                      }
                    
            
                      
            if(historique.pagesContent[i].value[j].key==`produitAlaUne 0`){
            
             this.arrayUne[i].push(this.fb.group({Une:''}))
             this.produitAlaune[i]=JSON.parse(historique.pagesContent[i].value[j].value)
           
            
            }
              else if(historique.pagesContent[i].value[j].key==`produitAlaUne ${j-2}`) {
            
                                    this.arrayUne[i].push(this.fb.group({Une:''}))
                                    this.produitAlaune.push(JSON.parse(historique.pagesContent[i].value[j].value))
                                  
                      }
                    
            
                                  if(historique.pagesContent[i].value[j].key==`produitBebe 0`){
                                   
                         this.arrayBebe[i].push(this.fb.group({Sante:''}))
                         this.produitBebe[i]=JSON.parse(historique.pagesContent[i].value[j].value)
                        
                         }
                        else if(historique.pagesContent[i].value[j].key==`produitBebe ${j}`){
                                   
                          this.arrayBebe[i].push(this.fb.group({Sante:''}))
                          this.produitBebe.push(JSON.parse(historique.pagesContent[i].value[j].value))
                         
                         }
                                              if(historique.pagesContent[i].value[j].key==`produitbienEtre 0`){
                                   
                                                this.arraybienEtre[i].push(this.fb.group({bienEtre:''}))
                                           this.produitBienEtre[i]=JSON.parse(historique.pagesContent[i].value[j].value)
                                        
              }
                                              else if(historique.pagesContent[i].value[j].key==`produitbienEtre ${j}`){
                                   
                                                this.arraybienEtre[i].push(this.fb.group({bienEtre:''}))
                                           this.produitBienEtre.push(JSON.parse(historique.pagesContent[i].value[j].value))
                                        
              }
            
              if(historique.pagesContent[i].value[j].key==`produitBeaute 0`){
                                   
                this.arrayBeaute[i].push(this.fb.group({Beaute:''}))
                this.produitBeaute[i]=JSON.parse(historique.pagesContent[i].value[j].value)
            
            } 
            else if(historique.pagesContent[i].value[j].key==`produitBeaute ${j}`){
                                   
              this.arrayBeaute[i].push(this.fb.group({Beaute:''}))
              this.produitBeaute[i].push(JSON.parse(historique.pagesContent[i].value[j].value))
          
            }    
                                                                                                                          
                                      
                    if(historique.pagesContent[i].value[j].key==`produitSante 0`){
                                   
                 this.arraySante[i].push(this.fb.group({Bebe:''}))  
                this.produitSante[i]=JSON.parse(historique.pagesContent[i].value[j].value)
              
                }  
               else if(historique.pagesContent[i].value[j].key==`produitSante ${j}`){
                                   
                  this.arraySante[i].push(this.fb.group({Bebe:''}))  
                 this.produitSante.push(JSON.parse(historique.pagesContent[i].value[j].value))
               
                 }  
            
                      if(historique.pagesContent[i].value[j].key=='titreNouveaute 0'){
                        this.formJson.titreNew11=historique.pagesContent[i].value[j].value
                        this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
                        
                        this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
                        this.Nouvaute.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='titreNouveaute 1'){
                        this.formJson.titreNew32=historique.pagesContent[i].value[j].value
                        this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
                        this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
                        this.Nouvaute.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='titreNouveaute 2'){
                        this.formJson.titreNew33=historique.pagesContent[i].value[j].value
                        this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
                        this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
                        this.Nouvaute.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='titreNouveaute 3'){
                        this.formJson.titreNew34=historique.pagesContent[i].value[j].value
                        this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
                        this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
                        this.Nouvaute.push({titre:'',soustitre:'',description:''})
                      }
                  
                      if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 0'){
                        this.formJson.soustitreNew31=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
              
                      }
                      if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 1'){
                        this.formJson.soustitreNew32=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
                      
                      }
                      if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 2'){
                        this.formJson.soustitreNew33=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
                      
                      }
                      if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 3'){
                        this.formJson.soustitreNew34=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
                      
                      }
                      if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 0'){
                        this.formJson.descriptionNew31=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 1'){
                        this.formJson.descriptionNew32=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 2'){
                        this.formJson.descriptionNew33=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 3'){
                        this.formJson.descriptionNew34=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
                      }
                      
                      if(historique.pagesContent[i].value[j].key=='imgPub 2'){
                        this.formJson.imgPub3=historique.pagesContent[i].value[j].value
                        this.pubs[i]=historique.pagesContent[i].value[j].value
                        this.pubs.length++
                      }
                      if(historique.pagesContent[i].value[j].key=='titreconseil 2'){
                        this.formJson.titreConseil3=historique.pagesContent[i].value[j].value
                        this.conseils[j].titre=historique.pagesContent[i].value[j].value
                        this.conseils.push({titre:'',soustitre:'',description:''})
                        
                      }
                      if(historique.pagesContent[i].value[j].key=='sous-titre Conseil 2'){
                        this.formJson.soustitreConseil3=historique.pagesContent[i].value[j].value
                        this.conseils[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='textConseil 2'){
                        this.formJson.descriptionConseil3=historique.pagesContent[i].value[j].value
                        this.conseils[j].description=historique.pagesContent[i].value[j].value
                      }
                 
                      if(historique.pagesContent[i].value[j].key=='titreService 0'){
                        this.formJson.titreService31=historique.pagesContent[i].value[j].value
                        this.arrayConseil[i].push(this.fb.group({conseil:''}))
                        this.Services[j].titre=historique.pagesContent[i].value[j].value
                        this.Services.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='soustitre Service 0'){
                        this.formJson.soustitreService31=historique.pagesContent[i].value[j].value
                        this.Services[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='ImageService 0'){
                        this.formJson.imgService31=historique.pagesContent[i].value[j].value
                        this.Services[j].description=historique.pagesContent[i].value[j].value
                      }
            
                      if(historique.pagesContent[i].value[j].key=='titreService 1'){
                        this.formJson.titreService32=historique.pagesContent[i].value[j].value
                        this.arrayConseil[i].push(this.fb.group({conseil:''}))
                        this.Services[j].titre=historique.pagesContent[i].value[j].value
                        this.Services.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='soustitre Service 1'){
                        this.formJson.soustitreService32=historique.pagesContent[i].value[j].value
                        this.Services[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='ImageService 1'){
                        this.formJson.imgService32=historique.pagesContent[i].value[j].value
                        this.Services[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='titreService 2'){
                        this.formJson.titreService33=historique.pagesContent[i].value[j].value
                        this.arrayConseil[i].push(this.fb.group({conseil:''}))
                        this.Services[j].titre=historique.pagesContent[i].value[j].value
                        this.Services.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='soustitre Service 2'){
                        this.formJson.soustitreService33=historique.pagesContent[i].value[j].value
                        this.Services[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='ImageService 2'){
                        this.formJson.imgService33=historique.pagesContent[i].value[j].value
                        this.Services[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='titreService 3'){
                        this.formJson.titreService34=historique.pagesContent[i].value[j].value
                        this.arrayConseil[i].push(this.fb.group({conseil:''}))
                        this.Services[j].titre=historique.pagesContent[i].value[j].value
                        this.Services.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='soustitre Service 3'){
                        this.formJson.soustitreService34=historique.pagesContent[i].value[j].value
                        this.Services[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='ImageService 3'){
                        this.formJson.imgService34=historique.pagesContent[i].value[j].value
                        this.Services[j].description=historique.pagesContent[i].value[j].value
                      }
        
        
        }
      
      }
      if (i==3){
        for(var j=0;j<historique.pagesContent[i].value.length;j++){
          if(historique.pagesContent[i].value[j].key=="mois"){
            this.formJson.moisPharma4=historique.pagesContent[i].value[j].value
            this.selectedMois[i]=historique.pagesContent[i].value[j].value
            this.selectedMois.length++
                      }
                    
            
                      
            if(historique.pagesContent[i].value[j].key==`produitAlaUne 0`){
            
             this.arrayUne[i].push(this.fb.group({Une:''}))
             this.produitAlaune[i]=JSON.parse(historique.pagesContent[i].value[j].value)

            
            }
              else if(historique.pagesContent[i].value[j].key==`produitAlaUne ${j-2}`) {
            
                                    this.arrayUne[i].push(this.fb.group({Une:''}))
                                    this.produitAlaune.push(JSON.parse(historique.pagesContent[i].value[j].value))
                                   
                      }
                    
            
                                  if(historique.pagesContent[i].value[j].key==`produitBebe 0`){
                                   
                         this.arrayBebe[i].push(this.fb.group({Sante:''}))
                         this.produitBebe[i]=JSON.parse(historique.pagesContent[i].value[j].value)
                    
                         }
                        else if(historique.pagesContent[i].value[j].key==`produitBebe ${j}`){
                                   
                          this.arrayBebe[i].push(this.fb.group({Sante:''}))
                          this.produitBebe.push(JSON.parse(historique.pagesContent[i].value[j].value))
                     
                         }
                                              if(historique.pagesContent[i].value[j].key==`produitbienEtre 0`){
                                   
                                                this.arraybienEtre[i].push(this.fb.group({bienEtre:''}))
                                           this.produitBienEtre[i]=JSON.parse(historique.pagesContent[i].value[j].value)
                                          
              }
                                              else if(historique.pagesContent[i].value[j].key==`produitbienEtre ${j}`){
                                   
                                                this.arraybienEtre[i].push(this.fb.group({bienEtre:''}))
                                           this.produitBienEtre.push(JSON.parse(historique.pagesContent[i].value[j].value))
                                          
              }
            
              if(historique.pagesContent[i].value[j].key==`produitBeaute 0`){
                                   
                this.arrayBeaute[i].push(this.fb.group({Beaute:''}))
                this.produitBeaute[i]=JSON.parse(historique.pagesContent[i].value[j].value)
             
            } 
            else if(historique.pagesContent[i].value[j].key==`produitBeaute ${j}`){
                                   
              this.arrayBeaute[i].push(this.fb.group({Beaute:''}))
              this.produitBeaute[i].push(JSON.parse(historique.pagesContent[i].value[j].value))
           
            }    
                                                                                                                          
                                      
                    if(historique.pagesContent[i].value[j].key==`produitSante 0`){
                                   
                 this.arraySante[i].push(this.fb.group({Bebe:''}))  
                this.produitSante[i]=JSON.parse(historique.pagesContent[i].value[j].value)
               
                }  
               else if(historique.pagesContent[i].value[j].key==`produitSante ${j}`){
                                   
                  this.arraySante[i].push(this.fb.group({Bebe:''}))  
                 this.produitSante.push(JSON.parse(historique.pagesContent[i].value[j].value))
                
                 }  
            
                      if(historique.pagesContent[i].value[j].key=='titreNouveaute 0'){
                        this.formJson.titreNew41=historique.pagesContent[i].value[j].value
                        this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
                        
                        this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
                        this.Nouvaute.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='titreNouveaute 1'){
                        this.formJson.titreNew42=historique.pagesContent[i].value[j].value
                        this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
                        this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
                        this.Nouvaute.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='titreNouveaute 2'){
                        this.formJson.titreNew43=historique.pagesContent[i].value[j].value
                        this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
                        this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
                        this.Nouvaute.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='titreNouveaute 3'){
                        this.formJson.titreNew44=historique.pagesContent[i].value[j].value
                        this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
                        this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
                        this.Nouvaute.push({titre:'',soustitre:'',description:''})
                      }
                  
                      if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 0'){
                        this.formJson.soustitreNew41=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
              
                      }
                      if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 1'){
                        this.formJson.soustitreNew42=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
                      
                      }
                      if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 2'){
                        this.formJson.soustitreNew43=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
                      
                      }
                      if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 3'){
                        this.formJson.soustitreNew44=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
                      
                      }
                      if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 0'){
                        this.formJson.descriptionNew41=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 1'){
                        this.formJson.descriptionNew42=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 2'){
                        this.formJson.descriptionNew43=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 3'){
                        this.formJson.descriptionNew44=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
                      }
                      
                      if(historique.pagesContent[i].value[j].key=='imgPub 3'){
                        this.formJson.imgPub4=historique.pagesContent[i].value[j].value
                        this.pubs[i]=historique.pagesContent[i].value[j].value
                        this.pubs.length++
                      }
                      if(historique.pagesContent[i].value[j].key=='titreconseil 3'){
                        this.formJson.titreConseil4=historique.pagesContent[i].value[j].value
                        this.conseils[j].titre=historique.pagesContent[i].value[j].value
                        this.conseils.push({titre:'',soustitre:'',description:''})
                        
                      }
                      if(historique.pagesContent[i].value[j].key=='sous-titre Conseil 3'){
                        this.formJson.soustitreConseil4=historique.pagesContent[i].value[j].value
                        this.conseils[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='textConseil 3'){
                        this.formJson.descriptionConseil4=historique.pagesContent[i].value[j].value
                        this.conseils[j].description=historique.pagesContent[i].value[j].value
                      }
                    
                      if(historique.pagesContent[i].value[j].key=='titreService 0'){
                        this.formJson.titreService41=historique.pagesContent[i].value[j].value
                        this.arrayConseil[i].push(this.fb.group({conseil:''}))
                        this.Services[j].titre=historique.pagesContent[i].value[j].value
                        this.Services.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='soustitre Service 0'){
                        this.formJson.soustitreService41=historique.pagesContent[i].value[j].value
                        this.Services[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='ImageService 0'){
                        this.formJson.imgService41=historique.pagesContent[i].value[j].value
                        this.Services[j].description=historique.pagesContent[i].value[j].value
                      }
            
                      if(historique.pagesContent[i].value[j].key=='titreService 1'){
                        this.formJson.titreService42=historique.pagesContent[i].value[j].value
                        this.arrayConseil[i].push(this.fb.group({conseil:''}))
                        this.Services[j].titre=historique.pagesContent[i].value[j].value
                        this.Services.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='soustitre Service 1'){
                        this.formJson.soustitreService42=historique.pagesContent[i].value[j].value
                        this.Services[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='ImageService 1'){
                        this.formJson.imgService42=historique.pagesContent[i].value[j].value
                        this.Services[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='titreService 2'){
                        this.formJson.titreService43=historique.pagesContent[i].value[j].value
                        this.arrayConseil[i].push(this.fb.group({conseil:''}))
                        this.Services[j].titre=historique.pagesContent[i].value[j].value
                        this.Services.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='soustitre Service 2'){
                        this.formJson.soustitreService43=historique.pagesContent[i].value[j].value
                        this.Services[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='ImageService 2'){
                        this.formJson.imgService43=historique.pagesContent[i].value[j].value
                        this.Services[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='titreService 3'){
                        this.formJson.titreService44=historique.pagesContent[i].value[j].value
                        this.arrayConseil[i].push(this.fb.group({conseil:''}))
                        this.Services[j].titre=historique.pagesContent[i].value[j].value
                        this.Services.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='soustitre Service 3'){
                        this.formJson.soustitreService44=historique.pagesContent[i].value[j].value
                        this.Services[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='ImageService 3'){
                        this.formJson.imgService44=historique.pagesContent[i].value[j].value
                        this.Services[j].description=historique.pagesContent[i].value[j].value
                      }
        
        }
      
      }
      if (i==4){
        for(var j=0;j<historique.pagesContent[i].value.length;j++){
          if(historique.pagesContent[i].value[j].key=="mois"){
            this.formJson.moisPharma5=historique.pagesContent[i].value[j].value
            this.selectedMois[i]=historique.pagesContent[i].value[j].value
            this.selectedMois.length++
                      }
                    
            
                      
            if(historique.pagesContent[i].value[j].key==`produitAlaUne 0`){
            
             this.arrayUne[i].push(this.fb.group({Une:''}))
             this.produitAlaune[i]=JSON.parse(historique.pagesContent[i].value[j].value)
         
            
            }
              else if(historique.pagesContent[i].value[j].key==`produitAlaUne ${j-2}`) {
            
                                    this.arrayUne[i].push(this.fb.group({Une:''}))
                                    this.produitAlaune.push(JSON.parse(historique.pagesContent[i].value[j].value))
                                
                      }
                    
            
                                  if(historique.pagesContent[i].value[j].key==`produitBebe 0`){
                                   
                         this.arrayBebe[i].push(this.fb.group({Sante:''}))
                         this.produitBebe[i]=JSON.parse(historique.pagesContent[i].value[j].value)
                       
                         }
                        else if(historique.pagesContent[i].value[j].key==`produitBebe ${j}`){
                                   
                          this.arrayBebe[i].push(this.fb.group({Sante:''}))
                          this.produitBebe.push(JSON.parse(historique.pagesContent[i].value[j].value))
                        
                         }
                                              if(historique.pagesContent[i].value[j].key==`produitbienEtre 0`){
                                   
                                                this.arraybienEtre[i].push(this.fb.group({bienEtre:''}))
                                           this.produitBienEtre[i]=JSON.parse(historique.pagesContent[i].value[j].value)
                                          
              }
                                              else if(historique.pagesContent[i].value[j].key==`produitbienEtre ${j}`){
                                   
                                                this.arraybienEtre[i].push(this.fb.group({bienEtre:''}))
                                           this.produitBienEtre.push(JSON.parse(historique.pagesContent[i].value[j].value))
                                          
              }
            
              if(historique.pagesContent[i].value[j].key==`produitBeaute 0`){
                                   
                this.arrayBeaute[i].push(this.fb.group({Beaute:''}))
                this.produitBeaute[i]=JSON.parse(historique.pagesContent[i].value[j].value)
       
            } 
            else if(historique.pagesContent[i].value[j].key==`produitBeaute ${j}`){
                                   
              this.arrayBeaute[i].push(this.fb.group({Beaute:''}))
              this.produitBeaute[i].push(JSON.parse(historique.pagesContent[i].value[j].value))
     
            }    
                                                                                                                          
                                      
                    if(historique.pagesContent[i].value[j].key==`produitSante 0`){
                                   
                 this.arraySante[i].push(this.fb.group({Bebe:''}))  
                this.produitSante[i]=JSON.parse(historique.pagesContent[i].value[j].value)
               
                }  
               else if(historique.pagesContent[i].value[j].key==`produitSante ${j}`){
                                   
                  this.arraySante[i].push(this.fb.group({Bebe:''}))  
                 this.produitSante.push(JSON.parse(historique.pagesContent[i].value[j].value))
                
                 }  
            
                      if(historique.pagesContent[i].value[j].key=='titreNouveaute 0'){
                        this.formJson.titreNew51=historique.pagesContent[i].value[j].value
                        this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
                        
                        this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
                        this.Nouvaute.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='titreNouveaute 1'){
                        this.formJson.titreNew52=historique.pagesContent[i].value[j].value
                        this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
                        this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
                        this.Nouvaute.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='titreNouveaute 2'){
                        this.formJson.titreNew53=historique.pagesContent[i].value[j].value
                        this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
                        this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
                        this.Nouvaute.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='titreNouveaute 3'){
                        this.formJson.titreNew54=historique.pagesContent[i].value[j].value
                        this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
                        this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
                        this.Nouvaute.push({titre:'',soustitre:'',description:''})
                      }
                  
                      if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 0'){
                        this.formJson.soustitreNew51=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
              
                      }
                      if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 1'){
                        this.formJson.soustitreNew52=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
                      
                      }
                      if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 2'){
                        this.formJson.soustitreNew53=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
                      
                      }
                      if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 3'){
                        this.formJson.soustitreNew54=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
                      
                      }
                      if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 0'){
                        this.formJson.descriptionNew51=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 1'){
                        this.formJson.descriptionNew52=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 2'){
                        this.formJson.descriptionNew53=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 3'){
                        this.formJson.descriptionNew54=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
                      }
                      
                      if(historique.pagesContent[i].value[j].key=='imgPub 4'){
                        this.formJson.imgPub6=historique.pagesContent[i].value[j].value
                        this.pubs[i]=historique.pagesContent[i].value[j].value
                        this.pubs.length++
                      }
                      if(historique.pagesContent[i].value[j].key=='titreconseil 4 '){
                        this.formJson.titreConseil5=historique.pagesContent[i].value[j].value
                        this.conseils[j].titre=historique.pagesContent[i].value[j].value
                        this.conseils.push({titre:'',soustitre:'',description:''})
                        
                      }
                      if(historique.pagesContent[i].value[j].key=='sous-titre Conseil 4'){
                        this.formJson.soustitreConseil5=historique.pagesContent[i].value[j].value
                        this.conseils[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='textConseil 4'){
                        this.formJson.descriptionConseil5=historique.pagesContent[i].value[j].value
                        this.conseils[j].description=historique.pagesContent[i].value[j].value
                      }
                    
                      if(historique.pagesContent[i].value[j].key=='titreService 0'){
                        this.formJson.titreService51=historique.pagesContent[i].value[j].value
                        this.arrayConseil[i].push(this.fb.group({conseil:''}))
                        this.Services[j].titre=historique.pagesContent[i].value[j].value
                        this.Services.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='soustitre Service 0'){
                        this.formJson.soustitreService51=historique.pagesContent[i].value[j].value
                        this.Services[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='ImageService 0'){
                        this.formJson.imgService51=historique.pagesContent[i].value[j].value
                        this.Services[j].description=historique.pagesContent[i].value[j].value
                      }
            
                      if(historique.pagesContent[i].value[j].key=='titreService 1'){
                        this.formJson.titreService52=historique.pagesContent[i].value[j].value
                        this.arrayConseil[i].push(this.fb.group({conseil:''}))
                        this.Services[j].titre=historique.pagesContent[i].value[j].value
                        this.Services.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='soustitre Service 1'){
                        this.formJson.soustitreService52=historique.pagesContent[i].value[j].value
                        this.Services[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='ImageService 1'){
                        this.formJson.imgService52=historique.pagesContent[i].value[j].value
                        this.Services[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='titreService 2'){
                        this.formJson.titreService53=historique.pagesContent[i].value[j].value
                        this.arrayConseil[i].push(this.fb.group({conseil:''}))
                        this.Services[j].titre=historique.pagesContent[i].value[j].value
                        this.Services.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='soustitre Service 2'){
                        this.formJson.soustitreService53=historique.pagesContent[i].value[j].value
                        this.Services[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='ImageService 2'){
                        this.formJson.imgService53=historique.pagesContent[i].value[j].value
                        this.Services[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='titreService 3'){
                        this.formJson.titreService54=historique.pagesContent[i].value[j].value
                        this.arrayConseil[i].push(this.fb.group({conseil:''}))
                        this.Services[j].titre=historique.pagesContent[i].value[j].value
                        this.Services.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='soustitre Service 3'){
                        this.formJson.soustitreService54=historique.pagesContent[i].value[j].value
                        this.Services[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='ImageService 3'){
                        this.formJson.imgService54=historique.pagesContent[i].value[j].value
                        this.Services[j].description=historique.pagesContent[i].value[j].value
                      }
        }
      
      }
      if (i==5){
        for(var j=0;j<historique.pagesContent[i].value.length;j++){
          if(historique.pagesContent[i].value[j].key=="mois"){
            this.formJson.moisPharma6=historique.pagesContent[i].value[j].value
            this.selectedMois[i]=historique.pagesContent[i].value[j].value
            this.selectedMois.length++
                      }
                    
            
                      
            if(historique.pagesContent[i].value[j].key==`produitAlaUne 0`){
            
             this.arrayUne[i].push(this.fb.group({Une:''}))
             this.produitAlaune[i]=JSON.parse(historique.pagesContent[i].value[j].value)
         
            
            }
              else if(historique.pagesContent[i].value[j].key==`produitAlaUne ${j-2}`) {
            
                                    this.arrayUne[i].push(this.fb.group({Une:''}))
                                    this.produitAlaune.push(JSON.parse(historique.pagesContent[i].value[j].value))
                                    this.produitAlaune.length++
                      }
                    
            
                                  if(historique.pagesContent[i].value[j].key==`produitBebe 0`){
                                   
                         this.arrayBebe[i].push(this.fb.group({Sante:''}))
                         this.produitBebe[i]=JSON.parse(historique.pagesContent[i].value[j].value)
                         
                         }
                        else if(historique.pagesContent[i].value[j].key==`produitBebe ${j}`){
                                   
                          this.arrayBebe[i].push(this.fb.group({Sante:''}))
                          this.produitBebe.push(JSON.parse(historique.pagesContent[i].value[j].value))
                         
                         }
                                              if(historique.pagesContent[i].value[j].key==`produitbienEtre 0`){
                                   
                                                this.arraybienEtre[i].push(this.fb.group({bienEtre:''}))
                                           this.produitBienEtre[i]=JSON.parse(historique.pagesContent[i].value[j].value)
                                           this.produitBienEtre.length++     
              }
                                              else if(historique.pagesContent[i].value[j].key==`produitbienEtre ${j}`){
                                   
                                                this.arraybienEtre[i].push(this.fb.group({bienEtre:''}))
                                           this.produitBienEtre.push(JSON.parse(historique.pagesContent[i].value[j].value))
                                           this.produitBienEtre.length++     
              }
            
              if(historique.pagesContent[i].value[j].key==`produitBeaute 0`){
                                   
                this.arrayBeaute[i].push(this.fb.group({Beaute:''}))
                this.produitBeaute[i]=JSON.parse(historique.pagesContent[i].value[j].value)
                
            } 
            else if(historique.pagesContent[i].value[j].key==`produitBeaute ${j}`){
                                   
              this.arrayBeaute[i].push(this.fb.group({Beaute:''}))
              this.produitBeaute[i].push(JSON.parse(historique.pagesContent[i].value[j].value))
              
            }    
                                                                                                                          
                                      
                    if(historique.pagesContent[i].value[j].key==`produitSante 0`){
                                   
                 this.arraySante[i].push(this.fb.group({Bebe:''}))  
                this.produitSante[i]=JSON.parse(historique.pagesContent[i].value[j].value)
            
                }  
               else if(historique.pagesContent[i].value[j].key==`produitSante ${j}`){
                                   
                  this.arraySante[i].push(this.fb.group({Bebe:''}))  
                 this.produitSante.push(JSON.parse(historique.pagesContent[i].value[j].value))
             
                 }  
            
                      if(historique.pagesContent[i].value[j].key=='titreNouveaute 0'){
                        this.formJson.titreNew61=historique.pagesContent[i].value[j].value
                        this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
                        
                        this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
                        this.Nouvaute.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='titreNouveaute 1'){
                        this.formJson.titreNew62=historique.pagesContent[i].value[j].value
                        this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
                        this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
                        this.Nouvaute.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='titreNouveaute 2'){
                        this.formJson.titreNew63=historique.pagesContent[i].value[j].value
                        this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
                        this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
                        this.Nouvaute.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='titreNouveaute 3'){
                        this.formJson.titreNew64=historique.pagesContent[i].value[j].value
                        this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
                        this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
                        this.Nouvaute.push({titre:'',soustitre:'',description:''})
                      }
                  
                      if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 0'){
                        this.formJson.soustitreNew61=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
              
                      }
                      if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 1'){
                        this.formJson.soustitreNew62=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
                      
                      }
                      if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 2'){
                        this.formJson.soustitreNew63=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
                      
                      }
                      if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 3'){
                        this.formJson.soustitreNew64=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
                      
                      }
                      if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 0'){
                        this.formJson.descriptionNew61=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 1'){
                        this.formJson.descriptionNew62=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 2'){
                        this.formJson.descriptionNew63=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 3'){
                        this.formJson.descriptionNew64=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
                      }
                      
                      if(historique.pagesContent[i].value[j].key=='imgPub 5'){
                        this.formJson.imgPub6=historique.pagesContent[i].value[j].value
                        this.pubs[i]=historique.pagesContent[i].value[j].value
                        this.pubs.length++
                      }
                      if(historique.pagesContent[i].value[j].key=='titreconseil 5'){
                        this.formJson.titreConseil6=historique.pagesContent[i].value[j].value
                        this.conseils[j].titre=historique.pagesContent[i].value[j].value
                        this.conseils.push({titre:'',soustitre:'',description:''})
                        
                      }
                      if(historique.pagesContent[i].value[j].key=='sous-titre Conseil 5'){
                        this.formJson.soustitreConseil6=historique.pagesContent[i].value[j].value
                        this.conseils[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='textConseil 5'){
                        this.formJson.descriptionConseil6=historique.pagesContent[i].value[j].value
                        this.conseils[j].description=historique.pagesContent[i].value[j].value
                      }
                    
                      if(historique.pagesContent[i].value[j].key=='titreService 0'){
                        this.formJson.titreService41=historique.pagesContent[i].value[j].value
                        this.arrayConseil[i].push(this.fb.group({conseil:''}))
                        this.Services[j].titre=historique.pagesContent[i].value[j].value
                        this.Services.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='soustitre Service 0'){
                        this.formJson.soustitreService41=historique.pagesContent[i].value[j].value
                        this.Services[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='ImageService 0'){
                        this.formJson.imgService41=historique.pagesContent[i].value[j].value
                        this.Services[j].description=historique.pagesContent[i].value[j].value
                      }
            
                      if(historique.pagesContent[i].value[j].key=='titreService 1'){
                        this.formJson.titreService12=historique.pagesContent[i].value[j].value
                        this.arrayConseil[i].push(this.fb.group({conseil:''}))
                        this.Services[j].titre=historique.pagesContent[i].value[j].value
                        this.Services.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='soustitre Service 1'){
                        this.formJson.soustitreService12=historique.pagesContent[i].value[j].value
                        this.Services[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='ImageService 1'){
                        this.formJson.imgService12=historique.pagesContent[i].value[j].value
                        this.Services[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='titreService 2'){
                        this.formJson.titreService13=historique.pagesContent[i].value[j].value
                        this.arrayConseil[i].push(this.fb.group({conseil:''}))
                        this.Services[j].titre=historique.pagesContent[i].value[j].value
                        this.Services.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='soustitre Service 2'){
                        this.formJson.soustitreService13=historique.pagesContent[i].value[j].value
                        this.Services[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='ImageService 2'){
                        this.formJson.imgService13=historique.pagesContent[i].value[j].value
                        this.Services[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='titreService 3'){
                        this.formJson.titreService14=historique.pagesContent[i].value[j].value
                        this.arrayConseil[i].push(this.fb.group({conseil:''}))
                        this.Services[j].titre=historique.pagesContent[i].value[j].value
                        this.Services.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='soustitre Service 3'){
                        this.formJson.soustitreService14=historique.pagesContent[i].value[j].value
                        this.Services[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='ImageService 3'){
                        this.formJson.imgService14=historique.pagesContent[i].value[j].value
                        this.Services[j].description=historique.pagesContent[i].value[j].value
                      }
        
        }
      
      }
      if (i==6){
        for(var j=0;j<historique.pagesContent[i].value.length;j++){
          if(historique.pagesContent[i].value[j].key=="mois"){
            this.formJson.moisPharma7=historique.pagesContent[i].value[j].value
            this.selectedMois[i]=historique.pagesContent[i].value[j].value
            this.selectedMois.length++
                      }
                    
            
                      
            if(historique.pagesContent[i].value[j].key==`produitAlaUne 0`){
            
             this.arrayUne[i].push(this.fb.group({Une:''}))
             this.produitAlaune[i]=JSON.parse(historique.pagesContent[i].value[j].value)
          
            
            }
              else if(historique.pagesContent[i].value[j].key==`produitAlaUne ${j-2}`) {
            
                                    this.arrayUne[i].push(this.fb.group({Une:''}))
                                    this.produitAlaune.push(JSON.parse(historique.pagesContent[i].value[j].value))
                                 
                      }
                    
            
                                  if(historique.pagesContent[i].value[j].key==`produitBebe 0`){
                                   
                         this.arrayBebe[i].push(this.fb.group({Sante:''}))
                         this.produitBebe[i]=JSON.parse(historique.pagesContent[i].value[j].value)
                         
                         }
                        else if(historique.pagesContent[i].value[j].key==`produitBebe ${j}`){
                                   
                          this.arrayBebe[i].push(this.fb.group({Sante:''}))
                          this.produitBebe.push(JSON.parse(historique.pagesContent[i].value[j].value))
                          
                         }
                                              if(historique.pagesContent[i].value[j].key==`produitbienEtre 0`){
                                   
                                                this.arraybienEtre[i].push(this.fb.group({bienEtre:''}))
                                           this.produitBienEtre[i]=JSON.parse(historique.pagesContent[i].value[j].value)
                                             
              }
                                              else if(historique.pagesContent[i].value[j].key==`produitbienEtre ${j}`){
                                   
                                                this.arraybienEtre[i].push(this.fb.group({bienEtre:''}))
                                           this.produitBienEtre.push(JSON.parse(historique.pagesContent[i].value[j].value))
                                             
              }
            
              if(historique.pagesContent[i].value[j].key==`produitBeaute 0`){
                                   
                this.arrayBeaute[i].push(this.fb.group({Beaute:''}))
                this.produitBeaute[i]=JSON.parse(historique.pagesContent[i].value[j].value)
                
            } 
            else if(historique.pagesContent[i].value[j].key==`produitBeaute ${j}`){
                                   
              this.arrayBeaute[i].push(this.fb.group({Beaute:''}))
              this.produitBeaute[i].push(JSON.parse(historique.pagesContent[i].value[j].value))
              
            }    
                                                                                                                          
                                      
                    if(historique.pagesContent[i].value[j].key==`produitSante 0`){
                                   
                 this.arraySante[i].push(this.fb.group({Bebe:''}))  
                this.produitSante[i]=JSON.parse(historique.pagesContent[i].value[j].value)
               
                }  
               else if(historique.pagesContent[i].value[j].key==`produitSante ${j}`){
                                   
                  this.arraySante[i].push(this.fb.group({Bebe:''}))  
                 this.produitSante.push(JSON.parse(historique.pagesContent[i].value[j].value))
                
                 }  
            
                      if(historique.pagesContent[i].value[j].key=='titreNouveaute 0'){
                        this.formJson.titreNew71=historique.pagesContent[i].value[j].value
                        this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
                        
                        this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
                        this.Nouvaute.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='titreNouveaute 1'){
                        this.formJson.titreNew72=historique.pagesContent[i].value[j].value
                        this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
                        this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
                        this.Nouvaute.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='titreNouveaute 2'){
                        this.formJson.titreNew73=historique.pagesContent[i].value[j].value
                        this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
                        this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
                        this.Nouvaute.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='titreNouveaute 3'){
                        this.formJson.titreNew74=historique.pagesContent[i].value[j].value
                        this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
                        this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
                        this.Nouvaute.push({titre:'',soustitre:'',description:''})
                      }
                  
                      if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 0'){
                        this.formJson.soustitreNew71=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
              
                      }
                      if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 1'){
                        this.formJson.soustitreNew72=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
                      
                      }
                      if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 2'){
                        this.formJson.soustitreNew73=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
                      
                      }
                      if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 3'){
                        this.formJson.soustitreNew74=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
                      
                      }
                      if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 0'){
                        this.formJson.descriptionNew71=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 1'){
                        this.formJson.descriptionNew72=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 2'){
                        this.formJson.descriptionNew73=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 3'){
                        this.formJson.descriptionNew74=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
                      }
                      
                      if(historique.pagesContent[i].value[j].key=='imgPub 6'){
                        this.formJson.imgPub7=historique.pagesContent[i].value[j].value
                        this.pubs[i]=historique.pagesContent[i].value[j].value
                        this.pubs.length++
                      }
                      if(historique.pagesContent[i].value[j].key=='titreconseil 6'){
                        this.formJson.titreConseil7=historique.pagesContent[i].value[j].value
                        this.conseils[j].titre=historique.pagesContent[i].value[j].value
                        this.conseils.push({titre:'',soustitre:'',description:''})
                        
                      }
                      if(historique.pagesContent[i].value[j].key=='sous-titre Conseil 6'){
                        this.formJson.soustitreConseil7=historique.pagesContent[i].value[j].value
                        this.conseils[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='textConseil 6'){
                        this.formJson.descriptionConseil7=historique.pagesContent[i].value[j].value
                        this.conseils[j].description=historique.pagesContent[i].value[j].value
                      }
                    
                      if(historique.pagesContent[i].value[j].key=='titreService 0'){
                        this.formJson.titreService71=historique.pagesContent[i].value[j].value
                        this.arrayConseil[i].push(this.fb.group({conseil:''}))
                        this.Services[j].titre=historique.pagesContent[i].value[j].value
                        this.Services.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='soustitre Service 0'){
                        this.formJson.soustitreService71=historique.pagesContent[i].value[j].value
                        this.Services[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='ImageService 0'){
                        this.formJson.imgService71=historique.pagesContent[i].value[j].value
                        this.Services[j].description=historique.pagesContent[i].value[j].value
                      }
            
                      if(historique.pagesContent[i].value[j].key=='titreService 1'){
                        this.formJson.titreService72=historique.pagesContent[i].value[j].value
                        this.arrayConseil[i].push(this.fb.group({conseil:''}))
                        this.Services[j].titre=historique.pagesContent[i].value[j].value
                        this.Services.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='soustitre Service 1'){
                        this.formJson.soustitreService72=historique.pagesContent[i].value[j].value
                        this.Services[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='ImageService 1'){
                        this.formJson.imgService72=historique.pagesContent[i].value[j].value
                        this.Services[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='titreService 2'){
                        this.formJson.titreService73=historique.pagesContent[i].value[j].value
                        this.arrayConseil[i].push(this.fb.group({conseil:''}))
                        this.Services[j].titre=historique.pagesContent[i].value[j].value
                        this.Services.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='soustitre Service 2'){
                        this.formJson.soustitreService73=historique.pagesContent[i].value[j].value
                        this.Services[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='ImageService 2'){
                        this.formJson.imgService73=historique.pagesContent[i].value[j].value
                        this.Services[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='titreService 3'){
                        this.formJson.titreService74=historique.pagesContent[i].value[j].value
                        this.arrayConseil[i].push(this.fb.group({conseil:''}))
                        this.Services[j].titre=historique.pagesContent[i].value[j].value
                        this.Services.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='soustitre Service 3'){
                        this.formJson.soustitreService74=historique.pagesContent[i].value[j].value
                        this.Services[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='ImageService 3'){
                        this.formJson.imgService74=historique.pagesContent[i].value[j].value
                        this.Services[j].description=historique.pagesContent[i].value[j].value
                      }
        
        }
      
      }
      if (i==7){
        for(var j=0;j<historique.pagesContent[i].value.length;j++){
          if(historique.pagesContent[i].value[j].key=="mois"){
            this.formJson.moisPharma8=historique.pagesContent[i].value[j].value
            this.selectedMois[i]=historique.pagesContent[i].value[j].value
            this.selectedMois.length++
                      }
                    
            
                      
            if(historique.pagesContent[i].value[j].key==`produitAlaUne 0`){
            
             this.arrayUne[i].push(this.fb.group({Une:''}))
             this.produitAlaune[i]=JSON.parse(historique.pagesContent[i].value[j].value)
           
            
            }
              else if(historique.pagesContent[i].value[j].key==`produitAlaUne ${j-2}`) {
            
                                    this.arrayUne[i].push(this.fb.group({Une:''}))
                                    this.produitAlaune.push(JSON.parse(historique.pagesContent[i].value[j].value))
                                  
                      }
                    
            
                                  if(historique.pagesContent[i].value[j].key==`produitBebe 0`){
                                   
                         this.arrayBebe[i].push(this.fb.group({Sante:''}))
                         this.produitBebe[i]=JSON.parse(historique.pagesContent[i].value[j].value)
                         
                         }
                        else if(historique.pagesContent[i].value[j].key==`produitBebe ${j}`){
                                   
                          this.arrayBebe[i].push(this.fb.group({Sante:''}))
                          this.produitBebe.push(JSON.parse(historique.pagesContent[i].value[j].value))
                          
                         }
                                              if(historique.pagesContent[i].value[j].key==`produitbienEtre 0`){
                                   
                                                this.arraybienEtre[i].push(this.fb.group({bienEtre:''}))
                                           this.produitBienEtre[i]=JSON.parse(historique.pagesContent[i].value[j].value)
                                           
              }
                                              else if(historique.pagesContent[i].value[j].key==`produitbienEtre ${j}`){
                                   
                                                this.arraybienEtre[i].push(this.fb.group({bienEtre:''}))
                                           this.produitBienEtre.push(JSON.parse(historique.pagesContent[i].value[j].value))
                                           
              }
            
              if(historique.pagesContent[i].value[j].key==`produitBeaute 0`){
                                   
                this.arrayBeaute[i].push(this.fb.group({Beaute:''}))
                this.produitBeaute[i]=JSON.parse(historique.pagesContent[i].value[j].value)
                
            } 
            else if(historique.pagesContent[i].value[j].key==`produitBeaute ${j}`){
                                   
              this.arrayBeaute[i].push(this.fb.group({Beaute:''}))
              this.produitBeaute[i].push(JSON.parse(historique.pagesContent[i].value[j].value))
              
            }    
                                                                                                                          
                                      
                    if(historique.pagesContent[i].value[j].key==`produitSante 0`){
                                   
                 this.arraySante[i].push(this.fb.group({Bebe:''}))  
                this.produitSante[i]=JSON.parse(historique.pagesContent[i].value[j].value)
                
                }  
               else if(historique.pagesContent[i].value[j].key==`produitSante ${j}`){
                                   
                  this.arraySante[i].push(this.fb.group({Bebe:''}))  
                 this.produitSante.push(JSON.parse(historique.pagesContent[i].value[j].value))
                 
                 }  
            
                      if(historique.pagesContent[i].value[j].key=='titreNouveaute 0'){
                        this.formJson.titreNew81=historique.pagesContent[i].value[j].value
                        this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
                        
                        this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
                        this.Nouvaute.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='titreNouveaute 1'){
                        this.formJson.titreNew82=historique.pagesContent[i].value[j].value
                        this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
                        this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
                        this.Nouvaute.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='titreNouveaute 2'){
                        this.formJson.titreNew83=historique.pagesContent[i].value[j].value
                        this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
                        this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
                        this.Nouvaute.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='titreNouveaute 3'){
                        this.formJson.titreNew84=historique.pagesContent[i].value[j].value
                        this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
                        this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
                        this.Nouvaute.push({titre:'',soustitre:'',description:''})
                      }
                  
                      if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 0'){
                        this.formJson.soustitreNew81=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
              
                      }
                      if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 1'){
                        this.formJson.soustitreNew82=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
                      
                      }
                      if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 2'){
                        this.formJson.soustitreNew83=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
                      
                      }
                      if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 3'){
                        this.formJson.soustitreNew84=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
                      
                      }
                      if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 0'){
                        this.formJson.descriptionNew81=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 1'){
                        this.formJson.descriptionNew82=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 2'){
                        this.formJson.descriptionNew83=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 3'){
                        this.formJson.descriptionNew84=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
                      }
                      
                      if(historique.pagesContent[i].value[j].key=='imgPub 7'){
                        this.formJson.imgPub8=historique.pagesContent[i].value[j].value
                        this.pubs[i]=historique.pagesContent[i].value[j].value
                        this.pubs.length++
                      }
                      if(historique.pagesContent[i].value[j].key=='titreconseil 7'){
                        this.formJson.titreConseil8=historique.pagesContent[i].value[j].value
                        this.conseils[j].titre=historique.pagesContent[i].value[j].value
                        this.conseils.push({titre:'',soustitre:'',description:''})
                        
                      }
                      if(historique.pagesContent[i].value[j].key=='sous-titre Conseil 7'){
                        this.formJson.soustitreConseil8=historique.pagesContent[i].value[j].value
                        this.conseils[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='textConseil 7'){
                        this.formJson.descriptionConseil8=historique.pagesContent[i].value[j].value
                        this.conseils[j].description=historique.pagesContent[i].value[j].value
                      }
                    
                      if(historique.pagesContent[i].value[j].key=='titreService 0'){
                        this.formJson.titreService81=historique.pagesContent[i].value[j].value
                        this.arrayConseil[i].push(this.fb.group({conseil:''}))
                        this.Services[j].titre=historique.pagesContent[i].value[j].value
                        this.Services.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='soustitre Service 0'){
                        this.formJson.soustitreService81=historique.pagesContent[i].value[j].value
                        this.Services[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='ImageService 0'){
                        this.formJson.imgService81=historique.pagesContent[i].value[j].value
                        this.Services[j].description=historique.pagesContent[i].value[j].value
                      }
            
                      if(historique.pagesContent[i].value[j].key=='titreService 1'){
                        this.formJson.titreService82=historique.pagesContent[i].value[j].value
                        this.arrayConseil[i].push(this.fb.group({conseil:''}))
                        this.Services[j].titre=historique.pagesContent[i].value[j].value
                        this.Services.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='soustitre Service 1'){
                        this.formJson.soustitreService82=historique.pagesContent[i].value[j].value
                        this.Services[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='ImageService 1'){
                        this.formJson.imgService82=historique.pagesContent[i].value[j].value
                        this.Services[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='titreService 2'){
                        this.formJson.titreService83=historique.pagesContent[i].value[j].value
                        this.arrayConseil[i].push(this.fb.group({conseil:''}))
                        this.Services[j].titre=historique.pagesContent[i].value[j].value
                        this.Services.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='soustitre Service 2'){
                        this.formJson.soustitreService83=historique.pagesContent[i].value[j].value
                        this.Services[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='ImageService 2'){
                        this.formJson.imgService83=historique.pagesContent[i].value[j].value
                        this.Services[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='titreService 3'){
                        this.formJson.titreService84=historique.pagesContent[i].value[j].value
                        this.arrayConseil[i].push(this.fb.group({conseil:''}))
                        this.Services[j].titre=historique.pagesContent[i].value[j].value
                        this.Services.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='soustitre Service 3'){
                        this.formJson.soustitreService84=historique.pagesContent[i].value[j].value
                        this.Services[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='ImageService 3'){
                        this.formJson.imgService84=historique.pagesContent[i].value[j].value
                        this.Services[j].description=historique.pagesContent[i].value[j].value
                      }
        
        }
      
      }
      if (i==8){
        for(var j=0;j<historique.pagesContent[i].value.length;j++){
          if(historique.pagesContent[i].value[j].key=="mois"){
            this.formJson.moisPharma9=historique.pagesContent[i].value[j].value
            this.selectedMois[i]=historique.pagesContent[i].value[j].value
            this.selectedMois.length++
                      }
                    
            
                      
            if(historique.pagesContent[i].value[j].key==`produitAlaUne 0`){
            
             this.arrayUne[i].push(this.fb.group({Une:''}))
             this.produitAlaune[i]=JSON.parse(historique.pagesContent[i].value[j].value)
           
            
            }
              else if(historique.pagesContent[i].value[j].key==`produitAlaUne ${j-2}`) {
            
                                    this.arrayUne[i].push(this.fb.group({Une:''}))
                                    this.produitAlaune.push(JSON.parse(historique.pagesContent[i].value[j].value))
                                  
                      }
                    
            
                                  if(historique.pagesContent[i].value[j].key==`produitBebe 0`){
                                   
                         this.arrayBebe[i].push(this.fb.group({Sante:''}))
                         this.produitBebe[i]=JSON.parse(historique.pagesContent[i].value[j].value)
                         
                         }
                        else if(historique.pagesContent[i].value[j].key==`produitBebe ${j}`){
                                   
                          this.arrayBebe[i].push(this.fb.group({Sante:''}))
                          this.produitBebe.push(JSON.parse(historique.pagesContent[i].value[j].value))
                          
                         }
                                              if(historique.pagesContent[i].value[j].key==`produitbienEtre 0`){
                                   
                                                this.arraybienEtre[i].push(this.fb.group({bienEtre:''}))
                                           this.produitBienEtre[i]=JSON.parse(historique.pagesContent[i].value[j].value)
                                           this.produitBienEtre.length++     
              }
                                              else if(historique.pagesContent[i].value[j].key==`produitbienEtre ${j}`){
                                   
                                                this.arraybienEtre[i].push(this.fb.group({bienEtre:''}))
                                           this.produitBienEtre.push(JSON.parse(historique.pagesContent[i].value[j].value))
                                           this.produitBienEtre.length++     
              }
            
              if(historique.pagesContent[i].value[j].key==`produitBeaute 0`){
                                   
                this.arrayBeaute[i].push(this.fb.group({Beaute:''}))
                this.produitBeaute[i]=JSON.parse(historique.pagesContent[i].value[j].value)
               
            } 
            else if(historique.pagesContent[i].value[j].key==`produitBeaute ${j}`){
                                   
              this.arrayBeaute[i].push(this.fb.group({Beaute:''}))
              this.produitBeaute[i].push(JSON.parse(historique.pagesContent[i].value[j].value))
             
            }    
                                                                                                                          
                                      
                    if(historique.pagesContent[i].value[j].key==`produitSante 0`){
                                   
                 this.arraySante[i].push(this.fb.group({Bebe:''}))  
                this.produitSante[i]=JSON.parse(historique.pagesContent[i].value[j].value)
              
                }  
               else if(historique.pagesContent[i].value[j].key==`produitSante ${j}`){
                                   
                  this.arraySante[i].push(this.fb.group({Bebe:''}))  
                 this.produitSante.push(JSON.parse(historique.pagesContent[i].value[j].value))
               
                 }  
            
                      if(historique.pagesContent[i].value[j].key=='titreNouveaute 0'){
                        this.formJson.titreNew91=historique.pagesContent[i].value[j].value
                        this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
                        
                        this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
                        this.Nouvaute.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='titreNouveaute 1'){
                        this.formJson.titreNew92=historique.pagesContent[i].value[j].value
                        this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
                        this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
                        this.Nouvaute.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='titreNouveaute 2'){
                        this.formJson.titreNew93=historique.pagesContent[i].value[j].value
                        this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
                        this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
                        this.Nouvaute.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='titreNouveaute 3'){
                        this.formJson.titreNew94=historique.pagesContent[i].value[j].value
                        this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
                        this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
                        this.Nouvaute.push({titre:'',soustitre:'',description:''})
                      }
                  
                      if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 0'){
                        this.formJson.soustitreNew91=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
              
                      }
                      if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 1'){
                        this.formJson.soustitreNew92=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
                      
                      }
                      if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 2'){
                        this.formJson.soustitreNew93=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
                      
                      }
                      if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 3'){
                        this.formJson.soustitreNew94=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
                      
                      }
                      if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 0'){
                        this.formJson.descriptionNew91=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 1'){
                        this.formJson.descriptionNew92=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 2'){
                        this.formJson.descriptionNew93=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 3'){
                        this.formJson.descriptionNew94=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
                      }
                      
                      if(historique.pagesContent[i].value[j].key=='imgPub 8'){
                        this.formJson.imgPub9=historique.pagesContent[i].value[j].value
                        this.pubs[i]=historique.pagesContent[i].value[j].value
                        this.pubs.length++
                      }
                      if(historique.pagesContent[i].value[j].key=='titreconseil 8'){
                        this.formJson.titreConseil9=historique.pagesContent[i].value[j].value
                        this.conseils[j].titre=historique.pagesContent[i].value[j].value
                        this.conseils.push({titre:'',soustitre:'',description:''})
                        
                      }
                      if(historique.pagesContent[i].value[j].key=='sous-titre Conseil 8'){
                        this.formJson.soustitreConseil9=historique.pagesContent[i].value[j].value
                        this.conseils[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='textConseil 8'){
                        this.formJson.descriptionConseil9=historique.pagesContent[i].value[j].value
                        this.conseils[j].description=historique.pagesContent[i].value[j].value
                      }
                    
                      if(historique.pagesContent[i].value[j].key=='titreService 0'){
                        this.formJson.titreService91=historique.pagesContent[i].value[j].value
                        this.arrayConseil[i].push(this.fb.group({conseil:''}))
                        this.Services[j].titre=historique.pagesContent[i].value[j].value
                        this.Services.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='soustitre Service 0'){
                        this.formJson.soustitreService91=historique.pagesContent[i].value[j].value
                        this.Services[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='ImageService 0'){
                        this.formJson.imgService91=historique.pagesContent[i].value[j].value
                        this.Services[j].description=historique.pagesContent[i].value[j].value
                      }
            
                      if(historique.pagesContent[i].value[j].key=='titreService 1'){
                        this.formJson.titreService92=historique.pagesContent[i].value[j].value
                        this.arrayConseil[i].push(this.fb.group({conseil:''}))
                        this.Services[j].titre=historique.pagesContent[i].value[j].value
                        this.Services.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='soustitre Service 1'){
                        this.formJson.soustitreService92=historique.pagesContent[i].value[j].value
                        this.Services[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='ImageService 1'){
                        this.formJson.imgService92=historique.pagesContent[i].value[j].value
                        this.Services[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='titreService 2'){
                        this.formJson.titreService93=historique.pagesContent[i].value[j].value
                        this.arrayConseil[i].push(this.fb.group({conseil:''}))
                        this.Services[j].titre=historique.pagesContent[i].value[j].value
                        this.Services.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='soustitre Service 2'){
                        this.formJson.soustitreService93=historique.pagesContent[i].value[j].value
                        this.Services[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='ImageService 2'){
                        this.formJson.imgService93=historique.pagesContent[i].value[j].value
                        this.Services[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='titreService 3'){
                        this.formJson.titreService94=historique.pagesContent[i].value[j].value
                        this.arrayConseil[i].push(this.fb.group({conseil:''}))
                        this.Services[j].titre=historique.pagesContent[i].value[j].value
                        this.Services.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='soustitre Service 3'){
                        this.formJson.soustitreService94=historique.pagesContent[i].value[j].value
                        this.Services[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='ImageService 3'){
                        this.formJson.imgService94=historique.pagesContent[i].value[j].value
                        this.Services[j].description=historique.pagesContent[i].value[j].value
                      }
        }
      
      }
      if (i==9){
        for(var j=0;j<historique.pagesContent[i].value.length;j++){
          if(historique.pagesContent[i].value[j].key=="mois"){
            this.formJson.moisPharma10=historique.pagesContent[i].value[j].value
            this.selectedMois[i]=historique.pagesContent[i].value[j].value
            this.selectedMois.length++
                      }
                    
            
                      
            if(historique.pagesContent[i].value[j].key==`produitAlaUne 0`){
            
             this.arrayUne[i].push(this.fb.group({Une:''}))
             this.produitAlaune[i]=JSON.parse(historique.pagesContent[i].value[j].value)

            
            }
              else if(historique.pagesContent[i].value[j].key==`produitAlaUne ${j-2}`) {
            
                                    this.arrayUne[i].push(this.fb.group({Une:''}))
                                    this.produitAlaune.push(JSON.parse(historique.pagesContent[i].value[j].value))
                       
                      }
                    
            
                                  if(historique.pagesContent[i].value[j].key==`produitBebe 0`){
                                   
                         this.arrayBebe[i].push(this.fb.group({Sante:''}))
                         this.produitBebe[i]=JSON.parse(historique.pagesContent[i].value[j].value)
                       
                         }
                        else if(historique.pagesContent[i].value[j].key==`produitBebe ${j}`){
                                   
                          this.arrayBebe[i].push(this.fb.group({Sante:''}))
                          this.produitBebe.push(JSON.parse(historique.pagesContent[i].value[j].value))
                        
                         }
                                              if(historique.pagesContent[i].value[j].key==`produitbienEtre 0`){
                                   
                                                this.arraybienEtre[i].push(this.fb.group({bienEtre:''}))
                                           this.produitBienEtre[i]=JSON.parse(historique.pagesContent[i].value[j].value)
                                             
              }
                                              else if(historique.pagesContent[i].value[j].key==`produitbienEtre ${j}`){
                                   
                                                this.arraybienEtre[i].push(this.fb.group({bienEtre:''}))
                                           this.produitBienEtre.push(JSON.parse(historique.pagesContent[i].value[j].value))
                                             
              }
            
              if(historique.pagesContent[i].value[j].key==`produitBeaute 0`){
                                   
                this.arrayBeaute[i].push(this.fb.group({Beaute:''}))
                this.produitBeaute[i]=JSON.parse(historique.pagesContent[i].value[j].value)
                
            } 
            else if(historique.pagesContent[i].value[j].key==`produitBeaute ${j}`){
                                   
              this.arrayBeaute[i].push(this.fb.group({Beaute:''}))
              this.produitBeaute[i].push(JSON.parse(historique.pagesContent[i].value[j].value))
              
            }    
                                                                                                                          
                                      
                    if(historique.pagesContent[i].value[j].key==`produitSante 0`){
                                   
                 this.arraySante[i].push(this.fb.group({Bebe:''}))  
                this.produitSante[i]=JSON.parse(historique.pagesContent[i].value[j].value)
               
                }  
               else if(historique.pagesContent[i].value[j].key==`produitSante ${j}`){
                                   
                  this.arraySante[i].push(this.fb.group({Bebe:''}))  
                 this.produitSante.push(JSON.parse(historique.pagesContent[i].value[j].value))
                
                 }  
            
                      if(historique.pagesContent[i].value[j].key=='titreNouveaute 0'){
                        this.formJson.titreNew101=historique.pagesContent[i].value[j].value
                        this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
                        
                        this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
                        this.Nouvaute.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='titreNouveaute 1'){
                        this.formJson.titreNew102=historique.pagesContent[i].value[j].value
                        this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
                        this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
                        this.Nouvaute.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='titreNouveaute 2'){
                        this.formJson.titreNew103=historique.pagesContent[i].value[j].value
                        this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
                        this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
                        this.Nouvaute.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='titreNouveaute 3'){
                        this.formJson.titreNew104=historique.pagesContent[i].value[j].value
                        this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
                        this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
                        this.Nouvaute.push({titre:'',soustitre:'',description:''})
                      }
                  
                      if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 0'){
                        this.formJson.soustitreNew101=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
              
                      }
                      if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 1'){
                        this.formJson.soustitreNew102=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
                      
                      }
                      if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 2'){
                        this.formJson.soustitreNew103=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
                      
                      }
                      if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 3'){
                        this.formJson.soustitreNew104=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
                      
                      }
                      if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 0'){
                        this.formJson.descriptionNew101=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 1'){
                        this.formJson.descriptionNew102=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 2'){
                        this.formJson.descriptionNew103=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 3'){
                        this.formJson.descriptionNew104=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
                      }
                      
                      if(historique.pagesContent[i].value[j].key=='imgPub 9'){
                        this.formJson.imgPub10=historique.pagesContent[i].value[j].value
                        this.pubs[i]=historique.pagesContent[i].value[j].value
                        this.pubs.length++
                      }
                      if(historique.pagesContent[i].value[j].key=='titreconseil 9'){
                        this.formJson.titreConseil10=historique.pagesContent[i].value[j].value
                        this.conseils[j].titre=historique.pagesContent[i].value[j].value
                        this.conseils.push({titre:'',soustitre:'',description:''})
                        
                      }
                      if(historique.pagesContent[i].value[j].key=='sous-titre Conseil 9'){
                        this.formJson.soustitreConseil10=historique.pagesContent[i].value[j].value
                        this.conseils[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='textConseil 9'){
                        this.formJson.descriptionConseil10=historique.pagesContent[i].value[j].value
                        this.conseils[j].description=historique.pagesContent[i].value[j].value
                      }
                    
                      if(historique.pagesContent[i].value[j].key=='titreService 0'){
                        this.formJson.titreService101=historique.pagesContent[i].value[j].value
                        this.arrayConseil[i].push(this.fb.group({conseil:''}))
                        this.Services[j].titre=historique.pagesContent[i].value[j].value
                        this.Services.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='soustitre Service 0'){
                        this.formJson.soustitreService101=historique.pagesContent[i].value[j].value
                        this.Services[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='ImageService 0'){
                        this.formJson.imgService101=historique.pagesContent[i].value[j].value
                        this.Services[j].description=historique.pagesContent[i].value[j].value
                      }
            
                      if(historique.pagesContent[i].value[j].key=='titreService 1'){
                        this.formJson.titreService102=historique.pagesContent[i].value[j].value
                        this.arrayConseil[i].push(this.fb.group({conseil:''}))
                        this.Services[j].titre=historique.pagesContent[i].value[j].value
                        this.Services.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='soustitre Service 1'){
                        this.formJson.soustitreService102=historique.pagesContent[i].value[j].value
                        this.Services[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='ImageService 1'){
                        this.formJson.imgService102=historique.pagesContent[i].value[j].value
                        this.Services[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='titreService 2'){
                        this.formJson.titreService103=historique.pagesContent[i].value[j].value
                        this.arrayConseil[i].push(this.fb.group({conseil:''}))
                        this.Services[j].titre=historique.pagesContent[i].value[j].value
                        this.Services.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='soustitre Service 2'){
                        this.formJson.soustitreService103=historique.pagesContent[i].value[j].value
                        this.Services[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='ImageService 2'){
                        this.formJson.imgService103=historique.pagesContent[i].value[j].value
                        this.Services[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='titreService 3'){
                        this.formJson.titreService104=historique.pagesContent[i].value[j].value
                        this.arrayConseil[i].push(this.fb.group({conseil:''}))
                        this.Services[j].titre=historique.pagesContent[i].value[j].value
                        this.Services.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='soustitre Service 3'){
                        this.formJson.soustitreService104=historique.pagesContent[i].value[j].value
                        this.Services[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='ImageService 3'){
                        this.formJson.imgService104=historique.pagesContent[i].value[j].value
                        this.Services[j].description=historique.pagesContent[i].value[j].value
                      }
        
        }
      
      }
      if (i==10){
        for(var j=0;j<historique.pagesContent[i].value.length;j++){
          if(historique.pagesContent[i].value[j].key=="mois"){
            this.formJson.moisPharma11=historique.pagesContent[i].value[j].value
            this.selectedMois[i]=historique.pagesContent[i].value[j].value
            this.selectedMois.length++
                      }
                    
            
                      
            if(historique.pagesContent[i].value[j].key==`produitAlaUne 0`){
            
             this.arrayUne[i].push(this.fb.group({Une:''}))
             this.produitAlaune[i]=JSON.parse(historique.pagesContent[i].value[j].value)
           
            
            }
              else if(historique.pagesContent[i].value[j].key==`produitAlaUne ${j-2}`) {
            
                                    this.arrayUne[i].push(this.fb.group({Une:''}))
                                    this.produitAlaune.push(JSON.parse(historique.pagesContent[i].value[j].value))
                                  
                      }
                    
            
                                  if(historique.pagesContent[i].value[j].key==`produitBebe 0`){
                                   
                         this.arrayBebe[i].push(this.fb.group({Sante:''}))
                         this.produitBebe[i]=JSON.parse(historique.pagesContent[i].value[j].value)
                     
                         }
                        else if(historique.pagesContent[i].value[j].key==`produitBebe ${j}`){
                                   
                          this.arrayBebe[i].push(this.fb.group({Sante:''}))
                          this.produitBebe.push(JSON.parse(historique.pagesContent[i].value[j].value))
                      
                         }
                                              if(historique.pagesContent[i].value[j].key==`produitbienEtre 0`){
                                   
                                                this.arraybienEtre[i].push(this.fb.group({bienEtre:''}))
                                           this.produitBienEtre[i]=JSON.parse(historique.pagesContent[i].value[j].value)
                                  
              }
                                              else if(historique.pagesContent[i].value[j].key==`produitbienEtre ${j}`){
                                   
                                                this.arraybienEtre[i].push(this.fb.group({bienEtre:''}))
                                           this.produitBienEtre.push(JSON.parse(historique.pagesContent[i].value[j].value))
                                  
              }
            
              if(historique.pagesContent[i].value[j].key==`produitBeaute 0`){
                                   
                this.arrayBeaute[i].push(this.fb.group({Beaute:''}))
                this.produitBeaute[i]=JSON.parse(historique.pagesContent[i].value[j].value)
         
            } 
            else if(historique.pagesContent[i].value[j].key==`produitBeaute ${j}`){
                                   
              this.arrayBeaute[i].push(this.fb.group({Beaute:''}))
              this.produitBeaute[i].push(JSON.parse(historique.pagesContent[i].value[j].value))
       
            }    
                                                                                                                          
                                      
                    if(historique.pagesContent[i].value[j].key==`produitSante 0`){
                                   
                 this.arraySante[i].push(this.fb.group({Bebe:''}))  
                this.produitSante[i]=JSON.parse(historique.pagesContent[i].value[j].value)
                
                }  
               else if(historique.pagesContent[i].value[j].key==`produitSante ${j}`){
                                   
                  this.arraySante[i].push(this.fb.group({Bebe:''}))  
                 this.produitSante.push(JSON.parse(historique.pagesContent[i].value[j].value))
                 
                 }  
            
                      if(historique.pagesContent[i].value[j].key=='titreNouveaute 0'){
                        this.formJson.titreNew111=historique.pagesContent[i].value[j].value
                        this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
                        
                        this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
                        this.Nouvaute.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='titreNouveaute 1'){
                        this.formJson.titreNew112=historique.pagesContent[i].value[j].value
                        this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
                        this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
                        this.Nouvaute.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='titreNouveaute 2'){
                        this.formJson.titreNew113=historique.pagesContent[i].value[j].value
                        this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
                        this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
                        this.Nouvaute.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='titreNouveaute 3'){
                        this.formJson.titreNew114=historique.pagesContent[i].value[j].value
                        this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
                        this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
                        this.Nouvaute.push({titre:'',soustitre:'',description:''})
                      }
                  
                      if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 0'){
                        this.formJson.soustitreNew111=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
              
                      }
                      if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 1'){
                        this.formJson.soustitreNew112=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
                      
                      }
                      if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 2'){
                        this.formJson.soustitreNew113=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
                      
                      }
                      if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 3'){
                        this.formJson.soustitreNew114=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
                      
                      }
                      if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 0'){
                        this.formJson.descriptionNew111=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 1'){
                        this.formJson.descriptionNew112=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 2'){
                        this.formJson.descriptionNew113=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 3'){
                        this.formJson.descriptionNew114=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
                      }
                      
                      if(historique.pagesContent[i].value[j].key=='imgPub 10'){
                        this.formJson.imgPub11=historique.pagesContent[i].value[j].value
                        this.pubs[i]=historique.pagesContent[i].value[j].value
                        this.pubs.length++
                      }
                      if(historique.pagesContent[i].value[j].key=='titreconseil 10'){
                        this.formJson.titreConseil11=historique.pagesContent[i].value[j].value
                        this.conseils[j].titre=historique.pagesContent[i].value[j].value
                        this.conseils.push({titre:'',soustitre:'',description:''})
                        
                      }
                      if(historique.pagesContent[i].value[j].key=='sous-titre Conseil 10'){
                        this.formJson.soustitreConseil11=historique.pagesContent[i].value[j].value
                        this.conseils[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='textConseil 10'){
                        this.formJson.descriptionConseil11=historique.pagesContent[i].value[j].value
                        this.conseils[j].description=historique.pagesContent[i].value[j].value
                      }
                    
                      if(historique.pagesContent[i].value[j].key=='titreService 0'){
                        this.formJson.titreService111=historique.pagesContent[i].value[j].value
                        this.arrayConseil[i].push(this.fb.group({conseil:''}))
                        this.Services[j].titre=historique.pagesContent[i].value[j].value
                        this.Services.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='soustitre Service 0'){
                        this.formJson.soustitreService111=historique.pagesContent[i].value[j].value
                        this.Services[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='ImageService 0'){
                        this.formJson.imgService111=historique.pagesContent[i].value[j].value
                        this.Services[j].description=historique.pagesContent[i].value[j].value
                      }
            
                      if(historique.pagesContent[i].value[j].key=='titreService 1'){
                        this.formJson.titreService112=historique.pagesContent[i].value[j].value
                        this.arrayConseil[i].push(this.fb.group({conseil:''}))
                        this.Services[j].titre=historique.pagesContent[i].value[j].value
                        this.Services.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='soustitre Service 1'){
                        this.formJson.soustitreService112=historique.pagesContent[i].value[j].value
                        this.Services[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='ImageService 1'){
                        this.formJson.imgService112=historique.pagesContent[i].value[j].value
                        this.Services[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='titreService 2'){
                        this.formJson.titreService113=historique.pagesContent[i].value[j].value
                        this.arrayConseil[i].push(this.fb.group({conseil:''}))
                        this.Services[j].titre=historique.pagesContent[i].value[j].value
                        this.Services.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='soustitre Service 2'){
                        this.formJson.soustitreService113=historique.pagesContent[i].value[j].value
                        this.Services[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='ImageService 2'){
                        this.formJson.imgService113=historique.pagesContent[i].value[j].value
                        this.Services[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='titreService 3'){
                        this.formJson.titreService114=historique.pagesContent[i].value[j].value
                        this.arrayConseil[i].push(this.fb.group({conseil:''}))
                        this.Services[j].titre=historique.pagesContent[i].value[j].value
                        this.Services.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='soustitre Service 3'){
                        this.formJson.soustitreService114=historique.pagesContent[i].value[j].value
                        this.Services[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='ImageService 3'){
                        this.formJson.imgService114=historique.pagesContent[i].value[j].value
                        this.Services[j].description=historique.pagesContent[i].value[j].value
                      }
        
        }
      
      }
      if (i==11){
        for(var j=0;j<historique.pagesContent[i].value.length;j++){
          if(historique.pagesContent[i].value[j].key=="mois"){
            this.formJson.moisPharma12=historique.pagesContent[i].value[j].value
            this.selectedMois[i]=historique.pagesContent[i].value[j].value
            this.selectedMois.length++
                      }
                    
            
                      
            if(historique.pagesContent[i].value[j].key==`produitAlaUne 0`){
            
             this.arrayUne[i].push(this.fb.group({Une:''}))
             this.produitAlaune[i]=JSON.parse(historique.pagesContent[i].value[j].value)
  
            
            }
              else if(historique.pagesContent[i].value[j].key==`produitAlaUne ${j-2}`) {
            
                                    this.arrayUne[i].push(this.fb.group({Une:''}))
                                    this.produitAlaune.push(JSON.parse(historique.pagesContent[i].value[j].value))
                         
                      }
                    
            
                                  if(historique.pagesContent[i].value[j].key==`produitBebe 0`){
                                   
                         this.arrayBebe[i].push(this.fb.group({Sante:''}))
                         this.produitBebe[i]=JSON.parse(historique.pagesContent[i].value[j].value)
                
                         }
                        else if(historique.pagesContent[i].value[j].key==`produitBebe ${j}`){
                                   
                          this.arrayBebe[i].push(this.fb.group({Sante:''}))
                          this.produitBebe.push(JSON.parse(historique.pagesContent[i].value[j].value))
                 
                         }
                                              if(historique.pagesContent[i].value[j].key==`produitbienEtre 0`){
                                   
                                                this.arraybienEtre[i].push(this.fb.group({bienEtre:''}))
                                           this.produitBienEtre[i]=JSON.parse(historique.pagesContent[i].value[j].value)
                                      
              }
                                              else if(historique.pagesContent[i].value[j].key==`produitbienEtre ${j}`){
                                   
                                                this.arraybienEtre[i].push(this.fb.group({bienEtre:''}))
                                           this.produitBienEtre.push(JSON.parse(historique.pagesContent[i].value[j].value))
                                      
              }
            
              if(historique.pagesContent[i].value[j].key==`produitBeaute 0`){
                                   
                this.arrayBeaute[i].push(this.fb.group({Beaute:''}))
                this.produitBeaute[i]=JSON.parse(historique.pagesContent[i].value[j].value)
              
            } 
            else if(historique.pagesContent[i].value[j].key==`produitBeaute ${j}`){
                                   
              this.arrayBeaute[i].push(this.fb.group({Beaute:''}))
              this.produitBeaute[i].push(JSON.parse(historique.pagesContent[i].value[j].value))
            
            }    
                                                                                                                          
                                      
                    if(historique.pagesContent[i].value[j].key==`produitSante 0`){
                                   
                 this.arraySante[i].push(this.fb.group({Bebe:''}))  
                this.produitSante[i]=JSON.parse(historique.pagesContent[i].value[j].value)
            
                }  
               else if(historique.pagesContent[i].value[j].key==`produitSante ${j}`){
                                   
                  this.arraySante[i].push(this.fb.group({Bebe:''}))  
                 this.produitSante.push(JSON.parse(historique.pagesContent[i].value[j].value))
             
                 }  
            
                      if(historique.pagesContent[i].value[j].key=='titreNouveaute 0'){
                        this.formJson.titreNew121=historique.pagesContent[i].value[j].value
                        this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
                        
                        this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
                        this.Nouvaute.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='titreNouveaute 1'){
                        this.formJson.titreNew122=historique.pagesContent[i].value[j].value
                        this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
                        this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
                        this.Nouvaute.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='titreNouveaute 2'){
                        this.formJson.titreNew123=historique.pagesContent[i].value[j].value
                        this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
                        this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
                        this.Nouvaute.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='titreNouveaute 3'){
                        this.formJson.titreNew124=historique.pagesContent[i].value[j].value
                        this.arrayNouveaute[i].push(this.fb.group({conseil:''}))
                        this.Nouvaute[j].titre=historique.pagesContent[i].value[j].value
                        this.Nouvaute.push({titre:'',soustitre:'',description:''})
                      }
                  
                      if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 0'){
                        this.formJson.soustitreNew121=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
              
                      }
                      if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 1'){
                        this.formJson.soustitreNew122=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
                      
                      }
                      if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 2'){
                        this.formJson.soustitreNew123=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
                      
                      }
                      if(historique.pagesContent[i].value[j].key=='sous-titreNouveaute 3'){
                        this.formJson.soustitreNew124=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].soustitre=historique.pagesContent[i].value[j].value
                      
                      }
                      if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 0'){
                        this.formJson.descriptionNew121=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 1'){
                        this.formJson.descriptionNew122=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 2'){
                        this.formJson.descriptionNew123=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='descriptionNouveaute 3'){
                        this.formJson.descriptionNew124=historique.pagesContent[i].value[j].value
                        this.Nouvaute[j].description=historique.pagesContent[i].value[j].value
                      }
                      
                      if(historique.pagesContent[i].value[j].key=='imgPub 11'){
                        this.formJson.imgPub12=historique.pagesContent[i].value[j].value
                        this.pubs[i]=historique.pagesContent[i].value[j].value
                        this.pubs.length++
                      }
                      if(historique.pagesContent[i].value[j].key=='titreconseil 11'){
                        this.formJson.titreConseil12=historique.pagesContent[i].value[j].value
                        this.conseils[j].titre=historique.pagesContent[i].value[j].value
                        this.conseils.push({titre:'',soustitre:'',description:''})
                        
                      }
                      if(historique.pagesContent[i].value[j].key=='sous-titre Conseil 11'){
                        this.formJson.soustitreConseil12=historique.pagesContent[i].value[j].value
                        this.conseils[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='textConseil 11'){
                        this.formJson.descriptionConseil12=historique.pagesContent[i].value[j].value
                        this.conseils[j].description=historique.pagesContent[i].value[j].value
                      }
                    
                      if(historique.pagesContent[i].value[j].key=='titreService 0'){
                        this.formJson.titreService121=historique.pagesContent[i].value[j].value
                        this.arrayConseil[i].push(this.fb.group({conseil:''}))
                        this.Services[j].titre=historique.pagesContent[i].value[j].value
                        this.Services.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='soustitre Service 0'){
                        this.formJson.soustitreService121=historique.pagesContent[i].value[j].value
                        this.Services[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='ImageService 0'){
                        this.formJson.imgService121=historique.pagesContent[i].value[j].value
                        this.Services[j].description=historique.pagesContent[i].value[j].value
                      }
            
                      if(historique.pagesContent[i].value[j].key=='titreService 1'){
                        this.formJson.titreService122=historique.pagesContent[i].value[j].value
                        this.arrayConseil[i].push(this.fb.group({conseil:''}))
                        this.Services[j].titre=historique.pagesContent[i].value[j].value
                        this.Services.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='soustitre Service 1'){
                        this.formJson.soustitreService122=historique.pagesContent[i].value[j].value
                        this.Services[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='ImageService 1'){
                        this.formJson.imgService122=historique.pagesContent[i].value[j].value
                        this.Services[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='titreService 2'){
                        this.formJson.titreService123=historique.pagesContent[i].value[j].value
                        this.arrayConseil[i].push(this.fb.group({conseil:''}))
                        this.Services[j].titre=historique.pagesContent[i].value[j].value
                        this.Services.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='soustitre Service 2'){
                        this.formJson.soustitreService123=historique.pagesContent[i].value[j].value
                        this.Services[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='ImageService 2'){
                        this.formJson.imgService123=historique.pagesContent[i].value[j].value
                        this.Services[j].description=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='titreService 3'){
                        this.formJson.titreService124=historique.pagesContent[i].value[j].value
                        this.arrayConseil[i].push(this.fb.group({conseil:''}))
                        this.Services[j].titre=historique.pagesContent[i].value[j].value
                        this.Services.push({titre:'',soustitre:'',description:''})
                      }
                      if(historique.pagesContent[i].value[j].key=='soustitre Service 3'){
                        this.formJson.soustitreService124=historique.pagesContent[i].value[j].value
                        this.Services[j].soustitre=historique.pagesContent[i].value[j].value
                      }
                      if(historique.pagesContent[i].value[j].key=='ImageService 3'){
                        this.formJson.imgService124=historique.pagesContent[i].value[j].value
                        this.Services[j].description=historique.pagesContent[i].value[j].value
                      }
        
        }
      
      }
    }
  }
  imageUrl(pageCount,i,input){
    this.value={key:'',value:''}
    this.value.key=`ImageProduit${i}`
    this.value.value=input.value
    if(pageCount==0){
      if (i==0){
        this.formJson.imgProduit11=input.value
      }
      if (i==1){
        this.formJson.imgProduit12=input.value
      }
      if (i==2){
        this.formJson.imgProduit13=input.value
      }
      if (i==3){
        this.formJson.imgProduit14=input.value
      }
      
      
      }
      
      if(pageCount==1){
        if (i==0){
          this.formJson.imgProduit21=input.value
        }
        if (i==1){
          this.formJson.imgProduit22=input.value
        }
        if (i==2){
          this.formJson.imgProduit23=input.value
        }
        if (i==3){
          this.formJson.imgProduit24=input.value
        }
        }
      
        if(pageCount==2){
          if (i==0){
            this.formJson.imgProduit31=input.value
          }
          if (i==1){
            this.formJson.imgProduit32=input.value
          }
          if (i==2){
            this.formJson.imgProduit33=input.value
          }
          if (i==3){
            this.formJson.imgProduit34=input.value
          }
          
          
          }
          if(pageCount==3){
            if (i==0){
              this.formJson.imgProduit41=input.value
            }
            if (i==1){
              this.formJson.imgProduit42=input.value
            }
            if (i==2){
              this.formJson.imgProduit43=input.value
            }
            if (i==3){
              this.formJson.imgProduit44=input.value
            }
            
            
            }
            if(pageCount==4){
              if (i==0){
                this.formJson.imgProduit51=input.value
              }
              if (i==1){
                this.formJson.imgProduit52=input.value
              }
              if (i==2){
                this.formJson.imgProduit53=input.value
              }
              if (i==3){
                this.formJson.imgProduit54=input.value
              }
            }
              if(pageCount==5){
                if (i==0){
                  this.formJson.imgProduit61=input.value
                }
                if (i==1){
                  this.formJson.imgProduit62=input.value
                }
                if (i==2){
                  this.formJson.imgProduit63=input.value
                }
                if (i==3){
                  this.formJson.imgProduit64=input.value
                }
                
                
                }
      
                if(pageCount==6){
                  if (i==0){
                    this.formJson.imgProduit71=input.value
                  }
                  if (i==1){
                    this.formJson.imgProduit72=input.value
                  }
                  if (i==2){
                    this.formJson.imgProduit73=input.value
                  }
                  if (i==3){
                    this.formJson.imgProduit74=input.value
                  }
                  
                  
                  }
                  if(pageCount==7){
                    if (i==0){
                      this.formJson.imgProduit81=input.value
                    }
                    if (i==1){
                      this.formJson.imgProduit82=input.value
                    }
                    if (i==2){
                      this.formJson.imgProduit83=input.value
                    }
                    if (i==3){
                      this.formJson.imgProduit84=input.value
                    }
                    
                    
                    }
      
                    if(pageCount==8){
                      if (i==0){
                        this.formJson.imgProduit91=input.value
                      }
                      if (i==1){
                        this.formJson.imgProduit92=input.value
                      }
                      if (i==2){
                        this.formJson.imgProduit93=input.value
                      }
                      if (i==3){
                        this.formJson.imgProduit94=input.value
                      }
                      
                      
                      }
                      if(pageCount==9){
      if (i==0){
        this.formJson.imgProduit101=input.value
      }
      if (i==1){
        this.formJson.imgProduit102=input.value
      }
      if (i==2){
        this.formJson.imgProduit103=input.value
      }
      if (i==3){
        this.formJson.imgProduit104=input.value
      }
      
      
      }
      
      if(pageCount==10){
        if (i==0){
          this.formJson.imgProduit111=input.value
        }
        if (i==1){
          this.formJson.imgProduit112=input.value
        }
        if (i==2){
          this.formJson.imgProduit113=input.value
        }
        if (i==3){
          this.formJson.imgProduit114=input.value
        }
        
        
        }
      
        if(pageCount==11){
          if (i==0){
            this.formJson.imgProduit121=input.value
          }
          if (i==1){
            this.formJson.imgProduit122=input.value
          }
          if (i==2){
            this.formJson.imgProduit123=input.value
          }
          if (i==3){
            this.formJson.imgProduit124=input.value
          }
          if (i==3){
            this.formJson.imgProduit124=input.value
          }
          if (i==3){
            this.formJson.imgProduit124=input.value
          }
          
          }
  }
  imageBeaute(pageCount,i,input){
    this.value={key:'',value:''}
    this.value.key=`ImageService ${i}`
    this.value.value=input.value
    if(pageCount==0){
      if (i==0){
        this.formJson.imgService11=input.value
      }
      if (i==1){
        this.formJson.imgService12=input.value
      }
      if (i==2){
        this.formJson.imgService13=input.value
      }
      if (i==3){
        this.formJson.imgService14=input.value

      
      }
    }
      if(pageCount==1){
        if (i==0){
          this.formJson.imgService21=input.value
        }
        if (i==1){
          this.formJson.imgService22=input.value
        }
        if (i==2){
          this.formJson.imgService23=input.value
        }
        if (i==3){
          this.formJson.imgService24=input.value
        }
 
        }
      
        if(pageCount==2){
          if (i==0){
            this.formJson.imgService31=input.value
          }
          if (i==1){
            this.formJson.imgService32=input.value
          }
          if (i==2){
            this.formJson.imgService33=input.value
          }
          if (i==3){
            this.formJson.imgService34=input.value
          }
     
          
          }
          if(pageCount==3){
            if (i==0){
              this.formJson.imgService41=input.value
            }
            if (i==1){
              this.formJson.imgService42=input.value
            }
            if (i==2){
              this.formJson.imgService43=input.value
            }
            if (i==3){
              this.formJson.imgService44=input.value
            }
      
            
            }
            if(pageCount==4){
              if (i==0){
                this.formJson.imgService51=input.value
              }
              if (i==1){
                this.formJson.imgService52=input.value
              }
              if (i==2){
                this.formJson.imgService53=input.value
              }
              if (i==3){
                this.formJson.imgService54=input.value
              }
          
            }
              if(pageCount==5){
                if (i==0){
                  this.formJson.imgService61=input.value
                }
                if (i==1){
                  this.formJson.imgService62=input.value
                }
                if (i==2){
                  this.formJson.imgService63=input.value
                }
                if (i==3){
                  this.formJson.imgService64=input.value
                }
            
                
                }
      
                if(pageCount==6){
                  if (i==0){
                    this.formJson.imgService71=input.value
                  }
                  if (i==1){
                    this.formJson.imgService72=input.value
                  }
                  if (i==2){
                    this.formJson.imgService73=input.value
                  }
                  if (i==3){
                    this.formJson.imgService74=input.value
                  }
           
                  
                  }
                  if(pageCount==7){
                    if (i==0){
                      this.formJson.imgService81=input.value
                    }
                    if (i==1){
                      this.formJson.imgService82=input.value
                    }
                    if (i==2){
                      this.formJson.imgService83=input.value
                    }
                    if (i==3){
                      this.formJson.imgService84=input.value
                    }
              
                    
                    }
      
                    if(pageCount==8){
                      if (i==0){
                        this.formJson.imgService91=input.value
                      }
                      if (i==1){
                        this.formJson.imgService92=input.value
                      }
                      if (i==2){
                        this.formJson.imgService93=input.value
                      }
                      if (i==3){
                        this.formJson.imgService94=input.value
                      }
                    
                      
                      }
                      if(pageCount==9){
      if (i==0){
        this.formJson.imgService101=input.value
      }
      if (i==1){
        this.formJson.imgService102=input.value
      }
      if (i==2){
        this.formJson.imgService103=input.value
      }
      if (i==3){
        this.formJson.imgService104=input.value
      }
 
      
      }
      
      if(pageCount==10){
        if (i==0){
          this.formJson.imgService111=input.value
        }
        if (i==1){
          this.formJson.imgService112=input.value
        }
        if (i==2){
          this.formJson.imgService113=input.value
        }
        if (i==3){
          this.formJson.imgService114=input.value
        }

        
        }
      
        if(pageCount==11){
          if (i==0){
            this.formJson.imgService121=input.value
          }
          if (i==1){
            this.formJson.imgService122=input.value
          }
          if (i==2){
            this.formJson.imgService123=input.value
          }
          if (i==3){
            this.formJson.imgService124=input.value
          }
          if (i==3){
            this.formJson.imgService124=input.value
          }
          if (i==3){
            this.formJson.imgService124=input.value
          }
       
        
  }
    }
  trackChange(pageCount,input){
    this.value={key:'',value:''}
    this.value.key=`imgPub ${pageCount}`
    this.value.value=input.value
    this.historiqueLeaflet.pagesContent[pageCount].value[0]=this.value
if(pageCount==0){
  this.formJson.imgPub1=input.value
}
if(pageCount==1){
  this.formJson.imgPub2=input.value
}
if(pageCount==2){
  this.formJson.imgPub3=input.value
}
if(pageCount==3){
  this.formJson.imgPub4=input.value
}
if(pageCount==4){
  this.formJson.imgPub5=input.value
}
if(pageCount==5){
  this.formJson.imgPub6=input.value
}
if(pageCount==6){
  this.formJson.imgPub7=input.value
}
if(pageCount==7){
  this.formJson.imgPub8=input.value
}
if(pageCount==8){
  this.formJson.imgPub9=input.value
}
if(pageCount==9){
  this.formJson.imgPub10=input.value
}
if(pageCount==10){
  this.formJson.imgPub11=input.value
}
if(pageCount==11){
  this.formJson.imgPub12=input.value
}
  }
  imageSante(pageCount,i,input){
    this.value={key:'',value:''}
    this.value.key=`ImageNouveaute${i}`
    this.value.value=input.value
    if(pageCount==0){
      if (i==0){
        this.formJson.imgNew11=input.value
      }
      if (i==1){
        this.formJson.imgNew12=input.value
      }
      if (i==2){
        this.formJson.imgNew13=input.value
      }
      if (i==3){
        this.formJson.imgNew14=input.value
      }

      
      }
      
      if(pageCount==1){
        if (i==0){
          this.formJson.imgNew21=input.value
        }
        if (i==1){
          this.formJson.imgNew22=input.value
        }
        if (i==2){
          this.formJson.imgNew23=input.value
        }
        if (i==3){
          this.formJson.imgNew24=input.value
        }
     
        }
      
        if(pageCount==2){
          if (i==0){
            this.formJson.imgNew31=input.value
          }
          if (i==1){
            this.formJson.imgNew32=input.value
          }
          if (i==2){
            this.formJson.imgNew33=input.value
          }
          if (i==3){
            this.formJson.imgNew34=input.value
          }
      
          
          }
          if(pageCount==3){
            if (i==0){
              this.formJson.imgNew41=input.value
            }
            if (i==1){
              this.formJson.imgNew42=input.value
            }
            if (i==2){
              this.formJson.imgNew43=input.value
            }
            if (i==3){
              this.formJson.imgNew44=input.value
            }
        
            
            }
            if(pageCount==4){
              if (i==0){
                this.formJson.imgNew51=input.value
              }
              if (i==1){
                this.formJson.imgNew52=input.value
              }
              if (i==2){
                this.formJson.imgNew53=input.value
              }
              if (i==3){
                this.formJson.imgNew54=input.value
              }
       
            }
              if(pageCount==5){
                if (i==0){
                  this.formJson.imgNew61=input.value
                }
                if (i==1){
                  this.formJson.imgNew62=input.value
                }
                if (i==2){
                  this.formJson.imgNew63=input.value
                }
                if (i==3){
                  this.formJson.imgNew64=input.value
                }
          
                
                }
      
                if(pageCount==6){
                  if (i==0){
                    this.formJson.imgNew71=input.value
                  }
                  if (i==1){
                    this.formJson.imgNew72=input.value
                  }
                  if (i==2){
                    this.formJson.imgNew73=input.value
                  }
                  if (i==3){
                    this.formJson.imgNew74=input.value
                  }
                 
           
                  
                  }
                  if(pageCount==7){
                    if (i==0){
                      this.formJson.imgNew81=input.value
                    }
                    if (i==1){
                      this.formJson.imgNew82=input.value
                    }
                    if (i==2){
                      this.formJson.imgNew83=input.value
                    }
                    if (i==3){
                      this.formJson.imgNew84=input.value
                    }
             
                    
                    }
      
                    if(pageCount==8){
                      if (i==0){
                        this.formJson.imgNew91=input.value
                      }
                      if (i==1){
                        this.formJson.imgNew92=input.value
                      }
                      if (i==2){
                        this.formJson.imgNew93=input.value
                      }
                      if (i==3){
                        this.formJson.imgNew94=input.value
                      }
              
                      
                      }
                      if(pageCount==9){
      if (i==0){
        this.formJson.imgNew101=input.value
      }
      if (i==1){
        this.formJson.imgNew102=input.value
      }
      if (i==2){
        this.formJson.imgNew103=input.value
      }
      if (i==3){
        this.formJson.imgNew104=input.value
      }

      
      }
      
      if(pageCount==10){
        if (i==0){
          this.formJson.imgNew111=input.value
        }
        if (i==1){
          this.formJson.imgNew112=input.value
        }
        if (i==2){
          this.formJson.imgNew113=input.value
        }
        if (i==3){
          this.formJson.imgNew114=input.value
        }
 
        
        }
      
        if(pageCount==11){
          if (i==0){
            this.formJson.imgNew121=input.value
          }
          if (i==1){
            this.formJson.imgNew122=input.value
          }
          if (i==2){
            this.formJson.imgNew123=input.value
          }
          if (i==3){
            this.formJson.imgNew124=input.value
          }
          if (i==3){
            this.formJson.imgNew124=input.value
          }
          if (i==3){
            this.formJson.imgNew124=input.value
          }
     
        
          }
  }
  imagebienEtre(pageCount,i,input){
    this.value={key:'',value:''}
    this.value.key=`ImageConseil${i}`
    this.value.value=input.value
    this.historiqueLeaflet.pagesContent[pageCount].value.push(this.value)
    if(pageCount==0){
      this.formJson.imgConseil1=input.value
      
      }
      
      if(pageCount==1){
        this.formJson.imgConseil2=input.value
        }
      
        if(pageCount==2){
          this.formJson.imgConseil3=input.value
          
          }
          if(pageCount==3){
            this.formJson.imgConseil4=input.value
            
            }
            if(pageCount==4){
              this.formJson.imgConseil5=input.value
            }
              if(pageCount==5){
                this.formJson.imgConseil6=input.value
                
                }
      
                if(pageCount==6){
                  this.formJson.imgConseil7=input.value
                  
                  }
                  if(pageCount==7){
                    this.formJson.imgConseil8=input.value
                    
                    }
      
                    if(pageCount==8){
                      this.formJson.imgConseil9=input.value
      
      }
      
      if(pageCount==10){
        this.formJson.imgConseil11=input.value
        
        }
      
        if(pageCount==11){
          this.formJson.imgConseil12=input.value
          }
  }
  imageBebe(pageCount,i,input){
    this.value={key:'',value:''}
    this.value.key=`ImageBebe${i}`
    this.value.value=input.value
    if(pageCount==0){
      if (i==0){
        this.formJson.imgBebe11=input.value
      }
      if (i==1){
        this.formJson.imgBebe12=input.value
      }
      if (i==2){
        this.formJson.imgBebe13=input.value
      }
      if (i==3){
        this.formJson.imgBebe14=input.value
      }
      if (i==4){
        this.formJson.imgBebe15=input.value
      }
      if (i==5){
        this.formJson.imgBebe16=input.value
      }
      
      }
      
      if(pageCount==1){
        if (i==0){
          this.formJson.imgBebe21=input.value
        }
        if (i==1){
          this.formJson.imgBebe22=input.value
        }
        if (i==2){
          this.formJson.imgBebe23=input.value
        }
        if (i==3){
          this.formJson.imgBebe24=input.value
        }
        if (i==4){
          this.formJson.imgBebe25=input.value
        }
        if (i==5){
          this.formJson.imgBebe26=input.value
        }
        }
      
        if(pageCount==2){
          if (i==0){
            this.formJson.imgBebe31=input.value
          }
          if (i==1){
            this.formJson.imgBebe32=input.value
          }
          if (i==2){
            this.formJson.imgBebe33=input.value
          }
          if (i==3){
            this.formJson.imgBebe34=input.value
          }
          if (i==4){
            this.formJson.imgBebe35=input.value
          }
          if (i==5){
            this.formJson.imgBebe36=input.value
          }
          
          }
          if(pageCount==3){
            if (i==0){
              this.formJson.imgBebe41=input.value
            }
            if (i==1){
              this.formJson.imgBebe42=input.value
            }
            if (i==2){
              this.formJson.imgBebe43=input.value
            }
            if (i==3){
              this.formJson.imgBebe44=input.value
            }
            if (i==4){
              this.formJson.imgBebe45=input.value
            }
            if (i==5){
              this.formJson.imgBebe46=input.value
            }
            
            }
            if(pageCount==4){
              if (i==0){
                this.formJson.imgBebe51=input.value
              }
              if (i==1){
                this.formJson.imgBebe52=input.value
              }
              if (i==2){
                this.formJson.imgBebe53=input.value
              }
              if (i==3){
                this.formJson.imgBebe54=input.value
              }
              if (i==4){
                this.formJson.imgBebe55=input.value
              }
              if (i==5){
                this.formJson.imgBebe56=input.value
              }
            }
              if(pageCount==5){
                if (i==0){
                  this.formJson.imgBebe61=input.value
                }
                if (i==1){
                  this.formJson.imgBebe62=input.value
                }
                if (i==2){
                  this.formJson.imgBebe63=input.value
                }
                if (i==3){
                  this.formJson.imgBebe64=input.value
                }
                if (i==4){
                  this.formJson.imgBebe65=input.value
                }
                if (i==5){
                  this.formJson.imgBebe66=input.value
                }
                
                }
      
                if(pageCount==6){
                  if (i==0){
                    this.formJson.imgBebe71=input.value
                  }
                  if (i==1){
                    this.formJson.imgBebe72=input.value
                  }
                  if (i==2){
                    this.formJson.imgBebe73=input.value
                  }
                  if (i==3){
                    this.formJson.imgBebe74=input.value
                  }
                  if (i==4){
                    this.formJson.imgBebe75=input.value
                  }
                  if (i==5){
                    this.formJson.imgBebe76=input.value
                  }
                  
                  }
                  if(pageCount==7){
                    if (i==0){
                      this.formJson.imgBebe81=input.value
                    }
                    if (i==1){
                      this.formJson.imgBebe82=input.value
                    }
                    if (i==2){
                      this.formJson.imgBebe83=input.value
                    }
                    if (i==3){
                      this.formJson.imgBebe84=input.value
                    }
                    if (i==4){
                      this.formJson.imgBebe85=input.value
                    }
                    if (i==5){
                      this.formJson.imgBebe86=input.value
                    }
                    
                    }
      
                    if(pageCount==8){
                      if (i==0){
                        this.formJson.imgBebe91=input.value
                      }
                      if (i==1){
                        this.formJson.imgBebe92=input.value
                      }
                      if (i==2){
                        this.formJson.imgBebe93=input.value
                      }
                      if (i==3){
                        this.formJson.imgBebe94=input.value
                      }
                      if (i==4){
                        this.formJson.imgBebe95=input.value
                      }
                      if (i==5){
                        this.formJson.imgBebe96=input.value
                      }
                      
                      }
                      if(pageCount==9){
      if (i==0){
        this.formJson.imgBebe101=input.value
      }
      if (i==1){
        this.formJson.imgBebe102=input.value
      }
      if (i==2){
        this.formJson.imgBebe103=input.value
      }
      if (i==3){
        this.formJson.imgBebe104=input.value
      }
      if (i==4){
        this.formJson.imgBebe105=input.value
      }
      if (i==5){
        this.formJson.imgBebe106=input.value
      }
      
      }
      
      if(pageCount==10){
        if (i==0){
          this.formJson.imgBebe111=input.value
        }
        if (i==1){
          this.formJson.imgBebe112=input.value
        }
        if (i==2){
          this.formJson.imgBebe113=input.value
        }
        if (i==3){
          this.formJson.imgBebe114=input.value
        }
        if (i==4){
          this.formJson.imgBebe115=input.value
        }
        if (i==5){
          this.formJson.imgBebe116=input.value
        }
        
        }
      
        if(pageCount==11){
          if (i==0){
            this.formJson.imgBebe121=input.value
          }
          if (i==1){
            this.formJson.imgBebe122=input.value
          }
          if (i==2){
            this.formJson.imgBebe123=input.value
          }
          if (i==3){
            this.formJson.imgBebe124=input.value
          }
          if (i==3){
            this.formJson.imgBebe124=input.value
          }
          if (i==3){
            this.formJson.imgBebe124=input.value
          }
          if (i==4){
            this.formJson.imgBebe125=input.value
          }
          if (i==5){
            this.formJson.imgBebe126=input.value
          }
        
          }
  }

  get sellingPoints() {
    return this.productForm.get('selling_points') as FormArray;
  }

  addSellingPoint(pageCount) {
    if (pageCount==0){
   this.arrayUne[pageCount].push(this.fb.group({Une:''}))
       this.formJson.nbrElements1++
     }
     if (pageCount==1){
      this.arrayUne[pageCount].push(this.fb.group({Une:''}))
      this.formJson.nbrElements2++
    }
    if (pageCount==2){
      this.arrayUne[pageCount].push(this.fb.group({Une:''}))
      this.formJson.nbrElements3++
    }
    if (pageCount==3){
      this.arrayUne[pageCount].push(this.fb.group({Une:''}))
      this.formJson.nbrElements4++
    }
    if (pageCount==4){
      this.arrayUne[pageCount].push(this.fb.group({Une:''}))
      this.formJson.nbrElements5++
    }
    if (pageCount==5){
      this.arrayUne[pageCount].push(this.fb.group({Une:''}))
      this.formJson.nbrElements6++
    } 
    if (pageCount==6){
      this.arrayUne[pageCount].push(this.fb.group({Une:''}))
      this.formJson.nbrElements7++
    }
    if (pageCount==7){
      this.arrayUne[pageCount].push(this.fb.group({Une:''}))
      this.formJson.nbrElements8++
    }
    if (pageCount==8){
      this.arrayUne[pageCount].push(this.fb.group({Une:''}))
      this.formJson.nbrElements9++
    }
    if (pageCount==9){
      this.arrayUne[pageCount].push(this.fb.group({Une:''}))
      this.formJson.nbrElements10++
    }
    if (pageCount==10){
      this.arrayUne[pageCount].push(this.fb.group({Une:''}))
      this.formJson.nbrElements11++
    }
    if (pageCount==11){
      this.arrayUne[pageCount].push(this.fb.group({Une:''}))
      this.formJson.nbrElements12++
    }
    
  }
  deleteSellingPoint(pageCount,index) {
    this.arrayUne[pageCount].splice(index,1);
  }

  get Bebe() {
    return this.productForm.get('selection_Bebe') as FormArray;
  }
  addBebe(pageCount) {
   
    if (pageCount==0){
    
      this.arrayBebe[pageCount].push(this.fb.group({conseil:''}))
       this.formJson.nbrElements1++
       this.hiddenBebe[pageCount].count++
      this.hiddenBebe[pageCount].hide=false;
      if ( this.hiddenBebe[pageCount].count==6)
      this.hiddenBebe[pageCount].hide=true
     }
     if (pageCount==1){
      this.arrayBebe[pageCount].push(this.fb.group({conseil:''}))
      this.hiddenBebe[pageCount].count++
      this.hiddenBebe[pageCount].hide=false;
      if ( this.hiddenBebe[pageCount].count==6)
      this.hiddenBebe[pageCount].hide=true
      this.formJson.nbrElements2++
    }
    if (pageCount==2){
      this.arrayBebe[pageCount].push(this.fb.group({conseil:''}))
      this.hiddenBebe[pageCount].count++
      this.hiddenBebe[pageCount].hide=false;
      if ( this.hiddenBebe[pageCount].count==6)
      this.hiddenBebe[pageCount].hide=true
      this.formJson.nbrElements3++
    }
    if (pageCount==3){
      this.arrayBebe[pageCount].push(this.fb.group({conseil:''}))
      this.hiddenBebe[pageCount].count++
      this.hiddenBebe[pageCount].hide=false;
      if ( this.hiddenBebe[pageCount].count==6)
      this.hiddenBebe[pageCount].hide=true
      this.formJson.nbrElements4++
    }
    if (pageCount==4){
      this.arrayBebe[pageCount].push(this.fb.group({conseil:''}))
      this.hiddenBebe[pageCount].count++
      this.hiddenBebe[pageCount].hide=false;
      if ( this.hiddenBebe[pageCount].count==6)
      this.hiddenBebe[pageCount].hide=true
      this.formJson.nbrElements5++
    }
    if (pageCount==5){
      this.arrayBebe[pageCount].push(this.fb.group({conseil:''}))
      this.hiddenBebe[pageCount].count++
      this.hiddenBebe[pageCount].hide=false;
      if ( this.hiddenBebe[pageCount].count==6)
      this.hiddenBebe[pageCount].hide=true
      this.formJson.nbrElements6++
    } 
    if (pageCount==6){
      this.arrayBebe[pageCount].push(this.fb.group({conseil:''}))
      this.hiddenBebe[pageCount].count++
      this.hiddenBebe[pageCount].hide=false;
      if ( this.hiddenBebe[pageCount].count==6)
      this.hiddenBebe[pageCount].hide=true
      this.formJson.nbrElements7++
    }
    if (pageCount==7){
      this.arrayBebe[pageCount].push(this.fb.group({conseil:''}))
      this.hiddenBebe[pageCount].count++
      this.hiddenBebe[pageCount].hide=false;
      if ( this.hiddenBebe[pageCount].count==6)
      this.hiddenBebe[pageCount].hide=true
      this.formJson.nbrElements8++
    }
    if (pageCount==8){
      this.arrayBebe[pageCount].push(this.fb.group({conseil:''}))
      this.hiddenBebe[pageCount].count++
      this.hiddenBebe[pageCount].hide=false;
      if ( this.hiddenBebe[pageCount].count==6)
      this.hiddenBebe[pageCount].hide=true
      this.formJson.nbrElements9++
    }
    if (pageCount==9){
      this.arrayBebe[pageCount].push(this.fb.group({conseil:''}))
      this.hiddenBebe[pageCount].count++
      this.hiddenBebe[pageCount].hide=false;
      if ( this.hiddenBebe[pageCount].count==6)
      this.hiddenBebe[pageCount].hide=true
      this.formJson.nbrElements10++
    }
    if (pageCount==10){
      this.arrayBebe[pageCount].push(this.fb.group({conseil:''}))
      this.hiddenBebe[pageCount].count++
      this.hiddenBebe[pageCount].hide=false;
      if ( this.hiddenBebe[pageCount].count==6)
      this.hiddenBebe[pageCount].hide=true
      this.formJson.nbrElements11++
    }
    if (pageCount==11){
      this.arrayBebe[pageCount].push(this.fb.group({conseil:''}))
      this.hiddenBebe[pageCount].count++
      this.hiddenBebe[pageCount].hide=false;
      if ( this.hiddenBebe[pageCount].count==6)
      this.hiddenBebe[pageCount].hide=true
      this.formJson.nbrElements12++
    }
  }
  deleteBebe(pageCount,index) {
    this.arrayBebe[pageCount].splice(index,1);
    if(pageCount==0){
      this.hiddenBebe[pageCount].count--
    this.hiddenBebe[pageCount].hide=false;
    }
    if(pageCount==1){
      this.hiddenBebe[pageCount].count--
      this.hiddenBebe[pageCount].hide=false;
      }
      if(pageCount==2){
        this.hiddenBebe[pageCount].count--
        this.hiddenBebe[pageCount].hide=false;
        }
        if(pageCount==3){
          this.hiddenBebe[pageCount].count--
          this.hiddenBebe[pageCount].hide=false;
          }
          if(pageCount==4){
            this.hiddenBebe[pageCount].count--
            this.hiddenBebe[pageCount].hide=false;
            }
            if(pageCount==5){
              this.hiddenBebe[pageCount].count--
              this.hiddenBebe[pageCount].hide=false;
              }
              if(pageCount==6){
                this.hiddenBebe[pageCount].count--
                this.hiddenBebe[pageCount].hide=false;
                }
                if(pageCount==7){
                  this.hiddenBebe[pageCount].count--
                  this.hiddenBebe[pageCount].hide=false;
                  }
                  if(pageCount==8){
                    this.hiddenBebe[pageCount].count--
                    this.hiddenBebe[pageCount].hide=false;
                    }
                    if(pageCount==9){
                      this.hiddenBebe[pageCount].count--
                      this.hiddenBebe[pageCount].hide=false;
                      }
                      if(pageCount==10){
                        this.hiddenBebe[pageCount].count--
                        this.hiddenBebe[pageCount].hide=false;
                        }
                        if(pageCount==11){
                          this.hiddenBebe[pageCount].count--
                          this.hiddenBebe[pageCount].hide=false;
                          }  
  }


  get Sante() {
    return this.productForm.get('selection_Sante') as FormArray;
  }
  addSante(pageCount) {
   
    if (pageCount==0){
      this.arraySante[pageCount].push(this.fb.group({conseil:''}))
      this.hiddenSante[pageCount].count++
      this.hiddenSante[pageCount].hide=false;
    
     if ( this.hiddenSante[pageCount].count==6)
     this.hiddenSante[pageCount].hide=true
       this.formJson.nbrElements1++
     }
     if (pageCount==1){
      this.arraySante[pageCount].push(this.fb.group({conseil:''}))
      this.hiddenSante[pageCount].count++
      this.hiddenSante[pageCount].hide=false;
    
     if ( this.hiddenSante[pageCount].count==6)
     this.hiddenSante[pageCount].hide=true
      this.formJson.nbrElements2++
    }
    if (pageCount==2){
      this.arraySante[pageCount].push(this.fb.group({conseil:''}))
      this.hiddenSante[pageCount].count++
      this.hiddenSante[pageCount].hide=false;
    
     if ( this.hiddenSante[pageCount].count==6)
     this.hiddenSante[pageCount].hide=true
      this.formJson.nbrElements3++
    }
    if (pageCount==3){
      this.arraySante[pageCount].push(this.fb.group({conseil:''}))
      this.hiddenSante[pageCount].count++
      this.hiddenSante[pageCount].hide=false;
    
     if ( this.hiddenSante[pageCount].count==6)
     this.hiddenSante[pageCount].hide=true
      this.formJson.nbrElements4++
    }
    if (pageCount==4){
      this.arraySante[pageCount].push(this.fb.group({conseil:''}))
      this.hiddenSante[pageCount].count++
      this.hiddenSante[pageCount].hide=false;
    
     if ( this.hiddenSante[pageCount].count==6)
     this.hiddenSante[pageCount].hide=true
      this.formJson.nbrElements5++
    }
    if (pageCount==5){
      this.arraySante[pageCount].push(this.fb.group({conseil:''}))
      this.hiddenSante[pageCount].count++
      this.hiddenSante[pageCount].hide=false;
    
     if ( this.hiddenSante[pageCount].count==6)
     this.hiddenSante[pageCount].hide=true
      this.formJson.nbrElements6++
    } 
    if (pageCount==6){
      this.arraySante[pageCount].push(this.fb.group({conseil:''}))
      this.hiddenSante[pageCount].count++
      this.hiddenSante[pageCount].hide=false;
    
     if ( this.hiddenSante[pageCount].count==6)
     this.hiddenSante[pageCount].hide=true
      this.formJson.nbrElements7++
    }
    if (pageCount==7){
      this.arraySante[pageCount].push(this.fb.group({conseil:''}))
      this.hiddenSante[pageCount].count++
      this.hiddenSante[pageCount].hide=false;
    
     if ( this.hiddenSante[pageCount].count==6)
     this.hiddenSante[pageCount].hide=true
      this.formJson.nbrElements8++
    }
    if (pageCount==8){
      this.arraySante[pageCount].push(this.fb.group({conseil:''}))
      this.hiddenSante[pageCount].count++
      this.hiddenSante[pageCount].hide=false;
    
     if ( this.hiddenSante[pageCount].count==6)
     this.hiddenSante[pageCount].hide=true
      this.formJson.nbrElements9++
    }
    if (pageCount==9){
      this.arraySante[pageCount].push(this.fb.group({conseil:''}))
      this.hiddenSante[pageCount].count++
      this.hiddenSante[pageCount].hide=false;
    
     if ( this.hiddenSante[pageCount].count==6)
     this.hiddenSante[pageCount].hide=true
      this.formJson.nbrElements10++
    }
    if (pageCount==10){
      this.arraySante[pageCount].push(this.fb.group({conseil:''}))
      this.hiddenSante[pageCount].count++
      this.hiddenSante[pageCount].hide=false;
    
     if ( this.hiddenSante[pageCount].count==6)
     this.hiddenSante[pageCount].hide=true
      this.formJson.nbrElements11++
    }
    if (pageCount==11){
      this.arraySante[pageCount].push(this.fb.group({conseil:''}))
      this.hiddenSante[pageCount].count++
      this.hiddenSante[pageCount].hide=false;
    
     if ( this.hiddenSante[pageCount].count==6)
     this.hiddenSante[pageCount].hide=true
      this.formJson.nbrElements12++
    }
  }
  deleteSante(pageCount,index) {
    this.arraySante[pageCount].splice(index,1);
    if(pageCount==0){
      this.hiddenSante[pageCount].count--
    this.hiddenSante[pageCount].hide=false;
    }
    if(pageCount==1){
      this.hiddenSante[pageCount].count--
      this.hiddenSante[pageCount].hide=false;
      }
      if(pageCount==2){
        this.hiddenSante[pageCount].count--
        this.hiddenSante[pageCount].hide=false;
        }
        if(pageCount==3){
          this.hiddenSante[pageCount].count--
          this.hiddenSante[pageCount].hide=false;
          }
          if(pageCount==4){
            this.hiddenSante[pageCount].count--
            this.hiddenSante[pageCount].hide=false;
            }
            if(pageCount==5){
              this.hiddenSante[pageCount].count--
              this.hiddenSante[pageCount].hide=false;
              }
              if(pageCount==6){
                this.hiddenSante[pageCount].count--
                this.hiddenSante[pageCount].hide=false;
                }
                if(pageCount==7){
                  this.hiddenSante[pageCount].count--
                  this.hiddenSante[pageCount].hide=false;
                  }
                  if(pageCount==8){
                    this.hiddenSante[pageCount].count--
                    this.hiddenSante[pageCount].hide=false;
                    }
                    if(pageCount==9){
                      this.hiddenSante[pageCount].count--
                      this.hiddenSante[pageCount].hide=false;
                      }
                      if(pageCount==10){
                        this.hiddenSante[pageCount].count--
                        this.hiddenSante[pageCount].hide=false;
                        }
                        if(pageCount==11){
                          this.hiddenSante[pageCount].count--
                          this.hiddenSante[pageCount].hide=false;
                          }  
  }

  get bienEtre() {
    return this.productForm.get('selection_bienEtre') as FormArray;
  }
  addbienEtre(pageCount) {
   
    if (pageCount==0){
      this.arraybienEtre[pageCount].push(this.fb.group({conseil:''}))
      this.hiddenbienEtre[pageCount].count++
      this.hiddenbienEtre[pageCount].hide=false;
      if ( this.hiddenbienEtre[pageCount].count==6)
      this.hiddenbienEtre[pageCount].hide=true
       this.formJson.nbrElements1++
     }
     if (pageCount==1){
      this.arraybienEtre[pageCount].push(this.fb.group({conseil:''}))
      this.hiddenbienEtre[pageCount].count++
      this.hiddenbienEtre[pageCount].hide=false;
      if ( this.hiddenbienEtre[pageCount].count==6)
      this.hiddenbienEtre[pageCount].hide=true
      this.formJson.nbrElements2++
    }
    if (pageCount==2){
      this.arraybienEtre[pageCount].push(this.fb.group({conseil:''}))
      this.hiddenbienEtre[pageCount].count++
      this.hiddenbienEtre[pageCount].hide=false;
      if ( this.hiddenbienEtre[pageCount].count==6)
      this.hiddenbienEtre[pageCount].hide=true
      this.formJson.nbrElements3++
    }
    if (pageCount==3){
      this.arraybienEtre[pageCount].push(this.fb.group({conseil:''}))
      this.formJson.nbrElements4++
      this.hiddenbienEtre[pageCount].count++
      this.hiddenbienEtre[pageCount].hide=false;
      if ( this.hiddenbienEtre[pageCount].count==6)
      this.hiddenbienEtre[pageCount].hide=true
    }
    if (pageCount==4){
      this.arraybienEtre[pageCount].push(this.fb.group({conseil:''}))
      this.formJson.nbrElements5++
      this.hiddenbienEtre[pageCount].count++
      this.hiddenbienEtre[pageCount].hide=false;
      if ( this.hiddenbienEtre[pageCount].count==6)
      this.hiddenbienEtre[pageCount].hide=true
    }
    if (pageCount==5){
      this.arraybienEtre[pageCount].push(this.fb.group({conseil:''}))
      this.formJson.nbrElements6++
      this.hiddenbienEtre[pageCount].count++
      this.hiddenbienEtre[pageCount].hide=false;
      if ( this.hiddenbienEtre[pageCount].count==6)
      this.hiddenbienEtre[pageCount].hide=true
    } 
    if (pageCount==6){
      this.arraybienEtre[pageCount].push(this.fb.group({conseil:''}))
      this.formJson.nbrElements7++
      this.hiddenbienEtre[pageCount].count++
      this.hiddenbienEtre[pageCount].hide=false;
      if ( this.hiddenbienEtre[pageCount].count==6)
      this.hiddenbienEtre[pageCount].hide=true
    }
    if (pageCount==7){
      this.arraybienEtre[pageCount].push(this.fb.group({conseil:''}))
      this.formJson.nbrElements8++
      this.hiddenbienEtre[pageCount].count++
      this.hiddenbienEtre[pageCount].hide=false;
      if ( this.hiddenbienEtre[pageCount].count==6)
      this.hiddenbienEtre[pageCount].hide=true
    }
    if (pageCount==8){
      this.arraybienEtre[pageCount].push(this.fb.group({conseil:''}))
      this.formJson.nbrElements9++
      this.hiddenbienEtre[pageCount].count++
      this.hiddenbienEtre[pageCount].hide=false;
      if ( this.hiddenbienEtre[pageCount].count==6)
      this.hiddenbienEtre[pageCount].hide=true
    }

    if (pageCount==9){
      this.arraybienEtre[pageCount].push(this.fb.group({conseil:''}))
      this.formJson.nbrElements10++
      this.hiddenbienEtre[pageCount].count++
      this.hiddenbienEtre[pageCount].hide=false;
      if ( this.hiddenbienEtre[pageCount].count==6)
      this.hiddenbienEtre[pageCount].hide=true
    }
    if (pageCount==10){
      this.arraybienEtre[pageCount].push(this.fb.group({conseil:''}))
      this.formJson.nbrElements11++
      this.hiddenbienEtre[pageCount].count++
      this.hiddenbienEtre[pageCount].hide=false;
      if ( this.hiddenbienEtre[pageCount].count==6)
      this.hiddenbienEtre[pageCount].hide=true
    }
    if (pageCount==11){
      this.arraybienEtre[pageCount].push(this.fb.group({conseil:''}))
      this.formJson.nbrElements12++
      this.hiddenbienEtre[pageCount].count++
      this.hiddenbienEtre[pageCount].hide=false;
      if ( this.hiddenbienEtre[pageCount].count==6)
      this.hiddenbienEtre[pageCount].hide=true
    }
  }
  deletebienEtre(pageCount,index) {
    if(pageCount==0){
      this.hiddenbienEtre[pageCount].count--
    this.hiddenbienEtre[pageCount].hide=false;
    }
    if(pageCount==1){
      this.hiddenbienEtre[pageCount].count--
      this.hiddenbienEtre[pageCount].hide=false;
      }
      if(pageCount==2){
        this.hiddenbienEtre[pageCount].count--
        this.hiddenbienEtre[pageCount].hide=false;
        }
        if(pageCount==3){
          this.hiddenbienEtre[pageCount].count--
          this.hiddenbienEtre[pageCount].hide=false;
          }
          if(pageCount==4){
            this.hiddenbienEtre[pageCount].count--
            this.hiddenbienEtre[pageCount].hide=false;
            }
            if(pageCount==5){
              this.hiddenbienEtre[pageCount].count--
              this.hiddenbienEtre[pageCount].hide=false;
              }
              if(pageCount==6){
                this.hiddenbienEtre[pageCount].count--
                this.hiddenbienEtre[pageCount].hide=false;
                }
                if(pageCount==7){
                  this.hiddenbienEtre[pageCount].count--
                  this.hiddenbienEtre[pageCount].hide=false;
                  }
                  if(pageCount==8){
                    this.hiddenbienEtre[pageCount].count--
                    this.hiddenbienEtre[pageCount].hide=false;
                    }
                    if(pageCount==9){
                      this.hiddenbienEtre[pageCount].count--
                      this.hiddenbienEtre[pageCount].hide=false;
                      }
                      if(pageCount==10){
                        this.hiddenbienEtre[pageCount].count--
                        this.hiddenbienEtre[pageCount].hide=false;
                        }
                        if(pageCount==11){
                          this.hiddenbienEtre[pageCount].count--
                          this.hiddenbienEtre[pageCount].hide=false;
                          }  
    this.arraybienEtre[pageCount].splice(index,1);
    console.log(this.arrayConseil[pageCount]);
  }


  get Beaute() {
    return this.productForm.get('selection_Beaute') as FormArray;
  }
  addBeaute(pageCount) {
   
    if (pageCount==0){
      this.arrayBeaute[pageCount].push(this.fb.group({conseil:''}))
      this.hiddenBeaute[pageCount].count++
      this.hiddenBeaute[pageCount].hide=false;
    
     if ( this.hiddenBeaute[pageCount].count==6)
     this.hiddenBeaute[pageCount].hide=true
       this.formJson.nbrElements1++
     }
     if (pageCount==1){
      this.arrayBeaute[pageCount].push(this.fb.group({conseil:''}))
      this.hiddenBeaute[pageCount].count++
      this.hiddenBeaute[pageCount].hide=false;
    
     if ( this.hiddenBeaute[pageCount].count==6)
     this.hiddenBeaute[pageCount].hide=true
      this.formJson.nbrElements2++
    }
    if (pageCount==2){
      this.arrayBeaute[pageCount].push(this.fb.group({conseil:''}))
      this.hiddenBeaute[pageCount].count++
      this.hiddenBeaute[pageCount].hide=false;
    
     if ( this.hiddenBeaute[pageCount].count==6)
     this.hiddenBeaute[pageCount].hide=true
      this.formJson.nbrElements3++
    }
    if (pageCount==3){
      this.arrayBeaute[pageCount].push(this.fb.group({conseil:''}))
      this.hiddenBeaute[pageCount].count++
      this.hiddenBeaute[pageCount].hide=false;
    
     if ( this.hiddenBeaute[pageCount].count==6)
     this.hiddenBeaute[pageCount].hide=true
      this.formJson.nbrElements4++
    }
    if (pageCount==4){
      this.arrayBeaute[pageCount].push(this.fb.group({conseil:''}))
      this.hiddenBeaute[pageCount].count++
      this.hiddenBeaute[pageCount].hide=false;
    
     if ( this.hiddenBeaute[pageCount].count==6)
     this.hiddenBeaute[pageCount].hide=true
      this.formJson.nbrElements5++
    }
    if (pageCount==5){
      this.arrayBeaute[pageCount].push(this.fb.group({conseil:''}))
      this.hiddenBeaute[pageCount].count++
      this.hiddenBeaute[pageCount].hide=false;
    
     if ( this.hiddenBeaute[pageCount].count==6)
     this.hiddenBeaute[pageCount].hide=true
      this.formJson.nbrElements6++
    } 
    if (pageCount==6){
      this.arrayBeaute[pageCount].push(this.fb.group({conseil:''}))
      this.hiddenBeaute[pageCount].count++
      this.hiddenBeaute[pageCount].hide=false;
    
     if ( this.hiddenBeaute[pageCount].count==6)
     this.hiddenBeaute[pageCount].hide=true
      this.formJson.nbrElements7++
    }
    if (pageCount==7){
      this.arrayBeaute[pageCount].push(this.fb.group({conseil:''}))
      this.hiddenBeaute[pageCount].count++
      this.hiddenBeaute[pageCount].hide=false;
    
     if ( this.hiddenBeaute[pageCount].count==6)
     this.hiddenBeaute[pageCount].hide=true
      this.formJson.nbrElements8++
    }
    if (pageCount==8){
      this.arrayBeaute[pageCount].push(this.fb.group({conseil:''}))
      this.hiddenBeaute[pageCount].count++
      this.hiddenBeaute[pageCount].hide=false;
    
     if ( this.hiddenBeaute[pageCount].count==6)
     this.hiddenBeaute[pageCount].hide=true
      this.formJson.nbrElements9++
    }
    if (pageCount==9){
      this.arrayBeaute[pageCount].push(this.fb.group({conseil:''}))
      this.hiddenBeaute[pageCount].count++
      this.hiddenBeaute[pageCount].hide=false;
    
     if ( this.hiddenBeaute[pageCount].count==6)
     this.hiddenBeaute[pageCount].hide=true
      this.formJson.nbrElements10++
    }
    if (pageCount==10){
      this.arrayBeaute[pageCount].push(this.fb.group({conseil:''}))
      this.hiddenBeaute[pageCount].count++
      this.hiddenBeaute[pageCount].hide=false;
    
     if ( this.hiddenBeaute[pageCount].count==6)
     this.hiddenBeaute[pageCount].hide=true
      this.formJson.nbrElements11++
    }
    if (pageCount==11){
      this.arrayBeaute[pageCount].push(this.fb.group({conseil:''}))
      this.hiddenBeaute[pageCount].count++
      this.hiddenBeaute[pageCount].hide=false;
    
     if ( this.hiddenBeaute[pageCount].count==6)
     this.hiddenBeaute[pageCount].hide=true
      this.formJson.nbrElements12++
    }
  }
  deleteBeaute(pageCount,index) {
    this.arrayBeaute[pageCount].splice(index,1);
    if(pageCount==0){
      this.hiddenBeaute[pageCount].count--
    this.hiddenBeaute[pageCount].hide=false;
    }
    if(pageCount==1){
      this.hiddenBeaute[pageCount].count--
      this.hiddenBeaute[pageCount].hide=false;
      }
      if(pageCount==2){
        this.hiddenBeaute[pageCount].count--
        this.hiddenBeaute[pageCount].hide=false;
        }
        if(pageCount==3){
          this.hiddenBeaute[pageCount].count--
          this.hiddenBeaute[pageCount].hide=false;
          }
          if(pageCount==4){
            this.hiddenBeaute[pageCount].count--
            this.hiddenBeaute[pageCount].hide=false;
            }
            if(pageCount==5){
              this.hiddenBeaute[pageCount].count--
              this.hiddenBeaute[pageCount].hide=false;
              }
              if(pageCount==6){
                this.hiddenBeaute[pageCount].count--
                this.hiddenBeaute[pageCount].hide=false;
                }
                if(pageCount==7){
                  this.hiddenBeaute[pageCount].count--
                  this.hiddenBeaute[pageCount].hide=false;
                  }
                  if(pageCount==8){
                    this.hiddenBeaute[pageCount].count--
                    this.hiddenBeaute[pageCount].hide=false;
                    }
                    if(pageCount==9){
                      this.hiddenBeaute[pageCount].count--
                      this.hiddenBeaute[pageCount].hide=false;
                      }
                      if(pageCount==10){
                        this.hiddenBeaute[pageCount].count--
                        this.hiddenBeaute[pageCount].hide=false;
                        }
                        if(pageCount==11){
                          this.hiddenBeaute[pageCount].count--
                          this.hiddenBeaute[pageCount].hide=false;
                          }  
  }

  get conseil() {
    return this.productForm.get('conseils') as FormArray;
  }
  addConseil(item,index,pageCount) {
    
 if (pageCount==0){
  this.hidden[pageCount].count++
  this.hidden[pageCount].hide=false;

 if ( this.hidden[pageCount].count==3)
 this.hidden[pageCount].hide=true
  this.arrayConseil[pageCount].push(this.fb.group({conseil:''}))
   this.formJson.nbrElements1++
 }
 if (pageCount==1){
  this.hidden[pageCount].count++
  this.hidden[pageCount].hide=false

  if ( this.hidden[pageCount].count==3)
  this.hidden[pageCount].hide=true
  this.arrayConseil[pageCount].push(this.fb.group({conseil:''}))
  this.formJson.nbrElements2++
}
if (pageCount==2){
  this.hidden[pageCount].count++
  this.hidden[pageCount].hide=false
  
  if ( this.hidden[pageCount].count==3)
  this.hidden[pageCount].hide=true
  this.arrayConseil[pageCount].push(this.fb.group({conseil:''}))
  this.formJson.nbrElements3++
}
if (pageCount==3){
  this.hidden[pageCount].hide=false

  if ( this.hidden[pageCount].count==3)
  this.hidden[pageCount].hide=true
  this.arrayConseil[pageCount].push(this.fb.group({conseil:''}))
  this.formJson.nbrElements4++
}
if (pageCount==4){
  this.hidden[pageCount].hide=false
 
  if ( this.hidden[pageCount].count==3)
  this.hidden[pageCount].hide=true
  this.arrayConseil[pageCount].push(this.fb.group({conseil:''}))
  this.formJson.nbrElements5++
}
if (pageCount==5){
  this.hidden[pageCount].hide=false
  
  if ( this.hidden[pageCount].count==3)
  this.hidden[pageCount].hide=true
  this.arrayConseil[pageCount].push(this.fb.group({conseil:''}))
  this.formJson.nbrElements6++
} 
if (pageCount==6){
  this.hidden[pageCount].hide=false
   
  if ( this.hidden[pageCount].count==3)
  this.hidden[pageCount].hide=true
  this.arrayConseil[pageCount].push(this.fb.group({conseil:''}))
  this.formJson.nbrElements7++
}
if (pageCount==7){
  this.hidden[pageCount].hide=false
  
  if ( this.hidden[pageCount].count==3)
  this.hidden[pageCount].hide=true
  this.arrayConseil[pageCount].push(this.fb.group({conseil:''}))
  this.formJson.nbrElements8++
}
if (pageCount==8){
  this.hidden[pageCount].hide=false

  if ( this.hidden[pageCount].count==3)
  this.hidden[pageCount].hide=true
  this.arrayConseil[pageCount].push(this.fb.group({conseil:''}))
  this.formJson.nbrElements9++
}
if (pageCount==9){
  this.hidden[pageCount].hide=false
  
  if ( this.hidden[pageCount].count==3)
  this.hidden[pageCount].hide=true
  this.arrayConseil[pageCount].push(this.fb.group({conseil:''}))
  this.formJson.nbrElements10++
}
if (pageCount==10){
  this.hidden[pageCount].hide=false

  if ( this.hidden[pageCount].count==3)
  this.hidden[pageCount].hide=true
  this.arrayConseil[pageCount].push(this.fb.group({conseil:''}))
  this.formJson.nbrElements11++
}
if (pageCount==11){
  this.hidden[pageCount].hide=false

  if ( this.hidden[pageCount].count==3)
  this.hidden[pageCount].hide=true
  this.arrayConseil[pageCount].push(this.fb.group({conseil:''}))
  this.formJson.nbrElements12++
}
console.log(item)


  
  }
  deleteConseil(pageCount,index) {
  
    if(pageCount==0){
      this.hidden[pageCount].count--
    this.hidden[pageCount].hide=false;
    }
    if(pageCount==1){
      this.hidden[pageCount].count--
      this.hidden[pageCount].hide=false;
      }
      if(pageCount==2){
        this.hidden[pageCount].count--
        this.hidden[pageCount].hide=false;
        }
        if(pageCount==3){
          this.hidden[pageCount].count--
          this.hidden[pageCount].hide=false;
          }
          if(pageCount==4){
            this.hidden[pageCount].count--
            this.hidden[pageCount].hide=false;
            }
            if(pageCount==5){
              this.hidden[pageCount].count--
              this.hidden[pageCount].hide=false;
              }
              if(pageCount==6){
                this.hidden[pageCount].count--
                this.hidden[pageCount].hide=false;
                }
                if(pageCount==7){
                  this.hidden[pageCount].count--
                  this.hidden[pageCount].hide=false;
                  }
                  if(pageCount==8){
                    this.hidden[pageCount].count--
                    this.hidden[pageCount].hide=false;
                    }
                    if(pageCount==9){
                      this.hidden[pageCount].count--
                      this.hidden[pageCount].hide=false;
                      }
                      if(pageCount==10){
                        this.hidden[pageCount].count--
                        this.hidden[pageCount].hide=false;
                        }
                        if(pageCount==11){
                          this.hidden[pageCount].count--
                          this.hidden[pageCount].hide=false;
                          }  
    this.conseil.removeAt(index);
    this.arrayConseil[pageCount].splice(index,1);
    console.log(this.arrayConseil[pageCount]);
  }

  get nouvelle() {
    return this.productForm.get('nouveautes') as FormArray;
  }
  addNouvelle(pageCount) {

 

   if (pageCount==0){
    this.arrayNouveaute[pageCount].push(this.fb.group({conseil:''}))
     this.formJson.nbrElements1++
     this.hiddenNouveau[pageCount].count++
     this.hiddenNouveau[pageCount].hide=false;
   
    if ( this.hiddenNouveau[pageCount].count==3)
    this.hiddenNouveau[pageCount].hide=true
   }
   if (pageCount==1){
    this.arrayNouveaute[pageCount].push(this.fb.group({conseil:''}))
    this.formJson.nbrElements2++
    this.hiddenNouveau[pageCount].count++
    this.hiddenNouveau[pageCount].hide=false;
  
   if ( this.hiddenNouveau[pageCount].count==3)
   this.hiddenNouveau[pageCount].hide=true
  }
  if (pageCount==2){
    this.arrayNouveaute[pageCount].push(this.fb.group({conseil:''}))
    this.formJson.nbrElements3++
    this.hiddenNouveau[pageCount].count++
    this.hiddenNouveau[pageCount].hide=false;
  
   if ( this.hiddenNouveau[pageCount].count==3)
   this.hiddenNouveau[pageCount].hide=true
  }
  if (pageCount==3){
    this.arrayNouveaute[pageCount].push(this.fb.group({conseil:''}))
    this.formJson.nbrElements4++
    this.hiddenNouveau[pageCount].count++
    this.hiddenNouveau[pageCount].hide=false;
  
   if ( this.hiddenNouveau[pageCount].count==3)
   this.hiddenNouveau[pageCount].hide=true
  }
  if (pageCount==4){
    this.arrayNouveaute[pageCount].push(this.fb.group({conseil:''}))
    this.formJson.nbrElements5++
    this.hiddenNouveau[pageCount].count++
    this.hiddenNouveau[pageCount].hide=false;
  
   if ( this.hiddenNouveau[pageCount].count==3)
   this.hiddenNouveau[pageCount].hide=true
  }
  if (pageCount==5){
    this.arrayNouveaute[pageCount].push(this.fb.group({conseil:''}))
    this.formJson.nbrElements6++
    this.hiddenNouveau[pageCount].count++
    this.hiddenNouveau[pageCount].hide=false;
  
   if ( this.hiddenNouveau[pageCount].count==3)
   this.hiddenNouveau[pageCount].hide=true
  } 
  if (pageCount==6){
    this.arrayNouveaute[pageCount].push(this.fb.group({conseil:''}))
    this.formJson.nbrElements7++
    this.hiddenNouveau[pageCount].count++
    this.hiddenNouveau[pageCount].hide=false;
  
   if ( this.hiddenNouveau[pageCount].count==3)
   this.hiddenNouveau[pageCount].hide=true
  }
  if (pageCount==7){
    this.arrayNouveaute[pageCount].push(this.fb.group({conseil:''}))
    this.formJson.nbrElements8++
    this.hiddenNouveau[pageCount].count++
  this.hiddenNouveau[pageCount].hide=false;

 if ( this.hiddenNouveau[pageCount].count==3)
 this.hiddenNouveau[pageCount].hide=true
  }
  if (pageCount==8){
    this.arrayNouveaute[pageCount].push(this.fb.group({conseil:''}))
    this.formJson.nbrElements9++
    this.hiddenNouveau[pageCount].count++
  this.hiddenNouveau[pageCount].hide=false;

 if ( this.hiddenNouveau[pageCount].count==3)
 this.hiddenNouveau[pageCount].hide=true
  }
  if (pageCount==9){
    this.arrayNouveaute[pageCount].push(this.fb.group({conseil:''}))
    this.formJson.nbrElements10++
    this.hiddenNouveau[pageCount].count++
    this.hiddenNouveau[pageCount].hide=false;
  
   if ( this.hiddenNouveau[pageCount].count==3)
   this.hiddenNouveau[pageCount].hide=true
  }
  if (pageCount==10){
    this.arrayNouveaute[pageCount].push(this.fb.group({conseil:''}))
    this.formJson.nbrElements11++
    this.hiddenNouveau[pageCount].count++
  this.hiddenNouveau[pageCount].hide=false;

 if ( this.hiddenNouveau[pageCount].count==3)
 this.hiddenNouveau[pageCount].hide=true
  }
  if (pageCount==11){
    this.arrayNouveaute[pageCount].push(this.fb.group({conseil:''}))
    this.formJson.nbrElements12++
    this.hiddenNouveau[pageCount].count++
    this.hiddenNouveau[pageCount].hide=false;
  
   if ( this.hiddenNouveau[pageCount].count==3)
   this.hiddenNouveau[pageCount].hide=true
  }
  }
  deleteNouvelle(pageCount,index) {
    if(pageCount==0){
      this.hiddenNouveau[pageCount].count--
    this.hiddenNouveau[pageCount].hide=false;
    }
    if(pageCount==1){
      this.hiddenNouveau[pageCount].count--
      this.hiddenNouveau[pageCount].hide=false;
      }
      if(pageCount==2){
        this.hiddenNouveau[pageCount].count--
        this.hiddenNouveau[pageCount].hide=false;
        }
        if(pageCount==3){
          this.hiddenNouveau[pageCount].count--
          this.hiddenNouveau[pageCount].hide=false;
          }
          if(pageCount==4){
            this.hiddenNouveau[pageCount].count--
            this.hiddenNouveau[pageCount].hide=false;
            }
            if(pageCount==5){
              this.hiddenNouveau[pageCount].count--
              this.hiddenNouveau[pageCount].hide=false;
              }
              if(pageCount==6){
                this.hiddenNouveau[pageCount].count--
                this.hiddenNouveau[pageCount].hide=false;
                }
                if(pageCount==7){
                  this.hiddenNouveau[pageCount].count--
                  this.hiddenNouveau[pageCount].hide=false;
                  }
                  if(pageCount==8){
                    this.hiddenNouveau[pageCount].count--
                    this.hiddenNouveau[pageCount].hide=false;
                    }
                    if(pageCount==9){
                      this.hiddenNouveau[pageCount].count--
                      this.hiddenNouveau[pageCount].hide=false;
                      }
                      if(pageCount==10){
                        this.hiddenNouveau[pageCount].count--
                        this.hiddenNouveau[pageCount].hide=false;
                        }
                        if(pageCount==11){
                          this.hiddenNouveau[pageCount].count--
                          this.hiddenNouveau[pageCount].hide=false;
                          }  
  
    this.arrayNouveaute[pageCount].splice(index,1);
   
  }
  openDialogBeaute(pageCount,poitIndex){
    const dialogRef= this.dialog.open(DialogContentExampleDialogComponent
    )
dialogRef.afterClosed().subscribe(res=>{
  this.colorBeaute.length=poitIndex+1
  this.colorBeaute[poitIndex]='warn'

  console.log('resultat', res)
if (pageCount==0){
  if (poitIndex==0){
    this.formJson.prixBeaute11=res.nPrix
    this.formJson.ancienPrixBeaute11=res.aPrix
  }
  if (poitIndex==1){
    this.formJson.prixBeaute12=res.nPrix
    this.formJson.ancienPrixBeaute12=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixBeaute13=res.nPrix
    this.formJson.ancienPrixBeaute13=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixBeaute14=res.nPrix
    this.formJson.ancienPrixBeaute14=res.aPrix
  }
}
  
if (pageCount==1){
  if (poitIndex==0){
    this.formJson.prixBeaute21=res.nPrix
    this.formJson.ancienPrixBeaute21=res.aPrix
  }
  if (poitIndex==1){
    this.formJson.prixBeaute22=res.nPrix
    this.formJson.ancienPrixBeaute22=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixBeaute23=res.nPrix
    this.formJson.ancienPrixBeaute23=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixBeaute24=res.nPrix
    this.formJson.ancienPrixBeaute24=res.aPrix
  }
}
if (pageCount==2){
  if (poitIndex==0){
    this.formJson.prixBeaute31=res.nPrix
    this.formJson.ancienPrixBeaute31=res.aPrix
  }
  if (poitIndex==1){
    this.formJson.prixBeaute32=res.nPrix
    this.formJson.ancienPrixBeaute32=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixBeaute33=res.nPrix
    this.formJson.ancienPrixBeaute33=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixBeaute34=res.nPrix
    this.formJson.ancienPrixBeaute34=res.aPrix
  }
}
if (pageCount==3){
  if (poitIndex==0){
    this.formJson.prixBeaute41=res.nPrix
    this.formJson.ancienPrixBeaute41=res.aPrix
  }
  if (poitIndex==1){
    this.formJson.prixBeaute42=res.nPrix
    this.formJson.ancienPrixBeaute42=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixBeaute43=res.nPrix
    this.formJson.ancienPrixBeaute43=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixBeaute44=res.nPrix
    this.formJson.ancienPrixBeaute44=res.aPrix
  }
}
if (pageCount==4){
  if (poitIndex==0){
    this.formJson.prixBeaute51=res.nPrix
    this.formJson.ancienPrixBeaute51=res.aPrix
  }
  if (poitIndex==1){
    this.formJson.prixBeaute52=res.nPrix
    this.formJson.ancienPrixBeaute52=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixBeaute53=res.nPrix
    this.formJson.ancienPrixBeaute53=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixBeaute54=res.nPrix
    this.formJson.ancienPrixBeaute54=res.aPrix
  }
}
if (pageCount==5){
  if (poitIndex==0){
    this.formJson.prixBeaute61=res.nPrix
    this.formJson.ancienPrixBeaute61=res.aPrix
  }
  if (poitIndex==1){
    this.formJson.prixBeaute62=res.nPrix
    this.formJson.ancienPrixBeaute62=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixBeaute63=res.nPrix
    this.formJson.ancienPrixBeaute63=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixBeaute64=res.nPrix
    this.formJson.ancienPrixBeaute64=res.aPrix
  }
}
if (pageCount==6){
  if (poitIndex==0){
    this.formJson.prixBeaute71=res.nPrix
    this.formJson.ancienPrixBeaute71=res.aPrix
  }
  if (poitIndex==1){
    this.formJson.prixBeaute72=res.nPrix
    this.formJson.ancienPrixBeaute72=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixBeaute73=res.nPrix
    this.formJson.ancienPrixBeaute73=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixBeaute74=res.nPrix
    this.formJson.ancienPrixBeaute74=res.aPrix
  }
}
if (pageCount==7){
  if (poitIndex==0){
    this.formJson.prixBeaute81=res.nPrix
    this.formJson.ancienPrixBeaute81=res.aPrix
  }
  if (poitIndex==1){
    this.formJson.prixBeaute82=res.nPrix
    this.formJson.ancienPrixBeaute82=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixBeaute83=res.nPrix
    this.formJson.ancienPrixBeaute83=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixBeaute84=res.nPrix
    this.formJson.ancienPrixBeaute84=res.aPrix
  }
}
if (pageCount==8){
  if (poitIndex==0){
    this.formJson.prixBeaute91=res.nPrix
    this.formJson.ancienPrixBeaute91=res.aPrix
  }
  if (poitIndex==1){
    this.formJson.prixBeaute92=res.nPrix
    this.formJson.ancienPrixBeaute92=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixBeaute93=res.nPrix
    this.formJson.ancienPrixBeaute93=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixBeaute94=res.nPrix
    this.formJson.ancienPrixBeaute94=res.aPrix
  }
}
if (pageCount==9){
  if (poitIndex==0){
    this.formJson.prixBeaute101=res.nPrix
    this.formJson.ancienPrixBeaute101=res.aPrix
  }
  if (poitIndex==1){
    this.formJson.prixBeaute102=res.nPrix
    this.formJson.ancienPrixBeaute102=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixBeaute103=res.nPrix
    this.formJson.ancienPrixBeaute103=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixBeaute104=res.nPrix
    this.formJson.ancienPrixBeaute104=res.aPrix
  }
}
if (pageCount==10){
  if (poitIndex==0){
    this.formJson.prixBeaute111=res.nPrix
    this.formJson.ancienPrixBeaute11=res.aPrix
  }
  if (poitIndex==1){
    this.formJson.prixBeaute112=res.nPrix
    this.formJson.ancienPrixBeaute12=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixBeaute113=res.nPrix
    this.formJson.ancienPrixBeaute13=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixBeaute114=res.nPrix
    this.formJson.ancienPrixBeaute14=res.aPrix
  }
}
if (pageCount==11){
  if (poitIndex==0){
    this.formJson.prixBeaute121=res.nPrix
    this.formJson.ancienPrixBeaute11=res.aPrix
  }
  if (poitIndex==1){
    this.formJson.prixBeaute122=res.nPrix
    this.formJson.ancienPrixBeaute12=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixBeaute123=res.nPrix
    this.formJson.ancienPrixBeaute13=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixBeaute124=res.nPrix
    this.formJson.ancienPrixBeaute14=res.aPrix
  }
}


  
})



  
  }
  openDialogBebe(pageCount,poitIndex){
    const dialogRef= this.dialog.open(DialogContentExampleDialogComponent
    )
dialogRef.afterClosed().subscribe(res=>{
  this.colorBebe.length=poitIndex+1
  this.colorBebe[poitIndex]="warn"
  console.log('resultat', res)
if (pageCount==0){
  if (poitIndex==0){
    this.formJson.prixBebe11=res.nPrix
    this.formJson.ancienPrixBebe11=res.aPrix
  }
  if (poitIndex==1){
    this.formJson.prixBebe12=res.nPrix
    this.formJson.ancienPrixBebe12=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixBebe13=res.nPrix
    this.formJson.ancienPrixBebe13=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixBebe14=res.nPrix
    this.formJson.ancienPrixBebe14=res.aPrix
  }
}
  
if (pageCount==1){
  
  if (poitIndex==0){
    this.formJson.prixBebe21=res.nPrix
    this.formJson.ancienPrixBebe21=res.aPrix
    console.log('prixBebe21',this.formJson.prixBebe21)
  }
  if (poitIndex==1){
    this.formJson.prixBebe22=res.nPrix
    this.formJson.ancienPrixBebe22=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixBebe23=res.nPrix
    this.formJson.ancienPrixBebe23=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixBebe24=res.nPrix
    this.formJson.ancienPrixBebe24=res.aPrix
  }
}
if (pageCount==2){
  if (poitIndex==0){
    
    this.formJson.prixBebe31=res.nPrix
    this.formJson.ancienPrixBebe31=res.aPrix
    
  }
  if (poitIndex==1){
    this.formJson.prixBebe32=res.nPrix
    this.formJson.ancienPrixBebe32=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixBebe33=res.nPrix
    this.formJson.ancienPrixBebe33=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixBebe34=res.nPrix
    this.formJson.ancienPrixBebe34=res.aPrix
  }
}
if (pageCount==3){
  if (poitIndex==0){
    this.formJson.prixBebe41=res.nPrix
    this.formJson.ancienPrixBebe41=res.aPrix
  }
  if (poitIndex==1){
    this.formJson.prixBebe42=res.nPrix
    this.formJson.ancienPrixBebe42=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixBebe43=res.nPrix
    this.formJson.ancienPrixBebe43=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixBebe44=res.nPrix
    this.formJson.ancienPrixBebe44=res.aPrix
  }
}
if (pageCount==4){
  if (poitIndex==0){
    this.formJson.prixBebe51=res.nPrix
    this.formJson.ancienPrixBebe51=res.aPrix
  }
  if (poitIndex==1){
    this.formJson.prixBebe52=res.nPrix
    this.formJson.ancienPrixBebe52=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixBebe53=res.nPrix
    this.formJson.ancienPrixBebe53=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixBebe54=res.nPrix
    this.formJson.ancienPrixBebe54=res.aPrix
  }
}
if (pageCount==5){
  if (poitIndex==0){
    this.formJson.prixBebe61=res.nPrix
    this.formJson.ancienPrixBebe61=res.aPrix
  }
  if (poitIndex==1){
    this.formJson.prixBebe62=res.nPrix
    this.formJson.ancienPrixBebe62=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixBebe63=res.nPrix
    this.formJson.ancienPrixBebe63=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixBebe64=res.nPrix
    this.formJson.ancienPrixBebe64=res.aPrix
  }
}
if (pageCount==6){
  if (poitIndex==0){
    this.formJson.prixBebe71=res.nPrix
    this.formJson.ancienPrixBebe71=res.aPrix
  }
  if (poitIndex==1){
    this.formJson.prixBebe72=res.nPrix
    this.formJson.ancienPrixBebe72=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixBebe73=res.nPrix
    this.formJson.ancienPrixBebe73=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixBebe74=res.nPrix
    this.formJson.ancienPrixBebe74=res.aPrix
  }
}
if (pageCount==7){
  if (poitIndex==0){
    this.formJson.prixBebe81=res.nPrix
    this.formJson.ancienPrixBebe81=res.aPrix
  }
  if (poitIndex==1){
    this.formJson.prixBebe82=res.nPrix
    this.formJson.ancienPrixBebe82=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixBebe83=res.nPrix
    this.formJson.ancienPrixBebe83=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixBebe84=res.nPrix
    this.formJson.ancienPrixBebe84=res.aPrix
  }
}
if (pageCount==8){
  if (poitIndex==0){
    this.formJson.prixBebe91=res.nPrix
    this.formJson.ancienPrixBebe91=res.aPrix
  }
  if (poitIndex==1){
    this.formJson.prixBebe92=res.nPrix
    this.formJson.ancienPrixBebe92=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixBebe93=res.nPrix
    this.formJson.ancienPrixBebe93=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixBebe94=res.nPrix
    this.formJson.ancienPrixBebe94=res.aPrix
  }
}
if (pageCount==9){
  if (poitIndex==0){
    this.formJson.prixBebe101=res.nPrix
    this.formJson.ancienPrixBebe101=res.aPrix
  }
  if (poitIndex==1){
    this.formJson.prixBebe102=res.nPrix
    this.formJson.ancienPrixBebe102=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixBebe103=res.nPrix
    this.formJson.ancienPrixBebe103=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixBebe104=res.nPrix
    this.formJson.ancienPrixBebe104=res.aPrix
  }
}
if (pageCount==10){
  if (poitIndex==0){
    this.formJson.prixBebe111=res.nPrix
    this.formJson.ancienPrixBebe11=res.aPrix
  }
  if (poitIndex==1){
    this.formJson.prixBebe112=res.nPrix
    this.formJson.ancienPrixBebe12=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixBebe113=res.nPrix
    this.formJson.ancienPrixBebe13=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixBebe114=res.nPrix
    this.formJson.ancienPrixBebe14=res.aPrix
  }
}
if (pageCount==11){
  if (poitIndex==0){
    this.formJson.prixBebe121=res.nPrix
    this.formJson.ancienPrixBebe11=res.aPrix
  }
  if (poitIndex==1){
    this.formJson.prixBebe122=res.nPrix
    this.formJson.ancienPrixBebe12=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixBebe123=res.nPrix
    this.formJson.ancienPrixBebe13=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixBebe124=res.nPrix
    this.formJson.ancienPrixBebe14=res.aPrix
  }
}


  
})



  
  }
  openDialogbienEtre(pageCount,poitIndex){
    const dialogRef= this.dialog.open(DialogContentExampleDialogComponent
    )
dialogRef.afterClosed().subscribe(res=>{
  this.colorbienEtre.length=poitIndex+1

  this.colorbienEtre[poitIndex]='warn'
  console.log('resultat', res)
if (pageCount==0){
  if (poitIndex==0){
    this.formJson.prixBienetre11=res.nPrix
  .aPrix
  }
  if (poitIndex==1){
    this.formJson.prixBienetre12=res.nPrix
    this.formJson.ancienPrixBienetre12=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixBienetre13=res.nPrix
    this.formJson.ancienPrixBienetre13=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixBienetre14=res.nPrix
    this.formJson.ancienPrixBienetre14=res.aPrix
  }
}
  
if (pageCount==1){
  if (poitIndex==0){
    this.formJson.prixBienetre21=res.nPrix
    this.formJson.ancienPrixBienetre21=res.aPrix
  }
  if (poitIndex==1){
    this.formJson.prixBienetre22=res.nPrix
    this.formJson.ancienPrixBienetre22=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixBienetre23=res.nPrix
    this.formJson.ancienPrixBienetre23=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixBienetre24=res.nPrix
    this.formJson.ancienPrixBienetre24=res.aPrix
  }
}
if (pageCount==2){
  if (poitIndex==0){
    this.formJson.prixBienetre31=res.nPrix
    this.formJson.ancienPrixBienetre31=res.aPrix
  }
  if (poitIndex==1){
    this.formJson.prixBienetre32=res.nPrix
    this.formJson.ancienPrixBienetre32=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixBienetre33=res.nPrix
    this.formJson.ancienPrixBienetre33=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixBienetre34=res.nPrix
    this.formJson.ancienPrixBienetre34=res.aPrix
  }
}
if (pageCount==3){
  if (poitIndex==0){
    this.formJson.prixBienetre41=res.nPrix
    this.formJson.ancienPrixBienetre41=res.aPrix
  }
  if (poitIndex==1){
    this.formJson.prixBienetre42=res.nPrix
    this.formJson.ancienPrixBienetre42=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixBienetre43=res.nPrix
    this.formJson.ancienPrixBienetre43=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixBienetre44=res.nPrix
    this.formJson.ancienPrixBienetre44=res.aPrix
  }
}
if (pageCount==4){
  if (poitIndex==0){
    this.formJson.prixBienetre51=res.nPrix
    this.formJson.ancienPrixBienetre51=res.aPrix
  }
  if (poitIndex==1){
    this.formJson.prixBienetre52=res.nPrix
    this.formJson.ancienPrixBienetre52=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixBienetre53=res.nPrix
    this.formJson.ancienPrixBienetre53=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixBienetre54=res.nPrix
    this.formJson.ancienPrixBienetre54=res.aPrix
  }
}
if (pageCount==5){
  if (poitIndex==0){
    this.formJson.prixBienetre61=res.nPrix
    this.formJson.ancienPrixBienetre61=res.aPrix
  }
  if (poitIndex==1){
    this.formJson.prixBienetre62=res.nPrix
    this.formJson.ancienPrixBienetre62=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixBienetre63=res.nPrix
    this.formJson.ancienPrixBienetre63=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixBienetre64=res.nPrix
    this.formJson.ancienPrixBienetre64=res.aPrix
  }
}
if (pageCount==6){
  if (poitIndex==0){
    this.formJson.prixBienetre71=res.nPrix
    this.formJson.ancienPrixBienetre71=res.aPrix
  }
  if (poitIndex==1){
    this.formJson.prixBienetre72=res.nPrix
    this.formJson.ancienPrixBienetre72=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixBienetre73=res.nPrix
    this.formJson.ancienPrixBienetre73=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixBienetre74=res.nPrix
    this.formJson.ancienPrixBienetre74=res.aPrix
  }
}
if (pageCount==7){
  if (poitIndex==0){
    this.formJson.prixBienetre81=res.nPrix
    this.formJson.ancienPrixBienetre81=res.aPrix
  }
  if (poitIndex==1){
    this.formJson.prixBienetre82=res.nPrix
    this.formJson.ancienPrixBienetre82=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixBienetre83=res.nPrix
    this.formJson.ancienPrixBienetre83=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixBienetre84=res.nPrix
    this.formJson.ancienPrixBienetre84=res.aPrix
  }
}
if (pageCount==8){
  if (poitIndex==0){
    this.formJson.prixBienetre91=res.nPrix
    this.formJson.ancienPrixBienetre91=res.aPrix
  }
  if (poitIndex==1){
    this.formJson.prixBienetre92=res.nPrix
    this.formJson.ancienPrixBienetre92=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixBienetre93=res.nPrix
    this.formJson.ancienPrixBienetre93=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixBienetre94=res.nPrix
    this.formJson.ancienPrixBienetre94=res.aPrix
  }
}
if (pageCount==9){
  if (poitIndex==0){
    this.formJson.prixBienetre101=res.nPrix
    this.formJson.ancienPrixBienetre101=res.aPrix
  }
  if (poitIndex==1){
    this.formJson.prixBienetre102=res.nPrix
    this.formJson.ancienPrixBienetre102=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixBienetre103=res.nPrix
    this.formJson.ancienPrixBienetre103=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixBienetre104=res.nPrix
    this.formJson.ancienPrixBienetre104=res.aPrix
  }
}
if (pageCount==10){
  if (poitIndex==0){
    this.formJson.prixBienetre111=res.nPrix
    this.formJson.ancienPrixBienetre11=res.aPrix
  }
  if (poitIndex==1){
    this.formJson.prixBienetre112=res.nPrix
    this.formJson.ancienPrixBienetre12=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixBienetre113=res.nPrix
    this.formJson.ancienPrixBienetre13=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixBienetre114=res.nPrix
    this.formJson.ancienPrixBienetre14=res.aPrix
  }
}
if (pageCount==11){
  if (poitIndex==0){
    this.formJson.prixBienetre121=res.nPrix
    this.formJson.ancienPrixBienetre11=res.aPrix
  }
  if (poitIndex==1){
    this.formJson.prixBienetre122=res.nPrix
    this.formJson.ancienPrixBienetre12=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixBienetre123=res.nPrix
    this.formJson.ancienPrixBienetre13=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixBienetre124=res.nPrix
    this.formJson.ancienPrixBienetre124=res.aPrix
  }
}


  
})



  
  }
  openDialogSante(pageCount,poitIndex){
    const dialogRef= this.dialog.open(DialogContentExampleDialogComponent
    )
dialogRef.afterClosed().subscribe(res=>{
  this.colorSante.length=poitIndex+1
  
  this.colorSante[poitIndex]='warn'
  console.log('resultat', res)
if (pageCount==0){
  if (poitIndex==0){
    this.formJson.prixSante11=res.nPrix
    this.formJson.ancienPrixSante11=res.aPrix
  }
  if (poitIndex==1){
    this.formJson.prixSante12=res.nPrix
    this.formJson.ancienPrixSante12=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixSante13=res.nPrix
    this.formJson.ancienPrixSante13=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixSante14=res.nPrix
    this.formJson.ancienPrixSante14=res.aPrix
  }
}
  
if (pageCount==1){
  if (poitIndex==0){
    this.formJson.prixSante21=res.nPrix
    this.formJson.ancienPrixSante21=res.aPrix
  }
  if (poitIndex==1){
    this.formJson.prixSante22=res.nPrix
    this.formJson.ancienPrixSante22=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixSante23=res.nPrix
    this.formJson.ancienPrixSante23=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixSante24=res.nPrix
    this.formJson.ancienPrixSante24=res.aPrix
  }
}
if (pageCount==2){
  if (poitIndex==0){
    this.formJson.prixSante31=res.nPrix
    this.formJson.ancienPrixSante31=res.aPrix
  }
  if (poitIndex==1){
    this.formJson.prixSante32=res.nPrix
    this.formJson.ancienPrixSante32=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixSante33=res.nPrix
    this.formJson.ancienPrixSante33=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixSante34=res.nPrix
    this.formJson.ancienPrixSante34=res.aPrix
  }
}
if (pageCount==3){
  if (poitIndex==0){
    this.formJson.prixSante41=res.nPrix
    this.formJson.ancienPrixSante41=res.aPrix
  }
  if (poitIndex==1){
    this.formJson.prixSante42=res.nPrix
    this.formJson.ancienPrixSante42=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixSante43=res.nPrix
    this.formJson.ancienPrixSante43=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixSante44=res.nPrix
    this.formJson.ancienPrixSante44=res.aPrix
  }
}
if (pageCount==4){
  if (poitIndex==0){
    this.formJson.prixSante51=res.nPrix
    this.formJson.ancienPrixSante51=res.aPrix
  }
  if (poitIndex==1){
    this.formJson.prixSante52=res.nPrix
    this.formJson.ancienPrixSante52=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixSante53=res.nPrix
    this.formJson.ancienPrixSante53=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixSante54=res.nPrix
    this.formJson.ancienPrixSante54=res.aPrix
  }
}
if (pageCount==5){
  if (poitIndex==0){
    this.formJson.prixSante61=res.nPrix
    this.formJson.ancienPrixSante61=res.aPrix
  }
  if (poitIndex==1){
    this.formJson.prixSante62=res.nPrix
    this.formJson.ancienPrixSante62=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixSante63=res.nPrix
    this.formJson.ancienPrixSante63=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixSante64=res.nPrix
    this.formJson.ancienPrixSante64=res.aPrix
  }
}
if (pageCount==6){
  if (poitIndex==0){
    this.formJson.prixSante71=res.nPrix
    this.formJson.ancienPrixSante71=res.aPrix
  }
  if (poitIndex==1){
    this.formJson.prixSante72=res.nPrix
    this.formJson.ancienPrixSante72=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixSante73=res.nPrix
    this.formJson.ancienPrixSante73=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixSante74=res.nPrix
    this.formJson.ancienPrixSante74=res.aPrix
  }
}
if (pageCount==7){
  if (poitIndex==0){
    this.formJson.prixSante81=res.nPrix
    this.formJson.ancienPrixSante81=res.aPrix
  }
  if (poitIndex==1){
    this.formJson.prixSante82=res.nPrix
    this.formJson.ancienPrixSante82=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixSante83=res.nPrix
    this.formJson.ancienPrixSante83=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixSante84=res.nPrix
    this.formJson.ancienPrixSante84=res.aPrix
  }
}
if (pageCount==8){
  if (poitIndex==0){
    this.formJson.prixSante91=res.nPrix
    this.formJson.ancienPrixSante91=res.aPrix
  }
  if (poitIndex==1){
    this.formJson.prixSante92=res.nPrix
    this.formJson.ancienPrixSante92=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixSante93=res.nPrix
    this.formJson.ancienPrixSante93=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixSante94=res.nPrix
    this.formJson.ancienPrixSante94=res.aPrix
  }
}
if (pageCount==9){
  if (poitIndex==0){
    this.formJson.prixSante101=res.nPrix
    this.formJson.ancienPrixSante101=res.aPrix
  }
  if (poitIndex==1){
    this.formJson.prixSante102=res.nPrix
    this.formJson.ancienPrixSante102=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixSante103=res.nPrix
    this.formJson.ancienPrixSante103=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixSante104=res.nPrix
    this.formJson.ancienPrixSante104=res.aPrix
  }
}
if (pageCount==10){
  if (poitIndex==0){
    this.formJson.prixSante111=res.nPrix
    this.formJson.ancienPrixSante111=res.aPrix
  }
  if (poitIndex==1){
    this.formJson.prixSante112=res.nPrix
    this.formJson.ancienPrixSante112=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixSante113=res.nPrix
    this.formJson.ancienPrixSante113=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixSante114=res.nPrix
    this.formJson.ancienPrixSante114=res.aPrix
  }
}
if (pageCount==11){
  if (poitIndex==0){
    this.formJson.prixSante121=res.nPrix
    this.formJson.ancienPrixSante121=res.aPrix
  }
  if (poitIndex==1){
    this.formJson.prixSante122=res.nPrix
    this.formJson.ancienPrixSante122=res.aPrix
  }
  if (poitIndex==2){
    this.formJson.prixSante123=res.nPrix
    this.formJson.ancienPrixSante123=res.aPrix
  }
  if (poitIndex==3){
    this.formJson.prixSante124=res.nPrix
    this.formJson.ancienPrixSante124=res.aPrix
  }
  if (poitIndex==4){
    this.formJson.prixSante125=res.nPrix
    this.formJson.ancienPrixSante125=res.aPrix
  }
  if (poitIndex==5){
    this.formJson.prixSante126=res.nPrix
    this.formJson.ancienPrixSante126=res.aPrix
  }
}


  
})



  
  }
  openDialogUne(pageCount,poitIndex){
    const dialogRef= this.dialog.open(DialogContentExampleDialogComponent
      )
  dialogRef.afterClosed().subscribe(res=>{
    console.log('resultat', res)
    this.color.length=poitIndex+1
    this.color[poitIndex]='warn'
  if (pageCount==0){
    if (poitIndex==0){
      this.formJson.prixProduit11=res.nPrix
      this.formJson.ancienPrixProduit11=res.aPrix
    }
    if (poitIndex==1){
      this.formJson.prixProduit12=res.nPrix
      this.formJson.ancienPrixProduit12=res.aPrix
    }
    if (poitIndex==2){
      this.formJson.prixProduit13=res.nPrix
      this.formJson.ancienPrixProduit13=res.aPrix
    }
    if (poitIndex==3){
      this.formJson.prixProduit14=res.nPrix
      this.formJson.ancienPrixProduit14=res.aPrix
    }
  }
    
  if (pageCount==1){
    if (poitIndex==0){
      this.formJson.prixProduit21=res.nPrix
      this.formJson.ancienPrixProduit21=res.aPrix
    }
    if (poitIndex==1){
      this.formJson.prixProduit22=res.nPrix
      this.formJson.ancienPrixProduit22=res.aPrix
    }
    if (poitIndex==2){
      this.formJson.prixProduit23=res.nPrix
      this.formJson.ancienPrixProduit23=res.aPrix
    }
    if (poitIndex==3){
      this.formJson.prixProduit24=res.nPrix
      this.formJson.ancienPrixProduit24=res.aPrix
    }
  }
  if (pageCount==2){
    if (poitIndex==0){
      this.formJson.prixProduit31=res.nPrix
      this.formJson.ancienPrixProduit31=res.aPrix
    }
    if (poitIndex==1){
      this.formJson.prixProduit32=res.nPrix
      this.formJson.ancienPrixProduit32=res.aPrix
    }
    if (poitIndex==2){
      this.formJson.prixProduit33=res.nPrix
      this.formJson.ancienPrixProduit33=res.aPrix
    }
    if (poitIndex==3){
      this.formJson.prixProduit34=res.nPrix
      this.formJson.ancienPrixProduit34=res.aPrix
    }
  }
  if (pageCount==3){
    if (poitIndex==0){
      this.formJson.prixProduit41=res.nPrix
      this.formJson.ancienPrixProduit41=res.aPrix
    }
    if (poitIndex==1){
      this.formJson.prixProduit42=res.nPrix
      this.formJson.ancienPrixProduit42=res.aPrix
    }
    if (poitIndex==2){
      this.formJson.prixProduit43=res.nPrix
      this.formJson.ancienPrixProduit43=res.aPrix
    }
    if (poitIndex==3){
      this.formJson.prixProduit44=res.nPrix
      this.formJson.ancienPrixProduit44=res.aPrix
    }
  }
  if (pageCount==4){
    if (poitIndex==0){
      this.formJson.prixProduit51=res.nPrix
      this.formJson.ancienPrixProduit51=res.aPrix
    }
    if (poitIndex==1){
      this.formJson.prixProduit52=res.nPrix
      this.formJson.ancienPrixProduit52=res.aPrix
    }
    if (poitIndex==2){
      this.formJson.prixProduit53=res.nPrix
      this.formJson.ancienPrixProduit53=res.aPrix
    }
    if (poitIndex==3){
      this.formJson.prixProduit54=res.nPrix
      this.formJson.ancienPrixProduit54=res.aPrix
    }
  }
  if (pageCount==5){
    if (poitIndex==0){
      this.formJson.prixProduit61=res.nPrix
      this.formJson.ancienPrixProduit61=res.aPrix
    }
    if (poitIndex==1){
      this.formJson.prixProduit62=res.nPrix
      this.formJson.ancienPrixProduit62=res.aPrix
    }
    if (poitIndex==2){
      this.formJson.prixProduit63=res.nPrix
      this.formJson.ancienPrixProduit63=res.aPrix
    }
    if (poitIndex==3){
      this.formJson.prixProduit64=res.nPrix
      this.formJson.ancienPrixProduit64=res.aPrix
    }
  }
  if (pageCount==6){
    if (poitIndex==0){
      this.formJson.prixProduit71=res.nPrix
      this.formJson.ancienPrixProduit71=res.aPrix
    }
    if (poitIndex==1){
      this.formJson.prixProduit72=res.nPrix
      this.formJson.ancienPrixProduit72=res.aPrix
    }
    if (poitIndex==2){
      this.formJson.prixProduit73=res.nPrix
      this.formJson.ancienPrixProduit73=res.aPrix
    }
    if (poitIndex==3){
      this.formJson.prixProduit74=res.nPrix
      this.formJson.ancienPrixProduit74=res.aPrix
    }
  }
  if (pageCount==7){
    if (poitIndex==0){
      this.formJson.prixProduit81=res.nPrix
      this.formJson.ancienPrixProduit81=res.aPrix
    }
    if (poitIndex==1){
      this.formJson.prixProduit82=res.nPrix
      this.formJson.ancienPrixProduit82=res.aPrix
    }
    if (poitIndex==2){
      this.formJson.prixProduit83=res.nPrix
      this.formJson.ancienPrixProduit83=res.aPrix
    }
    if (poitIndex==3){
      this.formJson.prixProduit84=res.nPrix
      this.formJson.ancienPrixProduit84=res.aPrix
    }
  }
  if (pageCount==8){
    if (poitIndex==0){
      this.formJson.prixProduit91=res.nPrix
      this.formJson.ancienPrixProduit91=res.aPrix
    }
    if (poitIndex==1){
      this.formJson.prixProduit92=res.nPrix
      this.formJson.ancienPrixProduit92=res.aPrix
    }
    if (poitIndex==2){
      this.formJson.prixProduit93=res.nPrix
      this.formJson.ancienPrixProduit93=res.aPrix
    }
    if (poitIndex==3){
      this.formJson.prixProduit94=res.nPrix
      this.formJson.ancienPrixProduit94=res.aPrix
    }
  }
  if (pageCount==9){
    if (poitIndex==0){
      this.formJson.prixProduit101=res.nPrix
      this.formJson.ancienPrixProduit101=res.aPrix
    }
    if (poitIndex==1){
      this.formJson.prixProduit102=res.nPrix
      this.formJson.ancienPrixProduit102=res.aPrix
    }
    if (poitIndex==2){
      this.formJson.prixProduit103=res.nPrix
      this.formJson.ancienPrixProduit103=res.aPrix
    }
    if (poitIndex==3){
      this.formJson.prixProduit104=res.nPrix
      this.formJson.ancienPrixProduit104=res.aPrix
    }
  }
  if (pageCount==10){
    if (poitIndex==0){
      this.formJson.prixProduit111=res.nPrix
      this.formJson.ancienPrixProduit111=res.aPrix
    }
    if (poitIndex==1){
      this.formJson.prixProduit112=res.nPrix
      this.formJson.ancienPrixProduit112=res.aPrix
    }
    if (poitIndex==2){
      this.formJson.prixProduit113=res.nPrix
      this.formJson.ancienPrixProduit113=res.aPrix
    }
    if (poitIndex==3){
      this.formJson.prixProduit114=res.nPrix
      this.formJson.ancienPrixProduit114=res.aPrix
    }
  }
  if (pageCount==11){
    if (poitIndex==0){
      this.formJson.prixProduit121=res.nPrix
      this.formJson.ancienPrixProduit121=res.aPrix
    }
    if (poitIndex==1){
      this.formJson.prixProduit122=res.nPrix
      this.formJson.ancienPrixProduit122=res.aPrix
    }
    if (poitIndex==2){
      this.formJson.prixProduit123=res.nPrix
      this.formJson.ancienPrixProduit123=res.aPrix
    }
    if (poitIndex==3){
      this.formJson.prixProduit124=res.nPrix
      this.formJson.ancienPrixProduit124=res.aPrix
    }
    
  }
  
  
    
  })
  }
  selectSante(item,i,pageCount){
    this.value={key:'',value:''}
    this.value.key=`produitSante ${i}`
this.value.value=JSON.stringify(item)
if (i==0){
  this.historiqueLeaflet.pagesContent[pageCount].value[0]=this.value
}
else {
  this.historiqueLeaflet.pagesContent[pageCount].value.push(this.value)
}
    console.log(pageCount);
    console.log(item);
    this.storageService.write('validation',true);
this.produitChoisi=item;
this.productForm.value.selectionAlaUne[i]=item;
//console.log(i)
this.storageService.write(`produit ${i}`,this.produitChoisi);
if (pageCount==0){
  if(i==0){
  
 
  this.formJson.nomSante11=item.nom;
  this.formJson.uniteSante11=item.unit;
  this.formJson.gammeSante11=item.gamme;
  this.formJson.imgSante11=item.img;
}
if(i==1){

  this.formJson.nomSante12=item.nom;
  this.formJson.uniteSante12=item.unit ;
  this.formJson.gammeSante12=item.gamme ;
  this.formJson.imgSante12=item.img ;
}
if(i==2){

  this.formJson.nomSante13=item.nom;
  this.formJson.uniteSante13=item.unit;
  this.formJson.gammeSante13=item.gamme;
  this.formJson.imgSante13=item.img;
}
if(i==3){

  this.formJson.nomSante14=item.nom;
  this.formJson.uniteSante14=item.unit;
  this.formJson.gammeSante14=item.gamme;
  this.formJson.imgSante14=item.img
}
if(i==4){

  this.formJson.nomSante15=item.nom;
  this.formJson.uniteSante15=item.unit;
  this.formJson.gammeSante15=item.gamme;
  this.formJson.imgSante15=item.img;
}
if(i==5){

  this.formJson.nomSante16=item.nom;
  this.formJson.uniteSante16=item.unit;
  this.formJson.gammeSante16=item.gamme;
  this.formJson.imgSante16=item.img;
}
}
if (pageCount==1){
  if(i==0){
    

  this.formJson.nomSante21=item.nom;
  this.formJson.uniteSante21=item.unit;
  this.formJson.gammeSante21=item.gamme;
  this.formJson.imgSante21=item.img
};
if(i==1){

  this.formJson.nomSante22=item.nom;
  this.formJson.uniteSante22=item.unit;
  this.formJson.gammeSante22=item.gamme;
  this.formJson.imgSante22=item.img
}
if(i==2){

  this.formJson.nomSante23=item.nom;
  this.formJson.uniteSante23=item.unit;
  this.formJson.gammeSante23=item.gamme;
  this.formJson.imgSante23=item.img
}
if(i==3){

  this.formJson.nomSante24=item.nom;
  this.formJson.uniteSante24=item.unit;
  this.formJson.gammeSante24=item.gamme;
  this.formJson.imgSante24=item.img
}
if(i==4){
 
  this.formJson.nomSante25=item.nom;
  this.formJson.uniteSante25=item.unit;
  this.formJson.gammeSante25=item.gamme;
  this.formJson.imgSante25=item.img
}
if(i==5){

  this.formJson.nomSante26=item.nom;
  this.formJson.uniteSante26=item.unit;
  this.formJson.gammeSante26=item.gamme;
  this.formJson.imgSante26=item.img;
}
}
if (pageCount==2){
  if(i==0){
  
  this.formJson.nomSante31=item.nom;
  this.formJson.uniteSante31=item.unit;
  this.formJson.gammeSante31=item.gamme;
  this.formJson.imgSante31=item.img
}
if(i==1){
 
  this.formJson.nomSante32=item.nom;
  this.formJson.uniteSante32=item.unit;
  this.formJson.gammeSante32=item.gamme;
  this.formJson.imgSante32=item.img
}
if(i==2){

  this.formJson.nomSante33=item.nom;
  this.formJson.uniteSante33=item.unit;
  this.formJson.gammeSante33=item.gamme;
  this.formJson.imgSante33=item.img
}
if(i==3){

  this.formJson.nomSante34=item.nom
  this.formJson.uniteSante34=item.unit
  this.formJson.gammeSante34=item.gamme
  this.formJson.imgSante34=item.img
}
if(i==4){

  this.formJson.nomSante35=item.nom
  this.formJson.uniteSante35=item.unit
  this.formJson.gammeSante35=item.gamme
  this.formJson.imgSante35=item.img
}
if(i==5){
 
  this.formJson.nomSante36=item.nom
  this.formJson.uniteSante36=item.unit
  this.formJson.gammeSante36=item.gamme
  this.formJson.imgSante36=item.img
}
}
if (pageCount==3){
  if(i==0){

  this.formJson.nomSante41=item.nom
  this.formJson.uniteSante41=item.unit
  this.formJson.gammeSante41=item.gamme
  this.formJson.imgSante41=item.img
}
if(i==1){
 
  this.formJson.nomSante43=item.nom
  this.formJson.uniteSante44=item.unit
  this.formJson.gammeSante45=item.gamme
  this.formJson.imgSante46=item.img
}
if(i==2){
  
  this.formJson.nomSante43=item.nom
  this.formJson.uniteSante43=item.unit
  this.formJson.gammeSante43=item.gamme
  this.formJson.imgSante43=item.img
}
if(i==3){

  this.formJson.nomSante44=item.nom
  this.formJson.uniteSante44=item.unit
  this.formJson.gammeSante44=item.gamme
  this.formJson.imgSante44=item.img
}
if(i==4){

  this.formJson.nomSante45=item.nom
  this.formJson.uniteSante45=item.unit
  this.formJson.gammeSante45=item.gamme
  this.formJson.imgSante45=item.img
}
if(i==5){

  this.formJson.nomSante46=item.nom
  this.formJson.uniteSante46=item.unit
  this.formJson.gammeSante46=item.gamme
  this.formJson.imgSante46=item.img
}
}
if (pageCount==4){
  if(i==0){

  this.formJson.nomSante51=item.nom
  this.formJson.uniteSante51=item.unit
  this.formJson.gammeSante51=item.gamme
  this.formJson.imgSante51=item.img
}
if(i==1){

  this.formJson.nomSante52=item.nom
  this.formJson.uniteSante52=item.unit
  this.formJson.gammeSante52=item.gamme
  this.formJson.imgSante52=item.img
}
if(i==2){
 
  this.formJson.nomSante53=item.nom
  this.formJson.uniteSante53=item.unit
  this.formJson.gammeSante53=item.gamme
  this.formJson.imgSante53=item.img
}
if(i==3){
 
  this.formJson.nomSante54=item.nom
  this.formJson.uniteSante54=item.unit
  this.formJson.gammeSante54=item.gamme
  this.formJson.imgSante54=item.img
}
if(i==4){

  this.formJson.nomSante55=item.nom
  this.formJson.uniteSante55=item.unit
  this.formJson.gammeSante55=item.gamme
  this.formJson.imgSante55=item.img
}
if(i==5){


  this.formJson.nomSante56=item.nom
  this.formJson.uniteSante56=item.unit
  this.formJson.gammeSante56=item.gamme
  this.formJson.imgSante56=item.img
}
}
if (pageCount==5){
  if(i==0){
   
  this.formJson.nomSante61=item.nom
  this.formJson.uniteSante61=item.unit
  this.formJson.gammeSante61=item.gamme
  this.formJson.imgSante61=item.img
}
if(i==1){
  
  this.formJson.nomSante62=item.nom
  this.formJson.uniteSante62=item.unit
  this.formJson.gammeSante62=item.gamme
  this.formJson.imgSante62=item.img
}
if(i==2){
  
  this.formJson.nomSante63=item.nom
  this.formJson.uniteSante63=item.unit
  this.formJson.gammeSante63=item.gamme
  this.formJson.imgSante63=item.img
}
if(i==3){
  
  this.formJson.nomSante64=item.nom
  this.formJson.uniteSante64=item.unit
  this.formJson.gammeSante64=item.gamme
  this.formJson.imgSante64=item.img
}
if(i==4){
 
  this.formJson.nomSante66=item.nom
  this.formJson.uniteSante66=item.unit
  this.formJson.gammeSante66=item.gamme
  this.formJson.imgSante65=item.img
}
if(i==5){

  this.formJson.nomSante66=item.nom
  this.formJson.uniteSante66=item.unit
  this.formJson.gammeSante66=item.gamme
  this.formJson.imgSante66=item.img
}
}
if (pageCount==6){
  if(i==0){

  this.formJson.nomSante71=item.nom
  this.formJson.uniteSante71=item.unit
  this.formJson.gammeSante71=item.gamme
  this.formJson.imgSante71=item.img
}
if(i==1){

  this.formJson.nomSante72=item.nom
  this.formJson.uniteSante72=item.unit
  this.formJson.gammeSante72=item.gamme
  this.formJson.imgSante72=item.img
}
if(i==2){

  this.formJson.nomSante73=item.nom
  this.formJson.uniteSante73=item.unit
  this.formJson.gammeSante73=item.gamme
  this.formJson.imgSante73=item.img
}
if(i==3){

  this.formJson.nomSante74=item.nom
  this.formJson.uniteSante74=item.unit
  this.formJson.gammeSante74=item.gamme
  this.formJson.imgSante74=item.img
}
if(i==4){

  this.formJson.nomSante75=item.nom
  this.formJson.uniteSante75=item.unit
  this.formJson.gammeSante75=item.gamme
  this.formJson.imgSante75=item.img
}
if(i==5){
 
  this.formJson.nomSante76=item.nom
  this.formJson.uniteSante76=item.unit
  this.formJson.gammeSante76=item.gamme
  this.formJson.imgSante76=item.img
}
}
if (pageCount==7){
  if(i==0){
   
  this.formJson.nomSante81=item.nom
  this.formJson.uniteSante81=item.unit
  this.formJson.gammeSante81=item.gamme
  this.formJson.imgSante81=item.img
}
if(i==1){

  this.formJson.nomSante82=item.nom
  this.formJson.uniteSante82=item.unit
  this.formJson.gammeSante82=item.gamme
  this.formJson.imgSante82=item.img
}
if(i==2){
  
  this.formJson.nomSante83=item.nom
  this.formJson.uniteSante83=item.unit
  this.formJson.gammeSante83=item.gamme
  this.formJson.imgSante83=item.img
}
if(i==3){
  
  this.formJson.nomSante84=item.nom
  this.formJson.uniteSante84=item.unit
  this.formJson.gammeSante84=item.gamme
  this.formJson.imgSante84=item.img
}
if(i==4){


  this.formJson.nomSante85=item.nom
  this.formJson.uniteSante85=item.unit
  this.formJson.gammeSante85=item.gamme
  this.formJson.imgSante85=item.img
}
if(i==5){
  
  this.formJson.nomSante86=item.nom
  this.formJson.uniteSante86=item.unit
  this.formJson.gammeSante86=item.gamme
  this.formJson.imgSante86=item.img
}
}
if (pageCount==8){
  if(i==0){
   
  this.formJson.nomSante91=item.nom
  this.formJson.uniteSante91=item.unit
  this.formJson.gammeSante91=item.gamme
  this.formJson.imgSante91=item.img
}
if(i==1){

  this.formJson.nomSante92=item.nom
  this.formJson.uniteSante92=item.unit
  this.formJson.gammeSante92=item.gamme
  this.formJson.imgSante92=item.img
}
if(i==2){
 
  this.formJson.nomSante93=item.nom
  this.formJson.uniteSante93=item.unit
  this.formJson.gammeSante93=item.gamme
  this.formJson.imgSante93=item.img
}
if(i==3){
  
  this.formJson.nomSante94=item.nom
  this.formJson.uniteSante94=item.unit
  this.formJson.gammeSante94=item.gamme
  this.formJson.imgSante94=item.img
}
if(i==4){

  this.formJson.nomSante95=item.nom
  this.formJson.uniteSante95=item.unit
  this.formJson.gammeSante95=item.gamme
  this.formJson.imgSante95=item.img
}
if(i==5){
  
  this.formJson.nomSante96=item.nom
  this.formJson.uniteSante96=item.unit
  this.formJson.gammeSante96=item.gamme
  this.formJson.imgSante96=item.img
}
}
if (pageCount==9){
  if(i==0){
   
  this.formJson.nomSante101=item.nom
  this.formJson.uniteSante101=item.unit
  this.formJson.gammeSante101=item.gamme
  this.formJson.imgSante101=item.img
}
if(i==1){

  this.formJson.nomSante102=item.nom
  this.formJson.uniteSante102=item.unit
  this.formJson.gammeSante102=item.gamme
  this.formJson.imgSante102=item.img
}
if(i==2){

  this.formJson.nomSante103=item.nom
  this.formJson.uniteSante103=item.unit
  this.formJson.gammeSante103=item.gamme
  this.formJson.imgSante103=item.img
}
if(i==3){

  this.formJson.nomSante104=item.nom
  this.formJson.uniteSante104=item.unit
  this.formJson.gammeSante104=item.gamme
  this.formJson.imgSante104=item.img
}
if(i==4){
  
  this.formJson.nomSante105=item.nom
  this.formJson.uniteSante105=item.unit
  this.formJson.gammeSante105=item.gamme
  this.formJson.imgSante105=item.img
}
if(i==5){
  
  this.formJson.nomSante106=item.nom
  this.formJson.uniteSante106=item.unit
  this.formJson.gammeSante106=item.gamme
  this.formJson.imgSante106=item.img
}
}
if (pageCount==10){
  if(i==0){
    
  this.formJson.nomSante111=item.nom
  this.formJson.uniteSante111=item.unit
  this.formJson.gammeSante111=item.gamme
  this.formJson.imgSante111=item.img
}
if(i==1){

  this.formJson.nomSante112=item.nom
  this.formJson.uniteSante112=item.unit
  this.formJson.gammeSante112=item.gamme
  this.formJson.imgSante112=item.img
}
if(i==2){

  this.formJson.nomSante113=item.nom
  this.formJson.uniteSante113=item.unit
  this.formJson.gammeSante113=item.gamme
  this.formJson.imgSante113=item.img
}
if(i==3){
 
  this.formJson.nomSante114=item.nom
  this.formJson.uniteSante114=item.unit
  this.formJson.gammeSante114=item.gamme
  this.formJson.imgSante114=item.img
}
if(i==4){
 
  this.formJson.nomSante115=item.nom
  this.formJson.uniteSante115=item.unit
  this.formJson.gammeSante115=item.gamme
  this.formJson.imgSante115=item.img
}
if(i==5){

  this.formJson.nomSante116=item.nom
  this.formJson.uniteSante116=item.unit
  this.formJson.gammeSante116=item.gamme
  this.formJson.imgSante116=item.img
}
}
if (pageCount==11){
  if(i==0){

  this.formJson.nomSante121=item.nom
  this.formJson.uniteSante121=item.unit
  this.formJson.gammeSante121=item.gamme
  this.formJson.imgSante121=item.img
}
if(i==1){

  this.formJson.nomSante122=item.nom
  this.formJson.uniteSante122=item.unit
  this.formJson.gammeSante122=item.gamme
  this.formJson.imgSante122=item.img
}
if(i==2){
 
  this.formJson.nomSante123=item.nom
  this.formJson.uniteSante123=item.unit
  this.formJson.gammeSante123=item.gamme
  this.formJson.imgSante123=item.img
}
if(i==3){

  this.formJson.nomSante124=item.nom
  this.formJson.uniteSante124=item.unit
  this.formJson.gammeSante124=item.gamme
  this.formJson.imgSante124=item.img
}
if(i==4){

  this.formJson.nomSante125=item.nom
  this.formJson.uniteSante125=item.unit
  this.formJson.gammeSante125=item.gamme
  this.formJson.imgSante125=item.img
}
if(i==5){

  this.formJson.nomSante126=item.nom
  this.formJson.uniteSante126=item.unit
  this.formJson.gammeSante126=item.gamme
  this.formJson.imgSante126=item.img
}
}



  }
  titrechange(input,i,pageCount){
    this.value={key:'',value:''}
console.log(input.value)
this.value.key=`titreNouveaute ${i}`
this.value.value=input.value
if (i==0){
  this.historiqueLeaflet.pagesContent[pageCount].value[0]=this.value
}
else {
  this.historiqueLeaflet.pagesContent[pageCount].value.push(this.value)
}
if (pageCount==0){
  
}

if(pageCount==0){

if (i==0){
  this.formJson.titreNew11=input.value
}
if (i==1){
  this.formJson.titreNew12=input.value
}
if (i==2){
  this.formJson.titreNew13=input.value
}
if (i==3){
  this.formJson.titreNew14=input.value
}


}

if(pageCount==1){
  if (i==0){
    this.formJson.titreNew21=input.value
  }
  if (i==1){
    this.formJson.titreNew22=input.value
  }
  if (i==2){
    this.formJson.titreNew23=input.value
  }
  if (i==3){
    this.formJson.titreNew24=input.value
  }
  }

  if(pageCount==2){
    if (i==0){
      this.formJson.titreNew31=input.value
    }
    if (i==1){
      this.formJson.titreNew32=input.value
    }
    if (i==2){
      this.formJson.titreNew33=input.value
    }
    if (i==3){
      this.formJson.titreNew34=input.value
    }
    
    
    }
    if(pageCount==3){
      if (i==0){
        this.formJson.titreNew41=input.value
      }
      if (i==1){
        this.formJson.titreNew42=input.value
      }
      if (i==2){
        this.formJson.titreNew43=input.value
      }
      if (i==3){
        this.formJson.titreNew44=input.value
      }
      
      
      }
      if(pageCount==4){
        if (i==0){
          this.formJson.titreNew51=input.value
        }
        if (i==1){
          this.formJson.titreNew52=input.value
        }
        if (i==2){
          this.formJson.titreNew53=input.value
        }
        if (i==3){
          this.formJson.titreNew54=input.value
        }
      }
        if(pageCount==5){
          if (i==0){
            this.formJson.titreNew61=input.value
          }
          if (i==1){
            this.formJson.titreNew62=input.value
          }
          if (i==2){
            this.formJson.titreNew63=input.value
          }
          if (i==3){
            this.formJson.titreNew64=input.value
          }
          
          
          }

          if(pageCount==6){
            if (i==0){
              this.formJson.titreNew71=input.value
            }
            if (i==1){
              this.formJson.titreNew72=input.value
            }
            if (i==2){
              this.formJson.titreNew73=input.value
            }
            if (i==3){
              this.formJson.titreNew74=input.value
            }
            
            
            }
            if(pageCount==7){
              if (i==0){
                this.formJson.titreNew81=input.value
              }
              if (i==1){
                this.formJson.titreNew82=input.value
              }
              if (i==2){
                this.formJson.titreNew83=input.value
              }
              if (i==3){
                this.formJson.titreNew84=input.value
              }
              
              
              }

              if(pageCount==8){
                if (i==0){
                  this.formJson.titreNew91=input.value
                }
                if (i==1){
                  this.formJson.titreNew92=input.value
                }
                if (i==2){
                  this.formJson.titreNew93=input.value
                }
                if (i==3){
                  this.formJson.titreNew94=input.value
                }
                
                
                }
                if(pageCount==9){
if (i==0){
  this.formJson.titreNew101=input.value
}
if (i==1){
  this.formJson.titreNew102=input.value
}
if (i==2){
  this.formJson.titreNew103=input.value
}
if (i==3){
  this.formJson.titreNew104=input.value
}


}

if(pageCount==10){
  if (i==0){
    this.formJson.titreNew111=input.value
  }
  if (i==1){
    this.formJson.titreNew112=input.value
  }
  if (i==2){
    this.formJson.titreNew113=input.value
  }
  if (i==3){
    this.formJson.titreNew114=input.value
  }
  
  
  }

  if(pageCount==11){
    if (i==0){
      this.formJson.titreNew121=input.value
    }
    if (i==1){
      this.formJson.titreNew122=input.value
    }
    if (i==2){
      this.formJson.titreNew123=input.value
    }
    if (i==3){
      this.formJson.titreNew124=input.value
    }
    
    
    }
        
        



  }

  sousTitreChange(input,i,pageCount) {
    console.log(input.value)
    this.value={key:'',value:''}
    this.value.key=`sous-titreNouveaute ${i}`
this.value.value=input.value

this.historiqueLeaflet.pagesContent[pageCount].value.push(this.value)
    if(pageCount==0){
    if (i==0){
      this.formJson.soustitreNew11=input.value
    }
    if (i==1){
      this.formJson.soustitreNew12=input.value
    }
    if (i==2){
      this.formJson.soustitreNew13=input.value
    }
    if (i==3){
      this.formJson.soustitreNew14=input.value
    }
    
    
    }
    
    if(pageCount==1){
      if (i==0){
        this.formJson.soustitreNew21=input.value
      }
      if (i==1){
        this.formJson.soustitreNew22=input.value
      }
      if (i==2){
        this.formJson.soustitreNew23=input.value
      }
      if (i==3){
        this.formJson.soustitreNew24=input.value
      }
      }
    
      if(pageCount==2){
        if (i==0){
          this.formJson.soustitreNew31=input.value
        }
        if (i==1){
          this.formJson.soustitreNew32=input.value
        }
        if (i==2){
          this.formJson.soustitreNew33=input.value
        }
        if (i==3){
          this.formJson.soustitreNew34=input.value
        }
        
        
        }
        if(pageCount==3){
          if (i==0){
            this.formJson.soustitreNew41=input.value
          }
          if (i==1){
            this.formJson.soustitreNew42=input.value
          }
          if (i==2){
            this.formJson.soustitreNew43=input.value
          }
          if (i==3){
            this.formJson.soustitreNew44=input.value
          }
          
          
          }
          if(pageCount==4){
            if (i==0){
              this.formJson.soustitreNew51=input.value
            }
            if (i==1){
              this.formJson.soustitreNew52=input.value
            }
            if (i==2){
              this.formJson.soustitreNew53=input.value
            }
            if (i==3){
              this.formJson.soustitreNew54=input.value
            }
          }
            if(pageCount==5){
              if (i==0){
                this.formJson.soustitreNew61=input.value
              }
              if (i==1){
                this.formJson.soustitreNew62=input.value
              }
              if (i==2){
                this.formJson.soustitreNew63=input.value
              }
              if (i==3){
                this.formJson.soustitreNew64=input.value
              }
              
              
              }
    
              if(pageCount==6){
                if (i==0){
                  this.formJson.soustitreNew71=input.value
                }
                if (i==1){
                  this.formJson.soustitreNew72=input.value
                }
                if (i==2){
                  this.formJson.soustitreNew73=input.value
                }
                if (i==3){
                  this.formJson.soustitreNew74=input.value
                }
                
                
                }
                if(pageCount==7){
                  if (i==0){
                    this.formJson.soustitreNew81=input.value
                  }
                  if (i==1){
                    this.formJson.soustitreNew82=input.value
                  }
                  if (i==2){
                    this.formJson.soustitreNew83=input.value
                  }
                  if (i==3){
                    this.formJson.soustitreNew84=input.value
                  }
                  
                  
                  }
    
                  if(pageCount==8){
                    if (i==0){
                      this.formJson.soustitreNew91=input.value
                    }
                    if (i==1){
                      this.formJson.soustitreNew92=input.value
                    }
                    if (i==2){
                      this.formJson.soustitreNew93=input.value
                    }
                    if (i==3){
                      this.formJson.soustitreNew94=input.value
                    }
                    
                    
                    }
                    if(pageCount==9){
    if (i==0){
      this.formJson.soustitreNew101=input.value
    }
    if (i==1){
      this.formJson.soustitreNew102=input.value
    }
    if (i==2){
      this.formJson.soustitreNew103=input.value
    }
    if (i==3){
      this.formJson.soustitreNew104=input.value
    }
    
    
    }
    
    if(pageCount==10){
      if (i==0){
        this.formJson.soustitreNew111=input.value
      }
      if (i==1){
        this.formJson.soustitreNew112=input.value
      }
      if (i==2){
        this.formJson.soustitreNew113=input.value
      }
      if (i==3){
        this.formJson.soustitreNew114=input.value
      }
      
      
      }
    
      if(pageCount==11){
        if (i==0){
          this.formJson.soustitreNew121=input.value
        }
        if (i==1){
          this.formJson.soustitreNew122=input.value
        }
        if (i==2){
          this.formJson.soustitreNew123=input.value
        }
        if (i==3){
          this.formJson.soustitreNew124=input.value
        }
        
        
        }
            
            
    
    
    
      }

      descriptionChange(input,i,pageCount) {
        console.log(input.value)
        this.value={key:'',value:''}
        this.value.key=`descriptionNouveaute ${i}`
this.value.value=input.value


this.historiqueLeaflet.pagesContent[pageCount].value.push(this.value)

        if(pageCount==0){
        if (i==0){
          this.formJson.descriptionNew11=input.value
        }
        if (i==1){
          this.formJson.descriptionNew12=input.value
        }
        if (i==2){
          this.formJson.descriptionNew13=input.value
        }
        if (i==3){
          this.formJson.descriptionNew14=input.value
        }
        
        
        }
        
        if(pageCount==1){
          if (i==0){
            this.formJson.descriptionNew21=input.value
          }
          if (i==1){
            this.formJson.descriptionNew22=input.value
          }
          if (i==2){
            this.formJson.descriptionNew23=input.value
          }
          if (i==3){
            this.formJson.descriptionNew24=input.value
          }
          }
        
          if(pageCount==2){
            if (i==0){
              this.formJson.descriptionNew31=input.value
            }
            if (i==1){
              this.formJson.descriptionNew32=input.value
            }
            if (i==2){
              this.formJson.descriptionNew33=input.value
            }
            if (i==3){
              this.formJson.descriptionNew34=input.value
            }
            
            
            }
            if(pageCount==3){
              if (i==0){
                this.formJson.descriptionNew41=input.value
              }
              if (i==1){
                this.formJson.descriptionNew42=input.value
              }
              if (i==2){
                this.formJson.descriptionNew43=input.value
              }
              if (i==3){
                this.formJson.descriptionNew44=input.value
              }
              
              
              }
              if(pageCount==4){
                if (i==0){
                  this.formJson.descriptionNew51=input.value
                }
                if (i==1){
                  this.formJson.descriptionNew52=input.value
                }
                if (i==2){
                  this.formJson.descriptionNew53=input.value
                }
                if (i==3){
                  this.formJson.descriptionNew54=input.value
                }
              }
                if(pageCount==5){
                  if (i==0){
                    this.formJson.descriptionNew61=input.value
                  }
                  if (i==1){
                    this.formJson.descriptionNew62=input.value
                  }
                  if (i==2){
                    this.formJson.descriptionNew63=input.value
                  }
                  if (i==3){
                    this.formJson.descriptionNew64=input.value
                  }
                  
                  
                  }
        
                  if(pageCount==6){
                    if (i==0){
                      this.formJson.descriptionNew71=input.value
                    }
                    if (i==1){
                      this.formJson.descriptionNew72=input.value
                    }
                    if (i==2){
                      this.formJson.descriptionNew73=input.value
                    }
                    if (i==3){
                      this.formJson.descriptionNew74=input.value
                    }
                    
                    
                    }
                    if(pageCount==7){
                      if (i==0){
                        this.formJson.descriptionNew81=input.value
                      }
                      if (i==1){
                        this.formJson.descriptionNew82=input.value
                      }
                      if (i==2){
                        this.formJson.descriptionNew83=input.value
                      }
                      if (i==3){
                        this.formJson.descriptionNew84=input.value
                      }
                      
                      
                      }
        
                      if(pageCount==8){
                        if (i==0){
                          this.formJson.descriptionNew91=input.value
                        }
                        if (i==1){
                          this.formJson.descriptionNew92=input.value
                        }
                        if (i==2){
                          this.formJson.descriptionNew93=input.value
                        }
                        if (i==3){
                          this.formJson.descriptionNew94=input.value
                        }
                        
                        
                        }
                        if(pageCount==9){
        if (i==0){
          this.formJson.descriptionNew101=input.value
        }
        if (i==1){
          this.formJson.descriptionNew102=input.value
        }
        if (i==2){
          this.formJson.descriptionNew103=input.value
        }
        if (i==3){
          this.formJson.descriptionNew104=input.value
        }
        
        
        }
        
        if(pageCount==10){
          if (i==0){
            this.formJson.descriptionNew111=input.value
          }
          if (i==1){
            this.formJson.descriptionNew112=input.value
          }
          if (i==2){
            this.formJson.descriptionNew113=input.value
          }
          if (i==3){
            this.formJson.descriptionNew114=input.value
          }
          
          
          }
        
          if(pageCount==11){
            if (i==0){
              this.formJson.descriptionNew121=input.value
            }
            if (i==1){
              this.formJson.descriptionNew122=input.value
            }
            if (i==2){
              this.formJson.descriptionNew123=input.value
            }
            if (i==3){
              this.formJson.descriptionNew124=input.value
            }
            
            
            }
                
                
        
        
        
          }

          titreConseil(input,pageCount){
            this.value={key:'',value:''}
            this.value.key=`titreconseil ${pageCount} `
     
this.value.value=input.value

  this.historiqueLeaflet.pagesContent[pageCount].value[0]=this.value


            if(pageCount==0)
this.formJson.titreConseil1=input.value
if(pageCount==1)
this.formJson.titreConseil2=input.value
if(pageCount==2)
this.formJson.titreConseil3=input.value
if(pageCount==3)
this.formJson.titreConseil4=input.value
if(pageCount==4)
this.formJson.titreConseil5=input.value
if(pageCount==5)
this.formJson.titreConseil6=input.value
if(pageCount==6)
this.formJson.titreConseil7=input.value
if(pageCount==7)
this.formJson.titreConseil8=input.value
if(pageCount==8)
this.formJson.titreConseil9=input.value
if(pageCount==9)
this.formJson.titreConseil10=input.value
if(pageCount==10)
this.formJson.titreConseil11=input.value
if(pageCount==11)
this.formJson.titreConseil12=input.value

          }
          soustitreConseil(input,pageCount){
            
            this.value={key:'',value:''}
            this.value.key=`sous-titre Conseil ${pageCount} `
this.value.value=input.value


this.historiqueLeaflet.pagesContent[pageCount].value.push(this.value)

            if(pageCount==0)
            this.formJson.soustitreConseil1=input.value
            if(pageCount==1)
            this.formJson.soustitreConseil2=input.value
            if(pageCount==2)
            this.formJson.soustitreConseil3=input.value
            if(pageCount==3)
            this.formJson.soustitreConseil4=input.value
            if(pageCount==4)
            this.formJson.soustitreConseil5=input.value
            if(pageCount==5)
            this.formJson.soustitreConseil6=input.value
            if(pageCount==6)
            this.formJson.soustitreConseil7=input.value
            if(pageCount==7)
            this.formJson.soustitreConseil8=input.value
            if(pageCount==8)
            this.formJson.soustitreConseil9=input.value
            if(pageCount==9)
            this.formJson.soustitreConseil10=input.value
            if(pageCount==10)
            this.formJson.soustitreConseil11=input.value
            if(pageCount==11)
            this.formJson.soustitreConseil12=input.value
          }
          textConseil(input,pageCount){
            
            this.value={key:'',value:''}
            this.value.key=`textConseil ${pageCount}`
this.value.value=input.value


this.historiqueLeaflet.pagesContent[pageCount].value.push(this.value)
            if(pageCount==0)
this.formJson.descriptionConseil1=input.value
if(pageCount==1)
this.formJson.descriptionConseil2=input.value
if(pageCount==2)
this.formJson.descriptionConseil3=input.value
if(pageCount==3)
this.formJson.descriptionConseil4=input.value
if(pageCount==4)
this.formJson.descriptionConseil5=input.value
if(pageCount==5)
this.formJson.descriptionConseil6=input.value
if(pageCount==6)
this.formJson.descriptionConseil7=input.value
if(pageCount==7)
this.formJson.descriptionConseil8=input.value
if(pageCount==8)
this.formJson.descriptionConseil9=input.value
if(pageCount==9)
this.formJson.descriptionConseil10=input.value
if(pageCount==10)
this.formJson.descriptionConseil11=input.value
if(pageCount==11)
this.formJson.descriptionConseil12=input.value
          }

          titreserviceChange(input,i,pageCount){
            console.log(input.value)
            this.value={key:'',value:''}
            this.value.key=`titreService ${i}`
this.value.value=input.value
if (pageCount==0){
  this.historiqueLeaflet.pagesContent[pageCount].value[0]=this.value
}
else {
  this.historiqueLeaflet.pagesContent[pageCount].value.push(this.value)
}
            if(pageCount==0){
              if (i==0){
                this.formJson.titreService11=input.value
              }
              if (i==1){
                this.formJson.titreService12=input.value
              }
              if (i==2){
                this.formJson.titreService13=input.value
              }
              if (i==3){
                this.formJson.titreService14=input.value
              }
              
              
              }
              
              if(pageCount==1){
                if (i==0){
                  this.formJson.titreService21=input.value
                }
                if (i==1){
                  this.formJson.titreService22=input.value
                }
                if (i==2){
                  this.formJson.titreService23=input.value
                }
                if (i==3){
                  this.formJson.titreService24=input.value
                }
                }
              
                if(pageCount==2){
                  if (i==0){
                    this.formJson.titreService31=input.value
                  }
                  if (i==1){
                    this.formJson.titreService32=input.value
                  }
                  if (i==2){
                    this.formJson.titreService33=input.value
                  }
                  if (i==3){
                    this.formJson.titreService34=input.value
                  }
                  
                  
                  }
                  if(pageCount==3){
                    if (i==0){
                      this.formJson.titreService41=input.value
                    }
                    if (i==1){
                      this.formJson.titreService42=input.value
                    }
                    if (i==2){
                      this.formJson.titreService43=input.value
                    }
                    if (i==3){
                      this.formJson.titreService44=input.value
                    }
                    
                    
                    }
                    if(pageCount==4){
                      if (i==0){
                        this.formJson.titreService51=input.value
                      }
                      if (i==1){
                        this.formJson.titreService52=input.value
                      }
                      if (i==2){
                        this.formJson.titreService53=input.value
                      }
                      if (i==3){
                        this.formJson.titreService54=input.value
                      }
                    }
                      if(pageCount==5){
                        if (i==0){
                          this.formJson.titreService61=input.value
                        }
                        if (i==1){
                          this.formJson.titreService62=input.value
                        }
                        if (i==2){
                          this.formJson.titreService63=input.value
                        }
                        if (i==3){
                          this.formJson.titreService64=input.value
                        }
                        
                        
                        }
              
                        if(pageCount==6){
                          if (i==0){
                            this.formJson.titreService71=input.value
                          }
                          if (i==1){
                            this.formJson.titreService72=input.value
                          }
                          if (i==2){
                            this.formJson.titreService73=input.value
                          }
                          if (i==3){
                            this.formJson.titreService74=input.value
                          }
                          
                          
                          }
                          if(pageCount==7){
                            if (i==0){
                              this.formJson.titreService81=input.value
                            }
                            if (i==1){
                              this.formJson.titreService82=input.value
                            }
                            if (i==2){
                              this.formJson.titreService83=input.value
                            }
                            if (i==3){
                              this.formJson.titreService84=input.value
                            }
                            
                            
                            }
              
                            if(pageCount==8){
                              if (i==0){
                                this.formJson.titreService91=input.value
                              }
                              if (i==1){
                                this.formJson.titreService92=input.value
                              }
                              if (i==2){
                                this.formJson.titreService93=input.value
                              }
                              if (i==3){
                                this.formJson.titreService94=input.value
                              }
                              
                              
                              }
                              if(pageCount==9){
              if (i==0){
                this.formJson.titreService101=input.value
              }
              if (i==1){
                this.formJson.titreService102=input.value
              }
              if (i==2){
                this.formJson.titreService103=input.value
              }
              if (i==3){
                this.formJson.titreService104=input.value
              }
              
              
              }
              
              if(pageCount==10){
                if (i==0){
                  this.formJson.titreService111=input.value
                }
                if (i==1){
                  this.formJson.titreService112=input.value
                }
                if (i==2){
                  this.formJson.titreService113=input.value
                }
                if (i==3){
                  this.formJson.titreService114=input.value
                }
                
                
                }
              
                if(pageCount==11){
                  if (i==0){
                    this.formJson.titreService121=input.value
                  }
                  if (i==1){
                    this.formJson.titreService122=input.value
                  }
                  if (i==2){
                    this.formJson.titreService123=input.value
                  }
                  if (i==3){
                    this.formJson.titreService124=input.value
                  }
                  
                  
                  }
                      
          }

          soustitreServieChange(input,i,pageCount){
            this.value.key=`soustitre Service ${i}`
            this.value={key:'',value:''}
this.value.value=input.value

this.historiqueLeaflet.pagesContent[pageCount].value.push(this.value)

            console.log(input.value)
            if(pageCount==0){
              if (i==0){
                this.formJson.soustitreService11=input.value
              }
              if (i==1){
                this.formJson.soustitreService12=input.value
              }
              if (i==2){
                this.formJson.soustitreService13=input.value
              }
              if (i==3){
                this.formJson.soustitreService14=input.value
              }
              
              
              }
              
              if(pageCount==1){
                if (i==0){
                  this.formJson.soustitreService21=input.value
                }
                if (i==1){
                  this.formJson.soustitreService22=input.value
                }
                if (i==2){
                  this.formJson.soustitreService23=input.value
                }
                if (i==3){
                  this.formJson.soustitreService24=input.value
                }
                }
              
                if(pageCount==2){
                  if (i==0){
                    this.formJson.soustitreService31=input.value
                  }
                  if (i==1){
                    this.formJson.soustitreService32=input.value
                  }
                  if (i==2){
                    this.formJson.soustitreService33=input.value
                  }
                  if (i==3){
                    this.formJson.soustitreService34=input.value
                  }
                  
                  
                  }
                  if(pageCount==3){
                    if (i==0){
                      this.formJson.soustitreService41=input.value
                    }
                    if (i==1){
                      this.formJson.soustitreService42=input.value
                    }
                    if (i==2){
                      this.formJson.soustitreService43=input.value
                    }
                    if (i==3){
                      this.formJson.soustitreService44=input.value
                    }
                    
                    
                    }
                    if(pageCount==4){
                      if (i==0){
                        this.formJson.soustitreService51=input.value
                      }
                      if (i==1){
                        this.formJson.soustitreService52=input.value
                      }
                      if (i==2){
                        this.formJson.soustitreService53=input.value
                      }
                      if (i==3){
                        this.formJson.soustitreService54=input.value
                      }
                    }
                      if(pageCount==5){
                        if (i==0){
                          this.formJson.soustitreService61=input.value
                        }
                        if (i==1){
                          this.formJson.soustitreService62=input.value
                        }
                        if (i==2){
                          this.formJson.soustitreService63=input.value
                        }
                        if (i==3){
                          this.formJson.soustitreService64=input.value
                        }
                        
                        
                        }
              
                        if(pageCount==6){
                          if (i==0){
                            this.formJson.soustitreService71=input.value
                          }
                          if (i==1){
                            this.formJson.soustitreService72=input.value
                          }
                          if (i==2){
                            this.formJson.soustitreService73=input.value
                          }
                          if (i==3){
                            this.formJson.soustitreService74=input.value
                          }
                          
                          
                          }
                          if(pageCount==7){
                            if (i==0){
                              this.formJson.soustitreService81=input.value
                            }
                            if (i==1){
                              this.formJson.soustitreService82=input.value
                            }
                            if (i==2){
                              this.formJson.soustitreService83=input.value
                            }
                            if (i==3){
                              this.formJson.soustitreService84=input.value
                            }
                            
                            
                            }
              
                            if(pageCount==8){
                              if (i==0){
                                this.formJson.soustitreService91=input.value
                              }
                              if (i==1){
                                this.formJson.soustitreService92=input.value
                              }
                              if (i==2){
                                this.formJson.soustitreService93=input.value
                              }
                              if (i==3){
                                this.formJson.soustitreService94=input.value
                              }
                              
                              
                              }
                              if(pageCount==9){
              if (i==0){
                this.formJson.soustitreService101=input.value
              }
              if (i==1){
                this.formJson.soustitreService102=input.value
              }
              if (i==2){
                this.formJson.soustitreService103=input.value
              }
              if (i==3){
                this.formJson.soustitreService104=input.value
              }
              
              
              }
              
              if(pageCount==10){
                if (i==0){
                  this.formJson.soustitreService111=input.value
                }
                if (i==1){
                  this.formJson.soustitreService112=input.value
                }
                if (i==2){
                  this.formJson.soustitreService113=input.value
                }
                if (i==3){
                  this.formJson.soustitreService114=input.value
                }
                
                
                }
              
                if(pageCount==11){
                  if (i==0){
                    this.formJson.soustitreService121=input.value
                  }
                  if (i==1){
                    this.formJson.soustitreService122=input.value
                  }
                  if (i==2){
                    this.formJson.soustitreService123=input.value
                  }
                  if (i==3){
                    this.formJson.soustitreService124=input.value
                  }
                  
                  
                  }
          }

          iconChange(input,i,pageCount){

          }
  nextStep(){
    this.selectedIndex++
    console.log(this.formJson)
  }
  previousStep(){
    this.selectedIndex--
  }
  selectMois(moi,pageCount){
    this.value={value:'',key:''}
   if(pageCount==0){
     this.formJson.moisPharma1=moi
   }
   if(pageCount==1){
    this.formJson.moisPharma2=moi
  }
  if(pageCount==2){
    this.formJson.moisPharma3=moi
  }
  if(pageCount==3){
    this.formJson.moisPharma4=moi
  }
  if(pageCount==4){
    this.formJson.moisPharma5=moi
  }
  if(pageCount==5){
    this.formJson.moisPharma6=moi
  }
  if(pageCount==6){
    this.formJson.moisPharma7=moi
  }
  if(pageCount==7){
    this.formJson.moisPharma8=moi
  }
  if(pageCount==8){
    this.formJson.moisPharma9=moi
  }
  if(pageCount==9){
    this.formJson.moisPharma10=moi
  }
  if(pageCount==10){
    this.formJson.moisPharma11=moi
  }
  if(pageCount==11){
    this.formJson.moisPharma12=moi
  }
this.value.key="mois"
this.value.value=moi

  this.historiqueLeaflet.pagesContent[pageCount].value[0]=this.value

  }
  selectPharmacie(pharmacie,pageCount){
    this.value={value:'',key:''}
    if (pageCount==0){
     
      this.formJson.nomPharma1=pharmacie.name
this.formJson.adressePharma1=pharmacie.address.street+' '+pharmacie.address.city+' '+pharmacie.address.zip+' '+pharmacie.address.country
this.formJson.odPharma1=`${pharmacie.openingDetails[0].openingTime}   -> ${pharmacie.openingDetails[0].closingTime}` 
this.formJson.telPharma1=pharmacie.phone
this.formJson.villePharma1=pharmacie.address.city
this.formJson.webPharma1=pharmacie.web
this.formJson.mailPharma1=pharmacie.email

    }
    if (pageCount==1){
     
      this.formJson.nomPharma2=pharmacie.name
this.formJson.adressePharma2=pharmacie.address.street+' '+pharmacie.address.city+' '+pharmacie.address.zip+' '+pharmacie.address.country
this.formJson.odPharma2=`${pharmacie.openingDetails[0].openingTime}   -> ${pharmacie.openingDetails[0].closingTime}` 
this.formJson.telPharma2=pharmacie.phone
this.formJson.villePharma2=pharmacie.address.city
this.formJson.webPharma2=pharmacie.web
this.formJson.mailPharma2=pharmacie.email
  }
  if (pageCount==2){
     
    this.formJson.nomPharma3=pharmacie.name
this.formJson.adressePharma3=pharmacie.address.street+' '+pharmacie.address.city+' '+pharmacie.address.zip+' '+pharmacie.address.country
this.formJson.odPharma3=`${pharmacie.openingDetails[0].openingTime}   -> ${pharmacie.openingDetails[0].closingTime}` 
this.formJson.telPharma3=pharmacie.phone
this.formJson.villePharma3=pharmacie.address.city
this.formJson.webPharma3=pharmacie.web
this.formJson.mailPharma3=pharmacie.email
}

if (pageCount==3){
     
  this.formJson.nomPharma4=pharmacie.name
this.formJson.adressePharma4=pharmacie.address.street+' '+pharmacie.address.city+' '+pharmacie.address.zip+' '+pharmacie.address.country
this.formJson.odPharma4=`${pharmacie.openingDetails[0].openingTime}   -> ${pharmacie.openingDetails[0].closingTime}` 
this.formJson.telPharma4=pharmacie.phone
this.formJson.villePharma4=pharmacie.address.city
this.formJson.webPharma4=pharmacie.web
this.formJson.mailPharma4=pharmacie.email
}

if (pageCount==4){
     
  this.formJson.nomPharma5=pharmacie.name
this.formJson.adressePharma5=pharmacie.address.street+' '+pharmacie.address.city+' '+pharmacie.address.zip+' '+pharmacie.address.country
this.formJson.odPharma5=`${pharmacie.openingDetails[0].openingTime}   -> ${pharmacie.openingDetails[0].closingTime}` 
this.formJson.telPharma5=pharmacie.phone
this.formJson.villePharma5=pharmacie.address.city
this.formJson.webPharma5=pharmacie.web
this.formJson.mailPharma5=pharmacie.email
}

if (pageCount==5){
     
  this.formJson.nomPharma6=pharmacie.name
this.formJson.adressePharma6=pharmacie.address.street+' '+pharmacie.address.city+' '+pharmacie.address.zip+' '+pharmacie.address.country
this.formJson.odPharma6=`${pharmacie.openingDetails[0].openingTime}   -> ${pharmacie.openingDetails[0].closingTime}` 
this.formJson.telPharma6=pharmacie.phone
this.formJson.villePharma6=pharmacie.address.city
this.formJson.webPharma6=pharmacie.web
this.formJson.mailPharma6=pharmacie.email
}

if (pageCount==6){
     
  this.formJson.nomPharma7=pharmacie.name
this.formJson.adressePharma7=pharmacie.address.street+' '+pharmacie.address.city+' '+pharmacie.address.zip+' '+pharmacie.address.country
this.formJson.odPharma7=`${pharmacie.openingDetails[0].openingTime}   -> ${pharmacie.openingDetails[0].closingTime}` 
this.formJson.telPharma7=pharmacie.phone
this.formJson.villePharma7=pharmacie.address.city
this.formJson.webPharma7=pharmacie.web
this.formJson.mailPharma7=pharmacie.email
}

if (pageCount==7){
     
  this.formJson.nomPharma8=pharmacie.name
this.formJson.adressePharma8=pharmacie.address.street+' '+pharmacie.address.city+' '+pharmacie.address.zip+' '+pharmacie.address.country
this.formJson.odPharma8=`${pharmacie.openingDetails[0].openingTime}   -> ${pharmacie.openingDetails[0].closingTime}` 
this.formJson.telPharma8=pharmacie.phone
this.formJson.villePharma8=pharmacie.address.city
this.formJson.webPharma8=pharmacie.web
this.formJson.mailPharma8=pharmacie.email
}

if (pageCount==8){
     
  this.formJson.nomPharma9=pharmacie.name
this.formJson.adressePharma9=pharmacie.address.street+' '+pharmacie.address.city+' '+pharmacie.address.zip+' '+pharmacie.address.country
this.formJson.odPharma9=`${pharmacie.openingDetails[0].openingTime}   -> ${pharmacie.openingDetails[0].closingTime}` 
this.formJson.telPharma9=pharmacie.phone
this.formJson.villePharma9=pharmacie.address.city
this.formJson.webPharma9=pharmacie.web
this.formJson.mailPharma9=pharmacie.email
}

if (pageCount==9){
     
  this.formJson.nomPharma10=pharmacie.name
this.formJson.adressePharma10=pharmacie.address.street+' '+pharmacie.address.city+' '+pharmacie.address.zip+' '+pharmacie.address.country
this.formJson.odPharma10=`${pharmacie.openingDetails[0].openingTime}   -> ${pharmacie.openingDetails[0].closingTime}` 
this.formJson.telPharma10=pharmacie.phone
this.formJson.villePharma10=pharmacie.address.city
this.formJson.webPharma10=pharmacie.web
this.formJson.mailPharma10=pharmacie.email
}

if (pageCount==10){
     
  this.formJson.nomPharma11=pharmacie.name
this.formJson.adressePharma11=pharmacie.address.street+' '+pharmacie.address.city+' '+pharmacie.address.zip+' '+pharmacie.address.country
this.formJson.odPharma11=`${pharmacie.openingDetails[0].openingTime}   -> ${pharmacie.openingDetails[0].closingTime}` 
this.formJson.telPharma11=pharmacie.phone
this.formJson.villePharma11=pharmacie.address.city
this.formJson.webPharma11=pharmacie.web
this.formJson.mailPharma11=pharmacie.email
}

if (pageCount==11){
     
  this.formJson.nomPharma12=pharmacie.name
this.formJson.adressePharma12=pharmacie.address.street+' '+pharmacie.address.city+' '+pharmacie.address.zip+' '+pharmacie.address.country
this.formJson.odPharma12=`${pharmacie.openingDetails[0].openingTime}   -> ${pharmacie.openingDetails[0].closingTime}` 
this.formJson.telPharma12=pharmacie.phone
this.formJson.villePharma12=pharmacie.address.city
this.formJson.webPharma12=pharmacie.web
this.formJson.mailPharma12=pharmacie.email
}

this.value.key="pharmacie"
this.value.value=JSON.stringify(pharmacie)
this.historiqueLeaflet.pagesContent[pageCount].value.push(this.value)

}
  selectBeaute(item,i,pageCount){
    this.value={key:'',value:''}
    this.value.key=`produitBeaute ${i}`
    
this.value.value=JSON.stringify(item)
if (i==0){
  this.historiqueLeaflet.pagesContent[pageCount].value[0]=this.value
}
else {
  this.historiqueLeaflet.pagesContent[pageCount].value.push(this.value)
}
    this.produitChoisi=item
    this.productForm.value.Beaute[i]=item
    if (pageCount==0){
      if(i==0){
      
       
      this.formJson.nomBeaute11=item.nom
      this.formJson.uniteBeaute11=item.unit
      this.formJson.gammeBeaute11=item.gamme
      this.formJson.imgBeaute11=item.img
    }
    if(i==1){
      
      
      this.formJson.nomBeaute12=item.nom
      this.formJson.uniteBeaute12=item.unit
      this.formJson.gammeBeaute12=item.gamme
      this.formJson.imgBeaute12=item.img
    }
    if(i==2){
      

    
      this.formJson.nomBeaute13=item.nom
      this.formJson.uniteBeaute13=item.unit
      this.formJson.gammeBeaute13=item.gamme
      this.formJson.imgBeaute13=item.img
    }
    if(i==3){
      

      this.formJson.nomBeaute14=item.nom
      this.formJson.uniteBeaute14=item.unit
      this.formJson.gammeBeaute14=item.gamme
      this.formJson.imgBeaute14=item.img
    }
    if(i==4){
      
  
      this.formJson.nomBeaute15=item.nom
      this.formJson.uniteBeaute15=item.unit
      this.formJson.gammeBeaute15=item.gamme
      this.formJson.imgBeaute15=item.img
    }
    if(i==5){
      
    
      this.formJson.nomBeaute16=item.nom
      this.formJson.uniteBeaute16=item.unit
      this.formJson.gammeBeaute16=item.gamme
      this.formJson.imgBeaute16=item.img
    }
    }
    if (pageCount==1){
      if(i==0){
        
     
      this.formJson.nomBeaute21=item.nom
      this.formJson.uniteBeaute21=item.unit
      this.formJson.gammeBeaute21=item.gamme
      this.formJson.imgBeaute21=item.img
    }
    if(i==1){
 
      this.formJson.nomBeaute22=item.nom
      this.formJson.uniteBeaute22=item.unit
      this.formJson.gammeBeaute22=item.gamme
      this.formJson.imgBeaute22=item.img
    }
    if(i==2){
      

      this.formJson.nomBeaute23=item.nom
      this.formJson.uniteBeaute23=item.unit
      this.formJson.gammeBeaute23=item.gamme
      this.formJson.imgBeaute23=item.img
    }
    if(i==3){
      
     
      this.formJson.nomBeaute24=item.nom
      this.formJson.uniteBeaute24=item.unit
      this.formJson.gammeBeaute24=item.gamme
      this.formJson.imgBeaute24=item.img
    }
    if(i==4){
      
     
      this.formJson.nomBeaute25=item.nom
      this.formJson.uniteBeaute25=item.unit
      this.formJson.gammeBeaute25=item.gamme
      this.formJson.imgBeaute25=item.img
    }
    if(i==5){
      
  
      this.formJson.nomBeaute26=item.nom
      this.formJson.uniteBeaute26=item.unit
      this.formJson.gammeBeaute26=item.gamme
      this.formJson.imgBeaute26=item.img
    }
  }
  
    if (pageCount==2){
      if(i==0){
        
        this.formJson.prixBeaute31=3
        this.formJson.ancienPrixBeaute31='7'
      this.formJson.nomBeaute31=item.nom
      this.formJson.uniteBeaute31=item.unit
      this.formJson.gammeBeaute31=item.gamme
      this.formJson.imgBeaute31=item.img
    }
    if(i==1){
      
      this.formJson.prixBeaute32=3
      this.formJson.ancienPrixBeaute32='7'
      this.formJson.nomBeaute32=item.nom
      this.formJson.uniteBeaute32=item.unit
      this.formJson.gammeBeaute32=item.gamme
      this.formJson.imgBeaute32=item.img
    }
    if(i==2){
      
      this.formJson.prixBeaute33=3
      this.formJson.ancienPrixBeaute33='7'
      this.formJson.nomBeaute33=item.nom
      this.formJson.uniteBeaute33=item.unit
      this.formJson.gammeBeaute33=item.gamme
      this.formJson.imgBeaute33=item.img
    }
    if(i==3){
      
      this.formJson.prixBeaute34=3
      this.formJson.ancienPrixBeaute34='7'
      this.formJson.nomBeaute34=item.nom
      this.formJson.uniteBeaute34=item.unit
      this.formJson.gammeBeaute34=item.gamme
      this.formJson.imgBeaute34=item.img
    }
    if(i==4){
      
      this.formJson.prixBeaute35=3
      this.formJson.ancienPrixBeaute35='7'
      this.formJson.nomBeaute35=item.nom
      this.formJson.uniteBeaute35=item.unit
      this.formJson.gammeBeaute35=item.gamme
      this.formJson.imgBeaute35=item.img
    }
    if(i==5){
      
      this.formJson.prixBeaute36=3
      this.formJson.ancienPrixBeaute36='6'
      this.formJson.nomBeaute36=item.nom
      this.formJson.uniteBeaute36=item.unit
      this.formJson.gammeBeaute36=item.gamme
      this.formJson.imgBeaute36=item.img
    }
    }
    if (pageCount==3){
      if(i==0){
      
        this.formJson.prixBeaute41=3
        this.formJson.ancienPrixBeaute41='7'
      this.formJson.nomBeaute41=item.nom
      this.formJson.uniteBeaute41=item.unit
      this.formJson.gammeBeaute41=item.gamme
      this.formJson.imgBeaute41=item.img
    }
    if(i==1){
    
      this.formJson.prixBeaute42=3
      this.formJson.ancienPrixBeaute42='7'
      this.formJson.nomBeaute42=item.nom
      this.formJson.uniteBeaute42=item.unit
      this.formJson.gammeBeaute42=item.gamme
      this.formJson.imgBeaute42=item.img
    }
    if(i==2){
    
      this.formJson.prixBeaute43=3
      this.formJson.ancienPrixBeaute43='7'
      this.formJson.nomBeaute43=item.nom
      this.formJson.uniteBeaute43=item.unit
      this.formJson.gammeBeaute43=item.gamme
      this.formJson.imgBeaute43=item.img
    }
    if(i==3){
    
      this.formJson.prixBeaute44=3
      this.formJson.ancienPrixBeaute44='7'
      this.formJson.nomBeaute44=item.nom
      this.formJson.uniteBeaute44=item.unit
      this.formJson.gammeBeaute44=item.gamme
      this.formJson.imgBeaute44=item.img
    }
    if(i==4){
    
      this.formJson.prixBeaute45=3
      this.formJson.ancienPrixBeaute45='7'
      this.formJson.nomBeaute45=item.nom
      this.formJson.uniteBeaute45=item.unit
      this.formJson.gammeBeaute45=item.gamme
      this.formJson.imgBeaute45=item.img
    }
    if(i==5){
    
      this.formJson.prixBeaute46=3
      this.formJson.ancienPrixBeaute46='7'
      this.formJson.nomBeaute46=item.nom
      this.formJson.uniteBeaute46=item.unit
      this.formJson.gammeBeaute46=item.gamme
      this.formJson.imgBeaute46=item.img
    }
    }
    if (pageCount==4){
      if(i==0){
        
        this.formJson.prixBeaute51=3
        this.formJson.ancienPrixBeaute51='7'
      this.formJson.nomBeaute51=item.nom
      this.formJson.uniteBeaute51=item.unit
      this.formJson.gammeBeaute51=item.gamme
      this.formJson.imgBeaute51=item.img
    }
    if(i==1){
      
      this.formJson.prixBeaute52=3
      this.formJson.ancienPrixBeaute52='7'
      this.formJson.nomBeaute52=item.nom
      this.formJson.uniteBeaute52=item.unit
      this.formJson.gammeBeaute52=item.gamme
      this.formJson.imgBeaute52=item.img
    }
    if(i==2){
      
      this.formJson.prixBeaute53=3
      this.formJson.ancienPrixBeaute53='7'
      this.formJson.nomBeaute53=item.nom
      this.formJson.uniteBeaute53=item.unit
      this.formJson.gammeBeaute53=item.gamme
      this.formJson.imgBeaute53=item.img
    }
    if(i==3){
      
      this.formJson.prixBeaute54=3
      this.formJson.ancienPrixBeaute54='7'
      this.formJson.nomBeaute54=item.nom
      this.formJson.uniteBeaute54=item.unit
      this.formJson.gammeBeaute54=item.gamme
      this.formJson.imgBeaute54=item.img
    }
    if(i==4){
      
      this.formJson.prixBeaute55=3
      this.formJson.ancienPrixBeaute55='7'
      this.formJson.nomBeaute55=item.nom
      this.formJson.uniteBeaute55=item.unit
      this.formJson.gammeBeaute55=item.gamme
      this.formJson.imgBeaute55=item.img
    }
    if(i==5){
      
      this.formJson.prixBeaute56=3
      this.formJson.ancienPrixBeaute56='7'
      this.formJson.nomBeaute56=item.nom
      this.formJson.uniteBeaute56=item.unit
      this.formJson.gammeBeaute56=item.gamme
      this.formJson.imgBeaute56=item.img
    }
    }
    if (pageCount==5){
      if(i==0){
        
        this.formJson.prixBeaute61=3
        this.formJson.ancienPrixBeaute61='7'
      this.formJson.nomBeaute61=item.nom
      this.formJson.uniteBeaute61=item.unit
      this.formJson.gammeBeaute61=item.gamme
      this.formJson.imgBeaute61=item.img
    }
    if(i==1){
      
      this.formJson.prixBeaute62=3
      this.formJson.ancienPrixBeaute62='7'
      this.formJson.nomBeaute62=item.nom
      this.formJson.uniteBeaute62=item.unit
      this.formJson.gammeBeaute62=item.gamme
      this.formJson.imgBeaute62=item.img
    }
    if(i==2){
      
      this.formJson.prixBeaute63=3
      this.formJson.ancienPrixBeaute63='7'
      this.formJson.nomBeaute63=item.nom
      this.formJson.uniteBeaute63=item.unit
      this.formJson.gammeBeaute63=item.gamme
      this.formJson.imgBeaute63=item.img
    }
    if(i==3){
      
      this.formJson.prixBeaute64=3
      this.formJson.ancienPrixBeaute64='7'
      this.formJson.nomBeaute64=item.nom
      this.formJson.uniteBeaute64=item.unit
      this.formJson.gammeBeaute64=item.gamme
      this.formJson.imgBeaute64=item.img
    }
    if(i==4){
      
      this.formJson.prixBeaute65=3
      this.formJson.ancienPrixBeaute65='7'
      this.formJson.nomBeaute65=item.nom
      this.formJson.uniteBeaute65=item.unit
      this.formJson.gammeBeaute65=item.gamme
      this.formJson.imgBeaute65=item.img
    }
    if(i==5){
      
      this.formJson.prixBeaute66=3
      this.formJson.ancienPrixBeaute66='7'
      this.formJson.nomBeaute66=item.nom
      this.formJson.uniteBeaute66=item.unit
      this.formJson.gammeBeaute66=item.gamme
      this.formJson.imgBeaute66=item.img
    }
    }
    if (pageCount==6){
      if(i==0){
        
        this.formJson.prixBeaute71=3
        this.formJson.ancienPrixBeaute71='7'
      this.formJson.nomBeaute71=item.nom
      this.formJson.uniteBeaute71=item.unit
      this.formJson.gammeBeaute71=item.gamme
      this.formJson.imgBeaute71=item.img
    }
    if(i==1){
      
      this.formJson.prixBeaute72=3
      this.formJson.ancienPrixBeaute72='7'
      this.formJson.nomBeaute72=item.nom
      this.formJson.uniteBeaute72=item.unit
      this.formJson.gammeBeaute72=item.gamme
      this.formJson.imgBeaute72=item.img
    }
    if(i==2){
      
      this.formJson.prixBeaute73=3
      this.formJson.ancienPrixBeaute73='7'
      this.formJson.nomBeaute73=item.nom
      this.formJson.uniteBeaute73=item.unit
      this.formJson.gammeBeaute73=item.gamme
      this.formJson.imgBeaute73=item.img
    }
    if(i==3){
      
      this.formJson.prixBeaute74=3
      this.formJson.ancienPrixBeaute74='7'
      this.formJson.nomBeaute74=item.nom
      this.formJson.uniteBeaute74=item.unit
      this.formJson.gammeBeaute74=item.gamme
      this.formJson.imgBeaute74=item.img
    }
    if(i==4){
      
      this.formJson.prixBeaute75=3
      this.formJson.ancienPrixBeaute75='7'
      this.formJson.nomBeaute75=item.nom
      this.formJson.uniteBeaute75=item.unit
      this.formJson.gammeBeaute75=item.gamme
      this.formJson.imgBeaute75=item.img
    }
    if(i==5){
      
      this.formJson.prixBeaute76=3
      this.formJson.ancienPrixBeaute76='7'
      this.formJson.nomBeaute76=item.nom
      this.formJson.uniteBeaute76=item.unit
      this.formJson.gammeBeaute76=item.gamme
      this.formJson.imgBeaute76=item.img
    }
    }
    if (pageCount==7){
      if(i==0){
      
        this.formJson.prixBeaute81=3
        this.formJson.ancienPrixBeaute81='8'
      this.formJson.nomBeaute81=item.nom
      this.formJson.uniteBeaute81=item.unit
      this.formJson.gammeBeaute81=item.gamme
      this.formJson.imgBeaute81=item.img
    }
    if(i==1){
    
      this.formJson.prixBeaute82=3
      this.formJson.ancienPrixBeaute82='8'
      this.formJson.nomBeaute82=item.nom
      this.formJson.uniteBeaute82=item.unit
      this.formJson.gammeBeaute82=item.gamme
      this.formJson.imgBeaute82=item.img
    }
    if(i==2){
    
      this.formJson.prixBeaute83=3
      this.formJson.ancienPrixBeaute83='8'
      this.formJson.nomBeaute83=item.nom
      this.formJson.uniteBeaute83=item.unit
      this.formJson.gammeBeaute83=item.gamme
      this.formJson.imgBeaute83=item.img
    }
    if(i==3){
    
      this.formJson.prixBeaute84=3
      this.formJson.ancienPrixBeaute84='8'
      this.formJson.nomBeaute84=item.nom
      this.formJson.uniteBeaute84=item.unit
      this.formJson.gammeBeaute84=item.gamme
      this.formJson.imgBeaute84=item.img
    }
    if(i==4){
    
      this.formJson.prixBeaute85=3
      this.formJson.ancienPrixBeaute85='8'
      this.formJson.nomBeaute85=item.nom
      this.formJson.uniteBeaute85=item.unit
      this.formJson.gammeBeaute85=item.gamme
      this.formJson.imgBeaute85=item.img
    }
    if(i==5){
    
      this.formJson.prixBeaute86=3
      this.formJson.ancienPrixBeaute86='8'
      this.formJson.nomBeaute86=item.nom
      this.formJson.uniteBeaute86=item.unit
      this.formJson.gammeBeaute86=item.gamme
      this.formJson.imgBeaute86=item.img
    }
    
    if (pageCount==8){
    
      this.formJson.prixBeaute91=3
        this.formJson.ancienPrixBeaute91='7'
      this.formJson.nomBeaute91=item.nom
      this.formJson.uniteBeaute91=item.unit
      this.formJson.gammeBeaute91=item.gamme
      this.formJson.imgBeaute91=item.img
    }
    if(i==1){
    
      this.formJson.prixBeaute92=3
      this.formJson.ancienPrixBeaute92='7'
      this.formJson.nomBeaute92=item.nom
      this.formJson.uniteBeaute92=item.unit
      this.formJson.gammeBeaute92=item.gamme
      this.formJson.imgBeaute92=item.img
    }
    if(i==2){
    
      this.formJson.prixBeaute93=3
      this.formJson.ancienPrixBeaute93='7'
      this.formJson.nomBeaute93=item.nom
      this.formJson.uniteBeaute93=item.unit
      this.formJson.gammeBeaute93=item.gamme
      this.formJson.imgBeaute93=item.img
    }
    if(i==3){
    
      this.formJson.prixBeaute94=3
      this.formJson.ancienPrixBeaute94='7'
      this.formJson.nomBeaute94=item.nom
      this.formJson.uniteBeaute94=item.unit
      this.formJson.gammeBeaute94=item.gamme
      this.formJson.imgBeaute94=item.img
    }
    if(i==4){
    
      this.formJson.prixBeaute95=3
      this.formJson.ancienPrixBeaute95='7'
      this.formJson.nomBeaute95=item.nom
      this.formJson.uniteBeaute95=item.unit
      this.formJson.gammeBeaute95=item.gamme
      this.formJson.imgBeaute95=item.img
    }
    if(i==5){
    
      this.formJson.prixBeaute96=3
      this.formJson.ancienPrixBeaute96='7'
      this.formJson.nomBeaute96=item.nom
      this.formJson.uniteBeaute96=item.unit
      this.formJson.gammeBeaute96=item.gamme
      this.formJson.imgBeaute96=item.img
    }
    }
    if (pageCount==9){
      if(i==0){
    
        this.formJson.prixBeaute101=3
        this.formJson.ancienPrixBeaute101='7'
      this.formJson.nomBeaute101=item.nom
      this.formJson.uniteBeaute101=item.unit
      this.formJson.gammeBeaute101=item.gamme
      this.formJson.imgBeaute101=item.img
    }
    if(i==1){
  
      this.formJson.prixBeaute102=3
      this.formJson.ancienPrixBeaute102='7'
      this.formJson.nomBeaute102=item.nom
      this.formJson.uniteBeaute102=item.unit
      this.formJson.gammeBeaute102=item.gamme
      this.formJson.imgBeaute102=item.img
    }
    if(i==2){
  
      this.formJson.prixBeaute103=3
      this.formJson.ancienPrixBeaute103='7'
      this.formJson.nomBeaute103=item.nom
      this.formJson.uniteBeaute103=item.unit
      this.formJson.gammeBeaute103=item.gamme
      this.formJson.imgBeaute103=item.img
    }
    if(i==3){
  
      this.formJson.prixBeaute104=3
      this.formJson.ancienPrixBeaute104='7'
      this.formJson.nomBeaute104=item.nom
      this.formJson.uniteBeaute104=item.unit
      this.formJson.gammeBeaute104=item.gamme
      this.formJson.imgBeaute104=item.img
    }
    if(i==4){
  
      this.formJson.prixBeaute105=3
      this.formJson.ancienPrixBeaute105='7'
      this.formJson.nomBeaute105=item.nom
      this.formJson.uniteBeaute105=item.unit
      this.formJson.gammeBeaute105=item.gamme
      this.formJson.imgBeaute105=item.img
    }
    if(i==5){
  
      this.formJson.prixBeaute106=3
      this.formJson.ancienPrixBeaute106='7'
      this.formJson.nomBeaute106=item.nom
      this.formJson.uniteBeaute106=item.unit
      this.formJson.gammeBeaute106=item.gamme
      this.formJson.imgBeaute106=item.img
    }
    }
    if (pageCount==10){
      if(i==0){
        
        this.formJson.prixBeaute111=3
        this.formJson.ancienPrixBeaute111='7'
      this.formJson.nomBeaute111=item.nom
      this.formJson.uniteBeaute111=item.unit
      this.formJson.gammeBeaute111=item.gamme
      this.formJson.imgBeaute111=item.img
    }
    if(i==1){
      
      this.formJson.prixBeaute112=3
      this.formJson.ancienPrixBeaute112='7'
      this.formJson.nomBeaute112=item.nom
      this.formJson.uniteBeaute112=item.unit
      this.formJson.gammeBeaute112=item.gamme
      this.formJson.imgBeaute112=item.img
    }
    if(i==2){
      
      this.formJson.prixBeaute113=3
      this.formJson.ancienPrixBeaute113='7'
      this.formJson.nomBeaute113=item.nom
      this.formJson.uniteBeaute113=item.unit
      this.formJson.gammeBeaute113=item.gamme
      this.formJson.imgBeaute113=item.img
    }
    if(i==3){
      
      this.formJson.prixBeaute114=3
      this.formJson.ancienPrixBeaute114='7'
      this.formJson.nomBeaute114=item.nom
      this.formJson.uniteBeaute114=item.unit
      this.formJson.gammeBeaute114=item.gamme
      this.formJson.imgBeaute114=item.img
    }
    if(i==4){
      
      this.formJson.prixBeaute115=3
      this.formJson.ancienPrixBeaute115='7'
      this.formJson.nomBeaute115=item.nom
      this.formJson.uniteBeaute115=item.unit
      this.formJson.gammeBeaute115=item.gamme
      this.formJson.imgBeaute115=item.img
    }
    if(i==5){
      
      this.formJson.prixBeaute116=3
      this.formJson.ancienPrixBeaute116='7'
      this.formJson.nomBeaute116=item.nom
      this.formJson.uniteBeaute116=item.unit
      this.formJson.gammeBeaute116=item.gamme
      this.formJson.imgBeaute116=item.img
    }
    }
    if (pageCount==11){
      if(i==0){
       
        this.formJson.prixBeaute121=3
        this.formJson.ancienPrixBeaute121='7'
      this.formJson.nomBeaute121=item.nom
      this.formJson.uniteBeaute121=item.unit
      this.formJson.gammeBeaute121=item.gamme
      this.formJson.imgBeaute121=item.img
    }
    if(i==1){
     
      this.formJson.prixBeaute122=3
      this.formJson.ancienPrixBeaute122='7'
      this.formJson.nomBeaute122=item.nom
      this.formJson.uniteBeaute122=item.unit
      this.formJson.gammeBeaute122=item.gamme
      this.formJson.imgBeaute122=item.img
    }
    if(i==2){
     
      this.formJson.prixBeaute123=3
      this.formJson.ancienPrixBeaute123='7'
      this.formJson.nomBeaute123=item.nom
      this.formJson.uniteBeaute123=item.unit
      this.formJson.gammeBeaute123=item.gamme
      this.formJson.imgBeaute123=item.img
    }
    if(i==3){
     
      this.formJson.prixBeaute124=3
      this.formJson.ancienPrixBeaute124='7'
      this.formJson.nomBeaute124=item.nom
      this.formJson.uniteBeaute124=item.unit
      this.formJson.gammeBeaute124=item.gamme
      this.formJson.imgBeaute124=item.img
    }
    if(i==4){
     
      this.formJson.prixBeaute125=3
      this.formJson.ancienPrixBeaute125='7'
      this.formJson.nomBeaute125=item.nom
      this.formJson.uniteBeaute125=item.unit
      this.formJson.gammeBeaute125=item.gamme
      this.formJson.imgBeaute125=item.img
    }
    if(i==5){
     
      this.formJson.prixBeaute126=3
      this.formJson.ancienPrixBeaute126='7'
      this.formJson.nomBeaute126=item.nom
      this.formJson.uniteBeaute126=item.unit
      this.formJson.gammeBeaute126=item.gamme
      this.formJson.imgBeaute126=item.img
    }
  }

  }
  selectBebe(item,i,pageCount){
    this.value={key:'',value:''}
    this.value.key=`produitBebe ${i}`


this.value.value=JSON.stringify(item)
if (i==0){
  this.historiqueLeaflet.pagesContent[pageCount].value[0]=this.value
}
else {
  this.historiqueLeaflet.pagesContent[pageCount].value.push(this.value)
}
    this.produitChoisi=item
   // this.productForm.value.Bebe[i]=item
   if (pageCount==0){
    if(i==0){
    
  
    this.formJson.nomBebe11=item.nom
    this.formJson.uniteBebe11=item.unit
    this.formJson.gammeBebe11=item.gamme
    this.formJson.imgBebe11=item.img
  }
  if(i==1){

    this.formJson.nomBebe12=item.nom
    this.formJson.uniteBebe12=item.unit
    this.formJson.gammeBebe12=item.gamme
    this.formJson.imgBebe12=item.img
  }
  if(i==2){
   
    this.formJson.nomBebe13=item.nom
    this.formJson.uniteBebe13=item.unit
    this.formJson.gammeBebe13=item.gamme
    this.formJson.imgBebe13=item.img
  }
  if(i==3){

    this.formJson.nomBebe14=item.nom
    this.formJson.uniteBebe14=item.unit
    this.formJson.gammeBebe14=item.gamme
    this.formJson.imgBebe14=item.img
  }
  if(i==4){
  
    this.formJson.nomBebe15=item.nom
    this.formJson.uniteBebe15=item.unit
    this.formJson.gammeBebe15=item.gamme
    this.formJson.imgBebe15=item.img
  }
  if(i==5){
  
    this.formJson.nomBebe16=item.nom
    this.formJson.uniteBebe16=item.unit
    this.formJson.gammeBebe16=item.gamme
    this.formJson.imgBebe16=item.img
  }
  }
  if (pageCount==1){
    if(i==0){
  
    this.formJson.nomBebe21=item.nom
    this.formJson.uniteBebe21=item.unit
    this.formJson.gammeBebe21=item.gamme
    this.formJson.imgBebe21=item.img
  }
  if(i==1){

    this.formJson.nomBebe22=item.nom
    this.formJson.uniteBebe22=item.unit
    this.formJson.gammeBebe22=item.gamme
    this.formJson.imgBebe22=item.img
  }
  if(i==2){

    this.formJson.nomBebe23=item.nom
    this.formJson.uniteBebe23=item.unit
    this.formJson.gammeBebe23=item.gamme
    this.formJson.imgBebe23=item.img
  }
  if(i==3){
  
    this.formJson.nomBebe24=item.nom
    this.formJson.uniteBebe24=item.unit
    this.formJson.gammeBebe24=item.gamme
    this.formJson.imgBebe24=item.img
  }
  if(i==4){
  
    this.formJson.nomBebe25=item.nom
    this.formJson.uniteBebe25=item.unit
    this.formJson.gammeBebe25=item.gamme
    this.formJson.imgBebe25=item.img
  }
  if(i==5){
  
    this.formJson.nomBebe26=item.nom
    this.formJson.uniteBebe26=item.unit
    this.formJson.gammeBebe26=item.gamme
    this.formJson.imgBebe26=item.img
  }
}

  if (pageCount==2){
    if(i==0){
     
    this.formJson.nomBebe31=item.nom
    this.formJson.uniteBebe31=item.unit
    this.formJson.gammeBebe31=item.gamme
    this.formJson.imgBebe31=item.img
  }
  if(i==1){
   
    this.formJson.nomBebe32=item.nom
    this.formJson.uniteBebe32=item.unit
    this.formJson.gammeBebe32=item.gamme
    this.formJson.imgBebe32=item.img
  }
  if(i==2){

    this.formJson.nomBebe33=item.nom
    this.formJson.uniteBebe33=item.unit
    this.formJson.gammeBebe33=item.gamme
    this.formJson.imgBebe33=item.img
  }
  if(i==3){

    this.formJson.nomBebe34=item.nom
    this.formJson.uniteBebe34=item.unit
    this.formJson.gammeBebe34=item.gamme
    this.formJson.imgBebe34=item.img
  }
  if(i==4){

    this.formJson.nomBebe35=item.nom
    this.formJson.uniteBebe35=item.unit
    this.formJson.gammeBebe35=item.gamme
    this.formJson.imgBebe35=item.img
  }
  if(i==5){
   
    this.formJson.nomBebe36=item.nom
    this.formJson.uniteBebe36=item.unit
    this.formJson.gammeBebe36=item.gamme
    this.formJson.imgBebe36=item.img
  }
  }
  if (pageCount==3){
    if(i==0){

    this.formJson.nomBebe41=item.nom
    this.formJson.uniteBebe41=item.unit
    this.formJson.gammeBebe41=item.gamme
    this.formJson.imgBebe41=item.img
  }
  if(i==1){
  
    this.formJson.nomBebe42=item.nom
    this.formJson.uniteBebe42=item.unit
    this.formJson.gammeBebe42=item.gamme
    this.formJson.imgBebe42=item.img
  }
  if(i==2){

    this.formJson.nomBebe43=item.nom
    this.formJson.uniteBebe43=item.unit
    this.formJson.gammeBebe43=item.gamme
    this.formJson.imgBebe43=item.img
  }
  if(i==3){
 
    this.formJson.nomBebe44=item.nom
    this.formJson.uniteBebe44=item.unit
    this.formJson.gammeBebe44=item.gamme
    this.formJson.imgBebe44=item.img
  }
  if(i==4){

    this.formJson.nomBebe45=item.nom
    this.formJson.uniteBebe45=item.unit
    this.formJson.gammeBebe45=item.gamme
    this.formJson.imgBebe45=item.img
  }
  if(i==5){

    this.formJson.nomBebe46=item.nom
    this.formJson.uniteBebe46=item.unit
    this.formJson.gammeBebe46=item.gamme
    this.formJson.imgBebe46=item.img
  }
  }
  if (pageCount==4){
    if(i==0){
   
    this.formJson.nomBebe51=item.nom
    this.formJson.uniteBebe51=item.unit
    this.formJson.gammeBebe51=item.gamme
    this.formJson.imgBebe51=item.img
  }
  if(i==1){

    this.formJson.nomBebe52=item.nom
    this.formJson.uniteBebe52=item.unit
    this.formJson.gammeBebe52=item.gamme
    this.formJson.imgBebe52=item.img
  }
  if(i==2){

    this.formJson.nomBebe53=item.nom
    this.formJson.uniteBebe53=item.unit
    this.formJson.gammeBebe53=item.gamme
    this.formJson.imgBebe53=item.img
  }
  if(i==3){
   
    this.formJson.nomBebe54=item.nom
    this.formJson.uniteBebe54=item.unit
    this.formJson.gammeBebe54=item.gamme
    this.formJson.imgBebe54=item.img
  }
  if(i==4){

    this.formJson.nomBebe55=item.nom
    this.formJson.uniteBebe55=item.unit
    this.formJson.gammeBebe55=item.gamme
    this.formJson.imgBebe55=item.img
  }
  if(i==5){

    this.formJson.nomBebe56=item.nom
    this.formJson.uniteBebe56=item.unit
    this.formJson.gammeBebe56=item.gamme
    this.formJson.imgBebe56=item.img
  }
  }
  if (pageCount==5){
    if(i==0){
    
    this.formJson.nomBebe61=item.nom
    this.formJson.uniteBebe61=item.unit
    this.formJson.gammeBebe61=item.gamme
    this.formJson.imgBebe61=item.img
  }
  if(i==1){

    this.formJson.nomBebe62=item.nom
    this.formJson.uniteBebe62=item.unit
    this.formJson.gammeBebe62=item.gamme
    this.formJson.imgBebe62=item.img
  }
  if(i==2){

    this.formJson.nomBebe63=item.nom
    this.formJson.uniteBebe63=item.unit
    this.formJson.gammeBebe63=item.gamme
    this.formJson.imgBebe63=item.img
  }
  if(i==3){

    this.formJson.nomBebe64=item.nom
    this.formJson.uniteBebe64=item.unit
    this.formJson.gammeBebe64=item.gamme
    this.formJson.imgBebe64=item.img
  }
  if(i==4){
   
    this.formJson.nomBebe65=item.nom
    this.formJson.uniteBebe65=item.unit
    this.formJson.gammeBebe65=item.gamme
    this.formJson.imgBebe65=item.img
  }
  if(i==5){
 
    this.formJson.nomBebe66=item.nom
    this.formJson.uniteBebe66=item.unit
    this.formJson.gammeBebe66=item.gamme
    this.formJson.imgBebe66=item.img
  }
  }
  if (pageCount==6){
    if(i==0){

    this.formJson.nomBebe71=item.nom
    this.formJson.uniteBebe71=item.unit
    this.formJson.gammeBebe71=item.gamme
    this.formJson.imgBebe71=item.img
  }
  if(i==1){
 
    this.formJson.nomBebe72=item.nom
    this.formJson.uniteBebe72=item.unit
    this.formJson.gammeBebe72=item.gamme
    this.formJson.imgBebe72=item.img
  }
  if(i==2){
 
    this.formJson.nomBebe73=item.nom
    this.formJson.uniteBebe73=item.unit
    this.formJson.gammeBebe73=item.gamme
    this.formJson.imgBebe73=item.img
  }
  if(i==3){
 
    this.formJson.nomBebe74=item.nom
    this.formJson.uniteBebe74=item.unit
    this.formJson.gammeBebe74=item.gamme
    this.formJson.imgBebe74=item.img
  }
  if(i==4){
    
    this.formJson.nomBebe75=item.nom
    this.formJson.uniteBebe75=item.unit
    this.formJson.gammeBebe75=item.gamme
    this.formJson.imgBebe75=item.img
  }
  if(i==5){

    this.formJson.nomBebe76=item.nom
    this.formJson.uniteBebe76=item.unit
    this.formJson.gammeBebe76=item.gamme
    this.formJson.imgBebe76=item.img
  }
  }
  if (pageCount==7){
    if(i==0){
  
    this.formJson.nomBebe81=item.nom
    this.formJson.uniteBebe81=item.unit
    this.formJson.gammeBebe81=item.gamme
    this.formJson.imgBebe81=item.img
  }
  if(i==1){

    this.formJson.nomBebe82=item.nom
    this.formJson.uniteBebe82=item.unit
    this.formJson.gammeBebe82=item.gamme
    this.formJson.imgBebe82=item.img
  }
  if(i==2){
  
    this.formJson.nomBebe83=item.nom
    this.formJson.uniteBebe83=item.unit
    this.formJson.gammeBebe83=item.gamme
    this.formJson.imgBebe83=item.img
  }
  if(i==3){
  
    this.formJson.nomBebe84=item.nom
    this.formJson.uniteBebe84=item.unit
    this.formJson.gammeBebe84=item.gamme
    this.formJson.imgBebe84=item.img
  }
  if(i==4){
   
    this.formJson.nomBebe85=item.nom
    this.formJson.uniteBebe85=item.unit
    this.formJson.gammeBebe85=item.gamme
    this.formJson.imgBebe85=item.img
  }
  if(i==5){
    
    this.formJson.nomBebe86=item.nom
    this.formJson.uniteBebe86=item.unit
    this.formJson.gammeBebe86=item.gamme
    this.formJson.imgBebe86=item.img
  }
  }
  if (pageCount==8){
    if(i==0){

    this.formJson.nomBebe91=item.nom
    this.formJson.uniteBebe91=item.unit
    this.formJson.gammeBebe91=item.gamme
    this.formJson.imgBebe91=item.img
  }
  if(i==1){

    this.formJson.nomBebe92=item.nom
    this.formJson.uniteBebe92=item.unit
    this.formJson.gammeBebe92=item.gamme
    this.formJson.imgBebe92=item.img
  }
  if(i==2){
  
    this.formJson.nomBebe93=item.nom
    this.formJson.uniteBebe93=item.unit
    this.formJson.gammeBebe93=item.gamme
    this.formJson.imgBebe93=item.img
  }
  if(i==3){

    this.formJson.nomBebe94=item.nom
    this.formJson.uniteBebe94=item.unit
    this.formJson.gammeBebe94=item.gamme
    this.formJson.imgBebe94=item.img
  }
  if(i==4){

    this.formJson.nomBebe95=item.nom
    this.formJson.uniteBebe95=item.unit
    this.formJson.gammeBebe95=item.gamme
    this.formJson.imgBebe95=item.img
  }
  if(i==5){

    this.formJson.nomBebe96=item.nom
    this.formJson.uniteBebe96=item.unit
    this.formJson.gammeBebe96=item.gamme
    this.formJson.imgBebe96=item.img
  }
  }
  if (pageCount==9){
    if(i==0){
      this.formJson.prixBebe101=3
      this.formJson.ancienPrixBebe101='7'
    this.formJson.nomBebe101=item.nom
    this.formJson.uniteBebe101=item.unit
    this.formJson.gammeBebe101=item.gamme
    this.formJson.imgBebe101=item.img
  }
  if(i==1){
    
    this.formJson.nomBebe102=item.nom
    this.formJson.uniteBebe102=item.unit
    this.formJson.gammeBebe102=item.gamme
    this.formJson.imgBebe102=item.img
  }
  if(i==2){
  
  
    this.formJson.nomBebe103=item.nom
    this.formJson.uniteBebe103=item.unit
    this.formJson.gammeBebe103=item.gamme
    this.formJson.imgBebe103=item.img
  }
  if(i==3){
 
    this.formJson.nomBebe104=item.nom
    this.formJson.uniteBebe104=item.unit
    this.formJson.gammeBebe104=item.gamme
    this.formJson.imgBebe104=item.img
  }
  if(i==4){
  
    this.formJson.nomBebe105=item.nom
    this.formJson.uniteBebe105=item.unit
    this.formJson.gammeBebe105=item.gamme
    this.formJson.imgBebe105=item.img
  }
  if(i==5){
  
    this.formJson.nomBebe106=item.nom
    this.formJson.uniteBebe106=item.unit
    this.formJson.gammeBebe106=item.gamme
    this.formJson.imgBebe106=item.img
  }
  }
  if (pageCount==10){
    if(i==0){
    
    this.formJson.nomBebe111=item.nom
    this.formJson.uniteBebe111=item.unit
    this.formJson.gammeBebe111=item.gamme
    this.formJson.imgBebe111=item.img
  }
  if(i==1){

    this.formJson.nomBebe112=item.nom
    this.formJson.uniteBebe112=item.unit
    this.formJson.gammeBebe112=item.gamme
    this.formJson.imgBebe112=item.img
  }
  if(i==2){
  
    this.formJson.nomBebe113=item.nom
    this.formJson.uniteBebe113=item.unit
    this.formJson.gammeBebe113=item.gamme
    this.formJson.imgBebe113=item.img
  }
  if(i==3){

    this.formJson.nomBebe114=item.nom
    this.formJson.uniteBebe114=item.unit
    this.formJson.gammeBebe114=item.gamme
    this.formJson.imgBebe114=item.img
  }
  if(i==4){
   
    this.formJson.nomBebe115=item.nom
    this.formJson.uniteBebe115=item.unit
    this.formJson.gammeBebe115=item.gamme
    this.formJson.imgBebe115=item.img
  }
  if(i==5){
 
    this.formJson.nomBebe116=item.nom
    this.formJson.uniteBebe116=item.unit
    this.formJson.gammeBebe116=item.gamme
    this.formJson.imgBebe116=item.img
  }
  }
  if (pageCount==11){
    if(i==0){
     
    this.formJson.nomBebe121=item.nom
    this.formJson.uniteBebe121=item.unit
    this.formJson.gammeBebe121=item.gamme
    this.formJson.imgBebe121=item.img
  }
  if(i==1){
  
    this.formJson.nomBebe122=item.nom
    this.formJson.uniteBebe122=item.unit
    this.formJson.gammeBebe122=item.gamme
    this.formJson.imgBebe122=item.img
  }
  if(i==2){
    
    this.formJson.nomBebe123=item.nom
    this.formJson.uniteBebe123=item.unit
    this.formJson.gammeBebe123=item.gamme
    this.formJson.imgBebe123=item.img
  }
  if(i==3){

    this.formJson.nomBebe124=item.nom
    this.formJson.uniteBebe124=item.unit
    this.formJson.gammeBebe124=item.gamme
    this.formJson.imgBebe124=item.img
  }
  if(i==4){
  
    this.formJson.nomBebe125=item.nom
    this.formJson.uniteBebe125=item.unit
    this.formJson.gammeBebe125=item.gamme
    this.formJson.imgBebe125=item.img
  }
  if(i==5){
    
    this.formJson.nomBebe126=item.nom
    this.formJson.uniteBebe126=item.unit
    this.formJson.gammeBebe126=item.gamme
    this.formJson.imgBebe126=item.img
  }
}
}
  selectBienEtre(item,i,pageCount){
    this.value={key:'',value:''}
    this.value.key=`produitBienEtre ${i}`
this.value.value=JSON.stringify(item)
if (i==0){
  this.pagesContents[pageCount].value[0]=this.value
}
else {
  this.pagesContents[pageCount].value.push(this.value)
}
    if (pageCount==0){
      if(i==0){
      
     
      
      this.formJson.nomBienetre11=item.nom
      this.formJson.uniteBienetre11=item.unit
      this.formJson.gammeBienetre11=item.gamme
      this.formJson.imgBienetre11=item.img
    }
    if(i==1){
   
    
      this.formJson.nomBienetre12=item.nom
      this.formJson.uniteBienetre12=item.unit
      this.formJson.gammeBienetre12=item.gamme
      this.formJson.imgBienetre12=item.img
    }
    if(i==2){
   
    
      this.formJson.nomBienetre13=item.nom
      this.formJson.uniteBienetre13=item.unit
      this.formJson.gammeBienetre13=item.gamme
      this.formJson.imgBienetre13=item.img
    }
    if(i==3){
   
    
      this.formJson.nomBienetre14=item.nom
      this.formJson.uniteBienetre14=item.unit
      this.formJson.gammeBienetre14=item.gamme
      this.formJson.imgBienetre14=item.img
    }
    if(i==4){
   
    
      this.formJson.nomBienetre15=item.nom
      this.formJson.uniteBienetre15=item.unit
      this.formJson.gammeBienetre15=item.gamme
      this.formJson.imgBienetre15=item.img
    }
    if(i==5){
   
    
      this.formJson.nomBienetre16=item.nom
      this.formJson.uniteBienetre16=item.unit
      this.formJson.gammeBienetre16=item.gamme
      this.formJson.imgBienetre16=item.img
    }
    }
    if (pageCount==1){
      if(i==0){
        
     
      
      this.formJson.nomBienetre21=item.nom
      this.formJson.uniteBienetre21=item.unit
      this.formJson.gammeBienetre21=item.gamme
      this.formJson.imgBienetre21=item.img
    }
    if(i==1){
   
    
      this.formJson.nomBienetre22=item.nom
      this.formJson.uniteBienetre22=item.unit
      this.formJson.gammeBienetre22=item.gamme
      this.formJson.imgBienetre22=item.img
    }
    if(i==2){
   
    
      this.formJson.nomBienetre23=item.nom
      this.formJson.uniteBienetre23=item.unit
      this.formJson.gammeBienetre23=item.gamme
      this.formJson.imgBienetre23=item.img
    }
    if(i==3){
   
    
      this.formJson.nomBienetre24=item.nom
      this.formJson.uniteBienetre24=item.unit
      this.formJson.gammeBienetre24=item.gamme
      this.formJson.imgBienetre24=item.img
    }
    if(i==4){
   
    
      this.formJson.nomBienetre25=item.nom
      this.formJson.uniteBienetre25=item.unit
      this.formJson.gammeBienetre25=item.gamme
      this.formJson.imgBienetre25=item.img
    }
    if(i==5){
   
    
      this.formJson.nomBienetre26=item.nom
      this.formJson.uniteBienetre26=item.unit
      this.formJson.gammeBienetre26=item.gamme
      this.formJson.imgBienetre26=item.img
    }
  }
  
    if (pageCount==2){
      if(i==0){
     
      
      this.formJson.nomBienetre31=item.nom
      this.formJson.uniteBienetre31=item.unit
      this.formJson.gammeBienetre31=item.gamme
      this.formJson.imgBienetre31=item.img
    }
    if(i==1){
   
    
      this.formJson.nomBienetre32=item.nom
      this.formJson.uniteBienetre32=item.unit
      this.formJson.gammeBienetre32=item.gamme
      this.formJson.imgBienetre32=item.img
    }
    if(i==2){
   
    
      this.formJson.nomBienetre33=item.nom
      this.formJson.uniteBienetre33=item.unit
      this.formJson.gammeBienetre33=item.gamme
      this.formJson.imgBienetre33=item.img
    }
    if(i==3){
   
    
      this.formJson.nomBienetre34=item.nom
      this.formJson.uniteBienetre34=item.unit
      this.formJson.gammeBienetre34=item.gamme
      this.formJson.imgBienetre34=item.img
    }
    if(i==4){
   
    
      this.formJson.nomBienetre35=item.nom
      this.formJson.uniteBienetre35=item.unit
      this.formJson.gammeBienetre35=item.gamme
      this.formJson.imgBienetre35=item.img
    }
    if(i==5){
   
    
      this.formJson.nomBienetre36=item.nom
      this.formJson.uniteBienetre36=item.unit
      this.formJson.gammeBienetre36=item.gamme
      this.formJson.imgBienetre36=item.img
    }
    }
    if (pageCount==3){
      if(i==0){
     
      
      this.formJson.nomBienetre41=item.nom
      this.formJson.uniteBienetre41=item.unit
      this.formJson.gammeBienetre41=item.gamme
      this.formJson.imgBienetre41=item.img
    }
    if(i==1){
   
    
      this.formJson.nomBienetre42=item.nom
      this.formJson.uniteBienetre42=item.unit
      this.formJson.gammeBienetre42=item.gamme
      this.formJson.imgBienetre42=item.img
    }
    if(i==2){
   
    
      this.formJson.nomBienetre43=item.nom
      this.formJson.uniteBienetre43=item.unit
      this.formJson.gammeBienetre43=item.gamme
      this.formJson.imgBienetre43=item.img
    }
    if(i==3){
   
    
      this.formJson.nomBienetre44=item.nom
      this.formJson.uniteBienetre44=item.unit
      this.formJson.gammeBienetre44=item.gamme
      this.formJson.imgBienetre44=item.img
    }
    if(i==4){
   
    
      this.formJson.nomBienetre45=item.nom
      this.formJson.uniteBienetre45=item.unit
      this.formJson.gammeBienetre45=item.gamme
      this.formJson.imgBienetre45=item.img
    }
    if(i==5){
   
    
      this.formJson.nomBienetre46=item.nom
      this.formJson.uniteBienetre46=item.unit
      this.formJson.gammeBienetre46=item.gamme
      this.formJson.imgBienetre46=item.img
    }
    }
    if (pageCount==4){
      if(i==0){
     
      
      this.formJson.nomBienetre51=item.nom
      this.formJson.uniteBienetre51=item.unit
      this.formJson.gammeBienetre51=item.gamme
      this.formJson.imgBienetre51=item.img
    }
    if(i==1){
   
    
      this.formJson.nomBienetre52=item.nom
      this.formJson.uniteBienetre52=item.unit
      this.formJson.gammeBienetre52=item.gamme
      this.formJson.imgBienetre52=item.img
    }
    if(i==2){
   
    
      this.formJson.nomBienetre53=item.nom
      this.formJson.uniteBienetre53=item.unit
      this.formJson.gammeBienetre53=item.gamme
      this.formJson.imgBienetre53=item.img
    }
    if(i==3){
   
    
      this.formJson.nomBienetre54=item.nom
      this.formJson.uniteBienetre54=item.unit
      this.formJson.gammeBienetre54=item.gamme
      this.formJson.imgBienetre54=item.img
    }
    if(i==4){
   
    
      this.formJson.nomBienetre55=item.nom
      this.formJson.uniteBienetre55=item.unit
      this.formJson.gammeBienetre55=item.gamme
      this.formJson.imgBienetre55=item.img
    }
    if(i==5){
   
    
      this.formJson.nomBienetre56=item.nom
      this.formJson.uniteBienetre56=item.unit
      this.formJson.gammeBienetre56=item.gamme
      this.formJson.imgBienetre56=item.img
    }
    }
    if (pageCount==5){
      if(i==0){
     
      
      this.formJson.nomBienetre61=item.nom
      this.formJson.uniteBienetre61=item.unit
      this.formJson.gammeBienetre61=item.gamme
      this.formJson.imgBienetre61=item.img
    }
    if(i==1){
   
    
      this.formJson.nomBienetre62=item.nom
      this.formJson.uniteBienetre62=item.unit
      this.formJson.gammeBienetre62=item.gamme
      this.formJson.imgBienetre62=item.img
    }
    if(i==2){
   
    
      this.formJson.nomBienetre63=item.nom
      this.formJson.uniteBienetre63=item.unit
      this.formJson.gammeBienetre63=item.gamme
      this.formJson.imgBienetre63=item.img
    }
    if(i==3){
   
    
      this.formJson.nomBienetre64=item.nom
      this.formJson.uniteBienetre64=item.unit
      this.formJson.gammeBienetre64=item.gamme
      this.formJson.imgBienetre64=item.img
    }
    if(i==4){
   
    
      this.formJson.nomBienetre65=item.nom
      this.formJson.uniteBienetre65=item.unit
      this.formJson.gammeBienetre65=item.gamme
      this.formJson.imgBienetre65=item.img
    }
    if(i==5){
   
    
      this.formJson.nomBienetre66=item.nom
      this.formJson.uniteBienetre66=item.unit
      this.formJson.gammeBienetre66=item.gamme
      this.formJson.imgBienetre66=item.img
    }
    }
    if (pageCount==6){
      if(i==0){
     
      
      this.formJson.nomBienetre71=item.nom
      this.formJson.uniteBienetre71=item.unit
      this.formJson.gammeBienetre71=item.gamme
      this.formJson.imgBienetre71=item.img
    }
    if(i==1){
   
    
      this.formJson.nomBienetre72=item.nom
      this.formJson.uniteBienetre72=item.unit
      this.formJson.gammeBienetre72=item.gamme
      this.formJson.imgBienetre72=item.img
    }
    if(i==2){
   
    
      this.formJson.nomBienetre73=item.nom
      this.formJson.uniteBienetre73=item.unit
      this.formJson.gammeBienetre73=item.gamme
      this.formJson.imgBienetre73=item.img
    }
    if(i==3){
   
    
      this.formJson.nomBienetre74=item.nom
      this.formJson.uniteBienetre74=item.unit
      this.formJson.gammeBienetre74=item.gamme
      this.formJson.imgBienetre74=item.img
    }
    if(i==4){
   
    
      this.formJson.nomBienetre75=item.nom
      this.formJson.uniteBienetre75=item.unit
      this.formJson.gammeBienetre75=item.gamme
      this.formJson.imgBienetre75=item.img
    }
    if(i==5){
   
    
      this.formJson.nomBienetre76=item.nom
      this.formJson.uniteBienetre76=item.unit
      this.formJson.gammeBienetre76=item.gamme
      this.formJson.imgBienetre76=item.img
    }
    }
    if (pageCount==7){
      if(i==0){
     
      
      this.formJson.nomBienetre81=item.nom
      this.formJson.uniteBienetre81=item.unit
      this.formJson.gammeBienetre81=item.gamme
      this.formJson.imgBienetre81=item.img
    }
    if(i==1){
   
    
      this.formJson.nomBienetre82=item.nom
      this.formJson.uniteBienetre82=item.unit
      this.formJson.gammeBienetre82=item.gamme
      this.formJson.imgBienetre82=item.img
    }
    if(i==2){
   
    
      this.formJson.nomBienetre83=item.nom
      this.formJson.uniteBienetre83=item.unit
      this.formJson.gammeBienetre83=item.gamme
      this.formJson.imgBienetre83=item.img
    }
    if(i==3){
   
    
      this.formJson.nomBienetre84=item.nom
      this.formJson.uniteBienetre84=item.unit
      this.formJson.gammeBienetre84=item.gamme
      this.formJson.imgBienetre84=item.img
    }
    if(i==4){
   
    
      this.formJson.nomBienetre85=item.nom
      this.formJson.uniteBienetre85=item.unit
      this.formJson.gammeBienetre85=item.gamme
      this.formJson.imgBienetre85=item.img
    }
    if(i==5){
   
    
      this.formJson.nomBienetre86=item.nom
      this.formJson.uniteBienetre86=item.unit
      this.formJson.gammeBienetre86=item.gamme
      this.formJson.imgBienetre86=item.img
    }
    }
    if (pageCount==8){
      if(i==0){
     
      
      this.formJson.nomBienetre91=item.nom
      this.formJson.uniteBienetre91=item.unit
      this.formJson.gammeBienetre91=item.gamme
      this.formJson.imgBienetre91=item.img
    }
    if(i==1){
   
    
      this.formJson.nomBienetre92=item.nom
      this.formJson.uniteBienetre92=item.unit
      this.formJson.gammeBienetre92=item.gamme
      this.formJson.imgBienetre92=item.img
    }
    if(i==2){
   
    
      this.formJson.nomBienetre93=item.nom
      this.formJson.uniteBienetre93=item.unit
      this.formJson.gammeBienetre93=item.gamme
      this.formJson.imgBienetre93=item.img
    }
    if(i==3){
   
    
      this.formJson.nomBienetre94=item.nom
      this.formJson.uniteBienetre94=item.unit
      this.formJson.gammeBienetre94=item.gamme
      this.formJson.imgBienetre94=item.img
    }
    if(i==4){
   
    
      this.formJson.nomBienetre95=item.nom
      this.formJson.uniteBienetre95=item.unit
      this.formJson.gammeBienetre95=item.gamme
      this.formJson.imgBienetre95=item.img
    }
    if(i==5){
   
    
      this.formJson.nomBienetre96=item.nom
      this.formJson.uniteBienetre96=item.unit
      this.formJson.gammeBienetre96=item.gamme
      this.formJson.imgBienetre96=item.img
    }
    }
    if (pageCount==9){
      if(i==0){
     
      this.formJson.nomBienetre101=item.nom
      this.formJson.uniteBienetre101=item.unit
      this.formJson.gammeBienetre101=item.gamme
      this.formJson.imgBienetre101=item.img
    }
    if(i==1){
  

      this.formJson.nomBienetre102=item.nom
      this.formJson.uniteBienetre102=item.unit
      this.formJson.gammeBienetre102=item.gamme
      this.formJson.imgBienetre102=item.img
    }
    if(i==2){
  
      this.formJson.nomBienetre103=item.nom
      this.formJson.uniteBienetre103=item.unit
      this.formJson.gammeBienetre103=item.gamme
      this.formJson.imgBienetre103=item.img
    }
    if(i==3){
  
      this.formJson.nomBienetre104=item.nom
      this.formJson.uniteBienetre104=item.unit
      this.formJson.gammeBienetre104=item.gamme
      this.formJson.imgBienetre104=item.img
    }
    if(i==4){
  
      this.formJson.nomBienetre105=item.nom
      this.formJson.uniteBienetre105=item.unit
      this.formJson.gammeBienetre105=item.gamme
      this.formJson.imgBienetre105=item.img
    }
    if(i==5){
  
      this.formJson.nomBienetre106=item.nom
      this.formJson.uniteBienetre106=item.unit
      this.formJson.gammeBienetre106=item.gamme
      this.formJson.imgBienetre106=item.img
    }
    }
    if (pageCount==10){
      if(i==0){
     
      this.formJson.nomBienetre111=item.nom
      this.formJson.uniteBienetre111=item.unit
      this.formJson.gammeBienetre111=item.gamme
      this.formJson.imgBienetre111=item.img
    }
    if(i==1){
  
      this.formJson.nomBienetre112=item.nom
      this.formJson.uniteBienetre112=item.unit
      this.formJson.gammeBienetre112=item.gamme
      this.formJson.imgBienetre112=item.img
    }
    if(i==2){
  
      this.formJson.nomBienetre113=item.nom
      this.formJson.uniteBienetre113=item.unit
      this.formJson.gammeBienetre113=item.gamme
      this.formJson.imgBienetre113=item.img
    }
    if(i==3){
  
      this.formJson.nomBienetre114=item.nom
      this.formJson.uniteBienetre114=item.unit
      this.formJson.gammeBienetre114=item.gamme
      this.formJson.imgBienetre114=item.img
    }
    if(i==4){
  
      this.formJson.nomBienetre115=item.nom
      this.formJson.uniteBienetre115=item.unit
      this.formJson.gammeBienetre115=item.gamme
      this.formJson.imgBienetre115=item.img
    }
    if(i==5){
  
      this.formJson.nomBienetre116=item.nom
      this.formJson.uniteBienetre116=item.unit
      this.formJson.gammeBienetre116=item.gamme
      this.formJson.imgBienetre116=item.img
    }
    }
    if (pageCount==11){
      if(i==0){
     
      this.formJson.nomBienetre121=item.nom
      this.formJson.uniteBienetre121=item.unit
      this.formJson.gammeBienetre121=item.gamme
      this.formJson.imgBienetre121=item.img
    }
    if(i==1){
 
    
      this.formJson.nomBienetre122=item.nom
      this.formJson.uniteBienetre122=item.unit
      this.formJson.gammeBienetre122=item.gamme
      this.formJson.imgBienetre122=item.img
    }
    if(i==2){

      this.formJson.nomBienetre123=item.nom
      this.formJson.uniteBienetre123=item.unit
      this.formJson.gammeBienetre123=item.gamme
      this.formJson.imgBienetre123=item.img
    }
    if(i==3){
    
      this.formJson.nomBienetre124=item.nom
      this.formJson.uniteBienetre124=item.unit
      this.formJson.gammeBienetre124=item.gamme
      this.formJson.imgBienetre124=item.img
    }
    if(i==4){

      this.formJson.nomBienetre125=item.nom
      this.formJson.uniteBienetre125=item.unit
      this.formJson.gammeBienetre125=item.gamme
      this.formJson.imgBienetre125=item.img
    }
    if(i==5){
      
 
      this.formJson.nomBienetre126=item.nom
      this.formJson.uniteBienetre126=item.unit
      this.formJson.gammeBienetre126=item.gamme
      this.formJson.imgBienetre126=item.img
    }
  }
  }
  selectUne(item,i,pageCount){
    this.value={value:'',key:''}
    this.produitChoisi=item
    this.value.key=`produitAlaune ${i}`
    this.value.value=JSON.stringify(item)
    this.historiqueLeaflet.pagesContent[pageCount].value.push(this.value)
    this.productForm.value.Sante[i]=item
    if (pageCount==0){
      if(i==0){
      
    
        
      this.formJson.nomProduit11=item.nom
      this.formJson.uniteProduit11=item.unit
      this.formJson.gammeProduit11=item.gamme
      this.formJson.imgProduit11=item.img
    }
  
      
      this.formJson.nomProduit12=item.nom
      this.formJson.uniteProduit12=item.unit
      this.formJson.gammeProduit12=item.gamme
      this.formJson.imgProduit12=item.img
    }
    if(i==2){

      
      this.formJson.nomProduit13=item.nom
      this.formJson.uniteProduit13=item.unit
      this.formJson.gammeProduit13=item.gamme
      this.formJson.imgProduit13=item.img
    }
    if(i==3){
  
      
      this.formJson.nomProduit14=item.nom
      this.formJson.uniteProduit14=item.unit
      this.formJson.gammeProduit14=item.gamme
      this.formJson.imgProduit14=item.img
    }
  
    
    if (pageCount==1){
      if(i==0){
        
    
        
      this.formJson.nomProduit21=item.nom
      this.formJson.uniteProduit21=item.unit
      this.formJson.gammeProduit21=item.gamme
      this.formJson.imgProduit21=item.img
    }
  }
    if(i==1){
 
      
      this.formJson.nomProduit22=item.nom
      this.formJson.uniteProduit22=item.unit
      this.formJson.gammeProduit22=item.gamme
      this.formJson.imgProduit22=item.img
    }
    if(i==2){

      this.formJson.nomProduit23=item.nom
      this.formJson.uniteProduit23=item.unit
      this.formJson.gammeProduit23=item.gamme
      this.formJson.imgProduit23=item.img
    }
    if(i==3){
 
      
      this.formJson.nomProduit24=item.nom
      this.formJson.uniteProduit24=item.unit
      this.formJson.gammeProduit24=item.gamme
      this.formJson.imgProduit24=item.img
    }
 
  
  
    if (pageCount==2){
      if(i==0){
     
        
      this.formJson.nomProduit31=item.nom
      this.formJson.uniteProduit31=item.unit
      this.formJson.gammeProduit31=item.gamme
      this.formJson.imgProduit31=item.img
    }
    if(i==1){
   
      
      this.formJson.nomProduit32=item.nom
      this.formJson.uniteProduit32=item.unit
      this.formJson.gammeProduit32=item.gamme
      this.formJson.imgProduit32=item.img
    }
    if(i==2){

      this.formJson.nomProduit33=item.nom
      this.formJson.uniteProduit33=item.unit
      this.formJson.gammeProduit33=item.gamme
      this.formJson.imgProduit33=item.img
    }
    if(i==3){

      
      this.formJson.nomProduit34=item.nom
      this.formJson.uniteProduit34=item.unit
      this.formJson.gammeProduit34=item.gamme
      this.formJson.imgProduit34=item.img
    }

    }
    if (pageCount==3){
      if(i==0){
   
        
      this.formJson.nomProduit41=item.nom
      this.formJson.uniteProduit41=item.unit
      this.formJson.gammeProduit41=item.gamme
      this.formJson.imgProduit41=item.img
    }
    if(i==1){

      
      this.formJson.nomProduit42=item.nom
      this.formJson.uniteProduit42=item.unit
      this.formJson.gammeProduit42=item.gamme
      this.formJson.imgProduit42=item.img
    }
    if(i==2){

      
      this.formJson.nomProduit43=item.nom
      this.formJson.uniteProduit43=item.unit
      this.formJson.gammeProduit43=item.gamme
      this.formJson.imgProduit43=item.img
    }
    if(i==3){

      
      this.formJson.nomProduit44=item.nom
      this.formJson.uniteProduit44=item.unit
      this.formJson.gammeProduit44=item.gamme
      this.formJson.imgProduit44=item.img
    }

    }
    if (pageCount==4){
      if(i==0){
    
        
      this.formJson.nomProduit51=item.nom
      this.formJson.uniteProduit51=item.unit
      this.formJson.gammeProduit51=item.gamme
      this.formJson.imgProduit51=item.img
    }
    if(i==1){
    
      
      this.formJson.nomProduit52=item.nom
      this.formJson.uniteProduit52=item.unit
      this.formJson.gammeProduit52=item.gamme
      this.formJson.imgProduit52=item.img
    }
    if(i==2){

      
      this.formJson.nomProduit53=item.nom
      this.formJson.uniteProduit53=item.unit
      this.formJson.gammeProduit53=item.gamme
      this.formJson.imgProduit53=item.img
    }
    if(i==3){
 
      
      this.formJson.nomProduit54=item.nom
      this.formJson.uniteProduit54=item.unit
      this.formJson.gammeProduit54=item.gamme
      this.formJson.imgProduit54=item.img
    }

    }
    if (pageCount==5){
      if(i==0){

        
      this.formJson.nomProduit61=item.nom
      this.formJson.uniteProduit61=item.unit
      this.formJson.gammeProduit61=item.gamme
      this.formJson.imgProduit61=item.img
    }
    if(i==1){
  
      
      this.formJson.nomProduit62=item.nom
      this.formJson.uniteProduit62=item.unit
      this.formJson.gammeProduit62=item.gamme
      this.formJson.imgProduit62=item.img
    }
    if(i==2){

      
      this.formJson.nomProduit63=item.nom
      this.formJson.uniteProduit63=item.unit
      this.formJson.gammeProduit63=item.gamme
      this.formJson.imgProduit63=item.img
    }
    if(i==3){

      
      this.formJson.nomProduit64=item.nom
      this.formJson.uniteProduit64=item.unit
      this.formJson.gammeProduit64=item.gamme
      this.formJson.imgProduit64=item.img
    }

    }
    if (pageCount==6){
      if(i==0){
   
        
      this.formJson.nomProduit71=item.nom
      this.formJson.uniteProduit71=item.unit
      this.formJson.gammeProduit71=item.gamme
      this.formJson.imgProduit71=item.img
    }
    if(i==1){
      
      this.formJson.nomProduit72=item.nom
      this.formJson.uniteProduit72=item.unit
      this.formJson.gammeProduit72=item.gamme
      this.formJson.imgProduit72=item.img
    }
    if(i==2){
       
      this.formJson.nomProduit73=item.nom
      this.formJson.uniteProduit73=item.unit
      this.formJson.gammeProduit73=item.gamme
      this.formJson.imgProduit73=item.img
    }
    if(i==3){

      
      this.formJson.nomProduit74=item.nom
      this.formJson.uniteProduit74=item.unit
      this.formJson.gammeProduit74=item.gamme
      this.formJson.imgProduit74=item.img
    }
 
    }
    if (pageCount==7){
      if(i==0){
    
      this.formJson.nomProduit81=item.nom
      this.formJson.uniteProduit81=item.unit
      this.formJson.gammeProduit81=item.gamme
      this.formJson.imgProduit81=item.img
    }
    if(i==1){
   
      this.formJson.nomProduit82=item.nom
      this.formJson.uniteProduit82=item.unit
      this.formJson.gammeProduit82=item.gamme
      this.formJson.imgProduit82=item.img
    }
    if(i==2){
   
      this.formJson.nomProduit83=item.nom
      this.formJson.uniteProduit83=item.unit
      this.formJson.gammeProduit83=item.gamme
      this.formJson.imgProduit83=item.img
    }
    if(i==3){
 
      this.formJson.nomProduit84=item.nom
      this.formJson.uniteProduit84=item.unit
      this.formJson.gammeProduit84=item.gamme
      this.formJson.imgProduit84=item.img
    }

    }
    if (pageCount==8){
      if(i==0){
      
      this.formJson.nomProduit91=item.nom
      this.formJson.uniteProduit91=item.unit
      this.formJson.gammeProduit91=item.gamme
      this.formJson.imgProduit91=item.img
    }
    if(i==1){

      
      this.formJson.nomProduit92=item.nom
      this.formJson.uniteProduit92=item.unit
      this.formJson.gammeProduit92=item.gamme
      this.formJson.imgProduit92=item.img
    }
    if(i==2){

      
      this.formJson.nomProduit93=item.nom
      this.formJson.uniteProduit93=item.unit
      this.formJson.gammeProduit93=item.gamme
      this.formJson.imgProduit93=item.img
    }
    if(i==3){
    
      
      this.formJson.nomProduit94=item.nom
      this.formJson.uniteProduit94=item.unit
      this.formJson.gammeProduit94=item.gamme
      this.formJson.imgProduit94=item.img
    }

    }
    if (pageCount==9){
      if(i==0){
   
        1
      this.formJson.nomProduit101=item.nom
      this.formJson.uniteProduit101=item.unit
      this.formJson.gammeProduit101=item.gamme
      this.formJson.imgProduit101=item.img
    }
    if(i==1){

      1
      this.formJson.nomProduit102=item.nom
      this.formJson.uniteProduit102=item.unit
      this.formJson.gammeProduit102=item.gamme
      this.formJson.imgProduit102=item.img
    }
    if(i==2){

      1
      this.formJson.nomProduit103=item.nom
      this.formJson.uniteProduit103=item.unit
      this.formJson.gammeProduit103=item.gamme
      this.formJson.imgProduit103=item.img
    }
    if(i==3){
   
      this.formJson.nomProduit104=item.nom
      this.formJson.uniteProduit104=item.unit
      this.formJson.gammeProduit104=item.gamme
      this.formJson.imgProduit104=item.img

    }
  }
    if (pageCount==10){
      if(i==0){

      this.formJson.nomProduit111=item.nom
      this.formJson.uniteProduit111=item.unit
      this.formJson.gammeProduit111=item.gamme
      this.formJson.imgProduit111=item.img
    }
    if(i==1){

      this.formJson.nomProduit112=item.nom
      this.formJson.uniteProduit112=item.unit
      this.formJson.gammeProduit112=item.gamme
      this.formJson.imgProduit112=item.img
    }
    if(i==2){

      this.formJson.nomProduit113=item.nom
      this.formJson.uniteProduit113=item.unit
      this.formJson.gammeProduit113=item.gamme
      this.formJson.imgProduit113=item.img
    }
    if(i==3){

      this.formJson.nomProduit114=item.nom
      this.formJson.uniteProduit114=item.unit
      this.formJson.gammeProduit114=item.gamme
      this.formJson.imgProduit114=item.img
    }

    }
    if (pageCount==11){
      if(i==0){
    
      this.formJson.nomProduit121=item.nom
      this.formJson.uniteProduit121=item.unit
      this.formJson.gammeProduit121=item.gamme
      this.formJson.imgProduit121=item.img
    }
    if(i==1){
    
      this.formJson.nomProduit122=item.nom
      this.formJson.uniteProduit122=item.unit
      this.formJson.gammeProduit122=item.gamme
      this.formJson.imgProduit122=item.img
    }
    if(i==2){
 
      this.formJson.nomProduit123=item.nom
      this.formJson.uniteProduit123=item.unit
      this.formJson.gammeProduit123=item.gamme
      this.formJson.imgProduit123=item.img
    }
    if(i==3){
 
      this.formJson.nomProduit124=item.nom
      this.formJson.uniteProduit124=item.unit
      this.formJson.gammeProduit124=item.gamme
      this.formJson.imgProduit124=item.img
    }
  
  }
  }

  onSubmit(){
   
    this.storageService.write('fakeToken',12)
    this.spinner.show();
    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hide();
    }, 5000);
 
   this.flyerService.addParams(this.formJson).subscribe(
      response=>{
        console.log(this.formJson)
        console.log(this.formJson)

      console.log('historique', this.historiqueLeaflet)
      this.historiqueService.addHitorique(this.historiqueLeaflet).subscribe(
        (res)=>{
          console.log('historique added')
        }
      )
        console.log(response)
        console.log('in')
        const pdfblob=new Blob([response],{type:'application/pdf'}) //,{type:'application/pdf'}
        saveAs(pdfblob,`${Date.now()}.pdf`)
        console.log('working!')
 
        this.storageService.write('fakeToken',55)
      }
      )

  
}
}

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { conseilService } from 'app/pages/shared/services/conseil.service';
import { Router } from '@angular/router';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { NotificationService } from 'app/pages/shared/services/notification.service';
import { StorageService } from 'app/pages/shared/services/storage.service';
@Component({
  selector: 'app-add-conseil',
  templateUrl: './add-conseil.component.html',
  styleUrls: ['./add-conseil.component.scss']
})
export class AddConseilComponent implements OnInit {
  htmlContent=''
  public Editor = ClassicEditor;
  constructor(private _formBuilder:FormBuilder,private conseilService:conseilService,private router:Router,
              private notificationService:NotificationService,
              private storageService:StorageService) { }
form:FormGroup
  ngOnInit() {
    this.storageService.write('configBool',false)
    this.storageService.write('historiqueBool',false)
    this.form=this._formBuilder.group({
      title:[''],
      subtitle:[''],
      text:[''],
      Html:['']
    })
  }

  submit(){

this.conseilService.addConseil(this.form.value).subscribe(
  res=>{
    console.log(this.form.value)
    console.log(res)
    alert('succés!')
    const link=['/app/Conseils']
    this.router.navigate(link)
    this.notificationService.showToast('Ajout de la conseil avec succés');
  }
)
  }
  showChange(event){
    console.log(event)
  }

}

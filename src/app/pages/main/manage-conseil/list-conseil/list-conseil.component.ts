import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { conseilService } from 'app/pages/shared/services/conseil.service';
import { conseil } from 'app/pages/shared/models/conseil';

@Component({
  selector: 'app-list-conseil',
  templateUrl: './list-conseil.component.html',
  styleUrls: ['./list-conseil.component.scss']
})
export class ListConseilComponent implements OnInit {
  displayedColumns: string[] = [
    'Titre','Sous-titre', 'text','action'];
    Conseils:conseil[]
  constructor(private ActivatedRoute:ActivatedRoute,private conseilService:conseilService,private route:Router) { }

  ngOnInit() {
this.conseilService.getAllConseils().subscribe(
  conseils=>{
    console.log(conseils)
    this.Conseils=conseils
    console.log(this.Conseils)
  }
)

  }
  update(id){
const link=[`app/update-conseil/${id}`]
this.route.navigate(link)
  }
  applyFilter(e){}

}

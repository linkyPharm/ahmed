import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListConseilComponent } from './list-conseil.component';

describe('ListConseilComponent', () => {
  let component: ListConseilComponent;
  let fixture: ComponentFixture<ListConseilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListConseilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListConseilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

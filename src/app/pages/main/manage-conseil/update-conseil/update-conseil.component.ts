import { Component, OnInit } from '@angular/core';
import { conseilService } from 'app/pages/shared/services/conseil.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { conseil } from 'app/pages/shared/models/conseil';
import { ActivatedRoute, Router } from '@angular/router';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { NotificationService } from 'app/pages/shared/services/notification.service';
import { StorageService } from 'app/pages/shared/services/storage.service';
@Component({
  selector: 'app-update-conseil',
  templateUrl: './update-conseil.component.html',
  styleUrls: ['./update-conseil.component.scss']
})
export class UpdateConseilComponent implements OnInit {
Conseil:conseil
public Editor = ClassicEditor;
  constructor(private conseilService:conseilService,private route:Router, private _formbuilder:FormBuilder,private ActivatedRoute:ActivatedRoute,
              private notificationService:NotificationService,private storageService:StorageService ) { }
form:FormGroup
  ngOnInit() {
    this.storageService.write('configBool',false)
    this.storageService.write('historiqueBool',false)
    this.form=this._formbuilder.group({
      title:[''],
      subtitle:[''],
      text:['']
    })
this.ActivatedRoute.params.subscribe(
  params=>{
    this.conseilService.getConseilById(params.id).subscribe(
      res=>{
        console.log(res)
        this.Conseil=res
        console.log(this.Conseil)
      }
    )
  }
)

  }
  submit(){
    this.ActivatedRoute.params.subscribe(
      params=>{
this.conseilService.updateConseil(params.id,this.form.value).subscribe(
  res=>{
    console.log(res)
    alert('succès !')
    const link=['/app/Conseils']
    this.route.navigate(link)
    this.notificationService.showToast('Ajout de la conseil avec succés');
  }
)

      }
    )
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateConseilComponent } from './update-conseil.component';

describe('UpdateConseilComponent', () => {
  let component: UpdateConseilComponent;
  let fixture: ComponentFixture<UpdateConseilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateConseilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateConseilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

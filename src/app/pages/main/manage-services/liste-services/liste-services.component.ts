import { Component, OnInit, ViewChild } from '@angular/core';
import { Service } from 'app/pages/shared/models/Service';
import { ServicePharma } from 'app/pages/shared/services/Services.service';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { Router } from '@angular/router';
import { StorageService } from 'app/pages/shared/services/storage.service';

@Component({
  selector: 'app-liste-services',
  templateUrl: './liste-services.component.html',
  styleUrls: ['./liste-services.component.scss']
})
export class ListeServicesComponent implements OnInit {
Services:Service[]
displayedColumns: string[] = [
  'Icon', 'Titre','Sous-titre','action'];
  constructor(private servicePharma:ServicePharma,private router:Router,private storageService:StorageService) { }
  dataSource: MatTableDataSource<Service>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    this.storageService.write('configBool',false)
    this.storageService.write('historiqueBool',false)
this.servicePharma.getAllServices().subscribe(
  res=>{
    console.log(res)
    this.Services=res
    console.log(this.Services)
  }
)

  }
  applyFilter(e){}
  update(id){
    const link=[`/app/update-service/${id}`]
    this.router.navigate(link)
  }
}

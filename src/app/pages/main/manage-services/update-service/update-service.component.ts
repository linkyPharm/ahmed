import { Component, OnInit, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ServicePharma } from 'app/pages/shared/services/Services.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Service } from 'app/pages/shared/models/Service';
import { NotificationService } from 'app/pages/shared/services/notification.service';
import { StorageService } from 'app/pages/shared/services/storage.service';

@Component({
  selector: 'app-update-service',
  templateUrl: './update-service.component.html',
  styleUrls: ['./update-service.component.scss']
})
export class UpdateServiceComponent implements OnInit {
    base64textString: string;
    file: File;
    
  constructor(private activatedRoute:ActivatedRoute,
    private servicePharma:ServicePharma,
    private _formBuilder:FormBuilder,
    private route:Router,
    private notificationService:NotificationService,
    private storageService:StorageService) { }
form:FormGroup
service:Service
  ngOnInit() {
    this.storageService.write('configBool',false)
    this.storageService.write('historiqueBool',false)
    
this.activatedRoute.params.subscribe(
  params=>{
    this.servicePharma.getServiceById(params.id).subscribe(
      res=>{
        console.log(res)
this.service=res
console.log(this.service)

      }
    )
  }
)

this.form=this._formBuilder.group({
  description : [''],
  icon:[''],
  title  : [''],
  subtitle : ['']
})
  }

  @HostListener('change', ['$event.target.files']) emitFiles( event: FileList) {
    console.log('heyy')
    
    const file = event && event.item(0);
    this.file = file;
    console.log(this.file);
    if (this.file == null) {
        this.form.value.icon = 'vide';
    } else {
        this.storageService.write('fileName', file.name);
    }
    console.log(this.form.value.icon);
  }
  handleFileSelect(evt){
    var files = evt.target.files;
    var file = files[0];
  
  if (files && file) {
      var reader = new FileReader();

      reader.onload =this._handleReaderLoaded.bind(this);

      reader.readAsBinaryString(file);
  }
}

_handleReaderLoaded(readerEvt) {
   var binaryString = readerEvt.target.result;
          this.base64textString= btoa(binaryString);
          this.form.value.img=btoa(binaryString)
          //console.log(btoa(binaryString));
          console.log(this.base64textString)
  }

  submit(){
    console.log(this.form.value)
   this.activatedRoute.params.subscribe(
     params=>{
        /* if (this.form.value.icon === 'vide'){
             this.form.value.icon = '';
         }*/
       this.servicePharma.updateService(params.id,this.form.value).subscribe(
         res=>{
           console.log(res)
           alert('mise à jour réussie')
const link=['/app/Services']
this.route.navigate(link)
this.notificationService.showToast('Modification du service avec succés');
         }
       )
     }
   )
  }
}

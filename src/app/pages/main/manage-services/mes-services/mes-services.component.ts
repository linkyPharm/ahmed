import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { Subject, fromEvent, BehaviorSubject, Observable, merge } from 'rxjs';
import { takeUntil, debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { DataSource } from '@angular/cdk/table';
import { FuseUtils } from '@fuse/utils';
import { EcommerceProductsService } from 'app/pages/shared/services/ecommerce.service';
import { Router } from '@angular/router';
import { ProduitService } from 'app/pages/shared/services/produits.service';
import { MesServices } from 'app/pages/shared/services/mesServices.service';
import { ServicePharma } from 'app/pages/shared/services/Services.service';
import { RoleService } from 'app/pages/shared/services/role.service';
import { StorageService } from 'app/pages/shared/services/storage.service';
import {rolesConst} from '../../manage-role/RolesPriority';
@Component({
  selector: 'app-mes-services',
  templateUrl: './mes-services.component.html',
  styleUrls: ['./mes-services.component.scss']
})
export class MesServicesComponent implements OnInit {

    role0=rolesConst[0];
    role1=rolesConst[1];
    role2=rolesConst[2];
    role3=rolesConst[3];
  userRolePriority;
  dataSource: FilesDataSource | null;
  displayedColumns = ['Icon', 'Titre','Sous-titre','action'];

  @ViewChild(MatPaginator)
  paginator: MatPaginator;

  @ViewChild(MatSort)
  sort: MatSort;

  @ViewChild('filter')
  filter: ElementRef;

  // Private
  private _unsubscribeAll: Subject<any>;

  constructor(
      private roleService: RoleService,
    private storageService:StorageService,
      private _mesServices: MesServices,
      private router:Router,
      private produitService:ProduitService,
      private Service:ServicePharma
  )
  {
      // Set the private defaults
      this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void
  {
    let userToken ; 
    userToken = this.storageService.read('user');
     this.userRolePriority = this.roleService.getUserPriorityRole(userToken.role);
     this.roleService.changeAccountActivity(this.userRolePriority);
     if(this.storageService.read('currentRole') == undefined) {
        this.storageService.write('currentRole', this.userRolePriority);
    }
    this.userRolePriority = this.storageService.read('currentRole');
    this.storageService.write('configBool',false)
    this.storageService.write('historiqueBool',false)
      this.dataSource = new FilesDataSource(this._mesServices, this.paginator, this.sort);

      fromEvent(this.filter.nativeElement, 'keyup')
          .pipe(
              takeUntil(this._unsubscribeAll),
              debounceTime(150),
              distinctUntilChanged()
          )
          .subscribe(() => {
              if ( !this.dataSource )
              {
                  return;
              }

              this.dataSource.filter = this.filter.nativeElement.value;
          });
  }
  editService(id){
      const link=[`/app/update-service/${id}`]
  this.router.navigate(link)
  console.log(id)
  }

}


export class FilesDataSource extends DataSource<any>
{
  private _filterChange = new BehaviorSubject('');
  private _filteredDataChange = new BehaviorSubject('');

  /**
   * Constructor
   *
   * @param {EcommerceProductsService} _ecommerceProductsService
   * @param {MatPaginator} _matPaginator
   * @param {MatSort} _matSort
   */
  constructor(
      private _mesServices: MesServices,
      private _matPaginator: MatPaginator,
      private _matSort: MatSort
  )
  {
      super();

      this.filteredData = this._mesServices.services;
  }

  /**
   * Connect function called by the table to retrieve one stream containing the data to render.
   *
   * @returns {Observable<any[]>}
   */
  connect(): Observable<any[]>
  {
      const displayDataChanges = [
          this._mesServices.onProductsChanged,
          this._matPaginator.page,
          this._filterChange,
          this._matSort.sortChange
      ];

      return merge(...displayDataChanges)
          .pipe(
              map(() => {
                      let data = this._mesServices.services.slice();

                      data = this.filterData(data);

                      this.filteredData = [...data];

                      data = this.sortData(data);

                      // Grab the page's slice of data.
                      const startIndex = this._matPaginator.pageIndex * this._matPaginator.pageSize;
                      return data.splice(startIndex, this._matPaginator.pageSize);
                  }
              ));
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Accessors
  // -----------------------------------------------------------------------------------------------------

  // Filtered data
  get filteredData(): any
  {
      return this._filteredDataChange.value;
  }

  set filteredData(value: any)
  {
      this._filteredDataChange.next(value);
  }

  // Filter
  get filter(): string
  {
      return this._filterChange.value;
  }

  set filter(filter: string)
  {
      this._filterChange.next(filter);
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Filter data
   *
   * @param data
   * @returns {any}
   */
  filterData(data): any
  {
      if ( !this.filter )
      {
          return data;
      }
      return FuseUtils.filterArrayByString(data, this.filter);
  }

  /**
   * Sort data
   *
   * @param data
   * @returns {any[]}
   */
  sortData(data): any[]
  {
      if ( !this._matSort.active || this._matSort.direction === '' )
      {
          return data;
      }

      return data.sort((a, b) => {
          let propertyA: number | string = '';
          let propertyB: number | string = '';

          switch ( this._matSort.active )
          {
              case 'id':
                  [propertyA, propertyB] = [a.id, b.id];
                  break;
              case 'name':
                  [propertyA, propertyB] = [a.name, b.name];
                  break;
              case 'categories':
                  [propertyA, propertyB] = [a.categories[0], b.categories[0]];
                  break;
              case 'price':
                  [propertyA, propertyB] = [a.priceTaxIncl, b.priceTaxIncl];
                  break;
              case 'quantity':
                  [propertyA, propertyB] = [a.quantity, b.quantity];
                  break;
              case 'active':
                  [propertyA, propertyB] = [a.active, b.active];
                  break;
          }

          const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
          const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

          return (valueA < valueB ? -1 : 1) * (this._matSort.direction === 'asc' ? 1 : -1);
      });
  }

  /**
   * Disconnect
   */
  disconnect(): void
  {
  }
}

import {Component, OnInit} from '@angular/core';

import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';

import {locale as english} from './i18n/en';
import {locale as french} from './i18n/fr';
import {ActivatedRoute} from '@angular/router';
import {MenuService} from '../../shared/services/menu.service';
import { StorageService } from 'app/pages/shared/services/storage.service';

@Component({
    selector: 'sample',
    templateUrl: './sample.component.html',
    styleUrls: ['./sample.component.scss']
})
export class SampleComponent implements OnInit {

    constructor(private menuService: MenuService,
                private route: ActivatedRoute,
                private _fuseTranslationLoaderService: FuseTranslationLoaderService,
                private storageService:StorageService
    ) {
        this._fuseTranslationLoaderService.loadTranslations(english, french);
    }

    ngOnInit(): void {
        this.storageService.write('configBool',false)
        this.storageService.write('historiqueBool',false)
    }

}

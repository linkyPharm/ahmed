import {NgModule} from '@angular/core';
import {MatAutocompleteModule, MatIconModule} from '@angular/material';

import {FuseSharedModule} from '@fuse/shared.module';

import {Error500Component} from './500/error-500.component';
import {ErrorsRoutingModule} from './errors.routing';
import {Error404Component} from './404/error-404.component';


@NgModule({
    declarations: [
        Error404Component,
        Error500Component
    ],
    imports: [
        ErrorsRoutingModule,
        MatIconModule,
        MatAutocompleteModule,

        FuseSharedModule
    ]
})
export class ErrorsModule {
}

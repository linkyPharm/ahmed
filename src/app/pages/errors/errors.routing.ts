import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {Error500Component} from './500/error-500.component';
import {Error404Component} from './404/error-404.component';


const AppRouting: Routes = [
    {
        path: '404',
        component: Error404Component
    },
    {
        path: '500',
        component: Error500Component
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(AppRouting)
    ],
    exports: [
        RouterModule
    ]
})
export class ErrorsRoutingModule {

}

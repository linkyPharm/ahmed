import {Component, ViewEncapsulation} from '@angular/core';

import {FuseConfigService} from '@fuse/services/config.service';
import {Router} from '@angular/router';

@Component({
    selector: 'error-404',
    templateUrl: './error-404.component.html',
    styleUrls: ['./error-404.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class Error404Component {

    /**
     * Constructor
     *
     * @param {Router} router
     * @param {SearchService} searchService
     * @param {FuseConfigService} _fuseConfigService
     */
    constructor(
        private router: Router,
        private _fuseConfigService: FuseConfigService
    ) {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
    }

    /**
     * Search
     *
     * @param event
     */



}

import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { StorageService } from './storage.service';

@Injectable()
export class MesConseils implements Resolve<any>
{
    conseils: any[]=[];
    onProductsChanged: BehaviorSubject<any>;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient,
        private _storageService:StorageService
    )
    {
        // Set the defaults
        this.onProductsChanged = new BehaviorSubject({});
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getConseils(),
                console.log('in')
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get products
     *
     * @returns {Promise<any>}
     */
    getConseils(): Promise<any>
    {
        return new Promise((resolve, reject) => {
       console.log('in')
            this._httpClient.get("http://51.77.158.247:9999/conseil-service/conseil/")
                .subscribe((response: any) => {
           console.log(response)
                    this.conseils = response;
                    this.onProductsChanged.next(this.conseils);
                    resolve(response);
                }, reject);
        });
    }
}

import {Injectable} from '@angular/core';
import {StorageService} from './storage.service';
import {HttpClient} from '@angular/common/http';
import {GenericService} from './generic.service';
import {AppConfig} from '../app.config';
import {map} from 'rxjs/operators';

@Injectable()
export class MenuService extends GenericService {

    constructor(private http: HttpClient, private storageService: StorageService) {
        super();
    }

    getAllItems() {
        const url = AppConfig.baseUrl + '/menuitems';

        return this.http.get(url);
    }

    getAllActionItems() {
        const url = AppConfig.baseUrl + '/actionItems';
        return this.http.get(url).pipe(map((data) => {
            this.storageService.write('ACTION_ITEMS', data);
            return data;
        }));
    }
}

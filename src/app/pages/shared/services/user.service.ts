import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {GenericService} from './generic.service';
import {StorageService} from './storage.service';
import {Observable} from 'rxjs';
import {AppConfig} from '../app.config';
import {Credentials} from '../credentials';
import {User} from '../models/User';
import {map} from 'rxjs/operators';
import {LanguageService} from './language.service';
import { Personne } from 'app/pages/main/manage-post/model/personne';
import { element } from '@angular/core/src/render3/instructions';
import { Pharmacy } from '../models/Pharmacy';
import { userLogin } from '../models/login';
import { UserList } from '../models/UserList';


@Injectable()
export class UserService extends GenericService {
public personnes:Personne[]
public personne:Personne[]
public option:any
link='http://localhost:3000/api/personnes'
   public  currentUser:User=new User()
    public currentUserProfileDocId: string;
    public currentUserProfileDocUrl: string;
    public onLanguageChangeObservables: Array<OnLanguageChangeObservable> = [];
    public onUserFetchedObservable: Array<OnUserFetchedObservable> = [];

    constructor(private http: HttpClient, private storageService: StorageService,
                private languageService: LanguageService) {
        super();
        this.currentUserProfileDocUrl = 'https://demo.pokmy.net/img/avatar.png';
        this.personnes=[
            new Personne(1,"hama","ben ismail",777,"webdev",""),
            new Personne(2,"hama","ben fraj",77788,"webdev","")
          ]
    }

    login(credentials: Credentials): Observable<userLogin> {
       const url='http://51.77.158.247:9999/authentification-service/login?' + "username=" + credentials.username + "&password=" + credentials.password
const url1='http://192.168.1.18:9999/authentification-service/login?' + "username=" + credentials.username + "&password=" + credentials.password
const url2='http://51.77.158.247:9999/authentification-service/login?' + "username=" + 'van' + "&password=" + '0000'

        return this.http.post<userLogin>(url,{});
        
    }
    getoption(option){
this.option=option
    }
    returnOption(){
        return this.option
    }
getFakePersonne(){
 
    return this.personnes
}
getPharmacy():Observable<Pharmacy[]>{
    return this.http.get<Pharmacy[]>(this.link)
}
getPharmacyById(id):Observable<Pharmacy>{
  return this.http.get<Pharmacy>(this.link+`/${id}`)
}
addPharmacy(Pharmacy):Observable<any>{
return this.http.post(this.link,Pharmacy)
}
deletePharmacy(id){
return this.http.delete(this.link+`/${id}`)
}
updatePharmacy(Pharmacy){
    return this.http.put(this.link,Pharmacy)
    }
getFakePersonneById(id):Personne[]{
   return this.personne= this.personnes.filter(personne=>{
     if(personne.id==id){
         return personne.id
     }
     else {
         console.log('not found')
     }
         
    })
}
getPersonnes(){
 this.http.get<Personne[]>(this.link).subscribe(objs=>{
     this.personnes=objs
 })
console.log(this.personnes)
}
deleteFakePersonne(id){
this.personnes=this.personnes.splice(0,1)
console.log(this.personnes)

}
updateFakePersonne(personne,params){

const index=this.personnes.findIndex(obj=>{
return obj.id==params

})

console.log(index)

this.personnes[index]=<Personne>personne
console.log(this.personnes[index])



}


    register(user: User): Observable<any> {
        console.log(user);
        const url = AppConfig.baseUrl + 'authentification-service/signup/';
        const url2 = AppConfig.baseUrl1+'signup/' ;
        return this.http.post(url2, user);
    }

    isLoggedIn(): boolean {
        return this.storageService.read(AppConfig.accessTokenName) != null;
    }

    getToken(): string {
        return this.storageService.read(AppConfig.accessTokenName);
    }

    saveToken(token: string): void {
        this.storageService.write(AppConfig.accessTokenName, token);
    }

    clear(): void {
        this.storageService.removeAll();
    }

    getCurrentUser(): Observable<User> {
        const url = AppConfig.baseUrl + '/users/current?alt=json';
        return this.http.get(url).pipe(map((data: User) => {
            this.currentUser = data;
            this.storageService.write('user', data);
            if (this.onUserFetchedObservable) {
                this.onUserFetchedObservable.forEach(function (onUserFetchedObservable) {
                    onUserFetchedObservable.onUserFetched(data);
                });
            }
            return data;
        })).pipe(map((data: User) => {
            this.fetchUserProfilePictureDocument().subscribe(documents => {

            });
            return data;
        })) as Observable<User>;
    }


    sendResetLink(username: string): Observable<any> {
        const url = AppConfig.baseUrl + 'authentification-service/signup/forgot?' + "username=" + username ;

        return this.http.post(url, {
            username: username
        });

    }

    fetchUserProfilePictureDocument(): Observable<Array<Document>> {
        const url = AppConfig.baseUrl + '/documents/Image/user/' +
            this.currentUser._id + '?subjectId=' + this.currentUser._id + '&subjectType=user&type=Image';
        return this.http.get(url) as Observable<Array<Document>>;
    }


    getUserProfilePictureUrl() {
        return this.currentUserProfileDocUrl;
    }

    searchAllUsers(): Observable<Array<User>> {
        const url = AppConfig.baseUrl + '/users/searchAll';
        return this.http.post(url, {type: "INTERNAL"}) as Observable<Array<User>>;
    }

    editProfile(user: User): Observable<User> {
        const url = AppConfig.baseUrl + '/users';
        return this.http.post(url, user).pipe(map((data: User) => {
            this.storageService.write('user', data);
            this.currentUser = data;
            if (this.onUserFetchedObservable) {
                this.onUserFetchedObservable.forEach(function (onUserFetchedObservable) {
                    onUserFetchedObservable.onUserFetched(data);
                });
            }
            return data;
        })) as Observable<User>;
    }
  savecurrentuser(user){
      this.currentUser.email=user
  }
  getcurrentuser(){
return this.currentUser
  

  }
}

export class LoginResponse {
    account: any;
    token: string;
}


export interface OnLanguageChangeObservable {
    onLanguageChange(lang: string);
}


export interface OnUserFetchedObservable {
    onUserFetched(user: User);
}


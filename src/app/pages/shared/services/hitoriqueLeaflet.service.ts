
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { conseil } from "../models/conseil";
import { Observable } from "rxjs";
import { HistoriqueLeaflet } from "../models/historiqueLeaflet";

@Injectable({
    providedIn: 'root'
  })
export class HistoriqueLeafletService{
    constructor(private http:HttpClient) { }
getAllHistorique():Observable<HistoriqueLeaflet[]>{
    const link="http://51.77.158.247:9999/leaflet-service/leaflet/"
    return this.http.get<HistoriqueLeaflet[]>(link)
}
addHitorique(historique:HistoriqueLeaflet){
    const link="http://51.77.158.247:9999/leaflet-service/leaflet/"
    return this.http.post(link,historique)
}
getUniqueHistorique(id):Observable<any>{
    const link=`http://51.77.158.247:9999/leaflet-service/leaflet/single/${id} `
    return this.http.get<any>(link)
}
}
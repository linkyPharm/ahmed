import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Pharmacy } from '../models/Pharmacy';
import { Organization } from '../models/Organization';
import { pharmaExc } from '../models/pharmaExc';

@Injectable({
  providedIn: 'root'
})
export class PharmacieService {

  constructor(private http:HttpClient) { }
  submitPharmacy(pharmacie:Pharmacy){
    const link='http://51.77.158.247:9999/organizationunit-service/pharmacy/addPharmacy'
    const link1='http://192.168.1.18:9999/organizationunit-service/pharmacy/addPharmacy'
    return this.http.post(link,pharmacie)
  }
  getPharmaciesByOwnerId(id):Observable<Pharmacy[]>{
    const link1=`http://192.168.1.18:9999/organizationunit-service/pharmacy/user/${id}`
    const link=`http://51.77.158.247:9999/organizationunit-service/pharmacy/user/${id}`
    return this.http.get<Pharmacy[]>(link)
  }
  getAllPharmacies():Observable<Pharmacy[]>{
const link1="http://192.168.1.18:9999/organizationunit-service/pharmacy/getPharmacies"
const link='http://51.77.158.247:9999/organizationunit-service/pharmacy/getPharmacies'
return this.http.get<Pharmacy[]>(link)
  }
  getPharmacyById(id):Observable<Pharmacy>{
    const link1=`http://192.168.1.18:9999/organizationunit-service/pharmacy/${id}`
    const link=`http://51.77.158.247:9999/organizationunit-service/pharmacy/${id}`
    return this.http.get<Pharmacy>(link)
  }
  updatePharmacy(id,pharmacie){
    const link=`http://51.77.158.247:9999/organizationunit-service/pharmacy/${id}`
    return this.http.put(link,pharmacie)
  }
  deletePharmacy(id){
    const link1=`http://192.168.1.18:9999/organizationunit-service/pharmacy/${id}`
    const link=`http://51.77.158.247:9999/organizationunit-service/pharmacy/${id}`
    return this.http.delete(link)
  }
  getorgans():Observable<Organization[]>{
    const link1="http://192.168.1.18:9999/organizationunit-service/pharmacy/getOrgans"
    const link=`http://51.77.158.247:9999/organizationunit-service/pharmacy/getOrgans`
    return this.http.get<Organization[]>(link)
  }
  addMultiplePharmacy(pharmacys:pharmaExc[]){
    console.log('in')
    const link="http://51.77.158.247:9999/organizationunit-service/pharmacy/addPharmacies"
    const link1="http://192.168.1.18:9999/organizationunit-service/pharmacy/addPharmacies"
    return this.http.post(link,pharmacys)
  }
}

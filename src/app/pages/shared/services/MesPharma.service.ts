import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { StorageService } from './storage.service';

@Injectable()
export class MesPharmaService implements Resolve<any>
{
    pharmacies: any[]=[];
    onProductsChanged: BehaviorSubject<any>;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient,
        private _storageService:StorageService
    )
    {
        // Set the defaults
        this.onProductsChanged = new BehaviorSubject({});
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getProducts(this._storageService.read('id')),
                console.log('in')
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get products
     *
     * @returns {Promise<any>}
     */
    getProducts(id): Promise<any>
    {
        return new Promise((resolve, reject) => {
       console.log('in')
            this._httpClient.get(`http://51.77.158.247:9999/organizationunit-service/pharmacy/user/${id}`)
                .subscribe((response: any) => {
           console.log(response)
                    this.pharmacies = response;
                    this.onProductsChanged.next(this.pharmacies);
                    resolve(response);
                }, reject);
        });
    }
}

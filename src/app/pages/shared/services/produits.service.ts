import { Injectable } from "@angular/core";
import { Pharmacy } from "../models/Pharmacy";
import { produit } from "../models/Produits";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { link } from "fs";
import { historique } from "../models/historique";
import { toFormData } from "app/pages/main/manage-post/add-post/add-post.component";
import { FormBuilder, FormGroup } from "@angular/forms";





@Injectable()
export class ProduitService{

produits:produit[]

    constructor(private http:HttpClient){
/*this.produits=[
    new produit(1,'hama','12','sousse','haute','bonne','kilogramme','l','fort','',0),
    new produit(2,'hama','12','sousse','haute','bonne','kilogramme','l','fort','img',0)
]*/
}

getFakeProduits():produit[]{
    return this.produits
}
getFakeProduitById(id):produit{

const index=this.produits.findIndex(id)
  return this.produits[index]
}
submitProduit(produit:produit){
    const link="http://51.77.158.247:9999/product-service/product/addProduct";
    console.log("eheyehe");
    return this.http.post(link,produit)
}
getProduits():Observable<produit[]>{
    const link="http://51.77.158.247:9999/product-service/product/getProducts"
   return this.http.get<produit[]>(link)  
}
updtateProduit(id,produit){
    const link=`http://51.77.158.247:9999/product-service/product/${id}`
    const link1=`http://192.168.1.18:9999/product-service/product/${id}`
    return this.http.put(link,produit)
}
getProduitById(id:number):Observable<produit>{
    const link=`http://51.77.158.247:9999/product-service/product/${id}`
    const link1=`http://192.168.1.18:9999/product-service/product/${id}`
    return this.http.get<produit>(link)
}
deleteproduit(id){
    const link=`http://51.77.158.247:9999/product-service/product/${id}`
    const link1=`http://192.168.1.18:9999/product-service/product/${id}`
    return this.http.delete(link)
}
addMultipleProduits(produits:produit[]){
    const link="http://51.77.158.247:9999/product-service/product/addProducts"
    const link1="http://192.168.1.18:9999/product-service/product/addProducts"
    return this.http.post(link1,produits)
}
addHistoriqueProduit(historique:historique){
    const link1="http://192.168.1.18:9999/product-service/historique/"
    const link="http://51.77.158.247:9999/product-service/historique/"
    return this.http.post(link,historique)
}
getHistoriqueProduits(id):Observable<historique[]>{
    const link1=`http://192.168.1.18:9999/product-service/historique/${id}`
    const link=`http://51.77.158.247:9999/product-service/historique/${id}`
    return this.http.get<historique[]>(link)
}
getSingleHistorique(id):Observable<historique>{
    const link1=`http://192.168.1.18:9999/product-service/historique/byId/${id}`
    const link=`http://51.77.158.247:9999/product-service/historique/byId/${id}`
    return this.http.get<historique>(link)
}
afficheGenerate(forme:FormGroup){
   return this.http.post('http://51.77.158.247:3000/pdf/convert', toFormData(forme.value), {
    responseType:'blob'
 } )

}

    getProducts(status):Observable<produit[]>{
        const link = 'http://51.77.158.247:9999/product-service/product/getProducts/'+status;
        return (this.http.get<produit[]>(link));
    }

    validOrDeclineProduct(status,id):Observable<produit>{
        const link = 'http://51.77.158.247:9999/product-service/product/' + id +'/' + status;
        return (this.http.put<produit>(link,{}));
    }

    /*putProductsToValidate(){

    }*/
}

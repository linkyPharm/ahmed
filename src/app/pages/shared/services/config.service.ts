import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { StorageService } from './storage.service';


@Injectable()
export class configLeafletService implements Resolve<any>
{
    configs: any[]=[];
    products :any[]=[]
    suppConfigs:any[]=[]
    onProductsChanged: BehaviorSubject<any>;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient,
        private storageService:StorageService
    )
    {
        // Set the defaults
        this.onProductsChanged = new BehaviorSubject({});
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getProducts(),
                console.log('in')
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get products
     *
     * @returns {Promise<any>}
     */
    getProducts(): Promise<any>
    {
        return new Promise((resolve, reject) => {
       console.log('in')
            /*this._httpClient.get("http://51.77.158.247:9999/product-service/product/getProducts")
                .subscribe((response: any) => {
           console.log(response)
                    this.products = response;
                    this.onProductsChanged.next(this.products);
                    resolve(response);
                }, reject)*/;
this.configs=this.storageService.read('configurations')
if (this.configs==null){
    this.storageService.write('configurations',[])
    this.configs=this.storageService.read('configurations')
}
console.log(this.configs)
this.onProductsChanged.next(this.configs)
resolve(this.storageService.read('selected_page'))
        },
       
        );
    }
    deleteconf(index){
        return new Promise((resolve, reject) => {
        this.suppConfigs=this.storageService.read('configurations')
this.suppConfigs.splice(index,1)
this.onProductsChanged.next(this.suppConfigs)
this.storageService.write('configurations',this.suppConfigs)
resolve(this.suppConfigs)
    })
}
}
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {GenericService} from './generic.service';
import {AppConfig} from '../app.config';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable()
export class LanguageService extends GenericService {
    dictionaryFR: Map<string, string>;
    dictionaryEN: Map<string, string>;
    availableLanguages = ['fr', 'en'];

    constructor(private http: HttpClient) {
        super();
    }

    async getOrFetchDictionary(lang: string) {
        switch (lang) {
            case 'fr':
                if (!this.dictionaryFR) {
                    await this.fetchDictionary('fr_FR').toPromise().then((data: Map<string, string>) => {
                        this.dictionaryFR = data;
                    });
                }
                return this.dictionaryFR;
            case  'en':
                if (!this.dictionaryEN) {
                    await this.fetchDictionary('en_US').toPromise().then((data: Map<string, string>) => {
                        this.dictionaryEN = data;
                    });
                }
                return this.dictionaryEN;
        }
    }


    fetchDictionary(lang: string): Observable<Map<string, string>> {
        const url = AppConfig.baseUrl + '/lang?lang=' + lang;
        return this.http.get(url).pipe(map((data: Map<string, string>) => {
            return data;
        })) as Observable<Map<string, string>>;
    }

    getLangCodeFromId(lang: string): string {
        switch (lang) {
            case 'fr':
                return 'fr_FR';
            case 'en':
                return 'en_US';
            default:
                throw new Error('No supported language found for this input');
        }
    }

    getIdFromLangCode(lang: string): string {
        switch (lang) {
            case 'fr_FR':
                return 'fr';
            case 'en_US':
                return 'en';
            default:
                throw new Error('No supported language found for this input');
        }
    }

    getLabelFromLangCode(lang: string): string {
        switch (lang) {
            case 'fr_FR':
                return 'French';
            case 'en_US':
                return 'English';
            default:
                throw new Error('No supported language found for this input');
        }
    }
}

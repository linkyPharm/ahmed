import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { conseil } from "../models/conseil";
import { Observable } from "rxjs";
import { configuration } from "../models/configuration";

@Injectable({
    providedIn: 'root'
  })

export class configurationsService{
    constructor(private http:HttpClient) { }
    addConfiguration(configuration:configuration){
        const link='http://51.77.158.247:9999/leaflet-service/configleaflet/'
        return this.http.post(link,configuration)
    }
    showConfigurationsByGroupementId(groupementId):Observable<configuration[]>{
const link=`http://51.77.158.247:9999/leaflet-service/configleaflet/${groupementId}`
return this.http.get<configuration[]>(link)
    }
    showAllConfigurations(){
        const link='http://51.77.158.247:9999/leaflet-service/configleaflet/'
        const link1='http://192.168.1.18:9999/leaflet-service/configleaflet/'
        return this.http.get<any[]>(link)
    }
    editConfiguration(configuration){
const link='http://51.77.158.247:9999/leaflet-service/configleaflet/'
return this.http.put(link,configuration)
    }
    getUniqueConfiguration(id){
        const link1=`http://192.168.1.18:9999/leaflet-service/configleaflet/single/${id}`
    const link=`http://51.77.158.247:9999/leaflet-service/configleaflet/single/${id}`
        return this.http.get<any>(link)
    }
}
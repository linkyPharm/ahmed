import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class DataService {

  private historyBool = new BehaviorSubject<any>({});
  private configBool= new BehaviorSubject<any>({});
  currenthistoryBool = this.historyBool.asObservable();
  currentconfigBool=this.configBool.asObservable()

  constructor() { }

  changeconfigBool(bool=true) {
    this.configBool.next(bool)
  }
  changehistoryBool(bool=true) {
    this.historyBool.next(bool)
  }
}
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { conseil } from "../models/conseil";
import { Observable } from "rxjs";

@Injectable({
    providedIn: 'root'
  })

export class conseilService{
    constructor(private http:HttpClient) { }
    addConseil(conseil:conseil){
        const link="http://51.77.158.247:9999/conseil-service/conseil/"
      return  this.http.post(link,conseil)
    }
    getAllConseils():Observable<conseil[]>{
        const link="http://51.77.158.247:9999/conseil-service/conseil/"
        return this.http.get<conseil[]>(link)
    }
    getConseilById(id):Observable<conseil>{
        const link=`http://51.77.158.247:9999/conseil-service/conseil/${id}`
        return this.http.get<conseil>(link)
    }
    updateConseil(id,conseil:conseil){
        const link=`http://51.77.158.247:9999/conseil-service/conseil/${id}`
return this.http.put(link,conseil)
    }
}
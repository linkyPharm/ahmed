import {Injectable} from '@angular/core';

@Injectable()
export class StorageService {
    write(key: string, value: any): void {
        if (value) {
            value = JSON.stringify(value);
        }
        localStorage.setItem(key, value);
    }

    read<T>(key: string): T {
        const value: string = localStorage.getItem(key);

        if (value && value !== 'undefined' && value !== 'null') {
            return <T>JSON.parse(value);
        }

        return null;
    }

    remove(key: string): void {
        localStorage.removeItem(key);
    }

    removeAll(): void {
        localStorage.clear();
    }

    isExist(key: string): boolean {
        return localStorage.getItem(key) != null;
    }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';
import { UserList } from '../models/UserList';
import {rolesConst} from '../../main/manage-role/RolesPriority';
import { Role } from '../models/Role';
import { StorageService } from './storage.service';
@Injectable({
  providedIn: 'root'
})
export class RoleService {
    currentRole;
    // the user who's utilizing the application
    jsonUserType;
    link: string = 'http://51.77.158.247:9999/authentification-service/signup/';
roles=[];
    allRolesReturned = [];
  public constructor(private httpClient: HttpClient , private storageService: StorageService) { 
    }

    getUsersList(): Observable<UserList[]>{
        if (navigator.onLine){
            return this.httpClient.get<UserList[]>(this.link);
        }else {
            alert("You'Re Offline !! you should be online to see");
        }
  }

  putUserRoles(id: number , role){
      var url: string = 'http://51.77.158.247:9999/authentification-service/signup/updaterole/' + id + '/';
        return (this.httpClient.put(url, role));    
  }

  setUserRoles(userRoles){
    this.roles.push(userRoles);
  }

  getUserRoles(){
      return this.roles;
  }


  setAllRolesReturned(returnedRoles){
    this.allRolesReturned = returnedRoles;
  }
  getAllRolesReturned(){
      return this.allRolesReturned;
  }


  // rolePriority

  getUserPriorityRole(roles : Array<Role>) {
    for (let i = 0 ; i < rolesConst.length ; i++){
        let j = 0;
        while (j < roles.length){
            if (roles[j].label === rolesConst[i]){
                return (rolesConst[i]);
            }
            j++;
        }
    }
    return ('guest');
  }

  editToken(role){
    let userRoleToken;
    userRoleToken = this.storageService.read('user');
    userRoleToken.role = role;
    this.storageService.write('user', userRoleToken );
  }

  /*setCurrentRole(role){
    this.currentRole = role;
  }
  getCurrentRole(){
      return (this.currentRole);
  }*/

  changeAccountActivity(role){
    let user ;  
    user = this.storageService.read('user');
    for (let i = 0 ; i < user.role.length; i++){
         if (user.role[i].label == role) {
             user.role[i].currentRole = true;
         } else {
             user.role[i].currentRole = false;
         }
    }
    this.storageService.write('user',user);
    // console.log(user); 
 }
}

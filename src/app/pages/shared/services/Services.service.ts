import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Service } from "../models/Service";
import { Observable } from "rxjs";

@Injectable({
    providedIn: 'root'
  })

  export class ServicePharma{
    constructor(private http:HttpClient) { }
    ajouterService(service:Service){
        const link="http://51.77.158.247:9999/service-service/service/"
        return this.http.post(link,service)
    }
    getAllServices():Observable<Service[]>{
        const link="http://51.77.158.247:9999/service-service/service/"
        return this.http.get<Service[]>(link)
    }
    getServiceById(id):Observable<Service>{
        const link=`http://51.77.158.247:9999/service-service/service/${id}`
        return this.http.get<Service>(link)
    }
    updateService(id,service:Service){
        const link=`http://51.77.158.247:9999/service-service/service/${id}`
    return this.http.put(link,service)
    }
  }
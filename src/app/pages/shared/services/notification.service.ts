import {Injectable} from "@angular/core";
import swal, {SweetAlertType} from 'sweetalert2';
import {TranslateService} from "@ngx-translate/core";
// https://sweetalert2.github.io/
// https://github.com/sweetalert2/ngx-sweetalert2

@Injectable()
export class NotificationService {
    static SUCCESS_NOTIFICATION = 1;
    static WARNING_NOTIFICATION = 2;
    static INFO_NOTIFICATION = 3;
    static QUESTION_NOTIFICATION = 4;
    static ERROR_NOTIFICATION = -1;

    constructor(private translateService: TranslateService) {

    }

    showToast(title: string, type: number = 1) {
        const toast = swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 4000
        });
        let toastType: SweetAlertType;
        switch (type) {
            case NotificationService.SUCCESS_NOTIFICATION:
                toastType = 'success';
                break;
            case NotificationService.WARNING_NOTIFICATION:
                toastType = 'warning';
                break;
            case NotificationService.ERROR_NOTIFICATION:
                toastType = 'error';
                break;
            case NotificationService.INFO_NOTIFICATION:
                toastType = 'info';
                break;
            case NotificationService.QUESTION_NOTIFICATION:
                toastType = 'question';
                break;
        }
        this.translateService.get(title).subscribe(data => {
            toast({
                type: toastType,
                title: data
            });
        });
    }
}

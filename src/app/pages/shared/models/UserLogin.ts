import { Role } from "./Role";

export class UserLogin {
    
    role: Array<Role>;
    account: {id: number, username: string, token: string};
    token: string;
    
};
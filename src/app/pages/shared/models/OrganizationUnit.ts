import {Organization} from "./Organization";

export class OrganizationUnit {
    public id:number
    public code: string;
    public name: string;
    public email: string;
    public website: string;
    public fax: string;
    public phone: string;
    public organization: Organization;
}

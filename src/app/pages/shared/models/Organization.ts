import { User } from "./User";

export class Organization {
    public code: string;
    public description: string;
    public logo: string;
    public name: string;
    public parent:Organization
    public owner:User
    public organization:Organization
    //public parent:Organization
}

import { Owner } from "./owner";
import { relationship } from "./relationship";
import { Address } from "./Address";
import { openingDetails } from "./openingdetail";
import { additionalInfo } from "./additionalinfo";

export class pharmaExc{
    email: string
    fax: string
name: string
    phone: string
    website: string
    organizationId: number
    owner:Owner
       
    
    relationship: relationship
   address:Address
    openingDetails: openingDetails
    additionalInfo: additionalInfo
    constructor(email,fax,name,phone,website,organizationId,address,owner,openningDetails,additionalInfo,relationship){
        this.email=email
        this.fax=fax
        this.name=name
        this.phone=phone
        this.website=website
        this.organizationId=organizationId
        this.address=address
        this.owner=owner
        this.openingDetails=openningDetails
        this.additionalInfo=additionalInfo
        this.relationship=relationship
     }
}
import {Address} from './Address';
import {OrganizationUnit} from "./OrganizationUnit";
import {PersonDetails} from './PersonDetails';
import { Role } from './Role';
export class UserList {
    public active: boolean;
    public id: number;
    public login: string;
    public password: string;
    public resetToken: string;
    public userId: number;
    public roles: Array<Role>;
    public person: PersonDetails;
}
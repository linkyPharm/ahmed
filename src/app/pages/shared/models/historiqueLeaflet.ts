export class HistoriqueLeaflet{
    name:string
    configLeaflet:number
    userId:number
    pagesContent:Array<{pageNumber:number,value:Array<{key:string,value:string}>}>
    constructor(name,configleaflet,userId,pagesContents){
        this.name=name;
        this.configLeaflet=configleaflet
        this.userId=userId;
        this.pagesContent=pagesContents
    }
}
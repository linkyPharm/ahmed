import { Address } from "./Address";
import { Personne } from "app/pages/main/manage-post/model/personne";
import { User } from "./User";
import { relationship } from "./relationship";
import { openingDetails } from "./openingdetail";
import { additionalInfo } from "./additionalinfo";
import { Organization } from "./Organization";
import { List } from "lodash";
import { Owner } from "./owner";

export class Pharmacy{
   public id:number;
   public owner:Owner
   public email:string;
   public fax;string;
   public  name:string;
   public phone:string;
   public website:string;
   public organization:Organization
   public organizationId:number
   public   ownerId:number;
   public relationship:relationship;
   public  address:Address;
   public openingDetails: List<openingDetails>;
   public additionalInfo:List<additionalInfo>
constructor(email,fax,name,phone,website,organizationId,address,openningDetails,additionalInfo){
   this.email=email
   this.fax=fax
   this.name=name
   this.phone=phone
   this.website=website
   this.organizationId=organizationId
   this.address=address
   this.openingDetails=openningDetails
   this.additionalInfo=additionalInfo
}

}
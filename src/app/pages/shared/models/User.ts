import {Address} from './Address';
import {OrganizationUnit} from "./OrganizationUnit";

export class User {
    public _id: string;
    public firstName: string;
    public lastName: string;
    public login: string;
    public email: string;
    public password: string;
    public gender: string;
    public phone: string;
    public address: Address;
    public isOwner: boolean;
    public organizationUnitId:number
    public organizationUnit: OrganizationUnit;
}



export class Leaflet{
    nbrService:string
    nbrElements1:number
    nbrElements2:number
    nbrElements3:number
    nbrElements4:number
    nbrElements5:number
    nbrElements6:number
    nbrElements7:number
    nbrElements8:number
    nbrElements9:number
    nbrElements10:number
    nbrElements11:number
    nbrElements12:number
      //pages
    p1:number
    p2:number
    p3:number
    p4:number
    p5:number
    p6:number
    p7:number
    p8:number
    p9:number
    p10:number
    p11:number
    p12:number
    p13:number
    nbrPages:number
      //pages
    
      //otherDetails
    nomPharma1:string
    adressePharma1:string
    odPharma1:string
    nomPharma:string
    villePharma1:string
    telPharma1:string
    webPharma1:string
    mailPharma1:string
    moisPharma1:string
    
    
    nomPharma2:string
    adressePharma2:string
    odPharma2:string
    
    villePharma2:string
    telPharma2:string
    webPharma2:string
    mailPharma2:string
    moisPharma2:string
    
    
    nomPharma3:string
    adressePharma3:string
    odPharma3:string
    
    villePharma3:string
    telPharma3:string
    webPharma3:string
    mailPharma3:string
    moisPharma3:string
    
    nomPharma4:string
    adressePharma4:string
    odPharma4:string
    
    villePharma4:string
    telPharma4:string
    webPharma4:string
    mailPharma4:string
    moisPharma4:string
    
    nomPharma5:string
    adressePharma5:string
    odPharma5:string
    
    villePharma5:string
    telPharma5:string
    webPharma5:string
    mailPharma5:string
    moisPharma5:string
    
    nomPharma6:string
    adressePharma6:string
    odPharma6:string
    
    villePharma6:string
    telPharma6:string
    webPharma6:string
    mailPharma6:string
    moisPharma6:string
    
    nomPharma7:string
    adressePharma7:string
    odPharma7:string
    
    villePharma7:string
    telPharma7:string
    webPharma7:string
    mailPharma7:string
    moisPharma7:string
    
    nomPharma8:string
    adressePharma8:string
    odPharma8:string
    
    villePharma8:string
    telPharma8:string
    webPharma8:string
    mailPharma8:string
    moisPharma8:string
    
    nomPharma9:string
    adressePharma9:string
    odPharma9:string
    
    villePharma9:string
    telPharma9:string
    webPharma9:string
    mailPharma9:string
    moisPharma9:string
    
    nomPharma10:string
    adressePharma10:string
    odPharma10:string
    
    villePharma10:string
    telPharma10:string
    webPharma10:string
    mailPharma10:string
    moisPharma10:string
    
    nomPharma11:string
    adressePharma11:string
    odPharma11:string
    
    villePharma11:string
    telPharma11:string
    webPharma11:string
    mailPharma11:string
    moisPharma11:string
    
    nomPharma12:string
    adressePharma12:string
    odPharma12:string
    
    villePharma12:string
    telPharma12:string
    webPharma12:string
    mailPharma12:string
    moisPharma12:string
    
      //otherDetails
    //conseil
    imgConseil1:string
    titreConseil1:string
        soustitreConseil1:string
        descriptionConseil1:string
    
       
    
        imgConseil2:string
        titreConseil2:string
        soustitreConseil2:string
        descriptionConseil2:string
    
        imgConseil3:string
        titreConseil3:string
        soustitreConseil3:string
        descriptionConseil3:string
    
        imgConseil4:string
        titreConseil4:string
        soustitreConseil4:string
        descriptionConseil4:string
    
        imgConseil5:string
        titreConseil5:string
        soustitreConseil5:string
        descriptionConseil5:string
    
        
    
        imgConseil6:string
        titreConseil6:string
        soustitreConseil6:string
        descriptionConseil6:string
    
        
    
        imgConseil7:string
        titreConseil7:string
        soustitreConseil7:string
        descriptionConseil7:string
        //publicite
    imgPub1:string
    imgPub2:string
    imgPub3:string
    imgPub4:string
    imgPub5:string
    imgPub6:string
    imgPub7:string
    imgPub8:string
    imgPub9:string
    imgPub10:string   
    imgPub11:string     
    imgPub12:string    
    //publicite
        imgConseil8:string
        titreConseil8:string
        soustitreConseil8:string
        descriptionConseil8:string
    
        imgConseil9:string
        titreConseil9:string
        soustitreConseil9:string
        descriptionConseil9:string
    
        imgConseil10:string
        titreConseil10:string
        soustitreConseil10:string
        descriptionConseil10:string
    
        imgConseil11:string
        titreConseil11:string
        soustitreConseil11:string
        descriptionConseil11:string
    
        imgConseil12:string
        titreConseil12:string
        soustitreConseil12:string
        descriptionConseil12:string
    //conseil
    
    //service
    titreService11:string
    soustitreService11:string
    descriptionService11:string
    imgService11:string
    
    titreService12:string
    soustitreService12:string
    descriptionService12:string
    imgService12:string
    
    titreService13:string
    soustitreService13:string
    descriptionService13:string
    imgService13:string
    
    titreService14:string
    soustitreService14:string
    descriptionService14:string
    imgService14:string
    
    titreService21:string
    soustitreService21:string
    descriptionService21:string
    imgService21:string
    
    titreService22:string
    soustitreService22:string
    descriptionService22:string
    imgService22:string
    
    titreService23:string
    soustitreService23:string
    descriptionService23:string
    imgService23:string
    
    titreService24:string
    soustitreService24:string
    descriptionService24:string
    imgService24:string
    
    titreService31:string
    soustitreService31:string
    descriptionService31:string
    imgService31:string
    
    titreService32:string
    soustitreService32:string
    descriptionService32:string
    imgService32:string
    
    titreService33:string
    soustitreService33:string
    descriptionService33:string
    imgService33:string
    
    titreService34:string
    soustitreService34:string
    descriptionService34:string
    imgService34:string
    
    titreService41:string
    soustitreService41:string
    descriptionService41:string
    imgService41:string
    
    titreService42:string
    soustitreService42:string
    descriptionService42:string
    imgService42:string
    
    titreService43:string
    soustitreService43:string
    descriptionService43:string
    imgService43:string
    
    titreService44:string
    soustitreService44:string
    descriptionService44:string
    imgService44:string
    
    
    titreService51:string
    soustitreService51:string
    descriptionService51:string
    imgService51:string
    
    titreService52:string
    soustitreService52:string
    descriptionService52:string
    imgService52:string
    
    titreService53:string
    soustitreService53:string
    descriptionService53:string
    imgService53:string
    
    titreService54:string
    soustitreService54:string
    descriptionService54:string
    imgService54:string
    
    
    titreService61:string
    soustitreService61:string
    descriptionService61:string
    imgService61:string
    
    titreService62:string
    soustitreService62:string
    descriptionService62:string
    imgService62:string
    
    titreService63:string
    soustitreService63:string
    descriptionService63:string
    imgService63:string
    
    titreService64:string
    soustitreService64:string
    descriptionService64:string
    imgService64:string
    
    titreService71:string
    soustitreService71:string
    descriptionService71:string
    imgService71:string
    
    titreService72:string
    soustitreService72:string
    descriptionService72:string
    imgService72:string
    
    titreService73:string
    soustitreService73:string
    descriptionService73:string
    imgService73:string
    
    titreService74:string
    soustitreService74:string
    descriptionService74:string
    imgService74:string
    
    titreService81:string
    soustitreService81:string
    descriptionService81:string
    imgService81:string
    
    titreService82:string
    soustitreService82:string
    descriptionService82:string
    imgService82:string
    
    titreService83:string
    soustitreService83:string
    descriptionService83:string
    imgService83:string
    
    titreService84:string
    soustitreService84:string
    descriptionService84:string
    imgService84:string
    
    titreService91:string
    soustitreService91:string
    descriptionService91:string
    imgService91:string
    
    titreService92:string
    soustitreService92:string
    descriptionService92:string
    imgService92:string
    
    titreService93:string
    soustitreService93:string
    descriptionService93:string
    imgService93:string
    
    titreService94:string
    soustitreService94:string
    descriptionService94:string
    imgService94:string
    
    titreService101:string
    soustitreService101:string
    descriptionService101:string
    imgService101:string
    
    titreService102:string
    soustitreService102:string
    descriptionService102:string
    imgService102:string
    
    titreService103:string
    soustitreService103:string
    descriptionService103:string
    imgService103:string
    
    titreService104:string
    soustitreService104:string
    descriptionService104:string
    imgService104:string
    
    titreService111:string
    soustitreService111:string
    descriptionService111:string
    imgService111:string
    
    titreService112:string
    soustitreService112:string
    descriptionService112:string
    imgService112:string
    
    titreService113:string
    soustitreService113:string
    descriptionService113:string
    imgService113:string
    
    titreService114:string
    soustitreService114:string
    descriptionService114:string
    imgService114:string
    
    titreService121:string
    soustitreService121:string
    descriptionService121:string
    imgService121:string
    
    titreService122:string
    soustitreService122:string
    descriptionService122:string
    imgService122:string
    
    titreService123:string
    soustitreService123:string
    descriptionService123:string
    imgService123:string
    
    titreService124:string
    soustitreService124:string
    descriptionService124:string
    imgService124:string
    //service
    
    //nouveautés
    
    titreNew11:string
    soustitreNew11:string
    descriptionNew11:string
    imgNew11:string
    
    titreNew12:string
    soustitreNew12:string
    descriptionNew12:string
    imgNew12:string
    
    titreNew13:string
    soustitreNew13:string
    descriptionNew13:string
    imgNew13:string
    
    titreNew14:string
    soustitreNew14:string
    descriptionNew14:string
    imgNew14:string
    
    titreNew21:string
    soustitreNew21:string
    descriptionNew21:string
    imgNew21:string
    
    titreNew22:string
    soustitreNew22:string
    descriptionNew22:string
    imgNew22:string
    
    titreNew23:string
    soustitreNew23:string
    descriptionNew23:string
    imgNew23:string
    
    titreNew24:string
    soustitreNew24:string
    descriptionNew24:string
    imgNew24:string
    
    titreNew31:string
    soustitreNew31:string
    descriptionNew31:string
    imgNew31:string
    
    titreNew32:string
    soustitreNew32:string
    descriptionNew32:string
    imgNew32:string
    
    titreNew33:string
    soustitreNew33:string
    descriptionNew33:string
    imgNew33:string
    
    titreNew34:string
    soustitreNew34:string
    descriptionNew34:string
    imgNew34:string
    
    titreNew41:string
    soustitreNew41:string
    descriptionNew41:string
    imgNew41:string
    
    titreNew42:string
    soustitreNew42:string
    descriptionNew42:string
    imgNew42:string
    
    titreNew43:string
    soustitreNew43:string
    descriptionNew43:string
    imgNew43:string
    
    titreNew44:string
    soustitreNew44:string
    descriptionNew44:string
    imgNew44:string
    
    titreNew51:string
    soustitreNew51:string
    descriptionNew51:string
    imgNew51:string
    
    titreNew52:string
    soustitreNew52:string
    descriptionNew52:string
    imgNew52:string
    
    titreNew53:string
    soustitreNew53:string
    descriptionNew53:string
    imgNew53:string
    
    titreNew54:string
    soustitreNew54:string
    descriptionNew54:string
    imgNew54:string
    
    titreNew61:string
    soustitreNew61:string
    descriptionNew61:string
    imgNew61:string
    
    titreNew62:string
    soustitreNew62:string
    descriptionNew62:string
    imgNew62:string
    
    titreNew63:string
    soustitreNew63:string
    descriptionNew63:string
    imgNew63:string
    
    titreNew64:string
    soustitreNew64:string
    descriptionNew64:string
    imgNew64:string
    
    titreNew71:string
    soustitreNew71:string
    descriptionNew71:string
    imgNew71:string
    
    titreNew72:string
    soustitreNew72:string
    descriptionNew72:string
    imgNew72:string
    
    titreNew73:string
    soustitreNew73:string
    descriptionNew73:string
    imgNew73:string
    
    titreNew74:string
    soustitreNew74:string
    descriptionNew74:string
    imgNew74:string
    
    
    titreNew81:string
    soustitreNew81:string
    descriptionNew81:string
    imgNew81:string
    
    titreNew82:string
    soustitreNew82:string
    descriptionNew82:string
    imgNew82:string
    
    titreNew83:string
    soustitreNew83:string
    descriptionNew83:string
    imgNew83:string
    
    titreNew84:string
    soustitreNew84:string
    descriptionNew84:string
    imgNew84:string
    
    titreNew91:string
    soustitreNew91:string
    descriptionNew91:string
    imgNew91:string
    
    titreNew92:string
    soustitreNew92:string
    descriptionNew92:string
    imgNew92:string
    
    titreNew93:string
    soustitreNew93:string
    descriptionNew93:string
    imgNew93:string
    
    titreNew94:string
    soustitreNew94:string
    descriptionNew94:string
    imgNew94:string
    
    titreNew101:string
    soustitreNew101:string
    descriptionNew101:string
    imgNew101:string
    
    titreNew102:string
    soustitreNew102:string
    descriptionNew102:string
    imgNew102:string
    
    titreNew103:string
    soustitreNew103:string
    descriptionNew103:string
    imgNew103:string
    
    titreNew104:string
    soustitreNew104:string
    descriptionNew104:string
    imgNew104:string
    
    titreNew111:string
    soustitreNew111:string
    descriptionNew111:string
    imgNew111:string
    
    titreNew112:string
    soustitreNew112:string
    descriptionNew112:string
    imgNew112:string
    
    titreNew113:string
    soustitreNew113:string
    descriptionNew113:string
    imgNew113:string
    
    titreNew114:string
    soustitreNew114:string
    descriptionNew114:string
    imgNew114:string
    
    titreNew121:string
    soustitreNew121:string
    descriptionNew121:string
    imgNew121:string
    
    titreNew122:string
    soustitreNew122:string
    descriptionNew122:string
    imgNew122:string
    
    titreNew123:string
    soustitreNew123:string
    descriptionNew123:string
    imgNew123:string
    
    titreNew124:string
    soustitreNew124:string
    descriptionNew124:string
    imgNew124:string
    //nouveautés
    
    
      //Sante
      prixSante11:string
    ancienPrixSante11:string
    nomSante11:string
    uniteSante11:string
    gammeSante11:string
    imgSante11:string
    
    prixSante12:string
    ancienPrixSante12:string
    nomSante12:string
    uniteSante12:string
    gammeSante12:string
    imgSante12:string
    
    prixSante13:string
    ancienPrixSante13:string
    nomSante13:string
    uniteSante13:string
    gammeSante13:string
    imgSante13:string
    
    prixSante14:string
    ancienPrixSante14:string
    nomSante14:string
    uniteSante14:string
    gammeSante14:string
    imgSante14:string
    
    prixSante15:string
    ancienPrixSante15:string
    nomSante15:string
    uniteSante15:string
    gammeSante15:string
    imgSante15:string
    
    prixSante16:string
    ancienPrixSante16:string
    nomSante16:string
    uniteSante16:string
    gammeSante16:string
    imgSante16:string
    
    
    prixSante21:string
    ancienPrixSante21:string
    nomSante21:string
    uniteSante21:string
    gammeSante21:string
    imgSante21:string
    
    prixSante22:string
    ancienPrixSante22:string
    nomSante22:string
    uniteSante22:string
    gammeSante22:string
    imgSante22:string
    
    prixSante23:string
    ancienPrixSante23:string
    nomSante23:string
    uniteSante23:string
    gammeSante23:string
    imgSante23:string
    
    prixSante24:string
    ancienPrixSante24:string
    nomSante24:string
    uniteSante24:string
    gammeSante24:string
    imgSante24:string
    
    prixSante25:string
    ancienPrixSante25:string
    nomSante25:string
    uniteSante25:string
    gammeSante25:string
    imgSante25:string
    
    prixSante26:string
    ancienPrixSante26:string
    nomSante26:string
    uniteSante26:string
    gammeSante26:string
    imgSante26:string
    
    prixSante31:string
    ancienPrixSante31:string
    nomSante31:string
    uniteSante31:string
    gammeSante31:string
    imgSante31:string
    
    prixSante32:string
    ancienPrixSante32:string
    nomSante32:string
    uniteSante32:string
    gammeSante32:string
    imgSante32:string
    
    prixSante33:string
    ancienPrixSante33:string
    nomSante33:string
    uniteSante33:string
    gammeSante33:string
    imgSante33:string
    
    prixSante34:string
    ancienPrixSante34:string
    nomSante34:string
    uniteSante34:string
    gammeSante34:string
    imgSante34:string
    
    prixSante35:string
    ancienPrixSante35:string
    nomSante35:string
    uniteSante35:string
    gammeSante35:string
    imgSante35:string
    
    prixSante36:string
    ancienPrixSante36:string
    nomSante36:string
    uniteSante36:string
    gammeSante36:string
    imgSante36:string
    
    prixSante41:string
    ancienPrixSante41:string
    nomSante41:string
    uniteSante41:string
    gammeSante41:string
    imgSante41:string
    
    prixSante42:string
    ancienPrixSante42:string
    nomSante42:string
    uniteSante42:string
    gammeSante42:string
    imgSante42:string
    
    prixSante43:string
    ancienPrixSante43:string
    nomSante43:string
    uniteSante43:string
    gammeSante43:string
    imgSante43:string
    
    prixSante44:string
    ancienPrixSante44:string
    nomSante44:string
    uniteSante44:string
    gammeSante44:string
    imgSante44:string
    
    prixSante45:string
    ancienPrixSante45:string
    nomSante45:string
    uniteSante45:string
    gammeSante45:string
    imgSante45:string
    
    prixSante46:string
    ancienPrixSante46:string
    nomSante46:string
    uniteSante46:string
    gammeSante46:string
    imgSante46:string
    
    prixSante51:string
    ancienPrixSante51:string
    nomSante51:string
    uniteSante51:string
    gammeSante51:string
    imgSante51:string
    
    prixSante52:string
    ancienPrixSante52:string
    nomSante52:string
    uniteSante52:string
    gammeSante52:string
    imgSante52:string
    
    prixSante53:string
    ancienPrixSante53:string
    nomSante53:string
    uniteSante53:string
    gammeSante53:string
    imgSante53:string
    
    prixSante54:string
    ancienPrixSante54:string
    nomSante54:string
    uniteSante54:string
    gammeSante54:string
    imgSante54:string
    
    prixSante55:string
    ancienPrixSante55:string
    nomSante55:string
    uniteSante55:string
    gammeSante55:string
    imgSante55:string
    
    prixSante56:string
    ancienPrixSante56:string
    nomSante56:string
    uniteSante56:string
    gammeSante56:string
    imgSante56:string
    
    prixSante61:string
    ancienPrixSante61:string
    nomSante61:string
    uniteSante61:string
    gammeSante61:string
    imgSante61:string
    
    prixSante62:string
    ancienPrixSante62:string
    nomSante62:string
    uniteSante62:string
    gammeSante62:string
    imgSante62:string
    
    prixSante63:string
    ancienPrixSante63:string
    nomSante63:string
    uniteSante63:string
    gammeSante63:string
    imgSante63:string
    
    prixSante64:string
    ancienPrixSante64:string
    nomSante64:string
    uniteSante64:string
    gammeSante64:string
    imgSante64:string
    
    prixSante65:string
    ancienPrixSante65:string
    nomSante65:string
    uniteSante65:string
    gammeSante65:string
    imgSante65:string
    
    prixSante66:string
    ancienPrixSante66:string
    nomSante66:string
    uniteSante66:string
    gammeSante66:string
    imgSante66:string
    
    prixSante71:string
    ancienPrixSante71:string
    nomSante71:string
    uniteSante71:string
    gammeSante71:string
    imgSante71:string
    
    prixSante72:string
    ancienPrixSante72:string
    nomSante72:string
    uniteSante72:string
    gammeSante72:string
    imgSante72:string
    
    prixSante73:string
    ancienPrixSante73:string
    nomSante73:string
    uniteSante73:string
    gammeSante73:string
    imgSante73:string
    
    prixSante74:string
    ancienPrixSante74:string
    nomSante74:string
    uniteSante74:string
    gammeSante74:string
    imgSante74:string
    
    prixSante75:string
    ancienPrixSante75:string
    nomSante75:string
    uniteSante75:string
    gammeSante75:string
    imgSante75:string
    
    prixSante76:string
    ancienPrixSante76:string
    nomSante76:string
    uniteSante76:string
    gammeSante76:string
    imgSante76:string
    
    prixSante81:string
    ancienPrixSante81:string
    nomSante81:string
    uniteSante81:string
    gammeSante81:string
    imgSante81:string
    
    prixSante82:string
    ancienPrixSante82:string
    nomSante82:string
    uniteSante82:string
    gammeSante82:string
    imgSante82:string
    
    prixSante83:string
    ancienPrixSante83:string
    nomSante83:string
    uniteSante83:string
    gammeSante83:string
    imgSante83:string
    
    prixSante84:string
    ancienPrixSante84:string
    nomSante84:string
    uniteSante84:string
    gammeSante84:string
    imgSante84:string
    
    prixSante85:string
    ancienPrixSante85:string
    nomSante85:string
    uniteSante85:string
    gammeSante85:string
    imgSante85:string
    
    prixSante86:string
    ancienPrixSante86:string
    nomSante86:string
    uniteSante86:string
    gammeSante86:string
    imgSante86:string
    
    prixSante91:string
    ancienPrixSante91:string
    nomSante91:string
    uniteSante91:string
    gammeSante91:string
    imgSante91:string
    
    prixSante92:string
    ancienPrixSante92:string
    nomSante92:string
    uniteSante92:string
    gammeSante92:string
    imgSante92:string
    
    prixSante93:string
    ancienPrixSante93:string
    nomSante93:string
    uniteSante93:string
    gammeSante93:string
    imgSante93:string
    
    prixSante94:string
    ancienPrixSante94:string
    nomSante94:string
    uniteSante94:string
    gammeSante94:string
    imgSante94:string
    
    prixSante95:string
    ancienPrixSante95:string
    nomSante95:string
    uniteSante95:string
    gammeSante95:string
    imgSante95:string
    
    prixSante96:string
    ancienPrixSante96:string
    nomSante96:string
    uniteSante96:string
    gammeSante96:string
    imgSante96:string
    
    prixSante101:string
    ancienPrixSante101:string
    nomSante101:string
    uniteSante101:string
    gammeSante101:string
    imgSante101:string
    
    prixSante102:string
    ancienPrixSante102:string
    nomSante102:string
    uniteSante102:string
    gammeSante102:string
    imgSante102:string
    
    prixSante103:string
    ancienPrixSante103:string
    nomSante103:string
    uniteSante103:string
    gammeSante103:string
    imgSante103:string
    
    prixSante104:string
    ancienPrixSante104:string
    nomSante104:string
    uniteSante104:string
    gammeSante104:string
    imgSante104:string
    
    prixSante105:string
    ancienPrixSante105:string
    nomSante105:string
    uniteSante105:string
    gammeSante105:string
    imgSante105:string
    
    prixSante106:string
    ancienPrixSante106:string
    nomSante106:string
    uniteSante106:string
    gammeSante106:string
    imgSante106:string
    
    prixSante111:string
    ancienPrixSante111:string
    nomSante111:string
    uniteSante111:string
    gammeSante111:string
    imgSante111:string
    
    prixSante112:string
    ancienPrixSante112:string
    nomSante112:string
    uniteSante112:string
    gammeSante112:string
    imgSante112:string
    
    prixSante113:string
    ancienPrixSante113:string
    nomSante113:string
    uniteSante113:string
    gammeSante113:string
    imgSante113:string
    
    prixSante114:string
    ancienPrixSante114:string
    nomSante114:string
    uniteSante114:string
    gammeSante114:string
    imgSante114:string
    
    prixSante115:string
    ancienPrixSante115:string
    nomSante115:string
    uniteSante115:string
    gammeSante115:string
    imgSante115:string
    
    prixSante116:string
    ancienPrixSante116:string
    nomSante116:string
    uniteSante116:string
    gammeSante116:string
    imgSante116:string
    
    prixSante121:string
    ancienPrixSante121:string
    nomSante121:string
    uniteSante121:string
    gammeSante121:string
    imgSante121:string
    
    prixSante122:string
    ancienPrixSante122:string
    nomSante122:string
    uniteSante122:string
    gammeSante122:string
    imgSante122:string
    
    prixSante123:string
    ancienPrixSante123:string
    nomSante123:string
    uniteSante123:string
    gammeSante123:string
    imgSante123:string
    
    prixSante124:string
    ancienPrixSante124:string
    nomSante124:string
    uniteSante124:string
    gammeSante124:string
    imgSante124:string
    
    prixSante125:string
    ancienPrixSante125:string
    nomSante125:string
    uniteSante125:string
    gammeSante125:string
    imgSante125:string
    
    prixSante126:string
    ancienPrixSante126:string
    nomSante126:string
    uniteSante126:string
    gammeSante126:string
    imgSante126:string
    //Sante
    
    //selectionAlaUne
    
    prixProduit11:string
    ancienPrixProduit11:string
    nomProduit11:string
    uniteProduit11:string
    gammeProduit11:string
    imgProduit11:string
    
    prixProduit12:string
    ancienPrixProduit12:string
    nomProduit12:string
    uniteProduit12:string
    gammeProduit12:string
    imgProduit12:string
    
    prixProduit13:string
    ancienPrixProduit13:string
    nomProduit13:string
    uniteProduit13:string
    gammeProduit13:string
    imgProduit13:string
    
    prixProduit14:string
    ancienPrixProduit14:string
    nomProduit14:string
    uniteProduit14:string
    gammeProduit14:string
    imgProduit14:string
    
    prixProduit21:string
    ancienPrixProduit21:string
    nomProduit21:string
    uniteProduit21:string
    gammeProduit21:string
    imgProduit21:string
    
    prixProduit22:string
    ancienPrixProduit22:string
    nomProduit22:string
    uniteProduit22:string
    gammeProduit22:string
    imgProduit22:string
    
    prixProduit23:string
    ancienPrixProduit23:string
    nomProduit23:string
    uniteProduit23:string
    gammeProduit23:string
    imgProduit23:string
    
    prixProduit24:string
    ancienPrixProduit24:string
    nomProduit24:string
    uniteProduit24:string
    gammeProduit24:string
    imgProduit24:string
    
    prixProduit31:string
    ancienPrixProduit31:string
    nomProduit31:string
    uniteProduit31:string
    gammeProduit31:string
    imgProduit31:string
    
    prixProduit32:string
    ancienPrixProduit32:string
    nomProduit32:string
    uniteProduit32:string
    gammeProduit32:string
    imgProduit32:string
    
    prixProduit33:string
    ancienPrixProduit33:string
    nomProduit33:string
    uniteProduit33:string
    gammeProduit33:string
    imgProduit33:string
    
    prixProduit34:string
    ancienPrixProduit34:string
    nomProduit34:string
    uniteProduit34:string
    gammeProduit34:string
    imgProduit34:string
    
    prixProduit41:string
    ancienPrixProduit41:string
    nomProduit41:string
    uniteProduit41:string
    gammeProduit41:string
    imgProduit41:string
    
    prixProduit42:string
    ancienPrixProduit42:string
    nomProduit42:string
    uniteProduit42:string
    gammeProduit42:string
    imgProduit42:string
    
    prixProduit43:string
    ancienPrixProduit43:string
    nomProduit43:string
    uniteProduit43:string
    gammeProduit43:string
    imgProduit43:string
    
    prixProduit44:string
    ancienPrixProduit44:string
    nomProduit44:string
    uniteProduit44:string
    gammeProduit44:string
    imgProduit44:string
    
    prixProduit51:string
    ancienPrixProduit51:string
    nomProduit51:string
    uniteProduit51:string
    gammeProduit51:string
    imgProduit51:string
    
    prixProduit52:string
    ancienPrixProduit52:string
    nomProduit52:string
    uniteProduit52:string
    gammeProduit52:string
    imgProduit52:string
    
    prixProduit53:string
    ancienPrixProduit53:string
    nomProduit53:string
    uniteProduit53:string
    gammeProduit53:string
    imgProduit53:string
    
    prixProduit54:string
    ancienPrixProduit54:string
    nomProduit54:string
    uniteProduit54:string
    gammeProduit54:string
    imgProduit54:string
    
    prixProduit61:string
    ancienPrixProduit61:string
    nomProduit61:string
    uniteProduit61:string
    gammeProduit61:string
    imgProduit61:string
    
    prixProduit62:string
    ancienPrixProduit62:string
    nomProduit62:string
    uniteProduit62:string
    gammeProduit62:string
    imgProduit62:string
    
    prixProduit63:string
    ancienPrixProduit63:string
    nomProduit63:string
    uniteProduit63:string
    gammeProduit63:string
    imgProduit63:string
    
    prixProduit64:string
    ancienPrixProduit64:string
    nomProduit64:string
    uniteProduit64:string
    gammeProduit64:string
    imgProduit64:string
    
    prixProduit71:string
    ancienPrixProduit71:string
    nomProduit71:string
    uniteProduit71:string
    gammeProduit71:string
    imgProduit71:string
    
    prixProduit72:string
    ancienPrixProduit72:string
    nomProduit72:string
    uniteProduit72:string
    gammeProduit72:string
    imgProduit72:string
    
    prixProduit73:string
    ancienPrixProduit73:string
    nomProduit73:string
    uniteProduit73:string
    gammeProduit73:string
    imgProduit73:string
    
    prixProduit74:string
    ancienPrixProduit74:string
    nomProduit74:string
    uniteProduit74:string
    gammeProduit74:string
    imgProduit74:string
    
    prixProduit81:string
    ancienPrixProduit81:string
    nomProduit81:string
    uniteProduit81:string
    gammeProduit81:string
    imgProduit81:string
    
    prixProduit82:string
    ancienPrixProduit82:string
    nomProduit82:string
    uniteProduit82:string
    gammeProduit82:string
    imgProduit82:string
    
    prixProduit83:string
    ancienPrixProduit83:string
    nomProduit83:string
    uniteProduit83:string
    gammeProduit83:string
    imgProduit83:string
    
    prixProduit84:string
    ancienPrixProduit84:string
    nomProduit84:string
    uniteProduit84:string
    gammeProduit84:string
    imgProduit84:string
    
    prixProduit91:string
    ancienPrixProduit91:string
    nomProduit91:string
    uniteProduit91:string
    gammeProduit91:string
    imgProduit91:string
    
    prixProduit92:string
    ancienPrixProduit92:string
    nomProduit92:string
    uniteProduit92:string
    gammeProduit92:string
    imgProduit92:string
    
    prixProduit93:string
    ancienPrixProduit93:string
    nomProduit93:string
    uniteProduit93:string
    gammeProduit93:string
    imgProduit93:string
    
    prixProduit94:string
    ancienPrixProduit94:string
    nomProduit94:string
    uniteProduit94:string
    gammeProduit94:string
    imgProduit94:string
    
    prixProduit101:string
    ancienPrixProduit101:string
    nomProduit101:string
    uniteProduit101:string
    gammeProduit101:string
    imgProduit101:string
    
    prixProduit102:string
    ancienPrixProduit102:string
    nomProduit102:string
    uniteProduit102:string
    gammeProduit102:string
    imgProduit102:string
    
    prixProduit103:string
    ancienPrixProduit103:string
    nomProduit103:string
    uniteProduit103:string
    gammeProduit103:string
    imgProduit103:string
    
    prixProduit104:string
    ancienPrixProduit104:string
    nomProduit104:string
    uniteProduit104:string
    gammeProduit104:string
    imgProduit104:string
    
    prixProduit111:string
    ancienPrixProduit111:string
    nomProduit111:string
    uniteProduit111:string
    gammeProduit111:string
    imgProduit111:string
    
    prixProduit112:string
    ancienPrixProduit112:string
    nomProduit112:string
    uniteProduit112:string
    gammeProduit112:string
    imgProduit112:string
    
    prixProduit113:string
    ancienPrixProduit113:string
    nomProduit113:string
    uniteProduit113:string
    gammeProduit113:string
    imgProduit113:string
    
    prixProduit114:string
    ancienPrixProduit114:string
    nomProduit114:string
    uniteProduit114:string
    gammeProduit114:string
    imgProduit114:string
    
    
    prixProduit121:string
    ancienPrixProduit121:string
    nomProduit121:string
    uniteProduit121:string
    gammeProduit121:string
    imgProduit121:string
    
    
    prixProduit122:string
    ancienPrixProduit122:string
    nomProduit122:string
    uniteProduit122:string
    gammeProduit122:string
    imgProduit122:string
    
    
    prixProduit123:string
    ancienPrixProduit123:string
    nomProduit123:string
    uniteProduit123:string
    gammeProduit123:string
    imgProduit123:string
    
    
    prixProduit124:string
    ancienPrixProduit124:string
    nomProduit124:string
    uniteProduit124:string
    gammeProduit124:string
    imgProduit124:string
    //selectionAlaUne1
    
    //Beaute
    prixBeaute11:string
    ancienPrixBeaute11:string
    nomBeaute11:string
    uniteBeaute11:string
    gammeBeaute11:string
    imgBeaute11:string
    
    prixBeaute12:string
    ancienPrixBeaute12:string
    nomBeaute12:string
    uniteBeaute12:string
    gammeBeaute12:string
    imgBeaute12:string
    
    prixBeaute13:string
    ancienPrixBeaute13:string
    nomBeaute13:string
    uniteBeaute13:string
    gammeBeaute13:string
    imgBeaute13:string
    
    prixBeaute14:string
    ancienPrixBeaute14:string
    nomBeaute14:string
    uniteBeaute14:string
    gammeBeaute14:string
    imgBeaute14:string
    
    prixBeaute15:string
    ancienPrixBeaute15:string
    nomBeaute15:string
    uniteBeaute15:string
    gammeBeaute15:string
    imgBeaute15:string
    
    prixBeaute16:string
    ancienPrixBeaute16:string
    nomBeaute16:string
    uniteBeaute16:string
    gammeBeaute16:string
    imgBeaute16:string
    
    prixBeaute21:string
    ancienPrixBeaute21:string
    nomBeaute21:string
    uniteBeaute21:string
    gammeBeaute21:string
    imgBeaute21:string
    
    prixBeaute22:string
    ancienPrixBeaute22:string
    nomBeaute22:string
    uniteBeaute22:string
    gammeBeaute22:string
    imgBeaute22:string
    
    prixBeaute23:string
    ancienPrixBeaute23:string
    nomBeaute23:string
    uniteBeaute23:string
    gammeBeaute23:string
    imgBeaute23:string
    
    prixBeaute24:string
    ancienPrixBeaute24:string
    nomBeaute24:string
    uniteBeaute24:string
    gammeBeaute24:string
    imgBeaute24:string
    
    prixBeaute25:string
    ancienPrixBeaute25:string
    nomBeaute25:string
    uniteBeaute25:string
    gammeBeaute25:string
    imgBeaute25:string
    
    prixBeaute26:string
    ancienPrixBeaute26:string
    nomBeaute26:string
    uniteBeaute26:string
    gammeBeaute26:string
    imgBeaute26:string
    
    prixBeaute31:string
    ancienPrixBeaute31:string
    nomBeaute31:string
    uniteBeaute31:string
    gammeBeaute31:string
    imgBeaute31:string
    
    prixBeaute32:string
    ancienPrixBeaute32:string
    nomBeaute32:string
    uniteBeaute32:string
    gammeBeaute32:string
    imgBeaute32:string
    
    prixBeaute33:string
    ancienPrixBeaute33:string
    nomBeaute33:string
    uniteBeaute33:string
    gammeBeaute33:string
    imgBeaute33:string
    
    prixBeaute34:string
    ancienPrixBeaute34:string
    nomBeaute34:string
    uniteBeaute34:string
    gammeBeaute34:string
    imgBeaute34:string
    
    prixBeaute35:string
    ancienPrixBeaute35:string
    nomBeaute35:string
    uniteBeaute35:string
    gammeBeaute35:string
    imgBeaute35:string
    
    prixBeaute36:string
    ancienPrixBeaute36:string
    nomBeaute36:string
    uniteBeaute36:string
    gammeBeaute36:string
    imgBeaute36:string
    
    prixBeaute41:string
    ancienPrixBeaute41:string
    nomBeaute41:string
    uniteBeaute41:string
    gammeBeaute41:string
    imgBeaute41:string
    
    prixBeaute42:string
    ancienPrixBeaute42:string
    nomBeaute42:string
    uniteBeaute42:string
    gammeBeaute42:string
    imgBeaute42:string
    
    prixBeaute43:string
    ancienPrixBeaute43:string
    nomBeaute43:string
    uniteBeaute43:string
    gammeBeaute43:string
    imgBeaute43:string
    
    prixBeaute44:string
    ancienPrixBeaute44:string
    nomBeaute44:string
    uniteBeaute44:string
    gammeBeaute44:string
    imgBeaute44:string
    
    prixBeaute45:string
    ancienPrixBeaute45:string
    nomBeaute45:string
    uniteBeaute45:string
    gammeBeaute45:string
    imgBeaute45:string
    
    prixBeaute46:string
    ancienPrixBeaute46:string
    nomBeaute46:string
    uniteBeaute46:string
    gammeBeaute46:string
    imgBeaute46:string
    
    prixBeaute51:string
    ancienPrixBeaute51:string
    nomBeaute51:string
    uniteBeaute51:string
    gammeBeaute51:string
    imgBeaute51:string
    
    prixBeaute52:string
    ancienPrixBeaute52:string
    nomBeaute52:string
    uniteBeaute52:string
    gammeBeaute52:string
    imgBeaute52:string
    
    prixBeaute53:string
    ancienPrixBeaute53:string
    nomBeaute53:string
    uniteBeaute53:string
    gammeBeaute53:string
    imgBeaute53:string
    
    prixBeaute54:string
    ancienPrixBeaute54:string
    nomBeaute54:string
    uniteBeaute54:string
    gammeBeaute54:string
    imgBeaute54:string
    
    prixBeaute55:string
    ancienPrixBeaute55:string
    nomBeaute55:string
    uniteBeaute55:string
    gammeBeaute55:string
    imgBeaute55:string
    
    prixBeaute56:string
    ancienPrixBeaute56:string
    nomBeaute56:string
    uniteBeaute56:string
    gammeBeaute56:string
    imgBeaute56:string
    
    prixBeaute61:string
    ancienPrixBeaute61:string
    nomBeaute61:string
    uniteBeaute61:string
    gammeBeaute61:string
    imgBeaute61:string
    
    prixBeaute62:string
    ancienPrixBeaute62:string
    nomBeaute62:string
    uniteBeaute62:string
    gammeBeaute62:string
    imgBeaute62:string
    
    prixBeaute63:string
    ancienPrixBeaute63:string
    nomBeaute63:string
    uniteBeaute63:string
    gammeBeaute63:string
    imgBeaute63:string
    
    prixBeaute64:string
    ancienPrixBeaute64:string
    nomBeaute64:string
    uniteBeaute64:string
    gammeBeaute64:string
    imgBeaute64:string
    
    
    prixBeaute65:string
    ancienPrixBeaute65:string
    nomBeaute65:string
    uniteBeaute65:string
    gammeBeaute65:string
    imgBeaute65:string
    
    prixBeaute66:string
    ancienPrixBeaute66:string
    nomBeaute66:string
    uniteBeaute66:string
    gammeBeaute66:string
    imgBeaute66:string
    
    prixBeaute71:string
    ancienPrixBeaute71:string
    nomBeaute71:string
    uniteBeaute71:string
    gammeBeaute71:string
    imgBeaute71:string
    
    prixBeaute72:string
    ancienPrixBeaute72:string
    nomBeaute72:string
    uniteBeaute72:string
    gammeBeaute72:string
    imgBeaute72:string
    
    prixBeaute73:string
    ancienPrixBeaute73:string
    nomBeaute73:string
    uniteBeaute73:string
    gammeBeaute73:string
    imgBeaute73:string
    
    prixBeaute74:string
    ancienPrixBeaute74:string
    nomBeaute74:string
    uniteBeaute74:string
    gammeBeaute74:string
    imgBeaute74:string
    
    prixBeaute75:string
    ancienPrixBeaute75:string
    nomBeaute75:string
    uniteBeaute75:string
    gammeBeaute75:string
    imgBeaute75:string
    
    prixBeaute76:string
    ancienPrixBeaute76:string
    nomBeaute76:string
    uniteBeaute76:string
    gammeBeaute76:string
    imgBeaute76:string
    
    prixBeaute81:string
    ancienPrixBeaute81:string
    nomBeaute81:string
    uniteBeaute81:string
    gammeBeaute81:string
    imgBeaute81:string
    
    prixBeaute82:string
    ancienPrixBeaute82:string
    nomBeaute82:string
    uniteBeaute82:string
    gammeBeaute82:string
    imgBeaute82:string
    
    prixBeaute83:string
    ancienPrixBeaute83:string
    nomBeaute83:string
    uniteBeaute83:string
    gammeBeaute83:string
    imgBeaute83:string
    
    prixBeaute84:string
    ancienPrixBeaute84:string
    nomBeaute84:string
    uniteBeaute84:string
    gammeBeaute84:string
    imgBeaute84:string
    
    prixBeaute85:string
    ancienPrixBeaute85:string
    nomBeaute85:string
    uniteBeaute85:string
    gammeBeaute85:string
    imgBeaute85:string
    
    prixBeaute86:string
    ancienPrixBeaute86:string
    nomBeaute86:string
    uniteBeaute86:string
    gammeBeaute86:string
    imgBeaute86:string
    
    prixBeaute91:string
    ancienPrixBeaute91:string
    nomBeaute91:string
    uniteBeaute91:string
    gammeBeaute91:string
    imgBeaute91:string
    
    prixBeaute92:string
    ancienPrixBeaute92:string
    nomBeaute92:string
    uniteBeaute92:string
    gammeBeaute92:string
    imgBeaute92:string
    
    prixBeaute93:string
    ancienPrixBeaute93:string
    nomBeaute93:string
    uniteBeaute93:string
    gammeBeaute93:string
    imgBeaute93:string
    
    prixBeaute94:string
    ancienPrixBeaute94:string
    nomBeaute94:string
    uniteBeaute94:string
    gammeBeaute94:string
    imgBeaute94:string
    
    prixBeaute95:string
    ancienPrixBeaute95:string
    nomBeaute95:string
    uniteBeaute95:string
    gammeBeaute95:string
    imgBeaute95:string
    
    prixBeaute96:string
    ancienPrixBeaute96:string
    nomBeaute96:string
    uniteBeaute96:string
    gammeBeaute96:string
    imgBeaute96:string
    
    prixBeaute101:string
    ancienPrixBeaute101:string
    nomBeaute101:string
    uniteBeaute101:string
    gammeBeaute101:string
    imgBeaute101:string
    
    prixBeaute102:string
    ancienPrixBeaute102:string
    nomBeaute102:string
    uniteBeaute102:string
    gammeBeaute102:string
    imgBeaute102:string
    
    prixBeaute103:string
    ancienPrixBeaute103:string
    nomBeaute103:string
    uniteBeaute103:string
    gammeBeaute103:string
    imgBeaute103:string
    
    prixBeaute104:string
    ancienPrixBeaute104:string
    nomBeaute104:string
    uniteBeaute104:string
    gammeBeaute104:string
    imgBeaute104:string
    
    prixBeaute105:string
    ancienPrixBeaute105:string
    nomBeaute105:string
    uniteBeaute105:string
    gammeBeaute105:string
    imgBeaute105:string
    
    
    prixBeaute106:string
    ancienPrixBeaute106:string
    nomBeaute106:string
    uniteBeaute106:string
    gammeBeaute106:string
    imgBeaute106:string
    
    prixBeaute111:string
    ancienPrixBeaute111:string
    nomBeaute111:string
    uniteBeaute111:string
    gammeBeaute111:string
    imgBeaute111:string
    
    
    prixBeaute112:string
    ancienPrixBeaute112:string
    nomBeaute112:string
    uniteBeaute112:string
    gammeBeaute112:string
    imgBeaute112:string
    
    
    prixBeaute113:string
    ancienPrixBeaute113:string
    nomBeaute113:string
    uniteBeaute113:string
    gammeBeaute113:string
    imgBeaute113:string
    
    
    prixBeaute114:string
    ancienPrixBeaute114:string
    nomBeaute114:string
    uniteBeaute114:string
    gammeBeaute114:string
    imgBeaute114:string
    
    prixBeaute115:string
    ancienPrixBeaute115:string
    nomBeaute115:string
    uniteBeaute115:string
    gammeBeaute115:string
    imgBeaute115:string
    
    
    prixBeaute116:string
    ancienPrixBeaute116:string
    nomBeaute116:string
    uniteBeaute116:string
    gammeBeaute116:string
    imgBeaute116:string
    
    prixBeaute121:string
    ancienPrixBeaute121:string
    nomBeaute121:string
    uniteBeaute121:string
    gammeBeaute121:string
    imgBeaute121:string
    
    prixBeaute122:string
    ancienPrixBeaute122:string
    nomBeaute122:string
    uniteBeaute122:string
    gammeBeaute122:string
    imgBeaute122:string
    
    prixBeaute123:string
    ancienPrixBeaute123:string
    nomBeaute123:string
    uniteBeaute123:string
    gammeBeaute123:string
    imgBeaute123:string
    
    prixBeaute124:string
    ancienPrixBeaute124:string
    nomBeaute124:string
    uniteBeaute124:string
    gammeBeaute124:string
    imgBeaute124:string
    
    prixBeaute125:string
    ancienPrixBeaute125:string
    nomBeaute125:string
    uniteBeaute125:string
    gammeBeaute125:string
    imgBeaute125:string
    
    prixBeaute126:string
    ancienPrixBeaute126:string
    nomBeaute126:string
    uniteBeaute126:string
    gammeBeaute126:string
    imgBeaute126:string
    
    
    
    
    //Beaute
    
    //bebe
    prixBebe11:string
    ancienPrixBebe11:string
    nomBebe11:string
    uniteBebe11:string
    gammeBebe11:string
    imgBebe11:string
    
    prixBebe12:string
    ancienPrixBebe12:string
    nomBebe12:string
    uniteBebe12:string
    gammeBebe12:string
    imgBebe12:string
    
    prixBebe13:string
    ancienPrixBebe13:string
    nomBebe13:string
    uniteBebe13:string
    gammeBebe13:string
    imgBebe13:string
    
    prixBebe14:string
    ancienPrixBebe14:string
    nomBebe14:string
    uniteBebe14:string
    gammeBebe14:string
    imgBebe14:string
    
    prixBebe15:string
    ancienPrixBebe15:string
    nomBebe15:string
    uniteBebe15:string
    gammeBebe15:string
    imgBebe15:string
    
    prixBebe16:string
    ancienPrixBebe16:string
    nomBebe16:string
    uniteBebe16:string
    gammeBebe16:string
    imgBebe16:string
    
    prixBebe21:string
    ancienPrixBebe21:string
    nomBebe21:string
    uniteBebe21:string
    gammeBebe21:string
    imgBebe21:string
    
    prixBebe22:string
    ancienPrixBebe22:string
    nomBebe22:string
    uniteBebe22:string
    gammeBebe22:string
    imgBebe22:string
    
    prixBebe23:string
    ancienPrixBebe23:string
    nomBebe23:string
    uniteBebe23:string
    gammeBebe23:string
    imgBebe23:string
    
    prixBebe24:string
    ancienPrixBebe24:string
    nomBebe24:string
    uniteBebe24:string
    gammeBebe24:string
    imgBebe24:string
    
    prixBebe25:string
    ancienPrixBebe25:string
    nomBebe25:string
    uniteBebe25:string
    gammeBebe25:string
    imgBebe25:string
    
    prixBebe26:string
    ancienPrixBebe26:string
    nomBebe26:string
    uniteBebe26:string
    gammeBebe26:string
    imgBebe26:string
    
    prixBebe31:string
    ancienPrixBebe31:string
    nomBebe31:string
    uniteBebe31:string
    gammeBebe31:string
    imgBebe31:string
    
    prixBebe32:string
    ancienPrixBebe32:string
    nomBebe32:string
    uniteBebe32:string
    gammeBebe32:string
    imgBebe32:string
    
    prixBebe33:string
    ancienPrixBebe33:string
    nomBebe33:string
    uniteBebe33:string
    gammeBebe33:string
    imgBebe33:string
    
    prixBebe34:string
    ancienPrixBebe34:string
    nomBebe34:string
    uniteBebe34:string
    gammeBebe34:string
    imgBebe34:string
    
    prixBebe35:string
    ancienPrixBebe35:string
    nomBebe35:string
    uniteBebe35:string
    gammeBebe35:string
    imgBebe35:string
    
    prixBebe36:string
    ancienPrixBebe36:string
    nomBebe36:string
    uniteBebe36:string
    gammeBebe36:string
    imgBebe36:string
    
    prixBebe41:string
    ancienPrixBebe41:string
    nomBebe41:string
    uniteBebe41:string
    gammeBebe41:string
    imgBebe41:string
    
    prixBebe42:string
    ancienPrixBebe42:string
    nomBebe42:string
    uniteBebe42:string
    gammeBebe42:string
    imgBebe42:string
    
    prixBebe43:string
    ancienPrixBebe43:string
    nomBebe43:string
    uniteBebe43:string
    gammeBebe43:string
    imgBebe43:string
    
    prixBebe44:string
    ancienPrixBebe44:string
    nomBebe44:string
    uniteBebe44:string
    gammeBebe44:string
    imgBebe44:string
    
    prixBebe45:string
    ancienPrixBebe45:string
    nomBebe45:string
    uniteBebe45:string
    gammeBebe45:string
    imgBebe45:string
    
    prixBebe46:string
    ancienPrixBebe46:string
    nomBebe46:string
    uniteBebe46:string
    gammeBebe46:string
    imgBebe46:string
    
    prixBebe51:string
    ancienPrixBebe51:string
    nomBebe51:string
    uniteBebe51:string
    gammeBebe51:string
    imgBebe51:string
    
    prixBebe52:string
    ancienPrixBebe52:string
    nomBebe52:string
    uniteBebe52:string
    gammeBebe52:string
    imgBebe52:string
    
    prixBebe53:string
    ancienPrixBebe53:string
    nomBebe53:string
    uniteBebe53:string
    gammeBebe53:string
    imgBebe53:string
    
    prixBebe54:string
    ancienPrixBebe54:string
    nomBebe54:string
    uniteBebe54:string
    gammeBebe54:string
    imgBebe54:string
    
    prixBebe55:string
    ancienPrixBebe55:string
    nomBebe55:string
    uniteBebe55:string
    gammeBebe55:string
    imgBebe55:string
    
    prixBebe56:string
    ancienPrixBebe56:string
    nomBebe56:string
    uniteBebe56:string
    gammeBebe56:string
    imgBebe56:string
    
    prixBebe61:string
    ancienPrixBebe61:string
    nomBebe61:string
    uniteBebe61:string
    gammeBebe61:string
    imgBebe61:string
    
    prixBebe62:string
    ancienPrixBebe62:string
    nomBebe62:string
    uniteBebe62:string
    gammeBebe62:string
    imgBebe62:string
    
    prixBebe63:string
    ancienPrixBebe63:string
    nomBebe63:string
    uniteBebe63:string
    gammeBebe63:string
    imgBebe63:string
    
    prixBebe64:string
    ancienPrixBebe64:string
    nomBebe64:string
    uniteBebe64:string
    gammeBebe64:string
    imgBebe64:string
    
    prixBebe65:string
    ancienPrixBebe65:string
    nomBebe65:string
    uniteBebe65:string
    gammeBebe65:string
    imgBebe65:string
    
    prixBebe66:string
    ancienPrixBebe66:string
    nomBebe66:string
    uniteBebe66:string
    gammeBebe66:string
    imgBebe66:string
    
    prixBebe71:string
    ancienPrixBebe71:string
    nomBebe71:string
    uniteBebe71:string
    gammeBebe71:string
    imgBebe71:string
    
    prixBebe72:string
    ancienPrixBebe72:string
    nomBebe72:string
    uniteBebe72:string
    gammeBebe72:string
    imgBebe72:string
    
    prixBebe73:string
    ancienPrixBebe73:string
    nomBebe73:string
    uniteBebe73:string
    gammeBebe73:string
    imgBebe73:string
    
    prixBebe74:string
    ancienPrixBebe74:string
    nomBebe74:string
    uniteBebe74:string
    gammeBebe74:string
    imgBebe74:string
    
    prixBebe75:string
    ancienPrixBebe75:string
    nomBebe75:string
    uniteBebe75:string
    gammeBebe75:string
    imgBebe75:string
    
    prixBebe76:string
    ancienPrixBebe76:string
    nomBebe76:string
    uniteBebe76:string
    gammeBebe76:string
    imgBebe76:string
    
    
    prixBebe81:string
    ancienPrixBebe81:string
    nomBebe81:string
    uniteBebe81:string
    gammeBebe81:string
    imgBebe81:string
    
    
    prixBebe82:string
    ancienPrixBebe82:string
    nomBebe82:string
    uniteBebe82:string
    gammeBebe82:string
    imgBebe82:string
    
    
    prixBebe83:string
    ancienPrixBebe83:string
    nomBebe83:string
    uniteBebe83:string
    gammeBebe83:string
    imgBebe83:string
    
    
    prixBebe84:string
    ancienPrixBebe84:string
    nomBebe84:string
    uniteBebe84:string
    gammeBebe84:string
    imgBebe84:string
    
    
    prixBebe85:string
    ancienPrixBebe85:string
    nomBebe85:string
    uniteBebe85:string
    gammeBebe85:string
    imgBebe85:string
    
    
    prixBebe86:string
    ancienPrixBebe86:string
    nomBebe86:string
    uniteBebe86:string
    gammeBebe86:string
    imgBebe86:string
    
    prixBebe91:string
    ancienPrixBebe91:string
    nomBebe91:string
    uniteBebe91:string
    gammeBebe91:string
    imgBebe91:string
    
    prixBebe92:string
    ancienPrixBebe92:string
    nomBebe92:string
    uniteBebe92:string
    gammeBebe92:string
    imgBebe92:string
    
    prixBebe93:string
    ancienPrixBebe93:string
    nomBebe93:string
    uniteBebe93:string
    gammeBebe93:string
    imgBebe93:string
    
    prixBebe94:string
    ancienPrixBebe94:string
    nomBebe94:string
    uniteBebe94:string
    gammeBebe94:string
    imgBebe94:string
    
    prixBebe95:string
    ancienPrixBebe95:string
    nomBebe95:string
    uniteBebe95:string
    gammeBebe95:string
    imgBebe95:string
    
    prixBebe96:string
    ancienPrixBebe96:string
    nomBebe96:string
    uniteBebe96:string
    gammeBebe96:string
    imgBebe96:string
    
    prixBebe101:string
    ancienPrixBebe101:string
    nomBebe101:string
    uniteBebe101:string
    gammeBebe101:string
    imgBebe101:string
    
    prixBebe102:string
    ancienPrixBebe102:string
    nomBebe102:string
    uniteBebe102:string
    gammeBebe102:string
    imgBebe102:string
    
    prixBebe103:string
    ancienPrixBebe103:string
    nomBebe103:string
    uniteBebe103:string
    gammeBebe103:string
    imgBebe103:string
    
    prixBebe104:string
    ancienPrixBebe104:string
    nomBebe104:string
    uniteBebe104:string
    gammeBebe104:string
    imgBebe104:string
    
    prixBebe105:string
    ancienPrixBebe105:string
    nomBebe105:string
    uniteBebe105:string
    gammeBebe105:string
    imgBebe105:string
    
    prixBebe106:string
    ancienPrixBebe106:string
    nomBebe106:string
    uniteBebe106:string
    gammeBebe106:string
    imgBebe106:string
    
    prixBebe111:string
    ancienPrixBebe111:string
    nomBebe111:string
    uniteBebe111:string
    gammeBebe111:string
    imgBebe111:string
    
    prixBebe112:string
    ancienPrixBebe112:string
    nomBebe112:string
    uniteBebe112:string
    gammeBebe112:string
    imgBebe112:string
    
    prixBebe113:string
    ancienPrixBebe113:string
    nomBebe113:string
    uniteBebe113:string
    gammeBebe113:string
    imgBebe113:string
    
    prixBebe114:string
    ancienPrixBebe114:string
    nomBebe114:string
    uniteBebe114:string
    gammeBebe114:string
    imgBebe114:string
    
    prixBebe115:string
    ancienPrixBebe115:string
    nomBebe115:string
    uniteBebe115:string
    gammeBebe115:string
    imgBebe115:string
    
    prixBebe116:string
    ancienPrixBebe116:string
    nomBebe116:string
    uniteBebe116:string
    gammeBebe116:string
    imgBebe116:string
    
    prixBebe121:string
    ancienPrixBebe121:string
    nomBebe121:string
    uniteBebe121:string
    gammeBebe121:string
    imgBebe121:string
    
    prixBebe122:string
    ancienPrixBebe122:string
    nomBebe122:string
    uniteBebe122:string
    gammeBebe122:string
    imgBebe122:string
    
    prixBebe123:string
    ancienPrixBebe123:string
    nomBebe123:string
    uniteBebe123:string
    gammeBebe123:string
    imgBebe123:string
    
    prixBebe124:string
    ancienPrixBebe124:string
    nomBebe124:string
    uniteBebe124:string
    gammeBebe124:string
    imgBebe124:string
    
    prixBebe125:string
    ancienPrixBebe125:string
    nomBebe125:string
    uniteBebe125:string
    gammeBebe125:string
    imgBebe125:string
    
    prixBebe126:string
    ancienPrixBebe126:string
    nomBebe126:string
    uniteBebe126:string
    gammeBebe126:string
    imgBebe126:string
    //bebe
    
    //bienETRE
    prixBienetre11:string
    ancienPrixBienetre11:string
    nomBienetre11:string
    uniteBienetre11:string
    gammeBienetre11:string
    imgBienetre11:string
    
    prixBienetre12:string
    ancienPrixBienetre12:string
    nomBienetre12:string
    uniteBienetre12:string
    gammeBienetre12:string
    imgBienetre12:string
    
    prixBienetre13:string
    ancienPrixBienetre13:string
    nomBienetre13:string
    uniteBienetre13:string
    gammeBienetre13:string
    imgBienetre13:string
    
    prixBienetre14:string
    ancienPrixBienetre14:string
    nomBienetre14:string
    uniteBienetre14:string
    gammeBienetre14:string
    imgBienetre14:string
    
    prixBienetre15:string
    ancienPrixBienetre15:string
    nomBienetre15:string
    uniteBienetre15:string
    gammeBienetre15:string
    imgBienetre15:string
    
    prixBienetre16:string
    ancienPrixBienetre16:string
    nomBienetre16:string
    uniteBienetre16:string
    gammeBienetre16:string
    imgBienetre16:string
    
    prixBienetre21:string
    ancienPrixBienetre21:string
    nomBienetre21:string
    uniteBienetre21:string
    gammeBienetre21:string
    imgBienetre21:string
    
    prixBienetre22:string
    ancienPrixBienetre22:string
    nomBienetre22:string
    uniteBienetre22:string
    gammeBienetre22:string
    imgBienetre22:string
    
    prixBienetre23:string
    ancienPrixBienetre23:string
    nomBienetre23:string
    uniteBienetre23:string
    gammeBienetre23:string
    imgBienetre23:string
    
    prixBienetre24:string
    ancienPrixBienetre24:string
    nomBienetre24:string
    uniteBienetre24:string
    gammeBienetre24:string
    imgBienetre24:string
    
    prixBienetre25:string
    ancienPrixBienetre25:string
    nomBienetre25:string
    uniteBienetre25:string
    gammeBienetre25:string
    imgBienetre25:string
    
    prixBienetre26:string
    ancienPrixBienetre26:string
    nomBienetre26:string
    uniteBienetre26:string
    gammeBienetre26:string
    imgBienetre26:string
    
    prixBienetre31:string
    ancienPrixBienetre31:string
    nomBienetre31:string
    uniteBienetre31:string
    gammeBienetre31:string
    imgBienetre31:string
    
    prixBienetre32:string
    ancienPrixBienetre32:string
    nomBienetre32:string
    uniteBienetre32:string
    gammeBienetre32:string
    imgBienetre32:string
    
    prixBienetre33:string
    ancienPrixBienetre33:string
    nomBienetre33:string
    uniteBienetre33:string
    gammeBienetre33:string
    imgBienetre33:string
    
    prixBienetre34:string
    ancienPrixBienetre34:string
    nomBienetre34:string
    uniteBienetre34:string
    gammeBienetre34:string
    imgBienetre34:string
    
    prixBienetre35:string
    ancienPrixBienetre35:string
    nomBienetre35:string
    uniteBienetre35:string
    gammeBienetre35:string
    imgBienetre35:string
    
    prixBienetre36:string
    ancienPrixBienetre36:string
    nomBienetre36:string
    uniteBienetre36:string
    gammeBienetre36:string
    imgBienetre36:string
    
    prixBienetre41:string
    ancienPrixBienetre41:string
    nomBienetre41:string
    uniteBienetre41:string
    gammeBienetre41:string
    imgBienetre41:string
    
    prixBienetre42:string
    ancienPrixBienetre42:string
    nomBienetre42:string
    uniteBienetre42:string
    gammeBienetre42:string
    imgBienetre42:string
    
    prixBienetre43:string
    ancienPrixBienetre43:string
    nomBienetre43:string
    uniteBienetre43:string
    gammeBienetre43:string
    imgBienetre43:string
    
    prixBienetre44:string
    ancienPrixBienetre44:string
    nomBienetre44:string
    uniteBienetre44:string
    gammeBienetre44:string
    imgBienetre44:string
    
    prixBienetre45:string
    ancienPrixBienetre45:string
    nomBienetre45:string
    uniteBienetre45:string
    gammeBienetre45:string
    imgBienetre45:string
    
    
    prixBienetre46:string
    ancienPrixBienetre46:string
    nomBienetre46:string
    uniteBienetre46:string
    gammeBienetre46:string
    imgBienetre46:string
    
    prixBienetre51:string
    ancienPrixBienetre51:string
    nomBienetre51:string
    uniteBienetre51:string
    gammeBienetre51:string
    imgBienetre51:string
    
    prixBienetre52:string
    ancienPrixBienetre52:string
    nomBienetre52:string
    uniteBienetre52:string
    gammeBienetre52:string
    imgBienetre52:string
    
    prixBienetre53:string
    ancienPrixBienetre53:string
    nomBienetre53:string
    uniteBienetre53:string
    gammeBienetre53:string
    imgBienetre53:string
    
    prixBienetre54:string
    ancienPrixBienetre54:string
    nomBienetre54:string
    uniteBienetre54:string
    gammeBienetre54:string
    imgBienetre54:string
    
    prixBienetre55:string
    ancienPrixBienetre55:string
    nomBienetre55:string
    uniteBienetre55:string
    gammeBienetre55:string
    imgBienetre55:string
    
    prixBienetre56:string
    ancienPrixBienetre56:string
    nomBienetre56:string
    uniteBienetre56:string
    gammeBienetre56:string
    imgBienetre56:string
    
    prixBienetre61:string
    ancienPrixBienetre61:string
    nomBienetre61:string
    uniteBienetre61:string
    gammeBienetre61:string
    imgBienetre61:string
    
    prixBienetre62:string
    ancienPrixBienetre62:string
    nomBienetre62:string
    uniteBienetre62:string
    gammeBienetre62:string
    imgBienetre62:string
    
    prixBienetre63:string
    ancienPrixBienetre63:string
    nomBienetre63:string
    uniteBienetre63:string
    gammeBienetre63:string
    imgBienetre63:string
    
    prixBienetre64:string
    ancienPrixBienetre64:string
    nomBienetre64:string
    uniteBienetre64:string
    gammeBienetre64:string
    imgBienetre64:string
    
    prixBienetre65:string
    ancienPrixBienetre65:string
    nomBienetre65:string
    uniteBienetre65:string
    gammeBienetre65:string
    imgBienetre65:string
    
    prixBienetre66:string
    ancienPrixBienetre66:string
    nomBienetre66:string
    uniteBienetre66:string
    gammeBienetre66:string
    imgBienetre66:string
    
    prixBienetre71:string
    ancienPrixBienetre71:string
    nomBienetre71:string
    uniteBienetre71:string
    gammeBienetre71:string
    imgBienetre71:string
    
    prixBienetre72:string
    ancienPrixBienetre72:string
    nomBienetre72:string
    uniteBienetre72:string
    gammeBienetre72:string
    imgBienetre72:string
    
    prixBienetre73:string
    ancienPrixBienetre73:string
    nomBienetre73:string
    uniteBienetre73:string
    gammeBienetre73:string
    imgBienetre73:string
    
    prixBienetre74:string
    ancienPrixBienetre74:string
    nomBienetre74:string
    uniteBienetre74:string
    gammeBienetre74:string
    imgBienetre74:string
    
    prixBienetre75:string
    ancienPrixBienetre75:string
    nomBienetre75:string
    uniteBienetre75:string
    gammeBienetre75:string
    imgBienetre75:string
    
    prixBienetre76:string
    ancienPrixBienetre76:string
    nomBienetre76:string
    uniteBienetre76:string
    gammeBienetre76:string
    imgBienetre76:string
    
    
    prixBienetre81:string
    ancienPrixBienetre81:string
    nomBienetre81:string
    uniteBienetre81:string
    gammeBienetre81:string
    imgBienetre81:string
    
    prixBienetre82:string
    ancienPrixBienetre82:string
    nomBienetre82:string
    uniteBienetre82:string
    gammeBienetre82:string
    imgBienetre82:string
    
    prixBienetre83:string
    ancienPrixBienetre83:string
    nomBienetre83:string
    uniteBienetre83:string
    gammeBienetre83:string
    imgBienetre83:string
    
    prixBienetre84:string
    ancienPrixBienetre84:string
    nomBienetre84:string
    uniteBienetre84:string
    gammeBienetre84:string
    imgBienetre84:string
    
    prixBienetre85:string
    ancienPrixBienetre85:string
    nomBienetre85:string
    uniteBienetre85:string
    gammeBienetre85:string
    imgBienetre85:string
    
    prixBienetre86:string
    ancienPrixBienetre86:string
    nomBienetre86:string
    uniteBienetre86:string
    gammeBienetre86:string
    imgBienetre86:string
    
    prixBienetre91:string
    ancienPrixBienetre91:string
    nomBienetre91:string
    uniteBienetre91:string
    gammeBienetre91:string
    imgBienetre91:string
    
    prixBienetre92:string
    ancienPrixBienetre92:string
    nomBienetre92:string
    uniteBienetre92:string
    gammeBienetre92:string
    imgBienetre92:string
    
    prixBienetre93:string
    ancienPrixBienetre93:string
    nomBienetre93:string
    uniteBienetre93:string
    gammeBienetre93:string
    imgBienetre93:string
    
    prixBienetre94:string
    ancienPrixBienetre94:string
    nomBienetre94:string
    uniteBienetre94:string
    gammeBienetre94:string
    imgBienetre94:string
    
    prixBienetre95:string
    ancienPrixBienetre95:string
    nomBienetre95:string
    uniteBienetre95:string
    gammeBienetre95:string
    imgBienetre95:string
    
    prixBienetre96:string
    ancienPrixBienetre96:string
    nomBienetre96:string
    uniteBienetre96:string
    gammeBienetre96:string
    imgBienetre96:string
    
    prixBienetre101:string
    ancienPrixBienetre101:string
    nomBienetre101:string
    uniteBienetre101:string
    gammeBienetre101:string
    imgBienetre101:string
    
    prixBienetre102:string
    ancienPrixBienetre102:string
    nomBienetre102:string
    uniteBienetre102:string
    gammeBienetre102:string
    imgBienetre102:string
    
    prixBienetre103:string
    ancienPrixBienetre103:string
    nomBienetre103:string
    uniteBienetre103:string
    gammeBienetre103:string
    imgBienetre103:string
    
    prixBienetre104:string
    ancienPrixBienetre104:string
    nomBienetre104:string
    uniteBienetre104:string
    gammeBienetre104:string
    imgBienetre104:string
    
    prixBienetre105:string
    ancienPrixBienetre105:string
    nomBienetre105:string
    uniteBienetre105:string
    gammeBienetre105:string
    imgBienetre105:string
    
    prixBienetre106:string
    ancienPrixBienetre106:string
    nomBienetre106:string
    uniteBienetre106:string
    gammeBienetre106:string
    imgBienetre106:string
    
    prixBienetre111:string
    ancienPrixBienetre111:string
    nomBienetre111:string
    uniteBienetre111:string
    gammeBienetre111:string
    imgBienetre111:string
    
    prixBienetre112:string
    ancienPrixBienetre112:string
    nomBienetre112:string
    uniteBienetre112:string
    gammeBienetre112:string
    imgBienetre112:string
    
    prixBienetre113:string
    ancienPrixBienetre113:string
    nomBienetre113:string
    uniteBienetre113:string
    gammeBienetre113:string
    imgBienetre113:string
    
    prixBienetre114:string
    ancienPrixBienetre114:string
    nomBienetre114:string
    uniteBienetre114:string
    gammeBienetre114:string
    imgBienetre114:string
    
    prixBienetre115:string
    ancienPrixBienetre115:string
    nomBienetre115:string
    uniteBienetre115:string
    gammeBienetre115:string
    imgBienetre115:string
    
    prixBienetre116:string
    ancienPrixBienetre116:string
    nomBienetre116:string
    uniteBienetre116:string
    gammeBienetre116:string
    imgBienetre116:string
    
    prixBienetre121:string
    ancienPrixBienetre121:string
    nomBienetre121:string
    uniteBienetre121:string
    gammeBienetre121:string
    imgBienetre121:string
    
    prixBienetre122:string
    ancienPrixBienetre122:string
    nomBienetre122:string
    uniteBienetre122:string
    gammeBienetre122:string
    imgBienetre122:string
    
    prixBienetre123:string
    ancienPrixBienetre123:string
    nomBienetre123:string
    uniteBienetre123:string
    gammeBienetre123:string
    imgBienetre123:string
    
    prixBienetre124:string
    ancienPrixBienetre124:string
    nomBienetre124:string
    uniteBienetre124:string
    gammeBienetre124:string
    imgBienetre124:string
    
    prixBienetre125:string
    ancienPrixBienetre125:string
    nomBienetre125:string
    uniteBienetre125:string
    gammeBienetre125:string
    imgBienetre125:string
    
    prixBienetre126:string
    ancienPrixBienetre126:string
    nomBienetre126:string
    uniteBienetre126:string
    gammeBienetre126:string
    imgBienetre126:string
    //bienEtre
    
    
    };
import { Address } from "./Address";

export class Owner{
    id: number
    email: string
    firstName: string
    gender: string
    lastName: string
    phone: string
    address:Address
}
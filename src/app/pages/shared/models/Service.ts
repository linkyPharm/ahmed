export class Service{
    description:string
    icon:string
    title:string
    subtitle:string
    constructor(description,icon,title,subtitle){
        this.description=description
        this.icon=icon
        this.title=title
        this.subtitle=subtitle
    }
}
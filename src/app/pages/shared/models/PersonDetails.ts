import { Address } from "./Address";

export class PersonDetails {
    public id :number;
    public email: string;
    public firstName:string;
    public lastName:string;
    public phone : string;
    public gender:string;
    public adress:Address;
}
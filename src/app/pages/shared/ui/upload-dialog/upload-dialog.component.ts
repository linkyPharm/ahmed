import {Component, OnInit, ViewChild} from '@angular/core';
import {UploadService} from "./upload.service";
import {MatDialogRef} from "@angular/material";
import {forkJoin} from "rxjs";

@Component({
    selector: 'app-upload-dialog',
    templateUrl: './upload-dialog.component.html',
    styleUrls: ['./upload-dialog.component.scss']
})
export class UploadDialogComponent implements OnInit {
    @ViewChild('file') file;
    public files: Set<File> = new Set();
    progress;
    canBeClosed = true;
    primaryButtonText = 'Upload';
    showCancelButton = true;
    uploading = false;
    uploadSuccessful = false;

    imageChangedEvent: any = '';
    croppedImage: any = '';
    cropperReady = false;

    fileChangeEvent(event: any): void {
        this.imageChangedEvent = event;
    }

    imageCroppedBase64(image: string) {
        this.croppedImage = image;
        fetch(image)
            .then(res => res.blob())
            .then(blob => {
                const file = new File([blob], this.files.values().next().value.name);
                this.files = new Set();
                this.files.add(file);
            });
    }

    imageLoaded() {
        this.cropperReady = true;
    }

    loadImageFailed() {
        console.log('Load failed');
    }

    constructor(public dialogRef: MatDialogRef<UploadDialogComponent>, public uploadService: UploadService) {
    }

    ngOnInit() {
    }

    addFiles() {
        this.file.nativeElement.click();
    }

    onFilesAdded(event) {
        this.imageChangedEvent = event;
        this.files = new Set();
        const files: { [key: string]: File } = this.file.nativeElement.files;
        for (const key in files) {
            if (!isNaN(parseInt(key))) {
                const file = files[key];
                this.files.add(file);
            }
        }
    }

    closeDialog() {
        // if everything was uploaded already, just close the dialog
        if (this.uploadSuccessful) {
            return this.dialogRef.close();
        }

        // set the component state to "uploading"
        this.uploading = true;

        // start the upload and save the progress map
        this.progress = this.uploadService.upload(this.files);

        // convert the progress map into an array
        const allProgressObservables = [];
        for (const key in this.progress) {
            allProgressObservables.push(this.progress[key].progress);
        }

        // Adjust the state variables

        // The OK-button should have the text "Finish" now
        this.primaryButtonText = 'Finish';

        // The dialog should not be closed while uploading
        this.canBeClosed = false;
        this.dialogRef.disableClose = true;

        // Hide the cancel-button
        this.showCancelButton = false;

        // When all progress-observables are completed...
        forkJoin(allProgressObservables).subscribe(end => {
            // ... the dialog can be closed again...
            this.canBeClosed = true;
            this.dialogRef.disableClose = false;

            // ... the upload was successful...
            this.uploadSuccessful = true;

            // ... and the component is no longer uploading
            this.uploading = false;
            // this.closeDialog();
        });
    }

}

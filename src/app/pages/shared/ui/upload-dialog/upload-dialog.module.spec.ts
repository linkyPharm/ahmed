import { UploadDialogModule } from './upload-dialog.module';

describe('UploadDialogModule', () => {
  let uploadDialogModule: UploadDialogModule;

  beforeEach(() => {
    uploadDialogModule = new UploadDialogModule();
  });

  it('should create an instance', () => {
    expect(uploadDialogModule).toBeTruthy();
  });
});

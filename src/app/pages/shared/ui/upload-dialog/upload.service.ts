import {Injectable} from '@angular/core';
import {HttpClient, HttpEventType, HttpHeaders, HttpRequest, HttpResponse} from '@angular/common/http';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';
import {AppConfig} from "../../app.config";
import {UserService} from "../../services/user.service";
import {NotificationService} from "../../services/notification.service";
import {map} from "rxjs/operators";


@Injectable()
export class UploadService {
    constructor(private http: HttpClient,
                private userService: UserService,
                private notificationService: NotificationService) {
    }

    public upload(files: Set<File>): { [key: string]: Observable<number> } {
        // this will be the our resulting map
        const status = {};

        files.forEach(file => {
            // create a new multipart-form for every file
            const url = AppConfig.baseUrl + '/documents/factoryUploadTmpDocuments';
            const formData: FormData = new FormData();
            formData.append('document', file, file.name);

            // create a http-post request and pass the form
            // tell it to report the upload progress
            let headers = new HttpHeaders();
            headers = headers.set('Authorization', `Bearer ${this.userService.getToken()}`);
            headers = headers.set('Accept', 'application/json, text/plain, */*');

            const req = new HttpRequest('POST', url, formData, {
                reportProgress: true,
                headers: headers
            });

            // create a new progress-subject for every file
            const progress = new Subject<number>();

            // send the http-request and subscribe for progress-updates
            this.http.request(req).subscribe(event => {
                if (event.type === HttpEventType.UploadProgress) {

                    // calculate the progress percentage
                    const percentDone = Math.round(100 * event.loaded / event.total);

                    // pass the percentage into the progress-stream
                    progress.next(percentDone);
                } else if (event instanceof HttpResponse) {
                    const responseBody = (event as HttpResponse<Array<Document>>).body;
                    if (this.userService.currentUserProfileDocId) {
                        this.deleteDocumentFromSubject(this.userService.currentUserProfileDocId).subscribe(_ => {
                            this.attachDocumentToSubject('user',
                                this.userService.currentUser._id, responseBody[0]).subscribe();
                        });
                    } else {
                        this.attachDocumentToSubject('user',
                            this.userService.currentUser._id, responseBody[0]).subscribe();
                    }
                    /*

                    */
                    // Close the progress-stream if we get an answer form the API
                    // The upload is complete
                    progress.complete();
                }
            });

            // Save every progress-observable in a map of all observables
            status[file.name] = {
                progress: progress.asObservable()
            };
        });

        // return the map of progress.observables
        return status;
    }

    public attachDocumentToSubject(subject: string, subjectId: string, tmpDocument: Document) {
        const url = AppConfig.baseUrl + '/documents/uploadDocuments';
        return this.http.post(url, {
            tmp_documents: [
                tmpDocument
            ],
            multiple: false,
            type: "Image",
            subjectType: subject,
            subjectId: subjectId,
        }).pipe(map(data => {
            // this.userService.currentUserProfileDocId = responseBody[0]._id;
            // this.userService.buildUserProfilePictureUrl();
            // this.userService.getCurrentUser().subscribe();
            this.userService.fetchUserProfilePictureDocument().subscribe(documents => {
                if (documents.length > 0) {
                    
                    this.notificationService.showToast('ACTION_SUCCESSFULLY');
                }
            });
        }));
    }

    public deleteDocumentFromSubject(documentId: string) {
        const url = AppConfig.baseUrl + '/documents/' + documentId;
        return this.http.delete(url);
    }
}


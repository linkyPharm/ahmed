import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UploadDialogComponent} from './upload-dialog.component';
import {MatButtonModule, MatDialogModule, MatListModule, MatProgressBarModule} from "@angular/material";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {HttpClientModule} from "@angular/common/http";
import {FlexLayoutModule} from "@angular/flex-layout";
import {UploadService} from "./upload.service";
import {TranslateModule} from "@ngx-translate/core";
import {ImageCropperModule} from "ngx-image-cropper";

// https://malcoded.com/posts/angular-file-upload-component-with-express
@NgModule({
    imports: [
        CommonModule,
        MatButtonModule,
        MatDialogModule, MatListModule,
        FlexLayoutModule, HttpClientModule,
        BrowserAnimationsModule, MatProgressBarModule,
        ImageCropperModule,
        TranslateModule
    ],
    entryComponents: [UploadDialogComponent],
    declarations: [UploadDialogComponent],
    providers: [UploadService]
})
export class UploadDialogModule {
}

import { TestBed, async, inject } from '@angular/core/testing';

import { ThemeSettingGuard } from './theme-setting.guard';

describe('ThemeSettingGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ThemeSettingGuard]
    });
  });

  it('should ...', inject([ThemeSettingGuard], (guard: ThemeSettingGuard) => {
    expect(guard).toBeTruthy();
  }));
});

import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {UserService} from '../services/user.service';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {

    constructor(private userService: UserService, private router: Router) {

    }

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        const url: string = state.url;

        return this.checkLogin(url);
    }

    checkLogin(url: string): boolean {
        if (this.userService.isLoggedIn()) {
            return true;
        }
        // Navigate to the login page with extras
        let redirectUrl = '/auth/login';
        if (url !== '/') {
            redirectUrl += '?redirectUrl=' + url;
        }
        this.router.navigateByUrl(redirectUrl);
        return false;
    }
}

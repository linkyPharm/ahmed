import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {SharedService} from "../services/shared.service";

@Injectable({
    providedIn: 'root'
})
export class ThemeSettingGuard implements CanActivate {
    constructor(private sharedService: SharedService) {

    }

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        const url: string = state.url;
        this.sharedService.isUserInMainDashboardUrl = url.includes('app/overview/dashboard');
        return true;
    }
}

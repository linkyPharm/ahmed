import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {UserService} from '../services/user.service';
import {Observable, of} from 'rxjs';
import {Router} from '@angular/router';
import {catchError} from 'rxjs/operators';
import {NotificationService} from "../services/notification.service";
import { StorageService } from '../services/storage.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
    ingnoredInterceptedUris = ['factoryUploadTmpDocuments'];

    constructor(private userService: UserService,
                private router: Router,
                private notificationService: NotificationService,
                private storageService:StorageService
              ) {
    }


    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
 //setHeaders:  {
            // Authorization0:`Bearer ${this.userService.getToken()}`,
        let interceptRequest = true;
        this.ingnoredInterceptedUris.forEach((function (ingnoredInterceptedUri) {
            interceptRequest = interceptRequest && !request.url.includes(ingnoredInterceptedUri);
        }));
        request = interceptRequest ? request.clone({
            setHeaders: this.userService.isLoggedIn() && this.storageService.read('fakeToken')==55 ? {
                
           
            Authorization0:`Bearer ${this.userService.getToken()}`,
             Authorization:`Bearer ${this.userService.getToken()}`,
                'Content-Type': 'application/json;charset=UTF-8',
                Accept: 'application/json, text/plain, */*'
            }:{
                //Authorization0:`Bearer ${this.userService.getToken()}`,
                //Authorization:`Bearer ${this.userService.getToken()}`,
               // 'Content-Type':'application/x-www-form-urlencoded a'
           /* Authorization:`Bearer ${this.userService.getToken()}`,
             'Content-Type': 'application/json;charset=UTF-8',
             Accept: 'application/json, text/plain, '*/
            
    
            }


        }) : request;
        return next.handle(request)
            .pipe(catchError((err) => {
                if (err instanceof HttpErrorResponse) {
                    if (err.status === 401) {
                        this.userService.clear();
                        this.router.navigateByUrl(`/auth/login?redirectUrl=${this.router.url}`);
                        this.notificationService.showToast('SESSION_EXPIRED',
                            NotificationService.WARNING_NOTIFICATION);
                    } else if (err.status > 299 || err.status < 200) {
                        this.notificationService.showToast('ERROR',
                            NotificationService.ERROR_NOTIFICATION);
                    }
                }
                return of(HttpErrorResponse);
            }))as Observable<HttpEvent<any>>;
    }

    
}

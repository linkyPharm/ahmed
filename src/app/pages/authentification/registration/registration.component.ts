import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators} from '@angular/forms';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {FuseConfigService} from '@fuse/services/config.service';
import {fuseAnimations} from '@fuse/animations';
import {UserService} from '../../shared/services/user.service';
import {Router} from '@angular/router';
import {NotificationService} from '../../shared/services/notification.service';
import {User} from '../../shared/models/User';
import {Address} from "../../shared/models/Address";

@Component({
    selector: 'register-2',
    templateUrl: './registration.component.html',
    styleUrls: ['./registration.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class RegistrationComponent implements OnInit, OnDestroy {
    registerForm: FormGroup;
    user: User = new User();
    address: Address = new Address();
    // Private
    private _unsubscribeAll: Subject<any>;
    step: number;

    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private userService: UserService,
        private router: Router,
        private notificationService: NotificationService
    ) {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
        this.step = 1;
        console.log("test");
        this.user.gender = 'male';
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {

    }

    receiveUser($event) {
        this.user = $event;
        console.log(this.user);
        this.step++;
    }

    receiveAddress($event) {
        this.user.address = $event;
        console.log(this.address);
        console.log(this.user);
        this.step++;

      /*  this.userService.register(this.user).subscribe(data => {
             if (data) {
                 this.router.navigateByUrl('/auth/login');
                 this.notificationService.showToast('SUCCESSFULLY_REGISTRED');
             }
         });*/
    }
    receiveProfessionalInfos($event) {
       // console.log($event.organizationUnit.id)
        this.user.isOwner =$event.isOwner; 
        this.user.organizationUnitId = $event.organizationUnitId;

        console.log(this.user);
        this.step++;


       this.register();
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    register(): void {
        this.user.login = this.user.email;
        this.userService.register(this.user).subscribe(data => {
            if (data) {
                console.log(data)
                this.notificationService.showToast('SUCCESSFULLY_REGISTRED');
            }
        });
    }
}

/**
 * Confirm password validator
 *
 * @param {AbstractControl} control
 * @returns {ValidationErrors | null}
 */
export const confirmPasswordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

    if (!control.parent || !control) {
        return null;
    }

    const password = control.parent.get('password');
    const passwordConfirm = control.parent.get('passwordConfirm');

    if (!password || !passwordConfirm) {
        return null;
    }

    if (passwordConfirm.value === '') {
        return null;
    }

    if (password.value === passwordConfirm.value) {
        return null;
    }

    return {'passwordsNotMatching': true};
};

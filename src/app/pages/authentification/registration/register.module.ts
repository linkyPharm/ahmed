import {NgModule} from '@angular/core';
import {
    MatButtonModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatRadioModule,
    MatSelectModule,
    MatToolbarModule
} from '@angular/material';
import {FuseSharedModule} from '@fuse/shared.module';
import {FuseSearchBarModule, FuseShortcutsModule} from '@fuse/components';
import {TranslateModule} from '@ngx-translate/core';
import {SharedModule} from '../../shared/shared.module';
import {SweetAlert2Module} from '@toverux/ngx-sweetalert2';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Step1Component} from './step1/step1.component';
import {RegisterRoutingModule} from './register.routing';
import {RegistrationComponent} from './registration.component';
import {Step2Component} from './step2/step2.component';
import {Step3Component} from "./step3/step3.component";


@NgModule({
    declarations: [
        Step1Component,
        Step2Component,
        Step3Component,
        RegistrationComponent
    ],
    imports: [
        RegisterRoutingModule,
        MatButtonModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatIconModule,
        MatToolbarModule,
        MatInputModule,
        MatMenuModule,
        FuseSharedModule,
        FuseSearchBarModule,
        FuseShortcutsModule,
        MatSelectModule,
        TranslateModule,
        SharedModule,
        SweetAlert2Module,
        MatRadioModule,
        FormsModule,
        ReactiveFormsModule


    ]
})
export class RegisterModule {
}

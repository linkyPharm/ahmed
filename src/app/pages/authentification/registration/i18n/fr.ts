export const locale = {
    lang: 'fr',
    data: {
        'Login': {
            'Username': 'Nom d\'utilisateur,',
            'Password': 'Mot de passe',
            'Remember': 'Me rappeler',
            'Forget': 'Mot de passe oublié ?',
            'Login': 'Accèder',
            'Language': 'Choisir une langue: ',
            'Error.Username': 'Nom d\'utilisateur est obligatoire',
            'Error.Password': 'Mot de passe est obligatoire'
        }
    }
};

import {Component, EventEmitter, OnDestroy, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators} from '@angular/forms';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {FuseConfigService} from '@fuse/services/config.service';
import {fuseAnimations} from '@fuse/animations';
import {UserService} from '../../../shared/services/user.service';
import {Router} from '@angular/router';
import {NotificationService} from '../../../shared/services/notification.service';
import {User} from '../../../shared/models/User';
import {Country} from '../../../shared/models/Country';
import {SharedService} from '../../../shared/services/shared.service';
import {Address} from '../../../shared/models/Address';
import {Organization} from "../../../shared/models/Organization";
import {OrganizationUnit} from "../../../shared/models/OrganizationUnit";
import { HttpRequest, HttpClient } from '@angular/common/http';
import { Pharmacy } from 'app/pages/shared/models/Pharmacy';
export interface pharma{
    value:string,
    id:number
}
@Component({
    selector: 'step3',
    templateUrl: './step3.component.html',
    styleUrls: ['./step3.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class Step3Component implements OnInit, OnDestroy {
    registerForm: FormGroup;
    user: User = new User();
    address: Address = new Address();
    countries: Array<Country> = [];
    organizations: Array<Organization> = [];
    organizationsUnit: Array<OrganizationUnit> = [];
    userType: String;
    validate:boolean=false
    owner:boolean=false
   testArray:pharma[]=[
       {value:"pharmacie1",id:1 },
       {value:"pharmacie2",id:2 }
   ]
   phamacies:Pharmacy[]

   
    // Private
    private _unsubscribeAll: Subject<any>;

    @Output() professionalInfosEvent = new EventEmitter<User>();

    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private userService: UserService,
        private sharedService: SharedService,
        private router: Router,
        private notificationService: NotificationService,
        private http:HttpClient
    ) {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
        this.user.organizationUnit = new OrganizationUnit();
        console.log("test");
        // Set the private defaults
        this._unsubscribeAll = new Subject();

        this.user.isOwner = false;
        console.log(this.countries);
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.registerForm = this._formBuilder.group({
            isOwner: [' ',Validators.required],
            organizationUnit: ['', Validators.required],

        });

        this.getAllOrganizationsUnit();
this.http.get<Pharmacy[]>('http://51.77.158.247:9999/organizationunit-service/pharmacy0/getPharmacies').subscribe(
    (pharmacies)=>{
        this.phamacies=pharmacies
        console.log(this.phamacies)
    }
    
)

    }

    sendProfessionalInfos() {
//console.log(this.user.organizationUnit.id)
        this.professionalInfosEvent.emit(this.user);

    }
    validateEmployee(){
this.validate=true

console.log(this.validate)
    }
    validateOwner(){
        this.validate=false
      

    }
    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    register(): void {
        this.sendProfessionalInfos();

    }

    getAllOrganizations() {
       // this.organizations = this.sharedService.getAllOrganizations();
        /* this.sharedService.getAllOrganizations().subscribe(data => {
             if (data) {
               console.log(data);
             }
         });*/
    }

    getAllOrganizationsUnit() {
       // this.organizationsUnit = this.sharedService.getAllOrganizationsUnit();
        /* this.sharedService.getAllOrganizations().subscribe(data => {
             if (data) {
               console.log(data);
             }
         });*/
    }
    showid(id){
console.log(id)
this.user.organizationUnitId=id
    }

}

/**
 * Confirm password validator
 *
 * @param {AbstractControl} control
 * @returns {ValidationErrors | null}
 */
export const confirmPasswordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

    if (!control.parent || !control) {
        return null;
    }

    const password = control.parent.get('password');
    const passwordConfirm = control.parent.get('passwordConfirm');

    if (!password || !passwordConfirm) {
        return null;
    }

    if (passwordConfirm.value === '') {
        return null;
    }

    if (password.value === passwordConfirm.value) {
        return null;
    }

    return {'passwordsNotMatching': true};
};


import {Component, EventEmitter, OnDestroy, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators} from '@angular/forms';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {FuseConfigService} from '@fuse/services/config.service';
import {fuseAnimations} from '@fuse/animations';
import {UserService} from '../../../shared/services/user.service';
import {Router} from '@angular/router';
import {NotificationService} from '../../../shared/services/notification.service';
import {User} from '../../../shared/models/User';
import {Country} from '../../../shared/models/Country';
import {SharedService} from '../../../shared/services/shared.service';
import {Address} from '../../../shared/models/Address';

@Component({
    selector: 'step2',
    templateUrl: './step2.component.html',
    styleUrls: ['./step2.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class Step2Component implements OnInit, OnDestroy {
    registerForm: FormGroup;
    user: User = new User();
    address: Address = new Address();
    countries: Array<Country> = [];
    // Private
    private _unsubscribeAll: Subject<any>;

    @Output() addressEvent = new EventEmitter<Address>() ;

    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private userService: UserService,
        private sharedService: SharedService,
        private router: Router,
        private notificationService: NotificationService
    ) {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
        this.user.gender = 'male';
        // Set the private defaults
        this._unsubscribeAll = new Subject();

        this.countries = this.sharedService.getAllCountries();
        console.log(this.countries);
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.registerForm = this._formBuilder.group({
            country: ['', Validators.required],
            city: ['', Validators.required],
            street: ['', [Validators.required]],
            zip: ['', Validators.required],

        });

        this.getAllCountries();
    }

    sendAddress(){
        this.addressEvent.emit(this.address);
        console.log(this.user);
    }
    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    register(): void {
        this.sendAddress();
        this.router.navigateByUrl('/auth/register/step3');

    }

    getAllCountries() {

        /*this.sharedService.getAllCountries().subscribe(data => {
            if (data) {
              console.log(data);
            }
        });*/
    }
}

/**
 * Confirm password validator
 *
 * @param {AbstractControl} control
 * @returns {ValidationErrors | null}
 */
export const confirmPasswordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

    if (!control.parent || !control) {
        return null;
    }

    const password = control.parent.get('password');
    const passwordConfirm = control.parent.get('passwordConfirm');

    if (!password || !passwordConfirm) {
        return null;
    }

    if (passwordConfirm.value === '') {
        return null;
    }

    if (password.value === passwordConfirm.value) {
        return null;
    }

    return {'passwordsNotMatching': true};
};


import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {Step1Component} from './step1/step1.component';
import {Step2Component} from './step2/step2.component';
import {RegistrationComponent} from './registration.component';
import {Step3Component} from "./step3/step3.component";


const RegisterRouting: Routes = [

    {
        path: '',
        component: RegistrationComponent,
        children: [

            {
                path: 'step1',
                component: Step1Component
            },

            {
                path: 'step2',
                component: Step2Component
            },
            {
                path: 'step3',
                component: Step3Component
            }
        ]

}

]
;

@NgModule({
    imports: [
        RouterModule.forChild(RegisterRouting)
    ],
    exports: [
        RouterModule
    ]
})
export class RegisterRoutingModule {

}

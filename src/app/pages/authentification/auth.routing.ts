import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {LoginComponent} from './login/login.component';
import {ForgotPasswordComponent} from "./forgot-password/forgot-password.component";



const AuthRouting: Routes = [
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'forgot-password',
        component: ForgotPasswordComponent
    },
    {
        path: 'register',
        loadChildren: "./registration/register.module#RegisterModule"
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(AuthRouting)
    ],
    exports: [
        RouterModule
    ]
})
export class AuthRoutingModule {

}

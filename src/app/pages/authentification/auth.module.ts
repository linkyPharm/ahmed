import {NgModule} from '@angular/core';
import {
    MatButtonModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatRadioModule,
    MatSelectModule,
    MatToolbarModule
} from '@angular/material';
import {FuseSharedModule} from '@fuse/shared.module';
import {LoginComponent} from './login/login.component';
import {FuseSearchBarModule, FuseShortcutsModule} from '@fuse/components';
import {TranslateModule} from '@ngx-translate/core';
import {SharedModule} from '../shared/shared.module';
import {AuthRoutingModule} from './auth.routing';
import {ForgotPasswordComponent} from './forgot-password/forgot-password.component';
import {SweetAlert2Module} from '@toverux/ngx-sweetalert2';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RegisterModule} from './registration/register.module';
import {RegistrationComponent} from './registration/registration.component';


@NgModule({
    declarations: [
        LoginComponent,
        ForgotPasswordComponent,


    ],
    imports: [
        AuthRoutingModule,
        MatButtonModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatIconModule,
        MatToolbarModule,
        MatInputModule,
        MatMenuModule,
        FuseSharedModule,
        FuseSearchBarModule,
        FuseShortcutsModule,
        MatSelectModule,
        TranslateModule,
        SharedModule,
        SweetAlert2Module,
        MatRadioModule,
        FormsModule,
        ReactiveFormsModule,


    ]
})
export class AuthModule {
}

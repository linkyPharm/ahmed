import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {locale as english} from './i18n/en';
import {locale as french} from './i18n/fr';
import {FuseConfigService} from '@fuse/services/config.service';
import {fuseAnimations} from '@fuse/animations';
import {UserService} from '../../shared/services/user.service';
import {Credentials} from '../../shared/credentials';
import * as _ from 'lodash';
import {AppConfig} from '../../shared/app.config';
import {StorageService} from '../../shared/services/storage.service';
import {TranslateService} from '@ngx-translate/core';
import {ActivatedRoute, Router} from '@angular/router';
import {NotificationService} from "../../shared/services/notification.service";
import { User } from 'app/pages/shared/models/User';
import { UserLogin } from 'app/pages/shared/models/UserLogin';

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    credentials: Credentials;
    selectedLanguage: any;
    languages: any;
    redirectUrl: string;
current_User:User= new User()
    /**
     * Constructor
     *
     * @param {UserService} userService
     * @param {Router} router
     * @param {FuseConfigService} _fuseConfigService
     * @param {FormBuilder} _formBuilder
     * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
     * @param {TranslateService} _translateService
     * @param {StorageService} storageService
     * @param {ActivatedRoute} route
     * @param {NotificationService} notificationService
     */
    constructor(
        private userService: UserService,
        private router: Router,
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private _translateService: TranslateService,
        private storageService: StorageService,
        private route: ActivatedRoute,
        private notificationService: NotificationService,
        
    ) {
        this.credentials = new Credentials();

        this._fuseTranslationLoaderService.loadTranslations(english, french);
        this._translateService.use('en');

        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
        this.loginForm = this._formBuilder.group({
            email: ['', [Validators.required]],
            password: ['', Validators.required]
        });

        this.languages = [
            {
                id: 'en',
                title: 'English',
                flag: 'us'
            },
            {
                id: 'fr',
                title: 'Français',
                flag: 'fr'
            }
        ];
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        if (this.userService.isLoggedIn()) {
            this.router.navigateByUrl('/app/dashboard');
            return;
        }
        // Set the selected language from default languages
        this.selectedLanguage = _.find(this.languages, {'id': 'en'});
    }


    setLanguage(lang): void {
        // Set the selected language for the toolbar
        this.selectedLanguage = lang;

        // Use the selected language for translations
        this._translateService.use(lang.id);
        this.storageService.write(AppConfig.translateStorageKey, lang.id);
    }


    attemptLogin(): void {
        console.log('in')
        // this.router.navigateByUrl('/app/overview/dashboard');
        this.userService.login(this.credentials).subscribe(data => {
            console.log(data);
           if (data.account.token) {
                console.log(data);
                console.log(data.account.username)
                this.userService.savecurrentuser(data.account.username)
                this.notificationService.showToast('SUCCESSFULLY_LOGGED_IN');
                this.userService.saveToken(data.account.token);
                this.storageService.write('user', data);
                this.storageService.write('username', data.account.username);
                this.storageService.write('id', data.account.id);
                this.storageService.write('fakeToken', 55);
            
                console.log(data.isActive);
                // (!this.redirectUrl)
                if (data.isActive) {
                    this.router.navigateByUrl('/app/overview/dashboard');
                } else {
                    // (this.redirectUrl)
                    console.log("not activated");
                    this.router.navigateByUrl('/not-activated');
                }
                         

        }
        }); 
    }
}

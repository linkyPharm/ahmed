export const locale = {
    lang: 'en',
    data: {
        'Login': {
            'Username': 'Username',
            'Password': 'Password',
            'Remember': 'Remember me',
            'Forget': 'Forgot password ?',
            'Login': 'Login',
            'Language': 'Choose language ',
            'Error.Username': 'Username is required',
            'Error.Password': 'Password is required'
        }
    }
};
